---
layout: post.pug
title: "Room Not Found"
date: 2012-09-07
tags: ["post"]
---

I was staying in a hotel last week. I arrived and they gave me room 404. I
went to the fourth floor and looked and looked but couldn’t find it.

<!-- excerpt -->

I went back down to reception and they were very apologetic; they’d recently
combined it with 403 and it was no longer there. They checked me into room 200. I said ‘OK’.

#this_really_happened.
