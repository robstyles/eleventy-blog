---
layout: post.pug
title: "What next?"
date: 2012-04-24
tags: ["post", "Working at Talis"]
---

I’m leaving Talis.

For the past seven years I have had the great fortune to learn a huge amount
from awesome people. That has put me in the position of having some great
conversations about what I’ll be doing next; and those conversations are
exciting. More on that in a later post. First, how can someone be happy to be
leaving a great company?

<!-- excerpt -->

Back in late 2004 I joined a small library software vendor with some
interesting challenges,
<a href="http://talis.com/">Talis</a>. Since then we have become one of the
best known
<a href="http://en.wikipedia.org/wiki/Linked_data">Linked Data</a>
and
<a href="http://en.wikipedia.org/wiki/Semantic_Web">Semantic Web</a>
brands in the world. On that journey I have learnt so much. I’ve learnt
everything from an obscure late 1960s data format (MARC) to Big Data
conversions using
<a href="http://hadoop.apache.org/">Hadoop</a>. The technology has been the
least of it though.

I’ve been rewarded with the opportunity to hear some of the smartest people in
the world speak in some amazing places. I’ve pair-programmed with
<a href="http://iandavis.com/">Ian Davis</a>
and had breakfast with
<a href="http://www.w3.org/People/Berners-Lee/">Tim Berners-Lee</a>; I’ve seen
the Rockies in Banff and walked the Great Wall of China. As a result of our
brand and the work we’ve done I’ve been invited to help write the first
<a href="http://opendatacommons.org/">license for open data</a>; train
government departments on how to publish their data and talk about
<a href="http://www.bbc.co.uk/nature/life/Dinosaur">dinosaurs</a>
with
<a href="http://derivadow.com/">Tom Scott</a>
at the BBC.

Talis has always been about the people. People in Talis (Talisians); people
outside we’ve worked with and bounced ideas off; customers who have allowed us
to help with exciting projects. I have made some great friends and been taught
some humbling lessons.

Amongst the sharpest highlights has been an enormously rewarding day job. At
the start re-imagining <a href="http://www.capita-softwareandmanagedservices.co.uk/software/Pages/libraries-base.aspx" >Talis Base</a > and then <a href="http://www.capita-softwareandmanagedservices.co.uk/software/Pages/libraries-prism.aspx" >Talis Prism</a > ; seeding an
<a href="http://www.talisaspire.com/">education-focussed business</a>
and recently building an
<a href="http://consulting.talis.com/">expert, international consultancy</a>.

I joined Talis expecting to stay for a few years and found the journey so
rewarding it has kept me for so much longer. It’s now time for my journey and
Talis to diverge as I think about doing something different.

I still have a couple of consulting engagements to finalise, so if you’re one
of those then please don’t panic; we’ll be talking soon.
