---
layout: post.pug 
title: "What I learned Last Week..."
date: 2007-03-05
tags: ["post"]
legacy_wordpress_id: 164
---

So, last week I was over in Athens, GA at [code4libcon 2007](http://code4lib.org/2007/). I was expecting a good week full of ideas, beer and really great chats with really smart people, but it was much better than I expected.

Things started really well with [Karen Schneider's keynote](/2007/02/28/code4lib-2007). Karen is an impassioned speaker and is well-known throughout the community. Much of what she said made a lot of sense to me.

<!-- excerpt -->

She made the great point that there is no such thing as free software and brought up the "free as in beer or free as in kittens" analogy that appeared to result in the conference being inundated with images of kittens in the following presentations - those without kitten pictures had quickly edited their presentations to add apologies for the lack of kittens.

It also led to the wonderful quote "free as in... D'uh!", which essentially means, I think, that the software is unrestricted... or something like that.

Dan Chudnov gave a superb presentation that left me with the term ["ZeroConfOpenMetaHuh?"](http://www.code4lib.org/2007/chudnov) and involved live networking - live coding is brave, but this guy does live networking (and I mean TCP/IP, not pretending to talk to people over a coffee). Very cool work on making your search capabilities more discoverable and stuff we should apply to Project Cenote.

I managed to rehearse the [first segment of my upcoming EUSIDIC presentation](http://talis.s3.amazonaws.com/code4lib07_rob.wmv) (WMV) under the pretense of doing a 5 minute Lightning Talk and got great feedback from that.

<object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/cUjqhu_tuyo&hl=en&fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/cUjqhu_tuyo&hl=en&fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="344"></embed></object>

I found a new book that I must get round to ordering: [Ambient Findability by Peter Morville](http://www.amazon.com/Ambient-Findability-Peter-Morville/dp/0596007655/) and a great video, [the beauty of Venn diagrams](http://www.ibiblio.org/bess/?p=66) over at Bess Sadler's Blog, [Solvitur ambulando](http://www.ibiblio.org/bess). Bess is working [Library-in-a-box](http://www.code4lib.org/2007/sadler), which is something I hope we can help out with when the time is right.

Great quotes from the week were...

> <ehatcher> "I'm certainly a card carrying library geek"
> 
> <ehatcher> "I saw what MARC was all about, and I was literally terrified."
> 
> <rjw> "We're not four years old, we're seven"
> 
> <q><edsu> "yeah, i got cajoled into getting another beer</q>"

> <anarchivist> <q>i guess this means i can't show up to work in a dirty duran duran shirt anymore</q>
> 
> <q><dchud> <q>please do not encourage the geeks to view videos over flakey connections :P</q></q>
<q></q> These quotes are (mostly) explained by the presence of most of the attendees in an IRC channel while sat around at the conference - an excellent idea that allows everyone to talk about the presentation in real-time without disturbing the presenter... with obvious side-effects.

[Photos of the event are on Flikr under code4lib2007](http://www.flickr.com/photos/tags/code4lib2007/).
<div id="0767317B-992E-4b12-91E0-4F059A8CECA8:53ff4f52-2305-4d8a-9173-2de21eedff19" class="wlWriterSmartContent" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati tags: <a rel="tag" href="http://technorati.com/tags/Code4Lib">Code4Lib</a>, <a rel="tag" href="http://technorati.com/tags/Code4Lib2007">Code4Lib2007</a>, <a rel="tag" href="http://technorati.com/tags/Code4Lib.org">Code4Lib.org</a></div>
