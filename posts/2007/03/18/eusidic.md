---
layout: post.pug 
title: "EUSIDIC"
date: 2007-03-18
tags: ["post"]
legacy_wordpress_id: 407
---

I was at EUSIDIC in Roskilde, Denmark last week. The conference was held in a large lecture theatre at the university with great facilities, but the contrast with Code4Lib was extreme. I was the only person wearing jeans and I had to tuck my shirt in and do up another button just to feel adequately dressed!

<!-- excerpt -->

The Slides:

<iframe src="//www.slideshare.net/slideshow/embed_code/key/aV9czE54EBuRKY" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> __ <a href="//www.slideshare.net/mmmmmrob/eusidic-the-outlook-and-the-future" title="Eusidic, The Outlook And The Future" target="_blank">Eusidic, The Outlook And The Future</a> __ from __<a href="https://www.slideshare.net/mmmmmrob" target="_blank">mmmmmrob</a>__ </div>
