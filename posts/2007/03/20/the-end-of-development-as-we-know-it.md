---
layout: post.pug 
title: "The end of development as we know it..."
date: 2007-03-20
tags: ["post","Internet Social Impact"]
legacy_wordpress_id: 169
---

Over at <a title="The Wonderful World of Mr C" href="http://wonderfulworldofmrc.blogspot.com/">The Wonderful World of Mr C</a>&nbsp;my colleague Ian Corns keeps track of gaming technologies. He's into PS3s, Wiis and stuff like that - serious business, not childish toys these days.

He's written a great piece around this [abridged video of the Sony GDC 2007 Keynote](http://www.youtube.com/watch?v=qTFCAkTtRmA#)&nbsp;which shows a fully dynamic gaming environment, with a realistic physics engine. Let's unpack that a bit... In most games objects are coded in some kind of programming language and run directly in code - they can only be altered or maintained by altering the game's source and re-compiling. In this model from Sony the&nbsp;objects are declared by anyone and run within the game - so anyone can create games using this model. Ian's piece is here: <a title="permanent link" href="http://wonderfulworldofmrc.blogspot.com/2007/03/what-web20-developer-networks-should-be.html">What Web2.0 Developer Networks Should Be...</a>

<!-- excerpt -->

Now, Ian talks about the democratisation of development, and I totally agree with him on that trend. I've been watching [Teqlo](http://www.teqlo.com/)&nbsp;which allows visual design of web-based applications that plug together web services. [Coghead](http://www.coghead.com/) are on a similar track and there are others. This is a trend that started a while ago, and there has been much interest in it from inside Enterprise with lots of people looking at [Biztalk](http://www.microsoft.com/biztalk).

So Ian and I agree on the trend, but I'm not sure we agree on where it leads to. Ian sees the world like this:

> "But I want to build my own mash-ups and applications using library-relevant web services - I've got loads of ideas", I shout. "No", says Mr Developer, "you can't. Only we're allowed to do that because we have 'special knowledge'". 

I don't, most developers I know (but at [Talis](http://www.talis.com/) we only hire great developers, so not reflective of the industry, perhaps ;-) would say

> "sure, come on in, here's the editor, here's the compiler, here's some examples to get you started" 

Certainly I would, and have, and [my colleagues](http://talisians.com/) have too. So we disagree, I think, on the barriers to entry. I think another indication of this is here:

> It passes my favourite validation criteria for technology - namely "Could my Mum do it?" Within this game, my Mum can become as good a developer as anyone else because ALL developer complexity has been hidden. 

Now, I have to say that I don't know Ian's mum. I'm sure she's a really nice lady, smart too. But I'd love to see that experiment - Ian's mum trying to write a game using these tools. Without wishing to disrespect Ian's mum; I bet it would suck. For that matter, if my mum tried to write a game with these tools it would suck too, and she's a professional sysadmin (no, I'm not joking, my mum builds servers).

The reason is that these tools may hide the complexity of a language, of "code", but for good developers this is not the hard bit; writing the code is the easy bit. The hard bit is understanding the problem and devising a great solution. Now, as a hardened game player Ian has a substantial level of deviousness, alternative thinking, surreal thinking - the kind of thinking that leads to great solutions. I'd like to see the game he builds.
