---
layout: post.pug 
title: "The Twenty Minute Union"
date: 2007-03-01
tags: ["post"]
legacy_wordpress_id: 163
---

A little while ago at work I said that I thought we could [build a union catlogue in twenty minutes](http://www.talis.com/tdn/20minuteunion_video), in the same style as the ruby-on-rails videos where they <strike>take over the world</strike> build a simple blog in 20 minutes - there's a few of them around and 20 minutes seems to be the magic number.

Anyways, to cut a long story short I got 'called' on it and told that I had to put my money where my mouth was and, despite having not done it yet, we announced the upcoming title in a public webinar. No pressure...

<!-- excerpt -->

For anyone else thinking about trying "The Twenty Minute Whatever" I suggest you re-consider. It's only when you try to record yourself coding, live, without edits that you realise how badly you type!

We published it yesterday over on our developer network site :-)

[The Twenty Minute Union](http://www.talis.com/tdn/20minuteunion_video)
