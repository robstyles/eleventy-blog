---
layout: post.pug 
title: "Jeff Han demoing multi-touch at TED"
date: 2007-03-19
tags: ["post","Interaction Design"]
legacy_wordpress_id: 168
---

<a title="http://www.ted.com/tedtalks/tedtalksplayer.cfm?key=j_han" href="http://www.ted.com/tedtalks/tedtalksplayer.cfm?key=j_han">http://www.ted.com/tedtalks/tedtalksplayer.cfm?key=j_han</a>

This is the same kind of stuff we've seen from Jeff Han before, but it's great to hear giving a rapid summary of the uses.

<!-- excerpt -->
