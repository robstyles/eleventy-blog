---
layout: post.pug 
title: "What's going to change books the way iPods have changed music?"
date: 2007-03-19
tags: ["post","Interaction Design"]
legacy_wordpress_id: 167
---

Electronic Paper. I don't mean the kind of rubbish we see today from [Sony](http://www.learningcenter.sony.us/assets/itpd/reader/), [the iRex iLiad](http://www.irextechnologies.com/products/iliad) or even the [Amazon Kindle](http://www.engadget.com/2006/09/11/amazon-kindle-meet-amazons-e-book-reader/). I mean sheets of paper, that require no power, where you turn the pages with your hands. Like real paper, but you can replace the content.

[Xerox Parc were working on it](http://www2.parc.com/hsl/projects/gyricon/), along with 3M, but closed it down in 2005 and have been seeking partners to license the technology. [Philips were working on on it too](http://www.research.philips.com/technologies/display/ov_elpap.html), and early this year spun a company off from its research labs - [Polymer Vision](http://www.polymervision.com/index.html). [Readius](http://www.polymervision.com/ProductsApplications/Readius/Index.html) is their shiny little display, but how long before they produce a USB compatible book?

<!-- excerpt -->

The book format matters because it doesn't need power. If you only have a single sheet you need memory and power to update the screen. If you replace the screen with pages then you supply the power and each page is its own memory - it's a very clever form factor that old book.
