---
layout: post.pug 
title: "Politics and the English Language"
date: 2007-03-07
tags: ["post"]
legacy_wordpress_id: 165
---

Not by me! How&nbsp;arrogant would that be? By George Orwell - [posted by Guy Kawasaki](http://feeds.feedburner.com/~r/guykawasaki/Gypm/~3/97479850/politics_and_th.html)

What an amazing essay by such a great author.

<!-- excerpt -->

It made me think back to [How technical writing sucks](http://www.virtualchaos.co.uk/blog/2007/01/13/how-technical-writing-sucks-the-five-sins/) over at Nadeem's blog and my own [It's All Just Talking](/2007/01/15/its-all-just-talking). Nad and I were talking about code style as well as styles of writing at the time.

Apply everything George Orwell says in the essay above and see what happens? Getters and Setters in Java spring to mind. Isn't Behaviour Driven Design simply always using the active instead of the passive?

The more I read, the more I code, the more I think about stuff, the more I believe that writing code is the same as writing prose. If you can't write clearly in natural language then what hope do you have in a programming language?
