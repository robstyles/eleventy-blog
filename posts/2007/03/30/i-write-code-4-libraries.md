---
layout: post.pug 
title: "I write code 4 libraries"
date: 2007-03-30
tags: ["post","Working at Talis"]
legacy_wordpress_id: 172
---

__Warning: Emotional rant follows.__

For about a year now I've been hanging out on #code4lib, an IRC channel for coders working in the library/archives/learning/lms space. I'm not a salesperson and I've never tried to sell anything.

<!-- excerpt -->

So I was a bit upset to see [Dan Scott accuse me](http://onebiglibrary.net/story/code4lib-2007-brain-dump#comment-2984) of being&nbsp;"in full sales mode" in his comment on Dan Chudnov's blog. I take that kinda personally.

The reason I chose to do the lightning talk was essentially because I saw a lot of talk about Open Source and yet one of the biggest problems we face (all of us, not just me) is one of Open Data. I probably was "in full sales mode" but for Open Data, not for Talis.

I went to Code4Lib to share ideas, to listen and to talk. It was great for that. You know, we're _all_ paid to write code for libraries; except Erik Hatcher, who's paid to analyse Japanese texts or something.

That's the nub of it, we're all paid to write code 4 libraries. Some of us are paid directly, one-to-one from the library coffers, some of us are paid by a vendor, who then splits the costs across many libraries. If Dan can't "sell" his ideas to his bosses he doesn't get paid; if I can't sell my ideas to my bosses I don't get paid. Same thing.

&lt;overreaction&gt;

> So, my question to Dan is... What business models are OK then?
> 
>  <ul> <li>Being employed by the library as an individual?  <li>Selling your skills to the library as an indiviudal?  <li>Selling your skills to the library as a small company?  <li>Selling your skills to the library as a big company?  <li>Selling a product to the library as an indiviudal?  <li>Selling a product to the library as a small company?</li></ul> 
> 
> Or is it _how much_ we charge that's a problem? We haven't worked out costings for the Talis Platform, we're not accepting orders yet. 

&lt;/overreaction&gt;

We were there, sharing - give and take. How much did your vendor give to Code4Lib? How much did you give? (er, in Dan's case a lot actually.) I don't want you to think what we're doing is cool because Richard or I have a 'slick' presentation, we want you to think what we're doing is cool because we're trying to help you. Maybe that's just a step too far for an evil vendor. Here's how it played out on IRC some time ago:

> [[<abbr>20:17:28</abbr>](http://code4lib.org/irc-logs/index.php?date=2007-03-15#290a8ef8-d0f7-488e-9aa8-286d24960cf9)] <<cite>mmmmmRob</cite>> <q>@karma talis<br></q><q>...<br></q>[[<abbr>20:17:30</abbr>](http://code4lib.org/irc-logs/index.php?date=2007-03-15#b0864b19-1442-47ff-b7c4-ffad75b75fbb)] <<cite>panizzi</cite>> <q>mmmmmRob: Karma for "talis" has been increased 1 time and decreased 5 times for a total karma of -4.<br></q><q>...<br></q>[[<abbr>20:17:38</abbr>](http://code4lib.org/irc-logs/index.php?date=2007-03-15#cce816a2-dbc4-4e05-aacf-c641ffcb7fe6)] <<cite>mmmmmRob</cite>> <q>WTF?<br></q><q>...<br></q>[[<abbr>20:18:01</abbr>](http://code4lib.org/irc-logs/index.php?date=2007-03-15#64e2c04f-61b5-4f8d-89b6-53247c5946f3)] <<cite>mmmmmRob</cite>> <q>:-(<br></q>[[<abbr>20:18:04</abbr>](http://code4lib.org/irc-logs/index.php?date=2007-03-15#ebe9543c-320b-41f3-8d8b-ddb819b3e22a)] <<cite>miker_</cite>> <q>@karma mmmmmRob<br></q>[[<abbr>20:18:04</abbr>](http://code4lib.org/irc-logs/index.php?date=2007-03-15#6d5b6740-a9d1-47b0-972b-22ccaffb094d)] <<cite>panizzi</cite>> <q>miker_: Karma for "mmmmmRob" has been increased 13 times and decreased 0 times for a total karma of 13.<br></q>[[<abbr>20:18:12</abbr>](http://code4lib.org/irc-logs/index.php?date=2007-03-15#fc8dfade-8aef-48e8-bafe-25c8b9a1587b)] <<cite>miker_</cite>> <q>so, you're ok :)<br></q>[[<abbr>20:18:23</abbr>](http://code4lib.org/irc-logs/index.php?date=2007-03-15#ead36acf-a659-4c9f-9300-09c09eb48636)] <<cite>miker_</cite>> <q>you're karma reflects well on your overloards<br></q>[[<abbr>20:18:25</abbr>](http://code4lib.org/irc-logs/index.php?date=2007-03-15#c6fd8382-9752-4e89-bd49-4f7a81422083)] <<cite>mmmmmRob</cite>> <q>yeah, but WTF did we do to get hit four time?<br></q>[[<abbr>20:18:42</abbr>](http://code4lib.org/irc-logs/index.php?date=2007-03-15#cd113199-8048-4475-8371-83b86e9a00ed)] <<cite>miker_</cite>> <q>mmmmmRob: you're a ScaryVendor(tm)</q> 

The problem here is&nbsp;this distinction between the people and the vendor. I work for Talis, RJW works for Talis, Talis is us (well, not just us obviously).&nbsp; Just like Equinox is miker, bradl, phasefx and the guys. We're employee owned, we make the decisions - saying Talis is evil is the same as saying I am evil.

<q>That's also why I'm blogging this on my own blog; that's me; that's Talis.</q>

&lt;full sales mode&gt;

[Talis](http://www.talis.com/) is a great place to work, and we're hiring. mail me at:

> #!/bin/bash
> 
>  
> 
> NICK=mmmmmRob; SURNAME=Styles; SCARYVENDORNAME=talis;
> 
>  
> 
> echo ${NICK}.${SURNAME}@${SCARYVENDORNAME}.com | sed -e "s/[m]\{5\}\(.*\)/\1/"; 

&lt;/full sales mode&gt;
