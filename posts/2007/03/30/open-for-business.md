---
layout: post.pug 
title: "Open for Business..."
date: 2007-03-30
tags: ["post","Talis Technical"]
legacy_wordpress_id: 171
---

We're letting people have a play with some of the platform services we've been building, and asking for open, honest feedback. [Ross Singer has been playing with Bigfoot on the Talis Platform](http://dilettantes.code4lib.org/2007/03/29/in-search-of-bigfoot/).

He's written [bigfoot-ruby](http://www.code4lib.org/trac/browser/bigfoot-ruby), a ruby library for accessing some of the services. Both bigfoot-ruby and our platform are works in progress, so please excuse the noise as we dig around.

<!-- excerpt -->
