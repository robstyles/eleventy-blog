---
layout: post.pug 
title: "Read Write Wrangle with Talis"
date: 2007-12-20
tags: ["post","Working at Talis"]
legacy_wordpress_id: 216
---

[Read/Write Web have been digging into us in a bit more detail](http://www.readwriteweb.com/archives/talis_semantic_web.php).

The piece is a pretty good overview and it's great to see how much they've understood about us and the culture we're trying to build and maintain.

<!-- excerpt -->

[Project Cenote](http://cenote.talis.com/), a concept car [Nad](http://www.virtualchaos.co.uk/blog/) and I built some time ago gets a mention, described as having an "aesthetically pleasing interface" - with hindsight I think there just maybe a bit too much chrome going on ;-)

The author, [Zach Beauvais](http://www.zachbeauvais.com/), seems to be finding the idea that we are two things difficult. He's not the first. We're both a vertical software provider in the library market in the UK and a horizontal platform provider for anyone who want to play with Semantic Web data anywhere in the world. We have some of the best people in both of those arenas working for us - which actually makes it very easy to balance those two aspects internally.

It's incredibly pleasing to see this kind of review of us - it feels spot on, we try to be open and relaxed and do the very best we can. Hopefully we'll see more of this kind of coverage as more people get to know us.
