---
layout: post.pug 
title: "Should the President Use E-mail?"
date: 2007-12-14
tags: ["post","Personal","Random Thought"]
legacy_wordpress_id: 213
---

[Should the President Use E-mail?](http://feeds.feedburner.com/~r/FreakonomicsBlog/~3/196716621/)

That's the question posed by Freakonomics a few days ago, but not for the reasons you might suspect.

<!-- excerpt -->

Apparently email is a good format for delivering bad news. That seems interesting, apparently the dropping of many social cues allows people to receive information in a less defensive attitude. That may well be, but my mother always said

> If you've got something nice to say write it down, if it's not so nice say it to their face.

Of course, she also said

> If you haven't got anything nice to say don't say anything at all.

And if we're on the subject of things parents tell their children you should check out [today's xkcd](http://xkcd.com/357/)...

Going back to the question of Presidents receiving bad news, I'm not sure my mom's second piece of advice would go down so well.
