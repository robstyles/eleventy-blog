---
layout: post.pug 
title: "Cheap Wii Multitouch"
date: 2007-12-30
tags: ["post","Interaction Design"]
legacy_wordpress_id: 217
---

[Low-Cost Multi-touch Whiteboard using the Wiimote](http://www.youtube.com/watch?v=5s5EvhHy7eQ) is a great piece of innovation using the motion tracking camera in the wiimote to provide cheap tracking for a multi-touch display.

<!-- excerpt -->
