---
layout: post.pug 
title: "Open Data Commons License"
date: 2007-12-17
tags: ["post","Working at Talis"]
legacy_wordpress_id: 214
---

At work we've [just announced some great news](http://www.talis.com/platform/news_and_events/index.shtml#no054). The Open Data Commons license is out - in collaboration with Creative Commons and others.<br />

The new home for the license is at [http://www.opendatacommons.org/](http://www.opendatacommons.org/) and the new licenses are available there for comment.

<!-- excerpt -->

The only downside is that the full name of the license ended up as the Open Data Commons Public Domain Dedication and Licence ODC PDDL which will inevitably end up being called the pee-diddle.
