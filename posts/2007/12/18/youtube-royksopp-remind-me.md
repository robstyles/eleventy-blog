---
layout: post.pug 
title: "YouTube - Royksopp - Remind Me"
date: 2007-12-18
tags: ["post"]
legacy_wordpress_id: 215
---

[YouTube - Royksopp - Remind Me](http://www.youtube.com/watch?v=lBvaHZIrt0o):

Fantastic music video with constantly changing information visualisations.

<!-- excerpt -->

found via: [Boodleheimer](http://boodleheimer.wordpress.com/2007/06/10/fun-with-graphics-and-data/)
