---
layout: post.pug 
title: "Been Away Too Long"
date: 2007-07-31
tags: ["post","Blog on Blog","Working at Talis"]
legacy_wordpress_id: 189
---

life's been a little hectic this year, having been away 7 times in the first 6 months. Being back in the country I've been spending free time with my wife and 3 little cherubs rather than here blogging for whoever's reading.

But I have still been blogging, along with [Richard](http://itoccurs.wordpress.com), [Paul](http://paulmiller.typepad.com/thinking_about_the_future/), [Ian](http://iandavis.com/blog) and [Danny](http://dannyayers.com/) I've been blogging for work on both [Panlibus](http://blogs.talis.com/panlibus/) and [Nodalities](http://blogs.talis.com/nodalities/). If I'm not here then you may catch me there.

<!-- excerpt -->

<!-- technorati tags start --><p style="text-align:right;font-size:10px;">Technorati Tags: <a href="http://www.technorati.com/tag/Talis" rel="tag">Talis</a></p><!-- technorati tags end -->
