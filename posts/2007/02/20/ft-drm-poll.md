---
layout: post.pug 
title: "FT DRM Poll"
date: 2007-02-20
tags: ["post"]
legacy_wordpress_id: 161
---

The FT is [running a nice little poll](http://www.ft.com/drm)&nbsp;on the question of DRM in music. Unsurprisngly the results are overwhelmingly in favour of dropping DRM. The comments, though, contain some real gems in this debate.

<!-- excerpt -->
