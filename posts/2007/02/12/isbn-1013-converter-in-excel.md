---
layout: post.pug 
title: "ISBN 10/13 Converter in Excel"
date: 2007-02-12
tags: ["post","Other Technical"]
legacy_wordpress_id: 158
---

I had a batch of ISBNs to convert from ISBN13 back to ISBN10. I've got some C# to do it, and some Java, but figured this Excel spreadsheet converter would be useful for folks.

[ISBN10-13 Converter](/assets/tools/ISBN10-13%20Converter.xls)

<!-- excerpt -->

Free to do with what you will - provided without warranty.
