---
layout: post.pug 
title: "HD-DVD and Blu-Ray broken"
date: 2007-02-13
tags: ["post"]
legacy_wordpress_id: 159
---

When I said the other day in [comment on Steve Jobs Thought On Music](/2007/02/07/steve-jobs-apple-thoughts-on-music) that DRM doesn't work I really meant [it doesn't work](http://www.boingboing.net/2007/02/13/bluray_and_hddvd_bro.html).

The "Processing Key" of the AACS (Advanced Access Content System) protection system used on both of the new HD formats has been captured while in memory in clear text.

<!-- excerpt -->

The Processing Key is part of a sequence of steps, but basically allows you to calculate the Media Key that is then used to play the content.
