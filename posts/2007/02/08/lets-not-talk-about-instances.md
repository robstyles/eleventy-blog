---
layout: post.pug 
title: "Let's not talk about instances."
date: 2007-02-08
tags: ["post"]
legacy_wordpress_id: 155
---

One of the things that interests me is approaches to architecting SaaS, Software as a Service. Recently I've been thinking about approaches to multi-tenancy; at a very high, "I wonder what... would do", kind of level - nothing deep, nothing tested (yet).

My thoughts started with the fact that everything is nested in different levels of containers. Each line of our own code runs within some context that we create within our application code, that in turn runs inside a container, this might be the OS or it may Microsoft .Net, the PHP runtime or a J2EE or Servlet container, which in turn runs inside the OS, which in turn runs inside a machine, which in turn runs inside a data center...

<!-- excerpt -->

In the beginning we wrote software and put it in boxes, the tenants of our software took it away and installed it somewhere. They had to make their own way and isolation of tenants was only a licensing issue. The tenants were responsible for keeping their own installations separate and we only had to ensure that no-one was deliberately sharing systems and not paying us for it. Oracle are pretty much still in this place - using licensing to making sure no-one uses their software that is. Working out how much someone was using the software was a case of counting their CPUs, or employees, or customers or something like that&nbsp;every year or so.

Many tenants have never really been that happy about having to make their own way though, so a whole group of companies sprang up to offer Facilities Management contracts - multi-tenancy at the data center level. Each tenant still had a whole load of machines dedicated to them, but now they were someone else's problem. Isolating tenants became a problem for sysadmins and network engineers. Usage was still a case of counting CPUs, you just had to be sure to count the right ones.

With so much kit, mostly idle, it was inevitable we'd see forays into virtualization and this is working well, but still isolation is left at the (virtual) machine level, a problem for sysadmins and networking. Usage can still be at the same level as before but there is also the option of starting to pay for the computing power you use as the hardware can now be shared. Costs can go down as the overall usage of the hardware goes up and managing becomes a case of keeping track of contention ratios, well understood from network access at ISPs.

At this point the application still only needs to work with one tenant at a time, all configuration, logic, logging, tracking, and so on is separated by the virtual machines.

Still, there's a big overhead to running lots of VMs and you can't really load-balance and so on, although VMWare are making great leaps in this space with the ability to move running VMs from one machine to another.

Finally we start to get close to what I'd really call SaaS. The shared hosting providers. With these providers we start to have multiple applications running inside a single container, the same instance of the operating system, web server and physical machine. A standard package might be Apache, with a virtual host, PHP, MySql and so on. For your own software running multi-tenant you could run one instance per customer inside the container of your choosing: Apache, Tomcat, WebSphere, ASP.Net or Mono perhaps. At this point the application still only needs to know about one tenant inside each instance. Isolation of tenants is handled by the container, usage can be as well. The payback is that there will be an overhead to pay for having several instances of your application running.

Then finally we have applications that are natively aware of multiple tenants. That is to say the application logic has been written to take account of which tenant they're working for at every step: authentication, access control, data access, configuration, skinning, logging, usage tracking, prioritization and so on. There is only one instance of the application running&nbsp; for all tenants. Or rather, multiple instances are running for reasons of scaling, resilience and load-balancing etc rather than to facilitate multi-tenant running.

So what to do? Which container should I let handle the isolation?
