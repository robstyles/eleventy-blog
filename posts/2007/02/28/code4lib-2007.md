---
layout: post.pug 
title: "Code4Lib 2007"
date: 2007-02-28
tags: ["post"]
legacy_wordpress_id: 162
---

I'm sitting in a very nice conference venue in Athens, GA. It's the first conference I've been to that has desks for everyone to work on and plenty of power sockets. Imagine what you see at the G8 Summit, or other political meetings.

Keynote today is Karen Schneider, Florida State University, blogging as [The Free Range Librarian](http://freerangelibrarian.com/).

<!-- excerpt -->

In her presentation, entitled "hurry up please it's time", though I didn;t work out why, she declares that the sector is "in a state of emergency" with "sucky" library software, having given its most precious collections away, and having lost sight of its own direction.

She believes this is solvable, of course, and talks about a renaissance period of artisan librarians building the tools to do the job themselves. We see the world the same at Talis, and continue to build up our Developer Network to help with that as well as working to open up our ILS, other ILSs and most importantly the data.

She raises issues that I hope we all already recognise, such as:

> users are not going to your catlogue first to find information, it;s somewhere down there with asking your mother... and asking your mother is probably easier... and more accurate 

and that

> If you can do the same thing for less money why would you not do that? I would say you have a moral obligation to do that, in some cases you have a legal obligation to do that. 

Her examples of renaissance&nbsp;software include Evergreen, Umlaut, Scriblio and Solr.

These pieces, and librarian-built software, is important because it does four key things: it starts to restore the power balance [with vendors]; it helps reinstate the direction of what we do; it puts an emphasis back on the library [and the librarian] and it sends a message that we mean business.

These are great points, with which I agree, which is why we've been running our own competitions to support and encourage librarian-build innovation.

Finally, it was great to see her handle a phone ringing in the audience with:

> I don't think that's mine, at least I hope not...
> 
>  
> 
> Isn't it sad that phones aren't contextual, they have no sense of where you are and when to be quiet...
> 
>  
> 
> ...
> 
>  
> 
> Like the ultimate toddler. 

I always think the way a speaker handles the audience, with phones ringing, questions and so on, is a great way to see who they really are.
