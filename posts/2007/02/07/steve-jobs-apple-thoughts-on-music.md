---
layout: post.pug 
title: "Steve Jobs, Apple, Thoughts On Music"
date: 2007-02-07
tags: ["post","Internet Social Impact"]
legacy_wordpress_id: 154
---

Yesterday, Steve Jobs posted [an excellent short piece on apple.com talking about the future of music stores, players and DRM](http://www.apple.com/hotnews/thoughtsonmusic/). He doesn't address many of the detailed points about DRM, but talks about how it's not working. Only 3% of music on the average iPod is DRM protected. Personally I have over 6,000 songs on my machine none of which have any kind of DRM as they were all ripped from CDs that I own; I have no DRM protected music and no "stolen" music.

Steve's posting focusses right on the nub of the issue - why cripple consumers with the restrictions of DRM (and cripple the industry with the costs of DRM) when it simply doesn't work.

<!-- excerpt -->

> Why would the big four music companies agree to let Apple and others distribute their music without using DRM systems to protect it? The simplest answer is because DRMs haven’t worked, and may never work, to halt music piracy. 

[Bruce Schneier](http://www.schneier.com/) explains why _it will never really work_ in his posting on [Separating Data Ownership and Device Ownership](http://www.schneier.com/blog/archives/2006/11/separating_data.html).

> The [DRM] security problem is similar, but you store your valuables in someone else's safe. Even worse, it's someone you don't trust. He doesn't know the combination, but he controls access to the safe. He can try to break in at his leisure. He can transport the safe anyplace he needs to. He can use whatever tools he wants. In the first case, the safe needs to be secure, but it's still just a part of your overall home security. In the second case, the safe is the only security device you have. 

Steve seems frustrated by the lack of evidence used in this debate. James Boyle over on Google TechTalk: [7 Ways To Ruin A Technological Revolution](http://video.google.com/videoplay?docid=-8538240380002213699)&nbsp;give some great insights into intellectual law as well as content-ownership culture. He discusses the lack of any kind of empirical debate also.

For a detailed discussion on DRM (written for librarians), the ALA's [Digital Rights Management: A Guide for Librarians](http://www.ala.org/ala/washoff/WOissues/copyrightb/digitalrights/DRMfinal.pdf)&nbsp;is worth the effort.

found via Andrew Hankinson on [web4lib](http://lists.webjunction.org/web4lib/)&nbsp;where [Leo Klein](http://www.chicagolibrarian.com/) points out that "the Content Owners would prefer bankruptcy to DRM-free content".

__Update: __[Jonathon Coulton has picked up on this too](http://www.jonathancoulton.com/2007/02/07/steve-jobs-on-drm/). I only bother to mention it as he has written some fantastic songs, including [Code Monkey](http://www.jonathancoulton.com/2006/04/14/thing-a-week-29-code-monkey/).

 <div class="wlWriterSmartContent" id="0767317B-992E-4b12-91E0-4F059A8CECA8:5131c8e8-e93b-46d2-b4f5-f27d4739cf5b" contenteditable="false" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati tags: <a href="http://technorati.com/tags/DRM" rel="tag">DRM</a>, <a href="http://technorati.com/tags/Digital%20Rights%20Management" rel="tag">Digital Rights Management</a>, <a href="http://technorati.com/tags/Apple" rel="tag">Apple</a>, <a href="http://technorati.com/tags/iTunes" rel="tag">iTunes</a>, <a href="http://technorati.com/tags/Steve%20Jobs" rel="tag">Steve Jobs</a></div>
