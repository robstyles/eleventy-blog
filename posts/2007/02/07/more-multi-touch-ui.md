---
layout: post.pug 
title: "More Multi-Touch UI"
date: 2007-02-07
tags: ["post","Interaction Design"]
legacy_wordpress_id: 153
---

So, [the Apple iPhone makes a big deal of its multi-touch gesture interface](http://www.apple.com/quicktime/qtv/mwsf07/). I can see why, it's pretty slick, but there have been larger, desktop scale, demos of this kind of thing around for a while.

A lot of them look like they're getting close to saleable. [TactaDraw](http://tactiva.com/TactaDrawLarge.mov), from [Tactiva](http://tactiva.com)&nbsp;looks amazing, and is a showcase of their [TactaPad](http://tactiva.com/tactapad.html) hardware - they seem to have some industrial design work left to do on the device.

<!-- excerpt -->

[Perceptive Pixel](http://www.perceptivepixel.com/)&nbsp;are planning to market "the most advanced multi-touch system in the world" and judging by this [excellent video](http://fastcompany.com/video/general/perceptivepixel.html)&nbsp;they're not bragging too much. The product comes out of [earlier](http://www.youtube.com/watch?v=4nXHdkwgOMs)&nbsp;[work](http://cs.nyu.edu/~jhan/ftirtouch/).

[Reactable](http://mtg.upf.edu/reactable/), on the other hand uses devices rather than gestures, they have [an introductory video](http://mtg.upf.edu/reactable/videos/demo2_basic1.mp4),&nbsp;a [basic demo](http://mtg.upf.edu/reactable/videos/demo2_basic2.mp4)&nbsp;and a [free improvisation example](http://mtg.upf.edu/reactable/videos/demo2_improv.mp4).

I'm interested in this kind of thing partly for [work](http://www.talis.com/)&nbsp;and partly for play. You can imagine the possibilities of DJing with this stuff. [Final Scratch](http://www.stantondj.com/v2/fs/spt_fs2_video.asp) already integrates traditional decks with MP3 DJing and there are [other](http://www.xone.co.uk/3d.htm) [hardware](http://faderfox.de/) [devices](http://www.waveidea.com/en/products/bitstream_3x/#) too, I own a [Behringer BCD2000](http://www.behringer.com/BCD2000/index.cfm).

With cheap, modular components it's inevitable that a plethora of physical devices are around, including real interesting ones like this [LED Mixer](http://video.google.com/videoplay?docid=-2086731008145480666).

But the multi-touch gesture interfaces have the potential to make all of these obsolete. Imagine the interface you could create using those Reactable controls, with a handfule of "records" that you could instantly assign any MP3 to? Or a large version of the TactaPad, with virtual decks on screen - an obvious way to work.

The world of gaming often benefits from devices like this first&nbsp;and [World of Warcraft is an obvious target](http://www.youtube.com/watch?v=zEax1mJhJQ0&amp;search=touch%20screen). This example combines multi-touch gesture commands with voice recognition to command forces quickly and accurately.

If you were wondering why [Vista](http://www.microsoft.com/windows/products/windowsvista/default.mspx) has invested so much in completely re-engineering the UI model and going true 3D, or why OSX did the same and why the Linux community have built Beryl, this is why. We are on the brink of seeing some really good physical interaction capabilities coming out.

I wonder what might become possible if we combined some of this with RFID and [Physical World Hyperlinks](http://theponderingprimate.blogspot.com/2007/01/create-your-own-physical-world.html)?

But on a lighter note, [we'll all need to beef up](http://www.ok-cancel.com/comic/3.html).

By the way, if this stuff interests you, [we're looking for an interaction designer](http://www.talis.com/about/careers.shtml).

 <div class="wlWriterSmartContent" id="0767317B-992E-4b12-91E0-4F059A8CECA8:63e6bf1a-3827-477a-a7ed-bc6904f0b2a4" contenteditable="false" style="padding-right: 0px; display: inline; padding-left: 0px; float: none; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati tags: <a href="http://technorati.com/tags/Gesture%20Interface" rel="tag">Gesture Interface</a>, <a href="http://technorati.com/tags/Interface%20Design" rel="tag">Interface Design</a>, <a href="http://technorati.com/tags/Interaction%20Design" rel="tag">Interaction Design</a>, <a href="http://technorati.com/tags/Multi-Touch%20Interface" rel="tag">Multi-Touch Interface</a>, <a href="http://technorati.com/tags/Physical%20World%20Hyperlinks" rel="tag">Physical World Hyperlinks</a></div>
