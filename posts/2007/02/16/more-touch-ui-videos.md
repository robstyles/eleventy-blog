---
layout: post.pug 
title: "More Touch UI Videos"
date: 2007-02-16
tags: ["post","Interaction Design"]
legacy_wordpress_id: 160
---

I wrote last week about [the increasing visibility of multi-touch UIs, most recently with Apple's iPhone](/2007/02/07/more-multi-touch-ui). This week I've found some more to share...

[Ksan Lab](http://www.ksanlab.com/playground) have a nice demo of [Virtual Painter](http://www.youtube.com/watch?v=zCo_No_Bpwk&amp;mode=related&amp;search=) up on YouTube giving a nice view of the natural style of interaction allowed by these devices. The gestures used here are different to those seen in some of the other demos. It (or the demonstrator) seems to favor single-finger use.

<!-- excerpt -->

Here's [another great demo that combines touch with physical tokens](http://www.youtube.com/watch?v=zQRYK9Ee-Fw&amp;mode=related&amp;search=) to give a great Conservation Lab demo, it seems to be pitching at a Point of Information styles application.

Then, of course, there's [TouchTable](http://www.youtube.com/watch?v=X2pPeW4cUgU&amp;mode=related&amp;search=) which is much larger than a lot of what we've seen and is designed around a shared experience - I can imagine this having huge potential in so many applications from meetings (war rooms) and team gaming either co-operative or not.

But if it's large that you want then [iBar](http://www.youtube.com/watch?v=iaKehq6qsdY&amp;mode=related&amp;search=) should be up your street. It's not too clever, having a rather limited display repertoire, but still the _size_ is impressive.

[Lemur](http://www.jazzmutant.com/lemur_overview.php)&nbsp;from JazzMutant&nbsp;is designed specifically as a medi controller, so the dispaly is geared to ui artefacts rather than a desktop, but the different types of interactions are impressive. That's [being shown off here](http://www.youtube.com/watch?v=4th6X0b-vHs&amp;mode=related&amp;search=)&nbsp;in one mode and [here as a sequencer](http://www.youtube.com/watch?v=0O70FrnH2JU&amp;mode=related&amp;search=).

[Natural Interaction](http://naturalinteraction.org/)&nbsp;are also showcasing stuff like an interactive table&nbsp;called [tabulaTouch, showing tabulaMaps](http://www.youtube.com/watch?v=l2oMmCyiJZA). They stick to a lot fo the more common UI gestures that we've been seeing (I must find some names for them). They also have a [Minority Report style UI](http://naturalinteraction.org/movies/minorityxp.mpg).

Then there's the [Diamond Touch work that allows simultaneous multi-user touch input](http://www.youtube.com/watch?v=t35HXAjNW6s)&nbsp;and looks very slick again - this time working with a fairly standard OS interface. There's a nice touch when the team "accidentally" spill the coke on the screen. Diamond Touch comes out of [MERL](http://www.merl.com/projects/DiamondTouch/), so stands a good chance of hitting the shelves.

But of course there'd be no show without Punch, and I finally found some touch UI work from [Microsoft, coming on VIsta Tablets](http://www.youtube.com/watch?v=gQpYEyy930I&amp;eurl=). I could have cried watching Hilton Locke demoing this; the cursor doesn't stick with your finger! Even doing the demo is broken. Apparently people new to touch input will find it really intuitive 'cause "pretty much everyone uses a two button mouse". That's right, they've put a finger controlled mouse on screen! That sucks in so many ways I just can't start.

 <div class="wlWriterSmartContent" id="0767317B-992E-4b12-91E0-4F059A8CECA8:9dd2a812-9e46-49c1-8b62-f822cc9a4e65" contenteditable="false" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati tags: <a href="http://technorati.com/tags/Gesture%20Interface" rel="tag">Gesture Interface</a>, <a href="http://technorati.com/tags/Interface%20Design" rel="tag">Interface Design</a>, <a href="http://technorati.com/tags/Interaction%20Design" rel="tag">Interaction Design</a>, <a href="http://technorati.com/tags/Multi-Touch%20Interface" rel="tag">Multi-Touch Interface</a>, <a href="http://technorati.com/tags/Physical%20World%20Hyperlinks" rel="tag">Physical World Hyperlinks</a>, <a href="http://technorati.com/tags/Vista" rel="tag">Vista</a>, <a href="http://technorati.com/tags/Tablets" rel="tag">Tablets</a>, <a href="http://technorati.com/tags/Natural%20Interaction" rel="tag">Natural Interaction</a></div>
