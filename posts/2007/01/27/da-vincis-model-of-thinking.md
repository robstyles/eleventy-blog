---
layout: post.pug 
title: "Da Vinci's Model of Thinking"
date: 2007-01-27
tags: ["post"]
legacy_wordpress_id: 150
---

I'm on an interesting communications skills course for a few days, and already, by just 10.30 on day one, I have an interesting reference. Apparently, Da Vinci had 7 ways of thinking and exploring ideas, puzzles and so on.

 <ol> <li>Curiosità  <li>Dimonstrazione  <li>Sensazione  <li>Sfumato  <li>Arte/Scienza  <li>Corporalità  <li>Connessione</li></ol> 

<!-- excerpt -->

These are in Italian, so I'll expand and translate (not that it's hard to guess the English) when I get a chance.

These come from Michael Gelb's book _How to Think Like Leonardo Da Vinci_ which I haven't read - as I say it was just referenced on this course.
