---
layout: post.pug 
title: "“How do I know you are who you say you are?”"
date: 2007-01-23
tags: ["post","Security And Privacy"]
legacy_wordpress_id: 148
---

Over on Alan's blog he mentions that [banks are training us to be insecure](http://www.alandix.com/blog/2007/01/22/practicing-security/).

This is a hard problem to solve. The population at large can well understand that somebody _could_ phone them and say "hi, I'm from blah-blah bank", but the assumption is that they won't.

<!-- excerpt -->

This initial assumption of trust is what makes it easy to do business with each other, easy to have a conversation. But it also makes it easy for people to take advantage. [Kevin Mitnick](http://www.kevinmitnick.com/)'s book on the subject, [The Art of Deception](http://www.amazon.co.uk/Art-Deception-Controlling-Element-Security/dp/0471237124), is a great read. It's full of horror stories of how perfectly normal, smart, people are duped by simple things like "but I knew who hew was, he phoned the other day".

No, I know I'm a freak - I've used "[I Like Cheese](http://www.toolshed.com/blog/articles/2004/09/29/sweet-revenge-on-telemarketers)" to ward off tele-marketers - so I simply ask the bank&nbsp;for the 3rd and 5th letters of their password. It usually goes something like this:

> "Hi, this is Samantha, I'm calling from blah-blah bank. Is that Mr Styles"
> 
>  
> 
> "Yep, what can I do for you?"
> 
>  
> 
> "I need to check some details on your account, but first I need to ask you some security questions. Can I have the first line of your address?"
> 
>  
> 
> "Sure, but first I need to make sure you're who you say you are. Can I have the third and fifth letters from your password, please?"
> 
>  
> 
> "I'm sorry?"
> 
>  
> 
> "Well, you called me, so I don't know who you are until you answer some security questions. Can I have the third and fifth letters from your password please?"
> 
>  
> 
> "I'm sorry sir, I don't understand what you mean."
> 
>  
> 
> "Well, I need to know who you are before I can give you any of my details."
> 
>  
> 
> "oh, ok, I'm Samantha from blah-blah bank."
> 
>  
> 
> "Great, can I have the third and fifthe letters from your password please?"
> 
>  
> 
> "erm, I don't have a password sir, what is it you mean?"
> 
>  
> 
> "Well, you should have received a telephone banking password in the post in order to access your customer, that's me. I need you to tell me the third and fifth letters from that password, without revealing the whole password to me, before I can give you any details."
> 
>  
> 
> "ok, I don't have that password"
> 
>  
> 
> "ok, perhaps you can email then?"
> 
>  
> 
> "sure, I'll do that" 

Of course, by the end of the call the conversation has slowed to an incredulous and confused drawl, not the chipper, bright young thing that started off. I know, it's sad; I'm a freak, but it makes me laugh.
