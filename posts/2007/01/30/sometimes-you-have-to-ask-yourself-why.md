---
layout: post.pug 
title: "Sometimes, you have to ask yourself \"why?\""
date: 2007-01-30
tags: ["post"]
legacy_wordpress_id: 151
---

Sun seems to raise this question in my mind more than any other. On this occassion it was "why didn't Sun win the battle for the desktop?"

[This film, from 1992, might explain why](http://asktog.com/starfire/)...

<!-- excerpt -->

bear in mind when watching this that...

[Windows 3.x had been out for 2 years already](http://www.microsoft.com/windows/WinHistoryProGraphic.mspx)

[Columbo was more than 24 years old](http://en.wikipedia.org/wiki/Columbo)

[Logan's Run](http://www.imdb.com/title/tt0074812/)&nbsp;and [Battlestar Galactica](http://www.tv.com/battlestar-galactica-1978/show/1253/summary.html) were old hat and [even Star Wars was 15 years old](http://en.wikipedia.org/wiki/Star_Wars_Episode_IV:_A_New_Hope)!

And that's the best that "the talents of more than 100 engineers, designers, futurists, and filmakers." could come up with!
