---
layout: post.pug 
title: "It's all just talking."
date: 2007-01-15
tags: ["post"]
legacy_wordpress_id: 146
---

Nadeem's been talking about [How technical writing sucks](http://www.virtualchaos.co.uk/blog/2007/01/13/how-technical-writing-sucks-the-five-sins/)&nbsp;and worrying about his own abilities in this area. He even goes so far as to suggest several other people who are better at it, me included&nbsp;(aw Thanks Nad); although he does accuse us all of being old :-(

Then, this week, he blogs ["learning a new programming language is just about learning a new syntax"](http://www.virtualchaos.co.uk/blog/2007/01/14/learning-a-new-programming-language-is-just-about-learning-a-new-syntax/)

<!-- excerpt -->

Both posts are well-worth reading, but it made me wonder why they're two posts? The main reason IMHO that learning a new programming language isn't just about learning the syntax is because it's technical writing.

We've talked about this a lot at our Geek Book Club and I take a pretty extreme stance (nothing new for me there then). I don't care if your code compiles; I can get code to compile because I have an unambiguous arbiter of truth - the compiler. I care that your code expresses what it is trying to do clearly, concisely and above all accurately because I have no hope of making it work (even if it compiles) if I can't read your understanding of the problem. It's _all_ technical writing. It's all just talking; telling someone else what you know about the problem.

Sure, programming languages started as a way of describing a set of instructions for the computer to follow; to understand them you had to mentally model what the computer would do with each push, pop and jump. Then we started writing compilers that would translate descriptions written in our own words (variable and function names) into whatever the machine needs in its registers.

With that came different langauges for describing different way of thinking. What happens if you've only been taught to think one way? What if you've been taught that a program is a set of inctructions for a computer to follow, rather than a language for writing about a model? Then you'd be stuffed, right?

You'd have been taught practically nothing about how to assign responsibilities to different objects in an OO language or how to think about depth-first or breadth-first searches. You'd probably not have taught about Propositional Logic (and I really don't think _that_ way) or Modal Logic.

But let's look at "learning a new programming language is just about learning a new syntax" from another perspective; the language designer. This statement says that the language designer is doing nothing but making arbitrary changes to the use of semi-colons or braces; changing the choice of a few words, for, if, then. This is so incredibly disingenuous - to suggest that some of the most incredibly intelligent people in the industry are just playing with syntax.

Now, folks graduating from computing courses are not at fault for thinking that the point of a program is to tell the computer what to do - the lecturers are. [Joel Agrees](http://www.joelonsoftware.com/articles/ThePerilsofJavaSchools.html).

Perhaps we should be asking applicants to write a short essay rather than some code.

 <div class="wlWriterSmartContent" id="0767317B-992E-4b12-91E0-4F059A8CECA8:599455aa-ec1a-4d08-b36f-2dd3555341dc" contenteditable="false" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati tags: <a href="http://technorati.com/tags/ProgrammingLanguages" rel="tag">ProgrammingLanguages</a>, <a href="http://technorati.com/tags/ComputerScience" rel="tag">ComputerScience</a>, <a href="http://technorati.com/tags/Programming" rel="tag">Programming</a>, <a href="http://technorati.com/tags/Education" rel="tag">Education</a>, <a href="http://technorati.com/tags/Recruitment" rel="tag">Recruitment</a>, <a href="http://technorati.com/tags/TechnicalWriting" rel="tag">TechnicalWriting</a></div>
