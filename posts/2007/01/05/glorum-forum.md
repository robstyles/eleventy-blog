---
layout: post.pug 
title: "Glorum, Forum"
date: 2007-01-05
tags: ["post","New Toy"]
legacy_wordpress_id: 141
---

I discovered [Glorum](http://www.glorum.com/) today. (via the very nice [Thinking and Making](http://thinkingandmaking.com/)) So what is Glorum? By its own definition it's Forum 2.0, but let's cut the 2.0 neologisms for a moment. You know the way some feed readers display the news in Dave Winer's [river-of-news](http://www.reallysimplesyndication.com/riverOfNews) style? Well it's kind of like that for forums. The other thing it reminded me of is news groups, when you sort the messages by thread. Subject Navigation is emergent, the tags used on messages appear on the right. They haven't bothered with a snazzy tag cloud, but no doubt they will. What the emergent hierarchy allows is a forum that has more of the permission characteristics of a wiki - you want a new category? Add a tag. This tag based navigation also allows messages to appear in more than one category - changing the dynamic of the usual "this dicussion moved to ..." problem in forums. And then, the community self-polices too - each visitor can attribute positive or negative karma to each message (or reply) and if enough people give a message a negative mark it gets deleted. This looks nice - kind of a pileup between email, wiki, forums and newsgroups. Very nice.

 <div class="wlWriterSmartContent" id="0767317B-992E-4b12-91E0-4F059A8CECA8:4c44bdd5-a39f-4c56-ae40-da2ac27c655b" contenteditable="false" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati tags: <a href="http://technorati.com/tags/Glorum" rel="tag">Glorum</a>, <a href="http://technorati.com/tags/Web2.0" rel="tag">Web2.0</a>, <a href="http://technorati.com/tags/Forums" rel="tag">Forums</a></div>
