---
layout: post.pug 
title: "I Absolutely Do Or Don't Want OpenID"
date: 2007-01-09
tags: ["post","Security And Privacy"]
legacy_wordpress_id: 142
---

Over at work we're talking about OpenID, one of my colleagues, [Richard Wallis is skeptical](http://blogs.talis.com/panlibus/archives/2007/01/will_this_one_b.php)...

So am I.

<!-- excerpt -->

I don't want [OpenID](http://openid.net/), I'm an individual who spends a lot of time online. My online nick, mmmmmRob, already provides a rather incriminating history just on what [Google have indexed](http://www.google.co.uk/search?hl=en&amp;q=mmmmmrob&amp;btnG=Google+Search). I do not trust any provider (let alone Microsoft, Passport? Come on!) to authenticate me everywhere, because they also then get to see where I go, if not what I do.

I don't want OpenID, I don't want to have lots of passwords... the positive point of having lots of OpenID providers is great for the individual, but there are two sides to this choice. The sites I use will also have to decide which OpenID providers _they_ trust. There will be many occasions, I'm sure, where the lists won't intersect. I would choose (other objections aside)&nbsp;based on who will keep my data private and secure. Commercial sites will choose based on who they can share data with and how that will enable them to target me more successfully.

I don't want OpenID, I don't want one thing to break everything. If my OpenID&nbsp;is compromised all the sites I use are open to that one set of authentication details. Say it's a flaw in my provider or in OpenID itself, not just my account, then no matter how quickly I change it, it's compromised again; all the sites allowing OpenID remaining either vulnerable or off-air until it's fixed.

I don't want OpenID, because it doesn't give me one set of credentials. Online I live on the web, on IRC, in an assortment of Instant Messengers I can't see OpenID integrating with every protocol (although technically it could). When IRC servers support it then we may have something ubiquitous. The web is not the internet.

 <hr>  

Over at work we're talking about OpenID, one of my colleagues, [Richard Wallis is skeptical](http://blogs.talis.com/panlibus/archives/2007/01/will_this_one_b.php)...

I'm not.

I want OpenID. I have so many usernames and passwords I have to keep them all written down, I've forgotten how many accounts I had to re-create just because I changed my email address.

I want OpenID. I'm not sure I trust some of the sites I use to keep a password secure, so instead I make up some junk password and end up creating a new account each time I visit. If my&nbsp;OpenID provider will keep my credentials that'll be safer.

I want OpenID. I write internet sites. If I can cross-reference the browsing habits of my users with those seen on other sites I exchange data with then I can make my site better by offering links, promotions and other personalisation.

I want OpenID. Lots of people run several pieces of web server software, your website and your blog? For Universities it's the website, student portal, staff portal, reading lists, virtual learning and so on. Having them all support one easy single sign-on solution would be great.

I want OpenID, as services start to underpin each other - [S3](http://aws.amazon.com/s3) underpins [SmugMug](http://www.smugmug.com/) and more on the way - I need a way for me to share an identity, safely, between the two things. I want to use my own S3 account to store my SmugMug photos.

I want OpenID. As an internet geek it just seems _wrong_ that the internet doesn't have an established, global, standard, federated&nbsp;authentication infrastructure. It _should_ have.

 <hr>  

So, if OpenID is still some time off and not right for everything, what do I do now?

I use [Password Safe](http://passwordsafe.sourceforge.net/), a free,&nbsp;open-source, password manager. It stores all my passwords in a strongly encrypted file. I keep the file on a USB fob on my keyring and make backups to an online drive. It generates strong passwords for me, stuff like _b?L&gt;Jqa\v%4gM99_ if you want to go that far or _i2KPpS5W_ by default. It has a nice little feature that you can use to key press into other applications, so it even logs me on via SSH and authenticates me on IRC.

It's an easier solution than OpenID and it's here now, but it doesn't solve all my problems.

 <div style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati tags: <a href="http://technorati.com/tags/OpenID" rel="tag">OpenID</a>, <a href="http://technorati.com/tags/Talis" rel="tag">Talis</a>, <a href="http://technorati.com/tags/PasswordSafe" rel="tag">PasswordSafe</a>, <a href="http://technorati.com/tags/Authentication" rel="tag">Authentication</a>, <a href="http://technorati.com/tags/InternetSecurity" rel="tag">InternetSecurity</a></div>
