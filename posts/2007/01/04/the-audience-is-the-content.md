---
layout: post.pug 
title: "the audience is the content..."
date: 2007-01-04
tags: ["post"]
legacy_wordpress_id: 140
---

About time...

Managing Editor of TIME Magazine, Richard Stengel, writes in [Now it's Your Turn](http://www.time.com/time/magazine/article/0,9171,1570743,00.html)&nbsp;about TIME's decision that 2006's person of the year is... YOU. They even have a nice shiny mirrored cover.

<!-- excerpt -->

But what's with the reflection of Richard Stengel in the cover mirror? How ironic.

TIME's site is sooooo 1999, no trackbacks, no comments, no links!

It's a great story about how TIME is acknowledging the world has changed, but a travesty of traditional journalism meets new media. I can read about Tom in Connecticut, but I can't get to his video on YouTube,&nbsp;Richard tells me there are lots of comments on his own video asking for nominations, but I can't click-through to those either! He tells me of a Baghdad mother with a video phone, but without the link I don't know if this is true, or just a piece journalistic imagination.

> Journalists once had the exclusive province of taking people to places they'd never been. 

They now hold the exclusive province of _failing to link to places people could go_.
