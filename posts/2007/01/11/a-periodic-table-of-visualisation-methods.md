---
layout: post.pug 
title: "A Periodic Table of Visualisation Methods"
date: 2007-01-11
tags: ["post"]
legacy_wordpress_id: 143
---

Compilation of visualization methods called “[A Periodic Table of Visualization Methods](http://www.visual-literacy.org/periodic_table/periodic_table.html).” Interesting idea and some nice examples. I find a lot of these are about simplifying very complex projects into something everyone around a table can agree on. Usually by removing a lot of the interesting bits.

[found](http://blog.guykawasaki.com/2007/01/the_art_of_visu.html) via Guy Kawasaki's blog "[How to Change The World](http://blog.guykawasaki.com/)"

<!-- excerpt -->

Guy's post also contains links to [this most beautiful and wonderful mind map](http://guykawasaki.typepad.com/Innovationgraphic.jpg) of a talk he gave...

 <div class="wlWriterSmartContent" id="0767317B-992E-4b12-91E0-4F059A8CECA8:5397c63b-7942-47af-aa20-bacb9f09b840" contenteditable="false" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati tags: <a href="http://technorati.com/tags/Visualization" rel="tag">Visualization</a>, <a href="http://technorati.com/tags/GuyKawasaki" rel="tag">GuyKawasaki</a>, <a href="http://technorati.com/tags/Graphics" rel="tag">Graphics</a>, <a href="http://technorati.com/tags/InformationVisualization" rel="tag">InformationVisualization</a>, <a href="http://technorati.com/tags/Information" rel="tag">Information</a>, <a href="http://technorati.com/tags/ManagementCharts" rel="tag">ManagementCharts</a></div>
