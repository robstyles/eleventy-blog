---
layout: post.pug 
title: "Five Things"
date: 2007-01-11
tags: ["post"]
legacy_wordpress_id: 144
---

Well, after saying "shouldn't that finish with 'if you don't tag five friends your mom'll get cooties' or something like that" I guess I more or less asked for it. I have been caught out by the rather ridiculous Five Things Meme, courtesy of [Ian (internet alchemy) D](http://iandavis.com/blog/2007/01/five-things).

Now, if this were email I sure as hell wouldn't do it, but as it'd my blog I guess I have to. Now, the big problem is I don't actually have five friends. So... Five Things:

<!-- excerpt -->

 <ol> <li>I don't actually have five friends, at least not that blog. Hey, when I started writing that I felt kind of down, but it's just occured to me I can be really proud - I don't have five friends _who blog_.  <li>I've been a Christian for the past five years. I'm not very good at it.  <li>I fight a constant internal battle between dong what's right and doing what would be funny.  <li>I named my youngest son after [Sam](http://www.beobal.com/)&nbsp;(only joking).  <li>My favourite tipple is South African red wine, usually Merlot.</li></ol> 

So here are my tags...

 <ol> <li>[Jingye](http://jingyeluo.blogspot.com/2007/01/five-things.html) (another Talisian who needs to re-start blogging)  <li>[Andy](http://pragmaticintegration.blogspot.com/2007/01/my-contribution-to-this-five-things-fad.html)&nbsp;(another Talisian who needs to re-start blogging)  <li>[Ivan](http://ivan.truemesh.com/archives/000665.html) (putting the Tea into Team)  <li>[Alan C Francis](http://blog.alancfrancis.com/2007/01/i_really__dont_.html) (for getting me into [[Grid::Fatherhood]](http://www.google.co.uk/search?hl=en&amp;q=%5Bgrid%3A%3Afatherhood%5D&amp;btnG=Google+Search) a few years ago)  <li>[Rachel Davies](http://twelve71.typepad.com/rachel/) (go on Rachel, tell us you used to be a nun or something)</li></ol>
