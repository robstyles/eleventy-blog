---
layout: post.pug 
title: "Adaptive Path, Talis and Joel"
date: 2007-01-31
tags: ["post"]
legacy_wordpress_id: 152
---

Good companies to work for are coming out of their shells more and more in the style of writing used in adverts. The search for the best people is hotting up again. We've been on the trail of talented people for a few years now at Talis and we have one or two...

Anyways, it was great to read [JJG's post looking for a visual designer](http://www.adaptivepath.com/blog/2007/01/25/adaptive-path-seeks-experienced-visual-designer/). They've thought a lot about what the language says about them and feels a lot like briefs I've written looking for "people who get bored easily".

<!-- excerpt -->

If you think you'd like working at Adaptive Path, but are wanting to stay closer to home... [We're looking for an interaction designer](http://jobs.joelonsoftware.com/default.asp?1298).
