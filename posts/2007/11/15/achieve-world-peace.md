---
layout: post.pug 
title: "achieve world peace"
date: 2007-11-15
tags: ["post","commands I have issued"]
legacy_wordpress_id: 204
---

svn st | grep "^C" | awk '{ print $2 }' | xargs svn resolved

<!-- technorati tags start --><p style="text-align:right;font-size:10px;">Technorati Tags: <a href="http://www.technorati.com/tag/command-line" rel="tag">command-line</a>, <a href="http://www.technorati.com/tag/subversion" rel="tag">subversion</a>, <a href="http://www.technorati.com/tag/svn" rel="tag">svn</a></p><!-- technorati tags end -->

<!-- excerpt -->
