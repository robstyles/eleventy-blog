---
layout: post.pug 
title: "get rid of things I know nothing about"
date: 2007-11-15
tags: ["post","commands I have issued"]
legacy_wordpress_id: 202
---

svn st --ignore-externals | grep "^?" | awk '{ print $2 }' | xargs rm -rf

<!-- technorati tags start --><p style="text-align:right;font-size:10px;">Technorati Tags: <a href="http://www.technorati.com/tag/bash" rel="tag">bash</a>, <a href="http://www.technorati.com/tag/command-line" rel="tag">command-line</a>, <a href="http://www.technorati.com/tag/svn" rel="tag">svn</a>, <a href="http://www.technorati.com/tag/subversion" rel="tag">subversion</a></p><!-- technorati tags end -->

<!-- excerpt -->
