---
layout: post.pug 
title: "Larry Lessig: Open For Business"
date: 2007-11-15
tags: ["post","Internet Social Impact"]
legacy_wordpress_id: 203
---

In a classic [Lawrence Lessig presentation](http://www.ted.com/talks/view/id/187), this time at [TED](http://www.ted.com/), he explains why we must stop criminalizing our kids.

found via: [Powell, Andy and Johnston, Pete: Strangling creativity](http://efoundations.typepad.com/efoundations/2007/11/strangling-crea.html)

<!-- excerpt -->

<!-- technorati tags start --><p style="text-align:right;font-size:10px;">Technorati Tags: <a href="http://www.technorati.com/tag/copyright" rel="tag">copyright</a>, <a href="http://www.technorati.com/tag/law" rel="tag">law</a>, <a href="http://www.technorati.com/tag/licensing" rel="tag">licensing</a>, <a href="http://www.technorati.com/tag/lawrencelessig" rel="tag">lawrencelessig</a>, <a href="http://www.technorati.com/tag/open-content" rel="tag">open-content</a>, <a href="http://www.technorati.com/tag/remixing" rel="tag">remixing</a></p><!-- technorati tags end -->
