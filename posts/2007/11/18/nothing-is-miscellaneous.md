---
layout: post.pug 
title: "Nothing is Miscellaneous"
date: 2007-11-18
tags: ["post","Internet Social Impact"]
legacy_wordpress_id: 207
---

A few months back I read David Weinberger's _[Everything is Miscellaneous](http://www.everythingismiscellaneous.com/)_. After his contribution to _[The Cluetrain Manifesto](http://www.cluetrain.com/book/index.html)_ back in 1999 my expectations were high. Mine higher than most, perhaps, as the book is dedicated "to the librarians" and I work for a company with several decades of heritage in library systems.

I already knew I liked David's style of writing from Cluetrain, so I was glad to hear that same voice coming through loud and clear.

<!-- excerpt -->

By chapter three we're into library history; Dewey, Ranganathan, Carlyle and Panizzi all get a mention, but Dewey takes the brunt of the attack on geographies of knowledge. It would be easy to take Weinberger's text as an attack on libraries specifically and the organization of knowledge generally, and many have, but it appears to me these are only used as examples of the limitations we face when organizing anything made of atoms.

And that really forms the nub of this book, that digital media can be organized differently for different purposes at different times - lifting a major limitation of the physical world. I wrote in a similar vein earlier this year when I tried to explain [why virtual worlds suck](/2006/12/22/why-virtual-worlds-suck). In that I tried to show the difference between approaches to search in Second Life and approaches to search on any web search engine.

This is touched on briefly in a Second Life book club review of EiM where it was said:

> [18:23]  Teofila Matova: chaos leads to order
> <br />[18:23]  Stolvano Barbosa: yes
> <br />[18:23]  Teofila Matova: lets think of the librarian with all the books on the floor

Who in their right mind would suggest all the books in the library sitting in one "miscellaneous" pile on the floor? Surely Weinberger is mad. Or evil? Maybe he's _trying to destroy knowledge_!

But hold on there - a pile on the floor... isn't that essentially what automated archives are? [Large robot-managed warehouses like the National Library of Norway](http://www.ifla.org/VI/4/news/ipnn37.pdf) sort the books by size, paper type, frequency of access; anything that makes the storage facility more efficient rather than a map of human knowledge. Of course, where each document lives is noted and cross-referenced with title, author, subject and so on. 'Where it lives', like an address or a Resource Locator... If they were all the same they'd be Uniform Resource Locators or URLs.

That's what Weinberger's getting at, that things can have an address independent of any taxonomy. Not only that, but by giving something an address or identity that is not simply it's position in a taxonomy then you can cross-reference the same items in several different taxonomies at the same time and add more as and when you need them.

Which brings me to where I take issue with everything being miscellaneous; to explain let me take a little detour. I've spent many years trying to learn what makes code (programming that is, Java, C++, you know the stuff) better. One of the things that I've concluded about programming in OO languages is that there are a few terms that smell bad - 'utils' is one of my favorites. 'Utils' almost always means 'the things that don't it in the hierarchy of my code' and that usually means the hierarchy is wrong. The same goes for any taxonomy where things don't really fit and end up tagged in that 'miscellaneous' section on the end.

The things that end up in the 'misc' section are the things that weren't thought about or weren't really cared about or weren't really understood in the design of the structure of the knowledge; Dewey 297, Islam, Bahai and Babism - things that aren't Christian... Their importance and differences from each other hidden by the understanding set like concrete in the classification.

But somebody always cares about those things that end up in 'misc', probably deeply and in a way that they could classify in detail. Take a friend of mine doing a PhD analyzing the differences in scholarly texts that have  been cited a lot and those that have been only cited once. You should try finding a way to search for those scholarly texts that have been relegated to the bottom of the heap.

That is what the third order of order, as Weinberger coins it, allows us to do, for ourselves... Leading me to believe that the book should be called Nothing is Miscellaneous.

<!-- technorati tags start --><p style="text-align:right;font-size:10px;">Technorati Tags: <a href="http://www.technorati.com/tag/EiM" rel="tag">EiM</a>, <a href="http://www.technorati.com/tag/Everything is Miscellaneous" rel="tag">Everything is Miscellaneous</a>, <a href="http://www.technorati.com/tag/David Weinberger" rel="tag">David Weinberger</a>, <a href="http://www.technorati.com/tag/Libraries" rel="tag">Libraries</a>, <a href="http://www.technorati.com/tag/Semantic Web" rel="tag">Semantic Web</a></p><!-- technorati tags end -->
