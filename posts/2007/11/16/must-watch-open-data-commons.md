---
layout: post.pug 
title: "Must Watch: Open Data Commons"
date: 2007-11-16
tags: ["post","Working at Talis"]
legacy_wordpress_id: 205
---

If you're not watching what's happening on the Open Data Commons work we started last year, you should be - go, go and look now.

[Open Data Commons brief update](http://feeds.feedburner.com/~r/opencontentlawyer/~3/185661126/)

<!-- excerpt -->

I know what Jordan's not able to say and I am sooooooo excited.

<!-- technorati tags start --><p style="text-align:right;font-size:10px;">Technorati Tags: <a href="http://www.technorati.com/tag/copyright" rel="tag">copyright</a>, <a href="http://www.technorati.com/tag/internet social impact" rel="tag">internet social impact</a>, <a href="http://www.technorati.com/tag/law" rel="tag">law</a>, <a href="http://www.technorati.com/tag/licensing" rel="tag">licensing</a>, <a href="http://www.technorati.com/tag/open-data" rel="tag">open-data</a>, <a href="http://www.technorati.com/tag/opendatacommons" rel="tag">opendatacommons</a></p><!-- technorati tags end -->
