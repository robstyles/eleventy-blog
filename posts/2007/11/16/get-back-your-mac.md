---
layout: post.pug 
title: "get back your mac"
date: 2007-11-16
tags: ["post","Other Technical"]
legacy_wordpress_id: 206
---

Based on a script from here: http://blogs.ittoolbox.com/security/investigator/archives/stolen-machines-phone-home-10506

this now lives in /usr/bin/ipkeyb file

<!-- excerpt -->

```

#!/usr/bin/perl
# Report to a webserver (for tracking in the log as a 404) where our Macintosh is.

# Keep trying forever
while (1) {

# Wait 2 minutes for networks etc to attach
sleep 120;

# Do we have a network?
$network = `ifconfig -a inet 2>/dev/null | sed -n -e '/127.0.0.1/d' -e '/0.0.0.0/d' -e '/tunnel/d' -e '/inet/p' | wc -l`;
#print("network: $network\n");

# Carve out serial number information from system profiler
$serial_number1 = `system_profiler 2> /dev/null | grep \"Serial Number\"`;

# We want the second instance of serial number in our URL string
@serial_number2 = split (' ', $serial_number1);
#print("serial: $serial_number2[2]\n");

$url = "http://www.CHANGE_ME.com/ipkeyb/$serial_number2[2].html";
#print("url: $url\n");

# Let's identify
$useragent = "Where Am I (Mac OS X)";

# Okay, if we have network - make the request to the webserver
if ( $network > 0 ) {
#print("sending: $url");
$status = `curl -A \"$useragent\" $url`;
}

# Wait 3 hours before we try again
sleep 10800;
}

```

and has an entry in /etc/rc.local

<!-- technorati tags start --><p style="text-align:right;font-size:10px;">Technorati Tags: <a href="http://www.technorati.com/tag/OSX" rel="tag">OSX</a>, <a href="http://www.technorati.com/tag/"stolen laptop recovery"" rel="tag">"stolen laptop recovery"</a></p><!-- technorati tags end -->
