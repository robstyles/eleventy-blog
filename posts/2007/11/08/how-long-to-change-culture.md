---
layout: post.pug 
title: "How long to change culture"
date: 2007-11-08
tags: ["post","Software Engineering","Working at Talis"]
legacy_wordpress_id: 199
---

Over on Tiny Drops of Knowledge (the name belies the depth of the content) [Lyndsay posted about changing culture](http://tdoks.blogspot.com/2007/09/how-long-to-change-culture.html) - in his case being part of a team moving from VB to C#.

At Talis we find we're constantly changing, and have been for longer than my three years here. At Talis that change has been about talent, responsibility, empowerment, achievement, learning, fun and real values.

<!-- excerpt -->

Lyndsay says:

> A reoccurring challenge we've faced on this project is re-learning how to write software. Its more than just learning a new language, tool or framework, its more like loosing the shackles of apartheid.

Over the past few years I've concluded that this is the normal state of affairs. When I first started work I was given some great advice - my boss told me, when I handed my notice in, to make sure that wherever I went I wasn't the smartest person there. What he was getting at is the need in me, and many of the people I count as my closest friends, to be constantly finding out new things. I thought nothing more of it at the time.

But then, working with folks at Talis I realised the very real truth that "the more I learn the less I know". And I'm okay with that. Not everyone is, but I think it's key to being as productive as you can be. The change Lyndsay is talking about may be the change from writing instructions for the computer to modeling a problem for yourself and your colleagues. Once you've made that leap it makes sense to understand as many different ways of modeling problems as you can - at work we've been through OO design in Java through procedural code in PHP and functional code in XSLT to discussions of the similarities between Prolog and how we initially perceive how we might work with RDF.

There'll be something else tomorrow. I know there will because I work with a load of people who are smarter than I am.

<!-- technorati tags start --><p style="text-align:right;font-size:10px;">Technorati Tags: <a href="http://www.technorati.com/tag/software engineering" rel="tag">software engineering</a>, <a href="http://www.technorati.com/tag/talis" rel="tag">talis</a></p><!-- technorati tags end -->
