---
layout: post.pug 
title: "Amazon.com: Kindle: Amazon's New Wireless Reading Device: Kindle Store"
date: 2007-11-19
tags: ["post","New Toy"]
legacy_wordpress_id: 209
---

[Amazon.com: Kindle: Amazon's New Wireless Reading Device: Kindle Store](http://www.amazon.com/gp/product/B000FI73MA/ref=amb_link_5873612_3?pf_rd_m=ATVPDKIKX0DER&pf_rd_s=gateway-center-column&pf_rd_r=1YS8JS952V5P29XR8EST&pf_rd_t=101&pf_rd_p=329252801&pf_rd_i=507846):

Will Kindle change the game? I'm not sure, it's not open for you buy books from anywhere, uploading your own stuff to it requires emailing it via Amazon and it's far from iPhone in its looks.

<!-- excerpt -->

But... It is a hi-res hi-contrast display with what sounds like excellent battery life and widely available network access. It just may hit the sweet-spot for a lot of people. Or it might not.

<!-- technorati tags start --><p style="text-align:right;font-size:10px;">Technorati Tags: <a href="http://www.technorati.com/tag/electronic paper" rel="tag">electronic paper</a>, <a href="http://www.technorati.com/tag/Kindle" rel="tag">Kindle</a>, <a href="http://www.technorati.com/tag/ereader" rel="tag">ereader</a>, <a href="http://www.technorati.com/tag/ebook reader" rel="tag">ebook reader</a>, <a href="http://www.technorati.com/tag/ebook" rel="tag">ebook</a>, <a href="http://www.technorati.com/tag/Amazon" rel="tag">Amazon</a></p><!-- technorati tags end -->
