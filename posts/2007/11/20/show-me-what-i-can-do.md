---
layout: post.pug 
title: "Show me what I can do"
date: 2007-11-20
tags: ["post","commands I have issued"]
legacy_wordpress_id: 211
---

ls -l | grep "^[^d]..x" | awk '{ print $8 }' | sort

<!-- technorati tags start --><p style="text-align:right;font-size:10px;">Technorati Tags: <a href="http://www.technorati.com/tag/command-line" rel="tag">command-line</a>, <a href="http://www.technorati.com/tag/bash" rel="tag">bash</a></p><!-- technorati tags end -->

<!-- excerpt -->
