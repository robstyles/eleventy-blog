---
layout: post.pug 
title: "Schneider, Karen G: Kindle doesn’t light my fire"
date: 2007-11-20
tags: ["post","Library Tech"]
legacy_wordpress_id: 210
---

[Schneider, Karen G: Kindle doesn’t light my fire](http://feeds.feedburner.com/~r/freerangelibrarian/~3/187489022/):

I suspect the Kindle doesn't hit the high notes after all...

<!-- excerpt -->

<!-- technorati tags start --><p style="text-align:right;font-size:10px;">Technorati Tags: <a href="http://www.technorati.com/tag/Amazon" rel="tag">Amazon</a>, <a href="http://www.technorati.com/tag/ebook" rel="tag">ebook</a>, <a href="http://www.technorati.com/tag/ebook reader" rel="tag">ebook reader</a>, <a href="http://www.technorati.com/tag/Kindle" rel="tag">Kindle</a></p><!-- technorati tags end -->
