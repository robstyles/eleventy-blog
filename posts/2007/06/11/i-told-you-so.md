---
layout: post.pug 
title: "I told you so."
date: 2007-06-11
tags: ["post","Interaction Design"]
legacy_wordpress_id: 188
---

A few folks around the office and in the pub have been a bit skeptical about how fast we're going to have really big touchable displays.

Well, [here's a really big touchable display](http://blog.lookorfeel.com/index.php/2007/06/04/hp-multi-touch-interactive-canvas-launched-at-d5/).

<!-- excerpt -->

 <div class="wlWriterSmartContent" id="0767317B-992E-4b12-91E0-4F059A8CECA8:fd8b04ac-7dda-4c0a-a2b2-8cf5c2692ce4" contenteditable="false" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati tags: <a href="http://technorati.com/tags/Gesture%20Interface" rel="tag">Gesture Interface</a>, <a href="http://technorati.com/tags/Interface%20Design" rel="tag">Interface Design</a>, <a href="http://technorati.com/tags/Interaction%20Design" rel="tag">Interaction Design</a>, <a href="http://technorati.com/tags/Multi-Touch%20Interface" rel="tag">Multi-Touch Interface</a>, <a href="http://technorati.com/tags/Physical%20World%20Hyperlinks" rel="tag">Physical World Hyperlinks</a></div>
