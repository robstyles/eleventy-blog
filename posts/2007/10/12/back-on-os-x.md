---
layout: post.pug 
title: "Back on OS X"
date: 2007-10-12
tags: ["post","Other Technical"]
legacy_wordpress_id: 190
---

I've been head down for a while on work things, doing a whole load of data munging as well as the usual dev work. But my Mac went pop a couple of weeks ago and Apple decided the best thing was to replace it rather than fix it; fine by me. It seemed like a good opportunity to look at what I have installed and list what's on my machine and why:

<span style="font-size:13pt;">__[iWork 08](http://www.apple.com/iwork/)__</span>

<!-- excerpt -->

Makes work life so much easier than with Office. Keynote and Pages are a joy to work with on the odd occasion where I have to write something other than code.

<span style="font-size:13pt;">__[Firefox](http://www.mozilla.com/firefox/)__</span>

I know lots of mac users insist on using Safari and I agree with them that Safari's a great browser, but the extensions for Firefox are too useful, and we have one or two internally that help a lot. Firefox has to be the default. Extensions that go on straight away are: [Web Developer](https://addons.mozilla.org/en-US/firefox/addon/60); [Firebug](https://addons.mozilla.org/en-US/firefox/addon/1843); [Duplicate Tab](https://addons.mozilla.org/en-US/firefox/addon/28); [Download Statusbar](https://addons.mozilla.org/en-US/firefox/addon/26); [Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/748); [del.icio.us Bookmarks](https://addons.mozilla.org/en-US/firefox/addon/3615); and [Resizeable Textarea](https://addons.mozilla.org/en-US/firefox/addon/3818).

<span style="font-size:13pt;">__[Transmission](http://transmission.m0k.org/)__</span>

Very simple torrent client that seems to behave itself nicely.

<span style="font-size:13pt;">__[Sun Java 5](http://java.sun.com/javase/downloads/index_jdk5.jsp)__</span>

Got to have the real deal installed and running. The standard one shipping with OS X seems fine too.

<span style="font-size:13pt;">__[Eclipse PDT](http://www.eclipse.org/pdt/)__</span>

Much of what I do is a mix of Java and PHP right now. A departure from a few years ago. Eclipse PDT works really nicely. I'd rather be using Coda for the markup, but can't justify it right now.

<span style="font-size:13pt;">__[Subversion](http://subversion.tigris.org/)__</span>

The slickest source repository software I've ever worked with. Simple, fast and elegant.

<span style="font-size:13pt;">__[Colloquy](http://colloquy.info/)__</span>

We use IRC a lot to keep in-touch and ask quick questions, this is a great client, with customizable alerts and the ability to put in a sequence of auto-commands for when you connect to a server.

<span style="font-size:13pt;">__[Adium](http://www.adiumx.com/)__</span>

The best multi-network IM client I've ever used.

<span style="font-size:13pt;">__[Skype](http://www.skype.com/)__</span>

Of course. Phone home.

<span style="font-size:13pt;">__[Twitterific](http://iconfactory.com/software/twitterrific)__</span>

I said a while ago [I wasn't going to twitter any more](/2007/03/14/twitter-no-more). I was too hasty. When I moved over to the mac someone mailed me twitterific and it makes Twitter useful.

<span style="font-size:13pt;">__[Skitch](http://plasq.com/skitch)__</span>

Skitch is great - grab bits of screenshots, annotate and drop into emails, doc or post to their online service. Simple idea executed really, really well.

<span style="font-size:13pt;">__[Password Gorilla](http://www.fpx.de/fp/Software/Gorilla/)__</span>

I've been using Password Safe for years, but moving to Linux and Mac I needed something else. Pasword Gorilla is compatible with Pasword Safe, so I can just move my password files from machine to machine easily and securely.

<span style="font-size:13pt;">__[Mac The Ripper](http://www.mactheripper.org/)__</span>

Rips DVD images onto your disc, allowing them to be played by DVD Player while the disc stays at home. The other advantage is that the hard-drive uses loads less power, so you can watch at least a whole movie while on a flight - on one battery.

<span style="font-size:13pt;">__[EasyWMA](http://www.easywma.com/)__</span>

This great little tool takes a whole load of WMA files and converts them to MP3 and registers them with iTunes. A painless way to migrate from WMP.

<span style="font-size:13pt;">__[VMWare Fusion](http://www.vmware.com/products/fusion/)__</span>

I run XP very occasionally and Ubuntu quite often for testing under different OSs. Very handy. I sometimes develop under Ubuntu too, as Fusion can take snapshots I can play easily without wrecking my machine.

<span style="font-size:13pt;">__[Cisco VPN Client](http://www.versiontracker.com/dyn/moreinfo/macosx/12696)__</span><span style="font-size:13pt;">__ and __</span><span style="font-size:13pt;">__[Shimo](http://www.nexumoja.org/projects/Shimo/)__</span>

Connect to work via a Cisco VPN, nice and easy, fast and reliable from pretty much anywhere. Shimo sits in the menu bar allowing quick connections without having to open the cisco client up.

<span style="font-size:13pt;">__[Macports](http://www.macports.org/)__</span>

Open-Source project to make linux open-source projects available to OS X. Equivalent to apt-get or yum package managers. The folks behind this do a great job of keeping the builds up-to-date and providing repositories. There's [Fink](http://finkproject.org/) as well, and I've tried both. I found MacPorts better, but if I'm wrong please tell me!

<span style="font-size:13pt;">__[Vienna](http://www.opencommunity.co.uk/vienna2.php)__</span>

When I moved over to Mac I very nearly bought NetNewsWire for blog reading. Then I found Vienna; an open-source blog reader that is really good. On of the key things is the way it opens articles into tabs, keeping the feed handy when you've finished.

<span style="font-size:13pt;">__[Ecto](http://infinite-sushi.com/software/ecto/)__</span>

Not free, but worth the 11GBP it cost me. This is a great little offline blog editor. Hopefully might help me get a little more written here.

<span style="font-size:13pt;">__[Kismac](http://kismac-ng.org/)__</span>

Some people may have policy issues with this tool - it's a wireless network discovery tool that also allows you to crack WEP and WPA keys. I've used it to secure my own network, but I also use it to find open hotspots when I'm out and about. It's been moving about a bit, so if the link's broken then let me know. It was hosted from a site run by it's creator Michael Rossberg, but since a change to German law outlaws this tool he has handed it on.

<span style="font-size:13pt;">__[Desktop Manager](http://desktopmanager.berlios.de/)__</span>

I don't understand why OS X doesn't have multiple desktops built-in, but as 10.4.10 it doesn't. This is the nicest of the desktop managers I found. Also works with Smackbook if you're so inclined.

<span style="font-size:13pt;">__[Stuffit Expander](http://www.stuffit.com/mac/expander/download.html)__</span>

This used to be distributed as part of OS X apparently. But Smith Micro insist on you getting it from them now. Part of the process is giving them your email address; which they then spam until you tell them to stop. Useful piece of software, as stuff still comes in .sit form, but annoying model employed by Smith. They should read [cluetrain](http://www.cluetrain.com/).

<span style="font-size:13pt;">__[Creatures](http://www.apple.com/downloads/macosx/icons_screensavers/creaturesicons.html)__</span><span style="font-size:13pt;">__ and __</span><span style="font-size:13pt;">__[Creatures 2](http://www.apple.com/downloads/macosx/icons_screensavers/creaturesiconsvol2.html)__</span>

Finally - a little fun. The Creatures icons from Fast Icon are lovely and adorn my most useful folders.

<!-- technorati tags start --><p style="text-align:right;font-size:10px;">Technorati Tags: <a href="http://www.technorati.com/tag/OS X" rel="tag">OS X</a>, <a href="http://www.technorati.com/tag/recommended" rel="tag">recommended</a>, <a href="http://www.technorati.com/tag/tools" rel="tag">tools</a></p><!-- technorati tags end -->
