---
layout: post.pug 
title: "Cause and Effect"
date: 2007-10-16
tags: ["post","Software Engineering","Software Business"]
legacy_wordpress_id: 193
---

For those reading outside of England...

We've just had the first run on a bank in 150 years - Northern Rock has been forced to borrow billions of pounds from the Bank of England. It's been really interesting to watch.

<!-- excerpt -->

But what's really interesting is the discussion of blame. Here are the reasons why the Northern Rock went down according to their senior management:

<ul>
<li>The Bank of England refused an earlier rescue loan</li>
<li>Another major bank backed out of buying/bailing them out</li>
<li>The wholesale money markets closed</li>
<li>The BBC announced that they needed an emergency loan before they had an official comment ready</li>
</ul>

What's notable about these are that they are not the reason Northern Rock went down. That's plain and simple that they had an imbalance between how much money they had in deposits and how much they had tied up in non-liquid assets. This happened because there was an imbalance in priorities throughout Northern Rock culture that led to them being great at lending money and not as focussed on bringing it in.

The reason I find this interesting is that it ties in with other thoughts I've been having about root causes.

The things that the Northern Rock managers are bringing up in their defense are all things that didn't help, and maybe if some or all of them had been different there wouldn't have been a run on the bank - maybe.

We've been talking at work about why nobody seems to really effectively achieve code re-use and several reasons come up again and again. Deadline; too hard; component not good enough to re-use and so on.

But, as with Northern Rock, I think the main reason nobody really effectively achieves code re-use is because there's an imbalance in priorities throughout our industry that leads us to be great at producing new functionality and not as focussed on sharing capabilities.

<!-- technorati tags start --><p style="text-align:right;font-size:10px;">Technorati Tags: <a href="http://www.technorati.com/tag/code reuse" rel="tag">code reuse</a>, <a href="http://www.technorati.com/tag/software engineering" rel="tag">software engineering</a></p><!-- technorati tags end -->
