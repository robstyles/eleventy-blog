---
layout: post.pug 
title: "Open Data Licensing"
date: 2007-10-16
tags: ["post","Internet Social Impact","Software Business"]
legacy_wordpress_id: 192
---

Back at the end of September we finally got to the point of releasing the [first draft of the Open Data Commons License](http://www.opencontentlawyer.com/2007/09/24/open-data-commons-licence-now-out/). This is work I've been involved in since Ian's first draft of the TCL about a year and a half ago.

It's great to see this license come to fruition, having [argued about the need for this](http://blogs.talis.com/nodalities/2007/07/open_data_licensing_an_unnatur.php) more than once.

<!-- excerpt -->

It's interesting to see the conversation happening around [LibraryThing's Common Knowledge](http://www.librarything.com/blog/2007/10/common-knowledge-social-cataloging.php) and the [Open Library](http://demo.openlibrary.org/) project. Both of these are collections of factual data, I've been speaking to people involved in both and both have a clear desire to protect the data and ensure that it's available for the community into the future.

Licensing is critical to that - as I said in Banff ([listen](http://dannyayers.com/audio/www2007/rob-styles)) at the start of the year.

Back then we were concerned with navigating the difference in protection afforded to database in the EU and the US. In essence, databases have protection in the EU, but have no protection on the US. The reason we were looking at that was because the natural thinking goes something like this:

<p style="text-indent:20pt;">
Creative Commons extends Copyright to allow you to easily position yourself on the spectrum of 'All Rghts reserved' to 'Public Domain'.
</p><p style="text-indent:20pt;">
Therefore Open Data Commons must need to extend a Database Right to allow to position your data on the same spectrum.
</p>

Well, the Open Data Commons license gets around that by being couched in contract law. This seems like a great way to license data for open use and prevent it being locked away in future.

With all that's been going on then, it's no surprise that I missed the [Model Train Software case](http://lawandlifesiliconvalley.blogspot.com/2007/08/new-open-source-legal-decision-jacobsen.html) that could have a big impact on how Open-Source software licenses are drafted. A San Francisco judge ruled that the Artistic License was a contract - meaning that breach of the license did not necessarily mean infringing the copyright. That changes the legal redress and potential penalties available for breaching a license.

Interesting.

<!-- technorati tags start --><p style="text-align:right;font-size:10px;">Technorati Tags: <a href="http://www.technorati.com/tag/licensing" rel="tag">licensing</a>, <a href="http://www.technorati.com/tag/open-data" rel="tag">open-data</a>, <a href="http://www.technorati.com/tag/open-source" rel="tag">open-source</a>, <a href="http://www.technorati.com/tag/opendatacommons" rel="tag">opendatacommons</a>, <a href="http://www.technorati.com/tag/law" rel="tag">law</a></p><!-- technorati tags end -->
