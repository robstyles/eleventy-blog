---
layout: post.pug 
title: "Lost + Found"
date: 2007-10-16
tags: ["post","Random Thought"]
legacy_wordpress_id: 191
---

a while back, when I switched machines, I lost my feedreader - and all the blogs I was following with it. ho hum, a great opportunity to find out which I missed and to find new ones :-&gt;

Just discovered I miss [Tinfoil + Raccoon](http://rochellejustrochelle.typepad.com/). Paul and I caught up with Rochelle in the OCLC bloggers salon at ALA this summer. Much beer was consumed.

<!-- excerpt -->

Anyhow, I found her again a few weeks ago and then realised today that [she is certifiable](http://rochellejustrochelle.typepad.com/copilot/2007/10/remington-quiet.html). I mean, a typewriter? Unless you're expecting imminent doom from some kind of electro-magnetic pulse or you're a museum curator what on earth would you want one for? Even for $5 - you could have bought yourself a nice sandwich for that!

<!-- technorati tags start --><p style="text-align:right;font-size:10px;">Technorati Tags: <a href="http://www.technorati.com/tag/raccoon" rel="tag">raccoon</a>, <a href="http://www.technorati.com/tag/rochelle" rel="tag">rochelle</a>, <a href="http://www.technorati.com/tag/tinfoil" rel="tag">tinfoil</a></p><!-- technorati tags end -->
