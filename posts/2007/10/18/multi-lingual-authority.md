---
layout: post.pug 
title: "Multi-lingual Authority"
date: 2007-10-18
tags: ["post","Library Tech"]
legacy_wordpress_id: 196
---

Over at hangingtogether.org [Karen notes](http://hangingtogether.org/?p=286) that the CPSO  at Library of Congress have announced that:

> The major authority record exchange partners (British Library, Library of Congress, National Library of Medicine, and OCLC, Inc., in consultation with Library and Archives Canada) have agreed to a basic outline that will allow for the addition of references with non-Latin characters to name authority records that make up the LC/NACO Authority File.

<!-- excerpt -->

This is a great step, the LC/NACO authority files form a rich web of data that can be used to improve many things, including search as Karen mentions.

The only question is... why has it taken so long to accept non-latin characters in library land?

<!-- technorati tags start --><p style="text-align:right;font-size:10px;">Technorati Tags: <a href="http://www.technorati.com/tag/library" rel="tag">library</a>, <a href="http://www.technorati.com/tag/multi-lingual" rel="tag">multi-lingual</a></p><!-- technorati tags end -->
