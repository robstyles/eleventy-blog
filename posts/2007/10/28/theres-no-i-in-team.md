---
layout: post.pug 
title: "There's no I in Team"
date: 2007-10-28
tags: ["post","Software Engineering","Working at Talis"]
legacy_wordpress_id: 198
---

Back in June on The Berkun Blog, [Scott's talked about Asshole-Driven Development](http://www.scottberkun.com/blog/2007/asshole-driven-development/), and other great techniques for the dysfunctional office. He states clearly that his list is cynical, and that there is probably a happy list as well...

Well, I figured I'd take a pop at a happier list...

<!-- excerpt -->

First up, let's have:

<span style="font-size:13pt;">__Motivated and Empowered Individual method (ME,I)__</span>

This is how I'd describe the way Joel Spolsky has set up the guys at [Fog Creek](http://www.fogcreek.com/). Essentially the team breaks the solution down into parts and gives a part to a person. Each person is free to develop in their own way, within some bounds set by the team, and becomes the owner of an area of functionality. Without the distractions of other people working on the same code areas the owner can become very productive within the bounds of the code they own. Joel describes the people he hires as "[Smart and Gets Things Done](http://www.joelonsoftware.com/items/2007/06/05.html)", he wrote a book of the same name.

<span style="font-size:13pt;">__Smart Friends Development Model (SFDM)__</span>

I spotted this one at XTech in Paris earlier this year. There I met three smart friends who, in their spare-time, had developed [Quakr](http://www.quakr.co.uk/). Friendship in a development team provides a real boost to the way the team communicates and negotiates decisions and issues. In the case of Quakr they were friends first and decided to build Quakr second, but I've seen teams formed by other companies where effort has been put in to building great friendships.

<span style="font-size:13pt;">__Very-Clever and Nice People (VCNP)__</span>

Martin Fowler of Thoughtworks is open about trying to hire only the very best people. The main barrier to growing Thoughtworks is finding and hiring that talent. Once hired, they move people around, making sure they get to know all the other very clever people they've hired. Being clever isn't enough though, they also looking for soft skills; they hire nice people. The end result is that they can form teams who can work at a very high level and have a lot of fun sharing ideas and helping each other. This is essentially what Microsoft did in the early days too and how they came to have the Program Manager role. Comments over at Scott's piece talk about [responsibility without authority](http://www.scottberkun.com/blog/2007/asshole-driven-development/#comment-147805) in a very negative way, but if you have very clever and nice people this can clearly work and Thoughtworks show this with their teams.

<span style="font-size:13pt;">__Smart and Nice Entrepreneurs (SANE)__</span>

Back at [Talis](http://www.talis.com) we also hire smart people. We also try very hard to make sure they're nice too. We think we're all pretty nice really. But there's also a key self-motivational quality we look for; the ability to understand and be interested in how the software will make someone's life better, as well as how clean the code is under the bonnet. We think that combination is what's helping us develop some really great stuff and have fun doing it.

It saddens me to read posts like Scott's and the subsequent comments. I've had bad experiences with employers and managers who seem to have different motivations and values to mine, and I know from friends around the industry how prevalent the problems Scott and his commentors talk about are. Surely the best thing to do is to find somewhere worth working and move, or as [Martin Fowler apparently said "If you can't change your organization, change your organization!"](http://martinfowler.com/articles/xp2000.html#id2249808)

I had hoped to get to more than three happier methodologies. Perhaps that's a sign that the cynics are right.

<!-- technorati tags start --><p style="text-align:right;font-size:10px;">Technorati Tags: <a href="http://www.technorati.com/tag/software engineering" rel="tag">software engineering</a>, <a href="http://www.technorati.com/tag/talis" rel="tag">talis</a></p><!-- technorati tags end -->
