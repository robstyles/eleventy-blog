---
layout: post.pug 
title: "Free Speech"
date: 2007-10-17
tags: ["post","Working at Talis"]
legacy_wordpress_id: 195
---

Well, several free speeches actually. At least they are if you register and come to Birmingham...  Insight - A Library Conference for All  Insight is a free two day conference for anyone interested and involved in the future of libraries. Being UK-based we've set it close to our home in Birmingham.

The programme has some great names, headed up with a keynote by [Euan Semple](http://www.talis.com/applications/news_and_events/talis_insight_speakers.shtml#no001). Other include:

<!-- excerpt -->

<ul>
<li>[Tony Hey](http://www.talis.com/applications/news_and_events/talis_insight_speakers.shtml#no002), Microsoft - eResearch and Digital Scholarship</li>
<li>[Nicole Harris](http://www.talis.com/applications/news_and_events/talis_insight_speakers.shtml#no020) Senior Services Transition Manager, JISC</li>
<li>[Richard Cameron](http://www.talis.com/applications/news_and_events/talis_insight_speakers.shtml#no014) CiteULike</li>
<li>[Dave Pattern](http://www.talis.com/applications/news_and_events/talis_insight_speakers.shtml#no009) University of Huddersfield</li>
<li>[David Orrell](http://www.talis.com/applications/news_and_events/talis_insight_speakers.shtml#no013)&#38;nbsp;Eduserv</li>
<li>[Marshall Breeding](http://www.talis.com/applications/news_and_events/talis_insight_speakers.shtml#no018) Vanderbilt University, USA</li>
<li>[Roy Clare](http://www.talis.com/applications/news_and_events/talis_insight_speakers.shtml#no022), CBE, Chief Executive of the MLA</li>
<li>[Frances Hendrix](http://www.talis.com/applications/news_and_events/talis_insight_speakers.shtml#no016) and a panel of future librarians</li>
<li>[David Lightfoot](http://www.talis.com/applications/news_and_events/talis_insight_speakers.shtml#no028)&#38;nbsp;Lancashire Libraries</li>
<li>[Tim Coates](http://www.talis.com/applications/news_and_events/talis_insight_speakers.shtml#no012) Author and Bookseller</li>
<li>[Tony Durcan](http://www.talis.com/applications/news_and_events/talis_insight_speakers.shtml#no026) President, Society of Chief Librarians</li>
<li>[Zoinul Abidin](http://www.talis.com/applications/news_and_events/talis_insight_speakers.shtml#no017) Idea Store Manager, Tower Hamlets</li>
<li>[John Dolan](http://www.talis.com/applications/news_and_events/talis_insight_speakers.shtml#no003) Head of Library Policy at the MLA</li>
</ul>

Doing great stuff like this is why so many of us are at Talis. This is going to be a great conference.

<!-- technorati tags start --><p style="text-align:right;font-size:10px;">Technorati Tags: <a href="http://www.technorati.com/tag/birmingham" rel="tag">birmingham</a>, <a href="http://www.technorati.com/tag/conference" rel="tag">conference</a>, <a href="http://www.technorati.com/tag/library" rel="tag">library</a>, <a href="http://www.technorati.com/tag/future" rel="tag">future</a>, <a href="http://www.technorati.com/tag/insight" rel="tag">insight</a>, <a href="http://www.technorati.com/tag/talis" rel="tag">talis</a></p><!-- technorati tags end -->
