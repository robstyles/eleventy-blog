---
layout: post.pug 
title: "Names Names Names"
date: 2007-10-19
tags: ["post","Library Tech"]
legacy_wordpress_id: 197
---

Over at OCLC, [Thom](http://outgoing.typepad.com/outgoing/) and his team are doing work to [match names across several international Name Authorities](http://orlabs.oclc.org/viaf/). This comes after the recent announcement about [allowing non-latin characters into the LC/NACO Name Authority](/2007/10/18/multi-lingual-authority).

This is great work and ties up somewhat with threads I've been thinking about following discussions on other lists.

<!-- excerpt -->

Firstly, Nicole brings together [thoughts from Tim Spalding with blog comment on the RDA drafts](http://feeds.feedburner.com/~r/web2learning/YOVk/~3/171669761/1287). This challenges the notion that controlled subject vocabularies serve end-users particularly well. This is covered in David Weinberger's _[Everything is Miscellaneous](http://www.everythingismiscellaneous.com/)_, of course. This is one of the key things that changes when an index no longer require a huge room full of drawers of cards to keep it in.

Secondly, there's a thread [about linking to digitized books](http://listserv.nd.edu/cgi-bin/wa?A1=ind0710&amp;L=ngc4lib#13) (login required) going on over on [NGC4Lib](http://dewey.library.nd.edu/mailing-lists/ngc4lib/). In that thread folks are discussing the cataloguing of books digitized by Google Book Search and others.

Jan Szczepanski describes how she is cataloguing GBS books:

> You can collect in two ways, Tim's way or my way, or a combination of both.
> <br />
> <br />My way, or what I could call the quality way means that You carefully looks at every title. Who likes "white noise"? I use the same criteria I use for paper books.

Maurice York is interested in that approach:

> I'm curious about this trash-or-treasure line of thinking as a reasoned basis for the manual effort of selection of digitized texts. You are quite right that libraries specialize in selection and have been doing it for thousands of years (more in generalities than realities, since I don't believe any library with a currently functioning collection has been around for more than a few hundred). But it seems to me that this is the very reason Google saw libraries as such an attractive proposition for digitization--they have been building high-quality collections of print materials and (presumably) sorting much of the dross according to sustained plans over long periods of time. When you say that the vast majority of texts in Google are "bad quality, bad relevance", that seems more a dig at American libraries and how we collect than at Google, since Google's collection is no more and no less than what librarians have created. Let me expand that a bit....it's something of a criticism of the libraries of Spain, Germany, the Netherlands, Japan, England, and France as well, all of whom are digitizing books with Google.

These snippets of bits &#38; pieces are all starting to fit together. Not quite sure what the jigsaw is of yet, but it's going to be interesting.

It seems to me that subject classification has to be opened up to everyone - and simplified. It doesn't need to be a hierarchy anymore, and it doesn't need to be controlled. On the other hand, names need some real clever work, to decide which names are the same and which are different requires a huge amounts of intelligence and knowledge. Authority files have historically helped with this, but we need to make those work much harder.

I should write some more on how I think this stuff fits together - mental note to self, must write a paper on Marc and RDF just as soon as we have [Talis Insight](http://www.talis.com/insight/) out of the way.

<!-- technorati tags start --><p style="text-align:right;font-size:10px;">Technorati Tags: <a href="http://www.technorati.com/tag/library" rel="tag">library</a>, <a href="http://www.technorati.com/tag/multi-lingual" rel="tag">multi-lingual</a>, <a href="http://www.technorati.com/tag/open-data" rel="tag">open-data</a></p><!-- technorati tags end -->
