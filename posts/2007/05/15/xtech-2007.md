---
layout: post.pug 
title: "XTech 2007"
date: 2007-05-15
tags: ["post"]
legacy_wordpress_id: 181
---

I'm over in Paris right now, at [XTech](http://www.xtech.org/).  

Interesting presentations, sessions and lots of interesting people.

<!-- excerpt -->

Blogging for [work](http://www.talis.com) over at [Nodalities](http://blogs.talis.com/nodalities/). Nad's blogging on [Virtual Chaos](http://www.virtualchaos.co.uk/blog/)

 <div class="wlWriterSmartContent" id="0767317B-992E-4b12-91E0-4F059A8CECA8:3db5b40b-8329-40be-bf3b-2f1fbd05b298" contenteditable="false" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati tags: <a href="http://technorati.com/tags/xtech" rel="tag">xtech</a>, <a href="http://technorati.com/tags/xtech2007" rel="tag">xtech2007</a></div>
