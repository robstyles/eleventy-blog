---
layout: post.pug 
title: "Busy Days..."
date: 2007-05-10
tags: ["post","Internet Social Impact"]
legacy_wordpress_id: 179
---

<a title="Photo Sharing" href="http://www.flickr.com/photos/mmmmmrob/491616030/"><img style="margin: 0px 5px 0px 0px" height="240" alt="Tim Berners-Lee" src="http://farm1.static.flickr.com/223/491616030_e64294ac16_m.jpg" width="160" align="left"></a>

Spent 24 hours (door-to-door) traveling over to Banff for the WWW2007 conference, and we've now been here for a couple of days. The standard of conversation (all the sessions bar the keynote have been very interactive) has been very good.

<!-- excerpt -->

The thrust that I'm seeing and hearing here is one of Linked and [Open Data](http://en.wikipedia.org/wiki/Open_Data). Getting as much data as people can up on the semantic web using Cool URIs, RDF, GRRDL and other bits of the puzzle - the message from Tim and close friends is "do it now".

This probably shouldn't be a surprise, [2007 has been heralded as the make or break year](http://iandavis.com/blog/2007/02/make-or-break-for-the-semantic-web).

I haven't been blogging much as [Paul's doing an excellent job over at Nodalities](http://blogs.talis.com/nodalities/). But there are some interesting points coming out consistently.

First, and interesting, is the&nbsp;importance of the [graph](http://en.wikipedia.org/wiki/Graph_%28data_structure%29). The idea that the web is successful because of its chaos and freedom. This needs to be the same with Linked and Open Data. Trees are distinctly passé, as are tables and, frankly, anything that's not simply a graph.

This is a trend happening in many places, not just on the web. In OO programming languages, for example, there is a move to favour composition over inheritance - a graph over a tree.

There's also a big thrust around provenance - if you have a lot of data that you're mixing together you want to know where the bits came from, so you can make decisions on their authoritiveness. In his keynote Tim suggested that triple stores should really have four columns - subject, predicate, object, provenance.

Today was my turn to voice some stuff, along with 

 <ul> <li>[Steve Coast](http://www.opengeodata.org/) ([OpenStreetMap](http://www.openstreetmap.org/))  <li>[Peter Murray-Rust](http://wwmm.ch.cam.ac.uk/blogs/murrayrust/) ([University of Cambridge](http://www.cam.ac.uk/))  <li>[Jamie Taylor](http://www.freebase.com/view/user?id=%239202a8c04000641f8000000001047b28)&nbsp;(warning, link required registration)&nbsp;([Metaweb](http://metaweb.com/)) </li></ul> 

We were talking about various aspects of Open Data. Steve gave a great animated show of GPS data gathered for London by couriers, Peter talked about the need for openness in scientific publishing and Jamie talked brilliantly about the reasons to open data you might otherwise think is core to your business. I was talking about the importance of making your licensing clear.

The [slides are online here](http://talis-presentations.s3.amazonaws.com/www2007-opendatapanel.pdf), [Peter presented direct from his blog](http://wwmm.ch.cam.ac.uk/blogs/murrayrust/?p=307), and [Paul's talking about the panel over on Nodalities](http://blogs.talis.com/nodalities/2007/05/presentations_from_www2007_ope.php).

Allegedly Danny recorded the session, I'll update here if/when it appears.

 <div class="wlWriterSmartContent" id="0767317B-992E-4b12-91E0-4F059A8CECA8:c389047a-0b29-4913-8c10-77083c345392" contenteditable="false" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati tags: <a href="http://technorati.com/tags/www2007" rel="tag">www2007</a>, <a href="http://technorati.com/tags/tim%20berners-lee" rel="tag">tim berners-lee</a>, <a href="http://technorati.com/tags/open%20data" rel="tag">open data</a>, <a href="http://technorati.com/tags/open%20access" rel="tag">open access</a>, <a href="http://technorati.com/tags/licensing" rel="tag">licensing</a>, <a href="http://technorati.com/tags/open%20street%20map" rel="tag">open street map</a>, <a href="http://technorati.com/tags/semantic%20web" rel="tag">semantic web</a>, <a href="http://technorati.com/tags/rob%20styles" rel="tag">rob styles</a>, <a href="http://technorati.com/tags/steve%20coast" rel="tag">steve coast</a>, <a href="http://technorati.com/tags/peter%20murray-rust" rel="tag">peter murray-rust</a>, <a href="http://technorati.com/tags/jamie%20taylor" rel="tag">jamie taylor</a></div>
