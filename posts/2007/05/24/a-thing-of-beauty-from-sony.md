---
layout: post.pug 
title: "a thing of beauty from Sony"
date: 2007-05-24
tags: ["post","Interaction Design"]
legacy_wordpress_id: 183
---

<a title="Flexible, full-color OLED ::: Pink Tentacle" href="http://www.pinktentacle.com/2007/05/flexible-full-color-organic-el-display/">Flexible, full-color OLED</a>

This screen looks fantastic. If you don't see what the fuss is about, think what the implications could be for the form factor of the Nintendo DS, or the iPod. As they get bigger, which they inevitably will, imagine what that will mean for displays at home and at work. [Bill Buxton is saying that large multi-touch displays will be cheaper to install than whiteboards](/2007/05/11/youre-so-five-minutes-ago-aka-bill-buxton-part-one). I have to agree.

<!-- excerpt -->
