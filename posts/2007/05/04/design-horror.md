---
layout: post.pug 
title: "Design Horror"
date: 2007-05-04
tags: ["post","Interaction Design"]
legacy_wordpress_id: 178
---

Update: check out [Jeff's full reading list](http://www.codinghorror.com/blog/archives/000020.html).

[Over at Coding Horror Jeff Atwood has been cajoling developers to learn more about visual design](http://www.codinghorror.com/blog/archives/000853.html)&nbsp;and to [learn a graphics tool](http://www.codinghorror.com/blog/archives/000849.html). He's focussed on visual design in these two posts and I agree with him that these are critical skills for anyone doing anything with a visible UI.

<!-- excerpt -->

But it strikes me that he might want to push further; looking at what value there would be for developers in knowing more about other human-factors design disciplines. At the risk of pre-empting him I'd like to see everyone read these few books:

[<img style="border-top-width: 0px; border-left-width: 0px; border-bottom-width: 0px; margin: 0px 10px 0px 0px; border-right-width: 0px" height="120" alt="The Inmates Are Running The Asylum, by Alan Cooper" src="/assets/WindowsLiveWriter/DesignHorror_8B18/6d158de9-730c-492e-bac7-0cd3478532bf%5B1%5D%5B5%5D.jpg" width="78" align="left" border="0"> The Inmates Are Running The Asylum](http://cenote.talis.com/isbn/0672316498) by Alan Cooper.

This is a great read. It's a rant by Interaction Design guru Alan Cooper on why developers shouldn't be trusted to do any kind of design work and why the design has to be cast in stone before developers are allowed to touch it.

Knowing how bad many developers are basic interaction design he makes a good case; I would prefer to see us get better at it than allow Alan to cut us out of the process altogther. This book should give you the motivation to learn more about your users.

<a href="http://cenote.talis.com/isbn/9780262640374" atomicselection="true"><img style="border-right: 0px; border-top: 0px; margin: 0px 5px 0px 0px; border-left: 0px; border-bottom: 0px" height="120" src="/assets/WindowsLiveWriter/DesignHorror_8B18/5ae0ae61-1bd6-47b1-b12c-cbe8aff51072%5B1%5D%5B6%5D.jpg" width="79" align="left" border="0"></a> 

[The design of everyday things](http://cenote.talis.com/isbn/9780262640374) by Don Norman.

This classic text points out all those niggling little annoyances that we face every day as we pull at a door that should be pushed, or dribble tea on the table from a tea pot that doesn't pour properly.

Theses usability issues dog our day-to-day lives yet could be solved by paying just a little more attention to what you're designing.

Failing those two, which give you fantastic background and insight into this area, if you only read one book, and you need it to be short, Spolsky would have you read his...

<a href="http://cenote.talis.com/isbn/1893115941" atomicselection="true"><img style="border-right: 0px; border-top: 0px; margin: 0px 5px 0px 0px; border-left: 0px; border-bottom: 0px" height="120" src="/assets/WindowsLiveWriter/DesignHorror_8B18/863ab5cd-0307-459f-9b9c-aaa589a0baaf%5B1%5D%5B5%5D.jpg" width="96" align="left" border="0"></a> 

[User interface design for programmers](http://cenote.talis.com/isbn/1893115941) by Joel Spolsky.

We read this a few months back as one of our book club books (we have a geek book club at [work](http://www.talis.com)). It's short, very easy to read and gives you concrete dos and don'ts for making life easier for your users.

It includes wonderful reviews of some of the best and worse of Microsoft's interfaces and explains stuff in Joel's usual no-nonsense style. You should totally read this one.

I hope Jeff points us to some more as well.

 <div class="wlWriterSmartContent" id="0767317B-992E-4b12-91E0-4F059A8CECA8:767f437a-4e52-4a7d-a5ea-3fd3c7eb7322" contenteditable="false" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati tags: <a href="http://technorati.com/tags/User%20Interaction" rel="tag">User Interaction</a>, <a href="http://technorati.com/tags/Design" rel="tag">Design</a>, <a href="http://technorati.com/tags/Programming" rel="tag">Programming</a>, <a href="http://technorati.com/tags/Developer%20Skills" rel="tag">Developer Skills</a>, <a href="http://technorati.com/tags/Alan%20Cooper" rel="tag">Alan Cooper</a>, <a href="http://technorati.com/tags/Don%20Norman" rel="tag">Don Norman</a>, <a href="http://technorati.com/tags/Joel%20Spolsky" rel="tag">Joel Spolsky</a>, <a href="http://technorati.com/tags/The%20inmates%20are%20running%20the%20asylum" rel="tag">The inmates are running the asylum</a>, <a href="http://technorati.com/tags/The%20design%20of%20everyday%20things" rel="tag">The design of everyday things</a>, <a href="http://technorati.com/tags/User%20interface%20design%20for%20programmers" rel="tag">User interface design for programmers</a></div>
