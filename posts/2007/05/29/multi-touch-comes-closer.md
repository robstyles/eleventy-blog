---
layout: post.pug 
title: "Multi-Touch comes closer..."
date: 2007-05-29
tags: ["post","Interaction Design"]
legacy_wordpress_id: 184
---

A comment from the venerable Stefano Baraldi over on my post about [Bill Buxton's keynote](/2007/05/11/youre-so-five-minutes-ago-aka-bill-buxton-part-one)&nbsp;prompted me to take a quick look around. Catching up with Stefano's blog, [onTheTableTop](http://www.onthetabletop.eu/), I see that [tabulaTouch, blogged here previously](/2007/02/16/more-touch-ui-videos), is coming to market shortly as [Sensitive Table](http://www.sensitivetable.com/).

Stefano also provides a link to [Natural Interaction](http://www.naturalinteraction.org/) who have more stunning videos of just how engaging this stuff can be.

<!-- excerpt -->

 <div class="wlWriterSmartContent" id="0767317B-992E-4b12-91E0-4F059A8CECA8:cfef2215-98dc-4e8c-9c73-eafcd2d3574e" contenteditable="false" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati tags: <a href="http://technorati.com/tags/Gesture%20Interface" rel="tag">Gesture Interface</a>, <a href="http://technorati.com/tags/Interface%20Design" rel="tag">Interface Design</a>, <a href="http://technorati.com/tags/Interaction%20Design" rel="tag">Interaction Design</a>, <a href="http://technorati.com/tags/Multi-Touch%20Interface" rel="tag">Multi-Touch Interface</a>, <a href="http://technorati.com/tags/Physical%20World%20Hyperlinks" rel="tag">Physical World Hyperlinks</a>, <a href="http://technorati.com/tags/Vista" rel="tag">Vista</a>, <a href="http://technorati.com/tags/Tablets" rel="tag">Tablets</a>, <a href="http://technorati.com/tags/Natural%20Interaction" rel="tag">Natural Interaction</a></div>
