---
layout: post.pug 
title: "you're going to need gloves..."
date: 2007-05-31
tags: ["post","Interaction Design"]
legacy_wordpress_id: 186
---

<a title="Photo Sharing" href="http://www.flickr.com/photos/mmmmmrob/523179243/"><img style="margin: 0px 10px 0px 0px" height="240" alt="minority-report-tom-cruise" src="http://farm1.static.flickr.com/202/523179243_8d45df6fe2_m.jpg" width="160" align="left"></a>

So, the [Surface](/2007/05/30/microsoft-in-on-the-multi-touch-act). A nice interactive multi-touch device from Microsoft.

<!-- excerpt -->

I've been looking at the videos for this and it does look good, but a [video of the Surface Paint application on ZDNet](http://zdnet.com.com/1606-2_2-6186146.html?tag=nl.e589) caught my eye.

Watch it and come, go on, I'll wait.

So spot the difference between that and the shot on the left, taken from [Minority Report from 2002](http://www.imdb.com/title/tt0181689/)?

Tom Cruise is wearing gloves. And they're not just any old gloves, they're little black gloves with lights on the tips. Why?

Go back and watch the video of Surface again. notice the bit where Mark Bulger says "we can both paint at the same time". He then goes on to show how you can use a paint brush "just like in the real world".

Look again - did you see it yet? When the interviewer joins in the paint color used by Mark changes. two of you can apint, but you can only use one colour. This limitation is because the Surface has no way of knowing which finger is which. My four year old likes to finger paint - she likes to dip each finger in a different color paint and make rainbows in one stroke. You can't do that with Surface.

Now, don't get me wrong, I think the Surface is great, and I'm trying to get some time to play with one here in the UK, but it still has a missing piece in how real it will feel.

 <div class="wlWriterSmartContent" id="0767317B-992E-4b12-91E0-4F059A8CECA8:cb046d3b-fe48-45a3-bf84-eb9d88b2e613" contenteditable="false" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati tags: <a href="http://technorati.com/tags/Gesture%20Interface" rel="tag">Gesture Interface</a>, <a href="http://technorati.com/tags/Interface%20Design" rel="tag">Interface Design</a>, <a href="http://technorati.com/tags/Interaction%20Design" rel="tag">Interaction Design</a>, <a href="http://technorati.com/tags/Multi-Touch%20Interface" rel="tag">Multi-Touch Interface</a>, <a href="http://technorati.com/tags/Physical%20World%20Hyperlinks" rel="tag">Physical World Hyperlinks</a></div>
