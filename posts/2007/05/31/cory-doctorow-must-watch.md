---
layout: post.pug 
title: "Cory Doctorow must watch..."
date: 2007-05-31
tags: ["post","Internet Social Impact"]
legacy_wordpress_id: 187
---

<a title="http://www.youtube.com/watch?v=xgXwmXpaH2Q" href="http://www.youtube.com/watch?v=xgXwmXpaH2Q">http://www.youtube.com/watch?v=xgXwmXpaH2Q</a>

This is Cory talking at Google about the ways we're (well, the US really) killing our ability to participate in a true information age.

<!-- excerpt -->

 <div class="wlWriterSmartContent" id="0767317B-992E-4b12-91E0-4F059A8CECA8:54382b85-f591-41e7-ad5b-3987da8326cc" contenteditable="false" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati tags: <a href="http://technorati.com/tags/cory%20doctorow" rel="tag">cory doctorow</a>, <a href="http://technorati.com/tags/authors@google" rel="tag">authors@google</a>, <a href="http://technorati.com/tags/creative%20commons" rel="tag">creative commons</a>, <a href="http://technorati.com/tags/copyright%20law" rel="tag">copyright law</a>, <a href="http://technorati.com/tags/internet%20culture" rel="tag">internet culture</a>, <a href="http://technorati.com/tags/internet%20social%20impact" rel="tag">internet social impact</a></div>
