---
layout: post.pug 
title: "Microsoft in on the multi-touch act..."
date: 2007-05-30
tags: ["post","Interaction Design"]
legacy_wordpress_id: 185
---

"Surface Computing has Arrived" Microsoft Surface !!!

In a combination of many of the different touch techniques I've talked about before Microsoft have produced a beautiful marketing piece for multi-touch surfaces.

<!-- excerpt -->

<a title="http://www.microsoft.com/surface/" href="http://www.microsoft.com/surface/">http://www.microsoft.com/surface/</a>

The marketing goodness cleverly uses the gestures of the multi-touch UI as the animated tramnsitions in the Flash promo material very, very smart.

As well as multi-touch they also show the use of physical tokens like [Reactable](http://mtg.upf.edu/reactable/) and talk about "phones practical introduce themselves"; a social network of devices just as [Bill Buxton told us was coming](/2007/05/11/youre-so-five-minutes-ago-aka-bill-buxton-part-one).

Update: [Nad's found a nice launch video](http://www.virtualchaos.co.uk/blog/2007/05/30/microsoft-surface/)

 <div class="wlWriterSmartContent" id="0767317B-992E-4b12-91E0-4F059A8CECA8:e3071134-1d1b-42b6-9bd3-49931e9678d7" contenteditable="false" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati tags: <a href="http://technorati.com/tags/Gesture%20Interface" rel="tag">Gesture Interface</a>, <a href="http://technorati.com/tags/Interface%20Design" rel="tag">Interface Design</a>, <a href="http://technorati.com/tags/Interaction%20Design" rel="tag">Interaction Design</a>, <a href="http://technorati.com/tags/Multi-Touch%20Interface" rel="tag">Multi-Touch Interface</a>, <a href="http://technorati.com/tags/Physical%20World%20Hyperlinks" rel="tag">Physical World Hyperlinks</a></div>
