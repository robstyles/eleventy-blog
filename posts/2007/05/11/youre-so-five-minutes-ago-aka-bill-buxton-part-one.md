---
layout: post.pug 
title: "You're so five minutes ago aka Bill Buxton Part One"
date: 2007-05-11
tags: ["post","Internet Social Impact"]
legacy_wordpress_id: 180
---

<a title="Photo Sharing" href="http://www.flickr.com/photos/mmmmmrob/496728829/"><img style="margin: 10px 5px 0px 0px" height="240" alt="IMG_8481" src="http://farm1.static.flickr.com/212/496728829_b47bd154e0_m.jpg" width="160" align="left"></a>  

Bill Buxton is on stage right now talking about how narrow the web is - The World Narrow Web.

<!-- excerpt -->

Bill is an ebulient presenter, making sure the audience know exactly who they're supposed to be listening to. Dressed in a black suit and black shirt he looks slightly Italian; if he weren't so scruffy, a point he makes himself several times.

Talking about a visit to Pixar he points out that, looking at the equipment being used,&nbsp;he can't tell the difference between the animation department and the accounts department. I don't think he meant that even accounts is cool at Pixar.

And that's the narrowness. A lack of diversity, not of content, but of interaction. That we have machines that interact with all the social skills of a fruit fly.

Nicholas Negroponte, according to Bill, made lots of money touring the world asking people to imagine what things would be like when bandwidth was everywhere and effectively free. Bill's "Imagine" is displays. He asks us to imagine what the world will be like when you can buy pixels, 100dpi for about $10 a square foot; a time when it will be cheaper to put&nbsp;an enormous panel of pixels on your wall than a whiteboard.

Which brings him on nicely to his next point: Why aren't displays bi-directional. Why aren't they touch screens. He explains that he doesn't mean the kind of one finger touch screens that we see on kiosks, but the kind of rich, multi-touch displays that folks like Jeff Han are working on and that I have blogged about here before.

Bill and I agree that these large multi-touch displays will change the way we use computers in fundamental ways. He points out that if you;re writing software and not planning for this change to take place within five years you need to re-think.

Bill spent time at Xerox Parc ?? during which time he saw plenty of multi-touch work. That's why he objects to "a certain CEO" presenting multi-touch devices as if "his company" had invented them. I thought Microsoft had got past its _not able to say Apple_ phase.

One of the devices he saw was&nbsp;a tablet for creating animation. He dates this at 1968, papers published in 1969. I've blogged about innovation back then before. The ability he describes is being able to draw a line, and have that as the object to animate, then draw a second line as the path to animate it along. Imagine the obscure animation path feature of PowerPoint to give you an idea of what he means. But what this tablet could do was track the time it took you to draw the path and use that as the animation timing. The software treated time as a first class citizen.

He's very keen to see interaction technology like multi-touch hit the mainstream. As am I.

The point about time is interesting enough for him to develop further. He wanders of on a ramble about the history of white explorers in North America and Canada, Talking about Vancouver and ?? ?? (who relied on Sakagwea, although she fails to get a mention). His point comes out towards the end; two of the explorers missed each other by just 3 weeks, a little way north of (what is now called) Vancouver. That changed the course of history, but is so difficult to represent without writing a whole pile of custom animation of the data. Because time and space are not first class citizens of our systems it is not easy top represent these everyday things.

From this point we have a&nbsp;short diversion into using your watch to control very large display; those that are too big to touch practically. That leads us to the notion of a heterogenous social network of devices, requiring societal values that the devices adhere to in order that devices who have never previously met can co-operate.

> "A society of appliances. Heterogenous social network: of computers _and_ people" 

The logical extension of that is, of course, a heterogenous network of both people and machines. In no small way that is what the Semantic Web is about, of course.

> "why are web applications different to any other application once we have infinite bandwidth and sotrage - our job is to put the WWW out of business" 

So, once we have infinte bandwidth and infinte storage in the cloud, Bill goes on, what is the difference between web applications and other applications. Our CTO, Justin, has talked about this at length and describes it as the trend towards "Internet Inside" applications. Bill wants to see as many different "browsers" as there are types of paper. The classic debate between generalised and specialised user interfaces; although Bill doesn't suggest the general browser is wrong, just that we are wrong for continuing down the line that Mosaic set out without questioning or deviating.

Apparently we see the problem of the generic browser most in search. According to Bill search is not a task or an application...

> "Search is a bloody interruption! I'm in the middle of doing something and the thing isn't there where I need it!" 

Bill has presented, so far,&nbsp;in what most would describe as a "ranty" style. Much of what he talks about, mutli-touch UI, specialised browsers he talks about in a frustrated "why has it taken so long" tone. This is entertaining, if not entirely uplifting. He takes the opportunity to say at this point that his official presentation is over and that what follows is, very simply, a rant.

I'll blog about the rant next...

 <div class="wlWriterSmartContent" id="0767317B-992E-4b12-91E0-4F059A8CECA8:f5153702-7d06-4262-bb4d-1a8cef5d1bc5" contenteditable="false" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati tags: <a href="http://technorati.com/tags/www2007" rel="tag">www2007</a>, <a href="http://technorati.com/tags/www%202007" rel="tag">www 2007</a>, <a href="http://technorati.com/tags/bill%20buxton" rel="tag">bill buxton</a>, <a href="http://technorati.com/tags/microsoft" rel="tag">microsoft</a>, <a href="http://technorati.com/tags/keynote" rel="tag">keynote</a></div>
