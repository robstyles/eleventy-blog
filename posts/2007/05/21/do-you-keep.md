---
layout: post.pug 
title: "Do you keep"
date: 2007-05-21
tags: ["post","Security And Privacy"]
legacy_wordpress_id: 182
---

a draft of some posts that you write and think "nah, if I publish that I'll get in trouble"?

I do. They might be rants or they might criticise something that's really not up for criticism, socially I mean.

<!-- excerpt -->

Anyway, I'm bound to get a visit from the TSA, or refused entry into the US or someat for this...

Firstly: this is a [great skit on the absurdity of airline security](http://www.youtube.com/watch?v=ykzqFz_nHZE).

Secondly...

nah, better leave that in the drafts folder.

 <div class="wlWriterSmartContent" id="0767317B-992E-4b12-91E0-4F059A8CECA8:612ec098-5bf1-48f7-8e15-9c8da6d52673" contenteditable="false" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati tags: <a href="http://technorati.com/tags/airline%20security" rel="tag">airline security</a>, <a href="http://technorati.com/tags/humour" rel="tag">humour</a>, <a href="http://technorati.com/tags/TSA" rel="tag">TSA</a>, <a href="http://technorati.com/tags/Saturday%20Night%20Live" rel="tag">Saturday Night Live</a></div>
