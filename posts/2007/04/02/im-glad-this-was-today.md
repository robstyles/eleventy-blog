---
layout: post.pug 
title: "I'm glad this was today..."
date: 2007-04-02
tags: ["post","Internet Social Impact"]
legacy_wordpress_id: 173
---

I had to check the date, to make sure it wasn't posted yesterday, but this is awesome news. I might even start using iTunes.

[http://www.techcrunch.com/2007/04/02/emi-apple-are-announcing-sale-of-non-drm-music/](http://www.techcrunch.com/2007/04/02/emi-apple-are-announcing-sale-of-non-drm-music/)

<!-- excerpt -->
