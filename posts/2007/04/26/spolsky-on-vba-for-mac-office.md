---
layout: post.pug 
title: "Spolsky on VBA for Mac Office"
date: 2007-04-26
tags: ["post","Software Business"]
legacy_wordpress_id: 177
---

[Joel's talking](http://www.joelonsoftware.com/items/2007/04/25.html) about [Microsoft's withdrawal of VBA in the Office Suite for Mac](http://www.macnn.com/articles/06/08/07/ms.kills.virtualpc/) users.

Joel starts by explaining why VBA was strategic for MS:

<!-- excerpt -->

> However, it was seen as extremely "strategic." Here's what that meant. Microsoft thought that if people wrote lots and lots of VBA code, they would be locked in to Microsoft Office. 

As Joel was on the Excel team at the time you'd expect this to be an accurate reflection of what was happening. It makes sense, Office is the big cash cow and always has been.

Towards the end Joel says:

> they're effectively making it very hard for many Mac Office 2004 users to upgrade to Office 2008, forcing a lot of their customers to reevaluate which desktop applications to use. 

I'm guessing the outcome MS are hoping for is different; Apple have made great in-roads into compatibility with Windows, a lot of hard work of their own but also a move to internet and intranet approaches have helped shrink the gap. To the point where Macs are&nbsp;showing up more and more at conferences, in businesses and in homes. The removal of support for VBA on mac is unlikely to push mac users in business to run a different office suite, it's far more likely to push their corporate overlords to move them back to Windows, or run Windows through Parallels or Bootcamp - either way MS get a mac customer back onto windows.

Sad.

 <div class="wlWriterSmartContent" id="0767317B-992E-4b12-91E0-4F059A8CECA8:9368e8ae-bcb8-4364-af26-fa9b56d78ace" contenteditable="false" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati tags: <a href="http://technorati.com/tags/Microsoft%20Office" rel="tag">Microsoft Office</a>, <a href="http://technorati.com/tags/VBA" rel="tag">VBA</a>, <a href="http://technorati.com/tags/VBA%20for%20Macintosh" rel="tag">VBA for Macintosh</a>, <a href="http://technorati.com/tags/Joel" rel="tag">Joel</a>, <a href="http://technorati.com/tags/Joel%20on%20Software" rel="tag">Joel on Software</a>, <a href="http://technorati.com/tags/Windows" rel="tag">Windows</a></div>
