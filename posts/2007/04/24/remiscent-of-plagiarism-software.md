---
layout: post.pug 
title: "Remiscent of Plagiarism Software..."
date: 2007-04-24
tags: ["post","Internet Social Impact"]
legacy_wordpress_id: 176
---

The internet is a marvellous thing. As well as making such a vast amount of information available for legitimate use it also allows that information to be used to analyse infringing uses. There is plenty of [anti-plagiarism software](http://www.cshe.unimelb.edu.au/assessinglearning/03/plagsoftsumm1.html) available for analysing text.

We've used the technique here at work during recruitment and during assessment of third party work. We had one applicant present a multi-media piece as his own "award winning" work when the award site clearly gave someone elses name as the creator - someone we know in that specific instance...

<!-- excerpt -->

But in the past week or so a great new case appears to have popped up. It apparently started in April when a comic artist, Dave Kelly spotted that one of his images had been used on a t-shirt design published by Todd Goldman. [Wikipedia covers the basic about Todd Goldman and the accusation of plagiarism](http://en.wikipedia.org/wiki/Todd_Goldman).

Mike Tyndall has put together a piece showing a great number of what he describes as [Todd Goldman designs and extremely similiar design from elsewhere](http://www.miketyndall.com/todd_goldman/). He leaves it as an exercise for the reader to decide if any plagiarism or infringment has taken place. He's received objections to the content from Todd's lawyers.

I wonder how this one will play out.

 <div class="wlWriterSmartContent" id="0767317B-992E-4b12-91E0-4F059A8CECA8:408df745-de4c-4e9d-a7a1-436910399f25" contenteditable="false" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati tags: <a href="http://technorati.com/tags/copyright" rel="tag">copyright</a>, <a href="http://technorati.com/tags/todd%20goldman" rel="tag">todd goldman</a>, <a href="http://technorati.com/tags/plagiarism" rel="tag">plagiarism</a>, <a href="http://technorati.com/tags/mike%20tyndall" rel="tag">mike tyndall</a></div>
