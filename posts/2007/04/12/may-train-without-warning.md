---
layout: post.pug 
title: "May Train Without Warning"
date: 2007-04-12
tags: ["post","Random Thought"]
legacy_wordpress_id: 174
---

<a href="http://www.flickr.com/photos/mmmmmrob/456924552/" title="Photo Sharing"><img src="http://farm1.static.flickr.com/249/456924552_d5c45bc184_t.jpg" width="100" height="67" alt="IMG_5220" /></a>

I saw this on the back of a gun turret on HMS Belfast. It reminded me of various people I've worked with who "may train without warning". Ivan, Richard, Rachel, Nick, Ian, Dan - Hi, still thankful for the things you taught me.

<!-- excerpt -->
