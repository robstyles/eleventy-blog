---
layout: post.pug 
title: "ex-boss does podcast"
date: 2007-04-21
tags: ["post","Working at Talis"]
legacy_wordpress_id: 175
---

ex-CIO of Egg, ex-CEO of Lost Wax and my ex-boss (at internet bank Egg) Tom Ilube has done [a great podcast for us at Talis](http://talk.talis.com/archives/2007/04/tom_ilube_talks.html), talking about his current venture, Garlik.

[Garlik](http://www.garlik.com)'s a great service, try it out - I was surprised by just how much it found about me.

<!-- excerpt -->

Update: [Danny Ayers blogs about the podcast here](http://dannyayers.com/2007/04/21/semantic-web-the)
