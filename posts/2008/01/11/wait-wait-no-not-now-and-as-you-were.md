---
layout: post.pug 
title: "\"Wait, wait, no, not now\"... and \"as you were\""
date: 2008-01-11
tags: ["post","commands I have issued"]
legacy_wordpress_id: 220
---

ps -ax | grep getFromUris | grep -v grep | awk '{ print $1 }' | xargs kill -STOP

ps -ax | grep getFromUris | grep -v grep | awk '{ print $1 }' | xargs kill -CONT<br />

<!-- excerpt -->
