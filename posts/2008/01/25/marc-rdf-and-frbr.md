---
layout: post.pug 
title: "Marc, RDF and FRBR"
date: 2008-01-25
tags: ["post","Library Tech"]
legacy_wordpress_id: 222
---

I've been playing with some ideas over the past six months on how we can really move bibliographic data forwards into a structure that could have huge benefits.

The impetus to describe some of that for everyone finally came in the form of a conference, with a deadline for submission that is in just a few days time. The conference is [WWW2008](http://www2008.org/) and the workshop is entitles [Linked Data on the Web](http://events.linkeddata.org/ldow2008/). There are a whole load of reasons to go - I got to go to last years and learnt a huge amount as well as getting to speak about data licensing.

<!-- excerpt -->

This year I've submitted a substantial paper about the work I've done on finding relationships in MARC data, something I'm already scheduled to present on at [Code4Lib](http://code4lib.org/conference/2008/) late next month. I've had a lot of help thinking about these problems from [Nad](http://www.virtualchaos.co.uk/blog/) and he's helped out enormously on getting the paper finished. Thanks also have to go to [Danny](http://dannyayers.com/), he's been of huge help understanding how to think about RDF and how to describe it - he wrote chunks of the paper too.

Please, grab the paper, have a read and let me know what you think.

[Semantic Marc, MARC21 and The Semantic Web.](/assets/Semantic%20Marcup.pdf) (PDF, 440Kb)

Update:  Thanks to Damian for pointing out the error in the example turtle - don't know how we missed that!
