---
layout: post.pug 
title: "All my favourite things"
date: 2008-01-17
tags: ["post","Internet Social Impact"]
legacy_wordpress_id: 221
---

Open Content, The Library of Congress archive and Photography - I could spend hours here:

<p style="margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px Helvetica">[http://www.flickr.com/photos/library_of_congress/](http://www.flickr.com/photos/library_of_congress/)</p>

<!-- excerpt -->
