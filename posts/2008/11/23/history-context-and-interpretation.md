---
layout: post.pug 
title: "History, Context and Interpretation"
date: 2008-11-23
tags: ["post"]
legacy_wordpress_id: 350
---

I was talking about the US election with a friend recently. He's a historian by degree, though not by trade. We were discussing the different ways in which you could choose to understand speeches by Obama, McCain, Biden and Palin and how much we really knew about their background.

The reason for the conversation was lost by the end, we started out trying to decide what McCain had meant by something he'd said. I can't for the life of me remember what it was.

<!-- excerpt -->

The discussion, though, was about having to understand the background of the candidates in order to be able to interpret what they were saying and what they intended to do. This is an interesting thing to think about when speaking as well. The reverse of what Humpty Dumpty said in [Chapter 6 of Through the Looking Glass](http://www.sabian.org/Alice/lgchap06.htm).

> When _I_ use a word,' Humpty Dumpty said, in rather a scornful tone, `it means just what I choose it to mean -- neither more nor less.
The reverse of this being that words mean whatever the listener decides they mean. Everything is based on context and experience. In philosophy the distinction is between knowledge that is true regardless of experience and knowledge that is true based on experience is [A priori and a posteriori](http://en.wikipedia.org/wiki/A_priori_and_a_posteriori_(philosophy)) and what Humpty and Alice are discussing is basically the notion that there is no _a priori_ definition of language - that words mean one thing to the speaker and another to the receiver.

It strikes me that the same is true for solutions in computing. Beauty is most definitely in the eye of the beholder. I have said, and believed, for a long time now that  particular technologies, techniques or approaches are not "better" in an absolute sense than others unless discussed in the context of how they apply to a particular problem. Even when looking at the application of, say a language, to a particular problem it's not that one is better than another; merely that they have different tradeoffs. The common discussions about statically typed languages versus dynamically typed languages are a great example of this. Chris Smith wrote an excellent piece on [what to know before debating type systems](http://www.pphsg.org/cdsmith/types.html) back in 2007.

I had a great conversation about some of this stuff with Daniel at work on Friday. We <span style="text-decoration: line-through;">argued</span> chatted for so long that he was late leaving and got the dreaded "where are you?" call on his mobile. Sorry Daniel.

We started off talking about [Google Web Toolkit](http://code.google.com/webtoolkit/), GWT.

> With Google Web Toolkit (GWT), you write your AJAX front-end in the Java programming language which GWT then cross-compiles into optimized JavaScript that automatically works across all major browsers. During development, you can iterate quickly in the same "edit - refesh - view" cycle you're accustomed to with JavaScript, with the added benefit of being able to debug and step through your Java code line by line. When you're ready to deploy, GWT compiles your Java source code into optimized, standalone JavaScript files. Easily build one widget for an existing web page or an entire application using Google Web Toolkit.
This is obviously worthy of consideration as the code comes from Google - so it must be good. But good for what? It strikes me as odd that you would want to develop an application in Java to be "compiled" into JavaScript. The approach to development promoted by the GWT folks is that you develop in Java running in the JVM, so you can debug your code, then compile down to JavaScript for deployment. This separation between the development and deployment execution environments obviously has to be handled carefully to keep everything working identically. This puts me off as it seems like unnecessary risk and complexity when writing an AJAX app in JavaScript has never seemed tough. So I wanted to understand [who's using GWT](http://www.gwtsite.com/whos-using-gwt/) to try and understand what problem their using it to solve.

The post, from the start of this year, lists 8 interesting applications. Looking through them I see one of the first and most obvious things about them all is they're software delivered into the browser. By that I mean that they're windows-style GUI applications that happen to use the browser a convenient distribution mechanism. GWT makes a lot of sense in this context as it supports that conceptual model. If you want to write something that's more native to the web, with Cool URIs, a RESTful interface and that works as a part of a larger whole then it may make less sense.

So the number one problem it solves is to abstract away the web so that delivering more traditional software interfaces into the browser is made easier. That seems like a sensible thing to do. What else is it trying to do? An insight into it's philosophy can be gleaned, perhaps, from a post on the Google blog from August last year entitled [Google Web Toolkit: Towards a better web](http://googleblog.blogspot.com/2007/08/google-web-toolkit-towards-better-web.html).

> Instead of spending time becoming JavaScript gurus and fighting browser quirks, developers using GWT spend time productively coding and debugging in the robust Java programming language, using their existing Java tools and expertise.
So, there are several things all wrapped up in that sentence. The implication that learning JavaScript is time consuming, that browsers have lots of quirks, that Java supports productive coding and that existing java tools and expertise are strong. All of those things will be true in some contexts and not in others. Given the lack-lustre take-up of GWT within Google perhaps it's not true within Google.

On the other hand, listening to a couple of the GWT team present on it at Google Developer Day 2008 we get a different impression.

<object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/NvRa-CxkpZI&hl=en&fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/NvRa-CxkpZI&hl=en&fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="344"></embed></object>

If you're looking to deliver a fairly traditional gui within the browser and you're happy and productive working in Java then GWT looks like a good tool, maybe it works well for more webby apps as well, but that's not what they're showcasing. But GWT is just one thing, and we make decisions about what technologies, techniques and approaches to adopt every single day. Like anything else, it's all about context.
