---
layout: post.pug 
title: "More OCLC Policy..."
date: 2008-11-14
tags: ["post"]
legacy_wordpress_id: 338
---

There have been quite a few great posts and ideas circulating on how to respond to <a class="zem_slink" title="Online Computer Library Center" rel="homepage" href="http://www.oclc.org">OCLC</a>'s change in Record Usage Policy. The key thing is to act now - you really don't have long to stop this if you want to. OCLC wish the new policy to come into effect in Spring 2009.

This change is important as it moves the policy from being guidelines, that member libraries are politely asked to adhere to, to part of your contract with OCLC, that can be enforced. That's a major restriction on the members and far from being more open.

<!-- excerpt -->

So, to the ideas... Let's start with Aaron Swarz's post. Aaron is one of the folks responsible for OpenLibrary, so has a significant stake in this. His post is entitled [_Stealing Your Library: The OCLC Powergrab_](http://www.aaronsw.com/weblog/oclcscam) is a great explanation of why you should care about this. He finishes by asking that we sign up to a petition to [Stop the OCLC powergrab!](http://watchdog.net/c/stop-oclc)

Next up we have various suggestions circulating about libraries providing their own licensing statements. For example, in [More on OCLC’s policies](http://bibwild.wordpress.com/2008/11/05/more-on-oclcs-policies/) Jonathan Rochkind suggests putting it in the 996, just like the [OCLC 996 Policy Link](http://www.oclc.org/bibformats/en/9xx/996.shtm).

> Whether submitting the cataloging to OCLC, or anywhere else. Add your own 996 explaining that the data is released under Open Data Commons.
I had a little think about this and would suggest specifically using the ODC PDDL link as follows.
<table class="structuredexample" border="0" cellspacing="0" cellpadding="1">
<tbody>
<tr valign="top">
<td width="30" align="left">996</td>
<td width="10" align="right"></td>
<td width="17" align="left"></td>
<td width="90%" align="left"><span style="font-family: Arial,Helvetica,sans-serif;">‡</span> a ODCPDDL <span style="font-family: Arial,Helvetica,sans-serif;">‡</span> i This record is released under the Open Data Commons Public Domain Dedication and License. You are free to do with it as you please. <span style="font-family: Arial,Helvetica,sans-serif;">‡</span> u http://www.opendatacommons.org/odc-public-domain-dedication-and-licence/</td>
</tr>
</tbody></table>
Just like the OCLC policy link. This doesn't go as far as Aaron asks, with his suggestion

> Second, you put your own license on the records you contribute to OCLC, insisting that the entire catalog they appear in must be available under open terms.
The problem here is that there really isn't any Intellectual Property in a MARC Record. It may take effort, skill and diligence to create a good quality record. Creative or original, in terms of Copyright, it is not.

The issue of OCLC claiming these rights in catalog data has even made it onto Slashdot where they're covering how this [Non-Profit Org Claims Rights In Library Catalog Data](http://news.slashdot.org/article.pl?sid=08/11/13/1929213). Unfortunately the comments are the usual slashdot collection of ill-informed, semi-literate ramblings based on nothing more than a cursory glance of the post. Someone even appears to confuse OCLC and LoC in their response. ho hum.

Also worth mentioning is that Ryan Eby is keeping track of news and happenings with the [OCLC Record Usage Policy on the Code4Lib Wiki](http://wiki.code4lib.org/index.php/OCLC_Policy_Change).

Richard recorded a new Talking with Talis podcast yesterday. This will be posted on [Talis's Panlibus blog](http://blogs.talis.com/panlibus/archives/tag/talking-with-talis). I'll be covering that as soon as I've had chance to listen to it properly.

If you're on any mailing lists where this is being discussed, or spot other blog posts I should read then let me know in the comments.
