---
layout: post.pug 
title: "OCLC, Record Usage, Copyright, Contracts and the Law"
date: 2008-11-06
tags: ["post","Internet Social Impact","IP Law"]
legacy_wordpress_id: 327
---

[caption id="" align="alignleft" width="240" caption="FUD truck by John Markos on Flickr"][<img title="FUD truck by John Markos on Flickr" src="http://farm1.static.flickr.com/106/268268569_c5849f02c6_m.jpg" alt="FUD truck by John Markos on Flickr" width="240" height="135" />](http://www.flickr.com/photos/nhoj/268268569/)[/caption]

NB: This is my own blog. The opinions I publish do not necessarily reflect those of my employer. I am not a lawyer, but I did ask [James Grimmelmann](http://james.grimmelmann.net/) for his thoughts.

<!-- excerpt -->

Over on Metalogue, Karen Calhoun has been [clarifying OCLC's thinking behind its intention to change the usage policy for records sourced from WorldCat](http://community.oclc.org/metalogue/archives/2008/11/notes-on-oclcs-updated-record.html). It's great to see OCLC communicating this stuff, albeit a tad late given the furore that had already ensued. The question still remains though, are they right to be doing what they are?

Firstly, in the interest of full disclosure, let me make it perfectly clear that I work for [Talis](http://www.talis.com). I enjoy working for Talis and I agree with Talis's vision. I have to say that because Karen is clearly not happy with us:

> OCLC has been severely criticized for its WorldCat data sharing policies and practices. Some of these criticisms have come from people or organizations that would benefit economically if they could freely replicate WorldCat.
OCLC believe that Talis is one of those organisations, and we are. There are others too, <a class="zem_slink" title="LibraryThing" rel="homepage" href="http://www.librarything.com,">LibraryThing</a>, <a class="zem_slink" title="Reddit" rel="homepage" href="http://www.reddit.com/">Reddit</a>, [OpenLibrary](http://openlibrary.org), <a class="zem_slink" title="Amazon.com" rel="homepage" href="http://www.amazon.com">Amazon</a>, <a class="zem_slink" title="Google" rel="homepage" href="http://www.google.com/">Google</a>. Potentially many libraries could benefit too.

This isn't the first time I've talked about OCLC's business model. I wrote [an open letter to Karen Calhoun](http://blogs.talis.com/panlibus/archives/2007/12/does_anyone_hav.php) some time ago, talking about the issues of centralised control. The same concerns raise themselves again now. I feel there are several mis-conceptions in what Karen writes that I would like to offer a different perspective on.

First off, OCLC has no right to do this. That sounds all moral and indignant. I don't mean it that way. What I mean is, they have literally no right _in law_ - or at least only a very limited one.

Karen talks a lot about <a class="zem_slink" title="Creative Commons" rel="homepage" href="http://creativecommons.org/">Creative Commons</a> in her note, it's apparent that they even considered using a Creative Commons license

> And yes, while we considered simply adopting a Creative Commons license, we chose to retain an OCLC-specific policy to help us re-express well-established community practice from the _Guidelines_.
There is an important thing to know about CC. Applying a Creative Commons License to data is utterly worthless. It may indicate the intent of the publisher, but has absolutely no legal standing. This is because CC is a license scheme based on Copyright. [Data is not protected by Copyright](http://www.copyright.gov/title17/92chap1.html#102). The courts settled this in <a class="zem_slink" title="Feist Publications v. Rural Telephone Service" rel="wikipedia" href="http://en.wikipedia.org/wiki/Feist_Publications_v._Rural_Telephone_Service">Feist Publications v. Rural Telephone Service</a>.

This means that when [Karen Coombs asks for several rights for the data](http://www.librarywebchic.net/wordpress/2008/11/05/whos-record-is-it-anyway/):

> 1. Perpetual use - once I’ve downloaded something from OCLC I’ve for the right to use it forever period end of story. This promotes a bunch of things including the LOCKSS principle in the event something happens to OCLC
> 2. Right to share - records I’ve downloaded I’ve got the right to share with others
> This means share in any fashion which the library sees fit, be it Z39.50 access, SRU/W, OAI, or transmission of records via other means
> 3. Right to migrate format - Eventually, libraries may stop using MARC or need to move records into a non-MARC system. So libraries need the right to transform their records
it is simply a matter of the members telling OCLC that's how it's gonna be. For those not under contract with OCLC - you have these rights already!

Therein lies the nub of OCLC's problem. In Europe the database would be afforded legal protection simply by virtue of having taken effort or investment to create, the so called <a title="Sui generis" rel="wikipedia" href="http://en.wikipedia.org/wiki/Database_right">sui-generis</a> right. US law does not have any such protection for databases. I know this because I was heavily involved in the development of the [Open Data Commons PDDL](http://www.opendatacommons.org/odc-public-domain-dedication-and-licence/) and a _real-life lawyer_ told me.

So, other legal remedies that might be used to enforce the policy could include a claim for misappropriation - reaping where one has not sown. This would be under state, rather than federal, law. Though [NBA v. Motorola](http://www.law.cornell.edu/copyright/cases/105_F3d_841.htm) suggests that misappropriation may only apply if for some reason OCLC were unable to continue their service as a result. James Grimmelmann tells me

> RS: If I understand correctly that would mean the only option left for enforcing restrictions on the use of the data would be contractual. Have I missed something obvious?
> 
> JG: I could see a claim for misappropriation under state law -- OCLC has invested effort in creating WorldCat, and unauthorized use would amount to "reaping where one has not sown," in the classic phrase from INS  v. AP.  I doubt, however, that such a claim would succeed, since misappropriation law is almost completely preempted by copyright.  Recent statements of misappropriation doctrine (e.g., NBA v. Motorola) suggest that it might remain available only where the plaintiff's service couldn't be provided at all if the defendant were allowed to do what it's doing.  I don't think that applies here.  So you're right, it's only contractual.
Without any solid legal basis on which to build a license directly, the policy falls back to being simply a contract - and with any contract you can decide if you wish to accept it or not. That, I suspect, is why OCLC wish to turn the existing _guidelines_ into a _binding contract_.

So, OCLC members have the choice as to whether or not they accept the terms of the contract, but what about OpenLibrary? Some have suggested that this change could scupper that effort due to the viral nature of the reference to the usage policy in the records ultimately derived from WorldCat.

Nonsense. This is a truck load of FUD created around the new OCLC policy. Those talking about this possibility are right to be concerned, of course, as that may well be OCLC's intent, but it doesn't hold water. Given that the only enforcement of the policy is as a contract, it is only binding on those who are party to the contract. If OpenLibrary gets records from OCLC member libraries the presence of the policy statement does not create a contract, so OpenLibrary would not be considered party to the contract and not subject to enforcement of it. That is, if they haven't signed a contract with OCLC this policy means nothing to them. They are under no _legal_ obligation to adhere to it.

This is why OCLC are insisting that everyone has an upfront agreement with them. They know they need a contract. James Grimmelmann, who confirmed my interpretations of US Law for me said this in his reply this morning

> JG: Let me add that it is possible for entities that get records from entities that get records from OCLC to be parties to OCLC's contracts; it just requires that everyone involved be meticulous about making everyone else they deal with agree to the contract before giving them records.  But as soon as some entities start passing along records without insisting on a signature up front, there are players in the system who aren't bound, and OCLC has no contractual control over the records they get.
Jonathan Rochkind also concludes that [OCLC's focus on Copyright is bogus](http://bibwild.wordpress.com/2008/11/06/copyright-trivia/):

> All this is to say, the law has changed quite a bit since 1982. If OCLC is counting on a copyright, they should probably have their legal counsel investigate.  I’m not a lawyer, it doesn’t seem good to me–and even if they did have copyright, I can’t see how this would prevent people from taking sets of records anyway, as long as they didn’t take the whole database. But I’m still not a lawyer.
This is OCLC's fear, that the WorldCat will get out of the bag.

The comparisons with other projects that use licenses such as CC or GFDL, and even open-source licenses are also entirely without merit.

To understand why we have to understand the philosophy behind the use of licenses. In OCLC's case the intention is to restrict the usage of the data in order to prevent competing services from appearing. In the case of wikipedia and open-source projects the use of licenses is there to allow the community to fork the project in order to prevent monopoly ownership - i.e. to allow competing versions to appear. There are many versions of Linux, the community is better for that, the good ones thrive and the bad ones die. When a good one goes bad others rise up to take its place, starting from a point just before things went bad. If this is what OCLC want they must allow anyone to take the data, all of it, easily and create a competing service - under the same constraints, that the competing service must also make its data freely available. That's what the [ODC PDDL](http://www.opendatacommons.org/odc-public-domain-dedication-and-licence/) was designed for.

The reason this works in practice is that these are digital goods, in economic terms that means they are [non-rival](http://en.wikipedia.org/wiki/Rivalry_(economics)) - if I give you a copy I still have my own copy, unlike a rival good where giving it to you would mean giving it up myself. OCLC has built a business model based on the notion that its data is a rival good, but the internet, cheap computing and a more mature understanding shows that to be _broken_.

Jonathan Rochkind also talk about a difference in intent in criticising OCLC's comparison with Creative Commons:

> But there remains one very big difference between the CC-BY-NC license you used as a model, and the actual policy. Your actual policy requires some recipients of sharing to enter into an agreement with OCLC (which OCLC can refuse to offer to a particular entity).  The CC-BY-NC very explicitly and intentionally does NOT require this, and even _removes_ the ability of any sharers to require this.
> 
> This is a very big difference, as the entire purpose of the CC licenses is to avoid the possibility of someone requiring such a thing. So your policy may be like CC-BY-NC, while removing it’s very purpose.
Striving to prevent the creation of an alternative database is anti-competitive, reduces innovation and damages the member libraries in order to protect OCLC corp.

> Their [OCLC's record usage guidelines] stated rationale for imposing conditions on libraries' record sharing is that "member libraries have made a major investment in the OCLC Online Union Catalog and expect other member libraries, member networks and OCLC to take appropriate steps to protect the database."
This makes no sense. The investment has been made now. The money is gone. What matters now is how much it costs libraries to continue to do business. Those costs would be reduced by making the data a commodity. Several centralised efforts have the potential to do just that, but the internet itself has that potential too, a potential OCLC has been working against for a long time. Their fight has taken the form of asking member libraries and software authors like [Terry Reese](http://oregonstate.edu/~reeset/blog/archives/574) not to upset the status quo by facilitating easy access to the Z39.50 network and now this change to the policy.

What underlies this is a lack of trust in the members. OCLC know that if an alternative emerged its member libraries would move based on merit, and OCLC clearly doesn't believe it could compete on that level playing field. They are saying that they require a monopoly position in order to be viable.

However, what's good for members and what's good for OCLC are not one and the same thing. Members' investment would be better protected by ensuring that the data is as promiscuously copied as possible. If members were to force OCLC to release the entire database under terms that ensure anyone who takes a copy must also make that copy available to others under the same terms then competition and market would be created. Competition and market are what drive innovation both in features and in cost reduction. In fact, it would create exactly the kind of market that has caused US legislators to refuse a database right, repeatedly. Think about it.

Above all, don't be fooled that this data is anything but yours. The database is yours. All of yours.

If WorldCat were being made available in its entirety like this, it would be entirely reasonable to put clauses in to ensure any union catalogs taking the WorldCat data had to also publish their data reciprocally. That route leads us to a point where a truly global set of data becomes possible - where World(Cat) means _world_ rather than predominantly affluent American libraries.

Surely OCLC, with its expertise in service provision, its understanding of how to analyse this kind of data, its standing in the community and not to forget its substantial existing network of libraries and librarians would continue to carve out a substantial and prestigious role for itself?

I've met plenty of folks from OCLC and they're smart. They'll come up with plenty of stuff worth the membership fee - it just shouldn't be the data you already own.
