---
layout: post.pug 
title: "If I owed you a thousand dollars..."
date: 2008-09-05
tags: ["post","Music"]
legacy_wordpress_id: 277
---

would you still be my friend?

The opening line of _Someone Like That_ by <a href="http://www.debloismusic.com" target="_blank">Deblois</a> (pronounced by saying all the letters, it rhymes with &quot;choice&quot; or &quot;rolls royce&quot;). A rhythmic, mellow natural sound that just seems to melt through the air.

<!-- excerpt -->

Deblois says of herself:

> She plays acoustic roots music; soulful original tunes with an eye for the universal and is backed by the wonderfully funky Big House Band.  

This description really doesn't do her justice. Last time I felt this good about discovering new music was a few years ago when I had the good fortune to stumble across <a href="http://www.amoslee.com/" target="_blank">Amos Lee</a> playing live. I bought <a href="http://cdbaby.com/cd/amoslee2" target="_blank">the album</a> he was selling at the show and have listened to it over and over.

I get the same feeling I got from that album listening to <a href="http://cdbaby.com/cd/deblois" target="_blank">Leviathan</a>.

So, where did I find Deblois? <a href="http://www.last.fm/user/mmmmmrob" target="_blank">Last.fm</a>? A close friend? Nope. Luis Villa. <a href="http://tieguy.org/blog/2008/09/04/deblois-plays-nyc/" target="_blank">Deblois is Luis Villa's sister</a>.

&#160;

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:e267edf0-71a5-43b6-8492-8d051db57b2f" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/Deblois" rel="tag">Deblois</a>,<a href="http://technorati.com/tags/Luis%20Villa" rel="tag">Luis Villa</a>,<a href="http://technorati.com/tags/Music" rel="tag">Music</a>,<a href="http://technorati.com/tags/Folk" rel="tag">Folk</a>,<a href="http://technorati.com/tags/Soul" rel="tag">Soul</a>,<a href="http://technorati.com/tags/Acoustic" rel="tag">Acoustic</a>,<a href="http://technorati.com/tags/Leviathan" rel="tag">Leviathan</a>,<a href="http://technorati.com/tags/Velveteen" rel="tag">Velveteen</a></div>
