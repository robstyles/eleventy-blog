---
layout: post.pug 
title: "Twitter Updates for 2008-09-10"
date: 2008-09-10
tags: ["post","tweet","twitter"]
legacy_wordpress_id: 280
---

<ul class="aktt_tweet_digest">
	<li>@[danja](http://twitter.com/danja) at least the view is good? [in reply to danja](http://twitter.com/danja/statuses/914917534) [#](http://twitter.com/mmmmmrob/statuses/914947297)</li>
	<li>@[bengee](http://twitter.com/bengee) we'll take you The Griffin for a beer then :-) [in reply to bengee](http://twitter.com/bengee/statuses/914978719) [#](http://twitter.com/mmmmmrob/statuses/914984929)</li>
	<li>@[quoll](http://twitter.com/quoll) you're gonna regret that! [in reply to quoll](http://twitter.com/quoll/statuses/915022953) [#](http://twitter.com/mmmmmrob/statuses/915031796)</li>
</ul>

<!-- excerpt -->
