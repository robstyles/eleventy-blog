---
layout: post.pug 
title: "Thomson Reuters EndNote and Zotero"
date: 2008-09-29
tags: ["post","IP Law"]
legacy_wordpress_id: 298
---

Just blogged for work about [Thomson Reuters suing George Mason University over EndNote](http://blogs.talis.com/xiphos/2008/09/29/the-10-million-question/).

It seems slightly surprising that an organisation like Thomson Reuters, who are doing cool stuff elsewhere with projects like [Calais](http://www.opencalais.com/), would be asking for $10 million from an institution like [George Mason University](http://www.gmu.edu/).

<!-- excerpt -->

Unsurprisingly neither the George Mason site nor the Thomson Reuters site have any information regarding the dispute.
<div id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:ba02f46d-f84e-41f0-913f-cd41d7bea9a6" class="wlWriterSmartContent" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a rel="tag" href="http://technorati.com/tags/GMU">GMU</a>,<a rel="tag" href="http://technorati.com/tags/George%20Mason%20University">George Mason University</a>,<a rel="tag" href="http://technorati.com/tags/Thompson%20Reuters">Thomson Reuters</a>,<a rel="tag" href="http://technorati.com/tags/Law">Law</a>,<a rel="tag" href="http://technorati.com/tags/Zotero">Zotero</a>,<a rel="tag" href="http://technorati.com/tags/EndNote">EndNote</a></div>
