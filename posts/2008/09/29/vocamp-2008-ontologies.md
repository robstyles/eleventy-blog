---
layout: post.pug 
title: "Vocamp 2008 Ontologies"
date: 2008-09-29
tags: ["post","Semantic Web"]
legacy_wordpress_id: 299
---

Following on from [vocamp](http://vocamp.org/) I've got the ontologies I'm working with straightened out and about to be published.

First of all, I've made changes to [aiiso](http://purl.org/vocab/aiiso/schema) (the academic institution internal structures ontology) to integrate it with [foaf](http://www.foaf-project.org/). Basically what I've done is deprecate the original aiiso:organisationalUnit and subclass all of the aiiso: classes for departments, faculties etc from foaf:Organization directly. This should allow them to work with other ontologies designed to work with foaf.

<!-- excerpt -->

Next, the organisationalUnit and knowledgeGrouping have been deprecated. The organisationalUnit property has been replaced by organisation and joined by an inverse property of organizationWithin which hopefully makes the intentional direction clear. These are both declared with a domain and range of foaf:Organization, allowing them to be used on other places foaf is being used.

[Chris Wallace](http://thewallaceline.blogspot.com/) and I then spent some time modelling how people participate in institutions. We concluded that we could easily make the participation vocab general enough to use anywhere. Participation specifies the way a foaf:Agent (Organization, Person or Group) participates in a foaf:Group or foaf:Organization.

Having modelled it we discovered that [Knud Moeller](http://kantenwerk.org/) had arrived at the same model as we had when modelling participation in conferences. The only difference we had was that we were modelling different types of role (Chair, Speaker, Chancellor) as individuals and Knud was modelling them as subclasses of a Role class. After much debate I made the decision to spec Participation to use subclassing in the same way Knud had.

Chris then kindly pulled a set of roles out of one of his existing databases and we're publishing those under the aiiso-roles companion schema. This model means that Participation contains the details of how different things relate while different domains are free to create lists of roles that others can then re-use. Aiiso-roles is a first draft of how to do it, rather than a considered list of role titles in academia. Please suggest edits when it gets put online.

I had hoped to spend some time on the [Lifecycle](http://vocab.org/lifecycle/schema) ontology, but with everything else being discussed it just didn't happen. Though I did do a run-through of it with Knud as his PhD is on lifecycle of data and documents online. I hope it can be of some use to him.

The changes aren't published yet, but will be soon, I promise.

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:daed5451-6e43-4a30-b982-16f539551e94" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/vocamp" rel="tag">vocamp</a>,<a href="http://technorati.com/tags/vocamp2008" rel="tag">vocamp2008</a>,<a href="http://technorati.com/tags/vocamp2008oxford" rel="tag">vocamp2008oxford</a>,<a href="http://technorati.com/tags/ontologies" rel="tag">ontologies</a>,<a href="http://technorati.com/tags/rdfs" rel="tag">rdfs</a>,<a href="http://technorati.com/tags/owl" rel="tag">owl</a>,<a href="http://technorati.com/tags/rdf" rel="tag">rdf</a>,<a href="http://technorati.com/tags/linked%20data" rel="tag">linked data</a>,<a href="http://technorati.com/tags/linked%20open%20data" rel="tag">linked open data</a>,<a href="http://technorati.com/tags/aiiso" rel="tag">aiiso</a>,<a href="http://technorati.com/tags/participation" rel="tag">participation</a>,<a href="http://technorati.com/tags/lifecycle" rel="tag">lifecycle</a>,<a href="http://technorati.com/tags/workflow" rel="tag">workflow</a></div>
