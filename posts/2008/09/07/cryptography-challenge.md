---
layout: post.pug 
title: "Cryptography Challenge..."
date: 2008-09-07
tags: ["post","Internet Social Impact","Security And Privacy"]
legacy_wordpress_id: 278
---

<a href="http://www.flickr.com/photos/doctorow/2817314746/" target="_blank"><img style="margin: 5px 5px 5px 0px" src="http://farm4.static.flickr.com/3119/2817314746_1667b5ce6a_m.jpg" align="left" /></a> Cory Doctorow asked Bruce Schneier to give him a hand designing wedding rings. Not an obvious combination until you realise these are crypto rings...

There are two great discussions going on over at both blogs. Cory has asked his crowd to <a href="http://www.boingboing.net/2008/09/05/help-design-a-cipher.html" target="_blank">help design a cipher for his crypto wedding rings</a>. While Bruce simply said <a href="http://www.schneier.com/blog/archives/2008/09/contest_cory_do.html" target="_blank">_Contest: Cory Doctorow's Cipher Wheel Rings_</a>.

<!-- excerpt -->

The discussion on both posts is worth reading. A mixture of things popping up about the similarity between the three rings and the <a href="http://en.wikipedia.org/wiki/Enigma_machine" target="_blank">Enigma machine</a> as well as comments about <a href="http://www.monticello.org/reports/interests/wheel_cipher.html" target="_blank">Jefferson's Wheel Cipher</a>.

Like most things Cory does (or says) there's an element of the slightly bizarre. The prize, a not to be sniffed-at signed copy of Little Brother.

The full set of photos are on <a href="http://www.flickr.com/photos/doctorow/tags/weddingring/" target="_blank">Cory's Flickr account, tagged weddingring</a>.

Comparisons with the Enigma machine, I suspect, are bogus. While there is a visual similarity with the Enigma's wheels the Enigma's cipher was implemented in the electronics within the machine. The letters on the rotors simply enabling the correct starting positions to be selected. The Enigma machines perform a substitution cipher, but with the additional complexity that the substitution pattern changes for each letter through the message. I don't see a way to do that with these rings. There may be rotor ciphers that could be implemented - I don't know.

Jefferson's cipher is a much closer match, a fully manual system consisting of 26 wheels with the alphabet scrambled differently on each one. Similar to the Enigma machine, sender and receiver had to have the order of the wheels synchronised and each letter would use a different substitution scheme, though Jefferson's not as thorough as the Enigma.

As the rings cannot be altered and the alphabet is in order on all three wheels, any attempt that results in one character of cipher text for each character of plain text will be a simple substitution cipher. While it may take several complex steps to arrive at the cipher character it will only take an attacker one step to go back.

So, if you're thinking about this problem seriously there are some things you have to decide on first...

  <ol>   <li>Is the ring considered secret or not?      

This is isn't an unreasonable assumption (putting aside that the details have been published online). It's not that long ago that messages were transferred in plain text relying only on the emperor's seal - made in wax with a ring only he carried.

   </li>    <li>Can you include another secret?      

There are suggestions on the blogs of using most recent blog posts, first pages of known books and other items as keys to drive the cipher. This then involves taking the character from the key and the character from the plaintext and some form of mathematical computation (shifting rings up or down, finding the next dot above or below, that kind of thing) to arrive at the cipher text character.

   </li>    <li>Is the algorithm secret?      

Knowing <a href="http://www.schneier.com/crypto-gram-0205.html#1" target="_blank">Bruce's views on secrecy and security</a>, even suggesting it is pure heresy. Considering the ring to be secret may be part of this, or may not. Some of the ideas I've had fall outside being encryption and really fall into the realm of a 'secret encoding'. But hey, something has to be secret and if it can't be the ring, or the key, the maybe it has to be the algorithm.

   </li> </ol>  

Then, of course, you have to decide what to do with the rings. Any Cryptographic algorithm fulfils one of four basic purposes:

  <ol>   <li>Symmetric Encryption      

These algorithms use the same key to encrypt and decrypt the text. They may use a single algorithm, like ROT13, or they may use a matched pair of algorithms, like many other substitution ciphers.

   </li>    <li>Asymmetric Encryption      

These algorithms use one key to encrypt and another to decrypt. The keys in this case are paired and are usually termed public and private keys. Typically you would use the recipients public key to encrypt and they would use their own private key to decrypt.

   </li>    <li>Non-Decryptable Hashes      

Used mostly for storing passwords (I can't think of another use), these algorithms enable you to reliably convert plain text into a hash with little possibility of reversing the process. For passwords this means you store the hash of the password, then compare the hashed version of any sign-in attempt with the stored hash.

   </li>    <li>Signing      

Signing means adding some kind of addendum to the message that confirms you wrote it. Again this is done using public/private key pairs. You use your private key to create a hashed version of the message which others can then verify using your public key.

   </li> </ol>  

As well as thinking about all of that good stuff it might be worth looking for clues in the design of the rings. Bruce must have had something in mind when designing the rings.

Here are the obvious things to notice:

  <ol>   <li>All three rings feature the alphabet _in order_.</li>    <li>The dot patterns are not random.</li>    <li>The dot pattern follow a 1, 2, 3 pattern.</li>    <li>The dot pattern is not unique (it repeats) when looking across the three rings.</li> </ol>  

Less obvious:

  <ol>   <li>The S across three rings, looking at the dots above, makes dot, dot, dot while the O across the dots on top is three blanks (dash, dash, dash?) this made me go look at <a href="http://en.wikipedia.org/wiki/Morse_code" target="_blank">Morse Code</a> again.</li> </ol>  

Yep, that's all I spotted :-(

I'll be chatting with a coupe of colleagues to see if we can put our heads together and also watching to see what the winner comes up with.

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:19fc5e3a-7c8c-4dd9-a304-01ee152cea6f" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/cryptography" rel="tag">cryptography</a>,<a href="http://technorati.com/tags/cory%20doctorow" rel="tag">cory doctorow</a>,<a href="http://technorati.com/tags/bruce%20schneier" rel="tag">bruce schneier</a>,<a href="http://technorati.com/tags/cryptogram" rel="tag">cryptogram</a>,<a href="http://technorati.com/tags/competition" rel="tag">competition</a>,<a href="http://technorati.com/tags/wedding%20ring" rel="tag">wedding ring</a>,<a href="http://technorati.com/tags/interesting" rel="tag">interesting</a>,<a href="http://technorati.com/tags/inspiring" rel="tag">inspiring</a>,<a href="http://technorati.com/tags/frustrating" rel="tag">frustrating</a></div>
