---
layout: post.pug 
title: "Twitter Updates for 2008-09-03"
date: 2008-09-03
tags: ["post","tweet","twitter"]
legacy_wordpress_id: 272
---

<ul class="aktt_tweet_digest">
	<li>back from lunch with David Peterson and new team member Mark, burger and a pint, not too bad. [#](http://twitter.com/mmmmmrob/statuses/906757605)</li>
	<li>caching framework for rdf that keeps track of dependencies and invalidates the cache when you make changes - very pleased with today's work. [#](http://twitter.com/mmmmmrob/statuses/906970460)</li>
	<li>@[iand](http://twitter.com/iand) scalded_mouths-- [in reply to iand](http://twitter.com/iand/statuses/907181680) [#](http://twitter.com/mmmmmrob/statuses/907184133)</li>
	<li>well, Chrome Beta, running in XP under Vmware on a mac is noticeably faster than FF3 running natively. /me likes v.much [#](http://twitter.com/mmmmmrob/statuses/907204859)</li>
	<li>@[infod1va](http://twitter.com/infod1va) photo is set to private :-( [in reply to infod1va](http://twitter.com/infod1va/statuses/907194114) [#](http://twitter.com/mmmmmrob/statuses/907209637)</li>
	<li>@[iand](http://twitter.com/iand) or maybe that my broadband at home is faster than the pipe at the office? [in reply to iand](http://twitter.com/iand/statuses/907209822) [#](http://twitter.com/mmmmmrob/statuses/907214015)</li>
	<li>@[edsu](http://twitter.com/edsu) v8 is the JavaScript compiler, yes - fast by the looks of it, gmail very responsive for me [in reply to edsu](http://twitter.com/edsu/statuses/907245238) [#](http://twitter.com/mmmmmrob/statuses/907265522)</li>
	<li>right, going to bed now, to read 'The Book Thief' [#](http://twitter.com/mmmmmrob/statuses/907265950)</li>
	<li>in the office, about to get a cup of tea, then wrap cacheing functions in unit tests [#](http://twitter.com/mmmmmrob/statuses/907816782)</li>
	<li>@[ianibbo](http://twitter.com/ianibbo) it's written in a test-first style, just without the tests first... ;-) [in reply to ianibbo](http://twitter.com/ianibbo/statuses/907825619) [#](http://twitter.com/mmmmmrob/statuses/907836554)</li>
	<li>@[PaulMiller](http://twitter.com/PaulMiller) and I'm fed up of LiveWriter because it just has to adorn your markup with it's name, let me know what you find [in reply to PaulMiller](http://twitter.com/PaulMiller/statuses/907838352) [#](http://twitter.com/mmmmmrob/statuses/907851336)</li>
	<li>@[blisspix](http://twitter.com/blisspix) your skepticism may prove to be well placed, but you should try it - it's blisteringly fast [in reply to blisspix](http://twitter.com/blisspix/statuses/907887931) [#](http://twitter.com/mmmmmrob/statuses/907894006)</li>
</ul>
