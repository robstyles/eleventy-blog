---
layout: post.pug 
title: "Twitter Updates for 2008-09-17"
date: 2008-09-17
tags: ["post","tweet","twitter"]
legacy_wordpress_id: 284
---

<ul class="aktt_tweet_digest">
	<li>looking forward to @[danja](http://twitter.com/danja) 's arrival and stay in Brum [#](http://twitter.com/mmmmmrob/statuses/923233737)</li>
	<li>@[mattb](http://twitter.com/mattb) thanks for the link - "I am not stupid, some things are just hard" [in reply to mattb](http://twitter.com/mattb/statuses/923231398) [#](http://twitter.com/mmmmmrob/statuses/923235420)</li>
	<li>now has a friendfeed at <a href="http://friendfeed.com/mmmmmrob" rel="nofollow">http://friendfeed.com/mmmmmrob</a> [#](http://twitter.com/mmmmmrob/statuses/923295546)</li>
	<li>13,227 photos on flickr :-) now just have to tag, rotate and set them all [#](http://twitter.com/mmmmmrob/statuses/923507892)</li>
</ul>

<!-- excerpt -->
