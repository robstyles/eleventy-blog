---
layout: post.pug 
title: "Twitter Updates for 2008-09-04"
date: 2008-09-04
tags: ["post","tweet","twitter"]
legacy_wordpress_id: 275
---

<ul class="aktt_tweet_digest">
	<li>re-tweeing @[dfflanders](http://twitter.com/dfflanders) ROFLMAO: <a href="http://tinyurl.com/6zf82d" rel="nofollow">http://tinyurl.com/6zf82d</a> [#](http://twitter.com/mmmmmrob/statuses/908090401)</li>
	<li>@[dmje](http://twitter.com/dmje) you just need to install the flash plugin that youtube links to. [in reply to dmje](http://twitter.com/dmje/statuses/908253024) [#](http://twitter.com/mmmmmrob/statuses/908271281)</li>
	<li>ugh, subversion tagging and branching. Having to unpick a mistake for Andrew and Nad - very similar to a mistake I made last week. [#](http://twitter.com/mmmmmrob/statuses/909149143)</li>
</ul>

<!-- excerpt -->
