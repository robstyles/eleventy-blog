---
layout: post.pug 
title: "You may think this is boring but..."
date: 2008-09-04
tags: ["post","Non-Fiction Book Review"]
legacy_wordpress_id: 273
---

About a year ago I stole a book. Stole is definitely the right word as I took without asking. In fact the owner probably still doesn't know I've got it. I'll return it today - with an apology.

I shan't name the owner as the book in question, and others like it, carry a certain stigma. A stigma that the reader might be someone not terribly interesting; the kind of person who might, in a different context, show his collection of stamps or dead butterflies.

<!-- excerpt -->

The book in question is _<a href="http://www.amazon.co.uk/Digital-Copyright-Practice-Simon-Stokes/dp/1841135143" target="_blank">Simon Stokes' Digital Copyright Law and Practice</a>_.

Not everyone wants or needs to know why _A &amp; M Records Inc v Napster Inc_ was an important case, or how _Feist Publications v Rural Telephone Service Co_ has a day-to-day impact on what they can do on the web. For those publishing on the web and/or consuming things others have published some knowledge is a useful thing.

This book, however, is not a useful summary for those wanting a quick insight into the rights and wrongs of what they, or their users, may be doing. This book is a quite in-depth discussion of the state of protection offered to data and creative works in different jurisdictions along with explanations of the relevant precedent setting cases.

While Lessig et al talk about how Copyright reform is so desperately needed Stokes simply summarises the protections currently available in an objective and non-judgemental fashion, covering Copyright, Compilation Rights and Database Rights. Yes, he does talk about why Copyright is broken in an internet era, but he does not present his own preferred alternative, choosing instead to describe what he sees as the crossroads Copyright currently stands at.

The licenses that we use everyday - <a href="http://www.gnu.org/copyleft/gpl.html" target="_blank">GPL</a>, <a href="http://www.gnu.org/licenses/lgpl.html" target="_blank">LGPL</a>, <a href="http://www.apache.org/licenses/" target="_blank">Apache Licenses</a>, <a href="http://creativecommons.org/" target="_blank">Creative Commons</a> and <a href="http://www.opendatacommons.org/" target="_blank">Open Data Commons</a> - all rely on the law to underpin them. A sound understanding of those laws can really help in understanding which license really meets your needs. That sound understanding can be acquired by reading Stokes' book. If you can keep your eyes open long enough...
<div id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:f2bf4931-1532-40cf-872e-14160bae3aa6" class="wlWriterSmartContent" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a rel="tag" href="http://technorati.com/tags/law">law</a>,<a rel="tag" href="http://technorati.com/tags/copyright">copyright</a>,<a rel="tag" href="http://technorati.com/tags/digital%20copyright">digital copyright</a>,<a rel="tag" href="http://technorati.com/tags/simon%20stokes">simon stokes</a>,<a rel="tag" href="http://technorati.com/tags/licensing">licensing</a>,<a rel="tag" href="http://technorati.com/tags/creative%20commons">creative commons</a>,<a rel="tag" href="http://technorati.com/tags/open%20data%20commons">open data commons</a>,<a rel="tag" href="http://technorati.com/tags/open%20data">open data</a>,<a rel="tag" href="http://technorati.com/tags/licenses">licenses</a></div>
