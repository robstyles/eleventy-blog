---
layout: post.pug 
title: "Twitter Updates for 2008-09-16"
date: 2008-09-16
tags: ["post","tweet","twitter"]
legacy_wordpress_id: 283
---

<ul class="aktt_tweet_digest">
	<li>@[LuLubonbon](http://twitter.com/LuLubonbon) what sort of job are you looking for? [in reply to LuLubonbon](http://twitter.com/LuLubonbon/statuses/922161123) [#](http://twitter.com/mmmmmrob/statuses/922235816)</li>
	<li>is watching Brewster: <a href="http://tinyurl.com/57quk9" rel="nofollow">http://tinyurl.com/57quk9</a> [#](http://twitter.com/mmmmmrob/statuses/922260496)</li>
	<li>@[tommyh](http://twitter.com/tommyh) you don't have to follow the links to make sure it's spam... [in reply to tommyh](http://twitter.com/tommyh/statuses/922275233) [#](http://twitter.com/mmmmmrob/statuses/922280662)</li>
	<li>Trying to work out why JNI can't find my libraries [#](http://twitter.com/mmmmmrob/statuses/923081459)</li>
	<li>is staring, stunned, at @mauvedeity [#](http://twitter.com/mmmmmrob/statuses/923115905)</li>
	<li>wonders if @[andypowe11](http://twitter.com/andypowe11) means Dewey (http://www.oclc.org/dewey/) or Dewey (http://www.unshelved.com/primer.aspx) [#](http://twitter.com/mmmmmrob/statuses/923119337)</li>
</ul>
