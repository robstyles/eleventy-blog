---
layout: post.pug 
title: "Twitter Updates for 2008-09-01"
date: 2008-09-01
tags: ["post","tweet","twitter"]
legacy_wordpress_id: 266
---

<ul class="aktt_tweet_digest">
	<li>back at work after ten days at home with the kids, great ten days, now wading through emails... [#](http://twitter.com/mmmmmrob/statuses/905511646)</li>
	<li>finished catching up with most of the email from last week, now onto thinking about cacheing strategies [#](http://twitter.com/mmmmmrob/statuses/905580082)</li>
</ul>

<!-- excerpt -->
