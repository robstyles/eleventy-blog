---
layout: post.pug 
title: "Disrupting Class"
date: 2008-09-01
tags: ["post","Non-Fiction Book Review"]
legacy_wordpress_id: 267
---

If ever there was a title that took me straight back to my time at school, this is it. It's not that I was unruly, naughty or deliberately disruptive. Somehow I just always seemed to be the one asking "why?" and "How?" and often being told "That's all you need to know for the test".

Christensen et al are not writing about those pupils who make life difficult for teacher, though, at least not directly and not with the word disruptive. They're talking about the application of Christensen's theories of disruptive innovation to the American education sector, with a focus mostly on K-12.

<!-- excerpt -->

Christensen's theories do get covered briefly in the first few chapters, but a previous reading of The Innovator's Dilemma and ideally The Innovator's Solution as well will hold the reader in better stead.

In The Innovator's Dilemma Christensen explains how large companies such as DEC and GM were disrupted and ultimately destroyed by companies like Sony and Honda. His theory comes down to one important point - where the barrier to consuming a product (cost, availability, size, running expense) is too high there is room for a product to undercut it, even if that product is massively inferior to what the incumbents provide. Christensen terms this _competing with non-consumption_ - something is better than nothing.

Valve radios were disrupted this way by the transistor radio, the mini computer by the PC and big-engined pickups by the Nissans and Toyotas. In order for the disruption to be successful the incoming product is being measure against different criteria to the incumbent - the valve radio measured on its quality of sound, the transistor measured on a teens ability to listen away from their parents; the mini-computer on its processing power, the PC on its potential in the home.

The solution, posited by Christensen in The Innovators Solution is to recognise the factors of a successful disruption as it occurs and to accept that the disruption is inevitable - then set up _independent_ efforts to take advantage of the new market, unencumbered by your existing priorities, culture and markets. These new efforts will ultimately disrupt and destroy your existing business, but better that you do that yourself than someone else does it for you.

So what does that have to do with education? Surely with the many regulations about compulsory schooling, _no child left behind_ and other initiatives there cannot be a large number of non-consumers. Where is the large group of children not currently getting an education, yet sufficiently motivated to want something sub-standard?

Christensen et al analyse several opportunities, from those children wanting to learn subjects off the main curriculum, or take more advanced courses early to those who have failed courses and would otherwise not have the foundations on which to learn in future subjects. The disruptive technology is student-centric learning.

Student-centric learning offers the opportunity for students to learn using different pedagogical styles, those appropriate to their individual learning style and at a pace that suits them. Something that simply isn't possible with a single teacher lecturing to a passive class of thirty students. The facilitator for student-centric learning is, of course, the computer.

Christensen et al explain how the computer remains predominantly a topic of study in schools today, rather than a tool of study. Students use them as tools to write, to research and to communicate - but the established system of education does not make substantial use of the computer as a teaching and learning tool. The reason for this is not hard to find, quite simply most of the computer-based courses that we have seen so far are rubbish. The shining light held-up by Disrupting Class as the beacon is Virtual ChemLab, a computer-based chemistry lab that allows students to learn through experiments without having access to a real-world lab.

The 'et al' with Christensen are Michael B. Horn and Curtis W. Johnson, you can hear them talking through the book and the implications of the theories for the likes of Harvard and MIT on one of Talis' recent podcasts: <a href="http://blogs.talis.com/xiphos/2008/08/19/talis-talks-with-michael-b-horn-and-curtis-w-johnson-about-disrupting-class-and-the-application-of-disruptive-innovation-to-higher-education/" target="_blank">Talis talks with Michael B. Horn and Curtis W. Johnson about ‘Disrupting Class’, and the application of Disruptive Innovation to Higher Education</a>
<div id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:e5c6bb0e-ab93-44ef-b93a-7db7bfc5cdd3" class="wlWriterSmartContent" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a rel="tag" href="http://technorati.com/tags/Disrupting%20Class">Disrupting Class</a>,<a rel="tag" href="http://technorati.com/tags/Innovators%20Dilemma">Innovators Dilemma</a>,<a rel="tag" href="http://technorati.com/tags/Innovators%20Solution">Innovators Solution</a>,<a rel="tag" href="http://technorati.com/tags/Clayton%20Christensen">Clayton Christensen</a>,<a rel="tag" href="http://technorati.com/tags/Michael%20B%20Horn">Michael B Horn</a>,<a rel="tag" href="http://technorati.com/tags/Curtis%20W%20Johnson">Curtis W Johnson</a>,<a rel="tag" href="http://technorati.com/tags/Education">Education</a>,<a rel="tag" href="http://technorati.com/tags/Technology">Technology</a>,<a rel="tag" href="http://technorati.com/tags/Computer%20Based%20Teaching">Computer Based Teaching</a>,<a rel="tag" href="http://technorati.com/tags/Student%20Centric%20Teaching">Student Centric Teaching</a>,<a rel="tag" href="http://technorati.com/tags/Learning">Learning</a>,<a rel="tag" href="http://technorati.com/tags/K-12">K-12</a></div>
