---
layout: post.pug 
title: "Twitter Updates for 2008-09-15"
date: 2008-09-15
tags: ["post","tweet","twitter"]
legacy_wordpress_id: 282
---

<ul class="aktt_tweet_digest">
	<li>back to work after the weekend after 3 days away on school trip with J8. 8 year olds are cool, but not too cool to talk to you :-&gt; [#](http://twitter.com/mmmmmrob/statuses/921825435)</li>
	<li>ugh, firefox just lost keyboard input. haven't seen that bug for a while. [#](http://twitter.com/mmmmmrob/statuses/921852501)</li>
	<li>right, email caught up, time for another cuppa [#](http://twitter.com/mmmmmrob/statuses/921877738)</li>
	<li>Running 3 OSs simultaneously on MacBook Pro with 4Gb, runs fine - Windows XP, Ubuntu and OSX (of course) - nice separation of dev and office [#](http://twitter.com/mmmmmrob/statuses/921918529)</li>
</ul>

<!-- excerpt -->
