---
layout: post.pug 
title: "Throttling Flickr Uploads"
date: 2008-09-15
tags: ["post","Other Technical","commands I have issued"]
legacy_wordpress_id: 281
---

I'm part way through uploading a substantial backlog of photos (11,000+) to Flickr.

I had started out trying to go through them and decide which were worth uploading and which weren't, but that approach was taking far too much time. I needed some help. So I decided that I'd upload all of them and open them up to my family to help with the sorting - asking them to tag the photos with the people in them and also use tags like &quot;TODELETE&quot; to mark those that are just noise and should be thrown out.

<!-- excerpt -->

I could have opened the net wider, but there are photos of my kids and other people's kids in there so just family it is.

I'm <a href="/2008/08/19/flickr-bashing/" target="_blank">using phpFlickr to batch upload photos to Flickr</a> and my script uploads as fast as the bandwidth will allow, though admittedly single-threaded. This meant that I couldn't use it at work, at least not with a clear conscience, so have been uploading from home only. Those 8+ hours a day in the office have been bugging me though, so I was looking for a way to have photos uploading without having a detrimental impact on our connection.

A little googling found an article on <a href="http://www.macosxhints.com/article.php?story=20080119112509736" target="_blank">bandwidth throttling in OSX</a> that showed the basics of using <a href="http://developer.apple.com/documentation/Darwin/Reference/ManPages/man8/ipfw.8.html" target="_blank">ipfw</a> to limit transfer rates. A bit of tweaking and I ended up with

sudo ipfw pipe 1 config bw 128KByte/s   <br />sudo ipfw add 1 pipe 1 dst-ip 68.142.214.24

This limits the upload traffic to api.flickr.com to 128KB/s and means I'm not going to cause anyone a problem.

Sweet.

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:d61a9352-ecf8-4acf-a43a-acf5b4c73311" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/flickr" rel="tag">flickr</a>,<a href="http://technorati.com/tags/flickr%20api" rel="tag">flickr api</a>,<a href="http://technorati.com/tags/upload" rel="tag">upload</a>,<a href="http://technorati.com/tags/phpflickr" rel="tag">phpflickr</a>,<a href="http://technorati.com/tags/ipfw" rel="tag">ipfw</a>,<a href="http://technorati.com/tags/osx" rel="tag">osx</a></div>
