---
layout: post.pug 
title: "FUQ - Frequently Unanswered Questions"
date: 2008-09-21
tags: ["post","Working at Talis"]
legacy_wordpress_id: 291
---

[Rhys](http://uselessofblog.blogspot.com/) decided (probably for no other reason than to fill a slot on his blog) to write [answers to frequently asked questions - but without the questions](http://uselessofblog.blogspot.com/2008/09/frequently-asked-questions.html). Check it out, Rhys has a great, surreal sense-of-humour.

<!-- excerpt -->
