---
layout: post.pug 
title: "Whisky, Whisky, so good they named it twice"
date: 2008-09-24
tags: ["post","event"]
legacy_wordpress_id: 296
---

After lunch here at vocamp, [Tom](http://tomheath.com/) is putting up his initial thoughts about a Whisky ontology. His intention is to be able to describe aspects of Whisky such that you could run a shop from the data.

This is interesting as the things you want to know about a Whisky are pretty specific, it's not a question of price.

<!-- excerpt -->

Tom, like me, thinks about modelling in a nouns-first way. [Chris Wallace](http://thewallaceline.blogspot.com/) is asking if this is the right way to model, as RDFS emphasises properties over classes. This is a very interesting insight and one I hadn't considered. I had assumed that attaching classes to properties, through domains and ranges, was a different syntactic approach to attaching properties to classes. I hadn't considered it as a different philosophical approach.

It is, though. In OO modelling, where we attach properties to classes the property is scoped by the class. That means that you have to have complex hierarchies if you want to the properties on one class to be semantically the same properties as on another class. In RDFS the property is the same by virtue of it being defined independent of classes it is used on.

Putting the philosophical debate aside, and getting back to the Whisky...

The classes Tom defined are:

  <ul>   <li>Whisky     <br />This class is used for the wide variety of Whiskys available. There is a possibility of splitting this into subclasses, or adding additional classes to denote single malts, pure pot stills or blended whiskys.</li>    <li>Region</li>    <li>Distillery</li>    <li>Brand</li>    <li>Finish</li>    <li>Characteristic</li> </ul>  

There was also an assortment of properties proposed, most with specific domains and ranges, but I'll not bother to note those here:

  <ul>   <li>region, owner, produces, motto, headDistiller, waterSource, age, finish, brand, label, abv, colourDesc, strengthLevel, year, grain </li> </ul>  

We then concluded that trying to define this as a group was hard, so tasked [Ian](http://iandavis.com) and Tom to create an initial version based on the brainstorm and some genuine instance data and bring that back to the group.

Ian and Tom focussing on co-evolving the vocab alongside instance data was very interesting to watch. The resulting [Whisky Ontology](http://vocab.org/whisky/terms/) is up on [vocab.org](http://vocab.org)

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:693bc401-be44-4d73-8e33-15dbbb734001" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/vocamp" rel="tag">vocamp</a>,<a href="http://technorati.com/tags/vocamp2008" rel="tag">vocamp2008</a>,<a href="http://technorati.com/tags/vocamp2008oxford" rel="tag">vocamp2008oxford</a>,<a href="http://technorati.com/tags/rdf" rel="tag">rdf</a>,<a href="http://technorati.com/tags/linked%20data" rel="tag">linked data</a>,<a href="http://technorati.com/tags/linked%20open%20data" rel="tag">linked open data</a>,<a href="http://technorati.com/tags/rdfs" rel="tag">rdfs</a>,<a href="http://technorati.com/tags/owl" rel="tag">owl</a>,<a href="http://technorati.com/tags/whisky" rel="tag">whisky</a>,<a href="http://technorati.com/tags/whiskey" rel="tag">whiskey</a>,<a href="http://technorati.com/tags/ontology" rel="tag">ontology</a>,<a href="http://technorati.com/tags/schema" rel="tag">schema</a></div>
