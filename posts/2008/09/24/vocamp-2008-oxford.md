---
layout: post.pug 
title: "Vocamp 2008 Oxford"
date: 2008-09-24
tags: ["post","event"]
legacy_wordpress_id: 294
---

[<img style="margin: 0px 5px 5px 0px" src="http://farm2.static.flickr.com/1030/661835676_ae507b9d16.jpg" alt="" align="left" />](http://www.flickr.com/photos/sacred_destinations/661835676/) We're sitting in [Wolfson College, Oxford](http://www.wolfson.ox.ac.uk/) in a smart little meeting room overlooking a grassy quad. Nice location.

We've just done introductions and I'd be a liar if I said I wasn't intimidated by the number of PhDs in the room. There are people with substantial experience in neuro-science, computer science, insect genetics, philosophy and, of course, lots of semweb experience.

<!-- excerpt -->

Having split into groups, my colleague [Ian Davis](http://iandavis.com/) is just demoing [OpenVocab](http://code.google.com/p/openvocab/) to a few of us. This is an alpha project based on the notion of collaborative schema development - in the wiki style. So far the tool lets you create terms or classes within the OpenVocab namespace and add descriptions, labels, domains and ranges to them.

His intention is to solve what he sees is a problem in publishing linked data - where you are mostly using existing ontologies but need to add a class or a property there is an effort required to publish that single class or property that isn't worth it. OpenVocab is intended for publishing these simple adhoc extensions.

In the wiki vein, it keeps a version history of changes and anyone (within some as yet undecided constraints) can edit the descriptions of terms.

We talked about how this might affect the way people engage with the communities surrounding individual ontologies and the relationship between this and the formalisation of meanings of tags, perhaps. I would prefer to get any extensions I need into the original ontology - and have done with both [sioc](http://sioc-project.org/) and [bibliontology](http://bibliontology.com/), but failing that, OpenVocab offers a quick easy way to get those little things out there.

Discussion moves on to the problem we all seem to have - how to formally describe a composite ontology. By _Composite Ontology_ I mean an ontology that expects that you will use parts of other ontologies - in the way that bibliontology uses [dcterms](http://dublincore.org/documents/dcmi-terms/).

While human readable examples are crucial for this kind of composition, a formal description may also be key if we are to achieve generic creation and editing tools and/or for visualisation or validation.

I was hoping someone would say "ah, yes, you just have to..." but that wasn't forthcoming. Besides making sure they're in human documentation, suggestions currently are
<ol>
	<li>Don't mention them</li>
	<li>Copy the definitions from their own ontologies into yours, but don't relate them in any way</li>
	<li>Create your own and use subclass, subproperty or sameAs to relate them</li>
</ol>
All of these have their own problems, but broadly 2 seems to be the best bet right now - combined with human examples and documentation, of course.

There was a comment that [Peter F. Patel-Schneider](http://www.cs.bell-labs.com/cm/cs/who/pfps/) at Manchester may be doing some work on modularity of ontologies and fine-grained imports. Maybe that's what's needed.

Great lunch, sponsored by [Talis](http://www.talis.com) :-)
<div id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:98fd65ec-b422-4bcc-ba5a-33cf953bdc6a" class="wlWriterSmartContent" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a rel="tag" href="http://technorati.com/tags/vocamp">vocamp</a>,<a rel="tag" href="http://technorati.com/tags/vocamp2008">vocamp2008</a>,<a rel="tag" href="http://technorati.com/tags/vocamp2008oxford">vocamp2008oxford</a>,<a rel="tag" href="http://technorati.com/tags/rdf">rdf</a>,<a rel="tag" href="http://technorati.com/tags/semweb">semweb</a>,<a rel="tag" href="http://technorati.com/tags/rdfs">rdfs</a>,<a rel="tag" href="http://technorati.com/tags/owl">owl</a>,<a rel="tag" href="http://technorati.com/tags/ontologies">ontologies</a>,<a rel="tag" href="http://technorati.com/tags/vocabularies">vocabularies</a>,<a rel="tag" href="http://technorati.com/tags/schema">schema</a></div>
Photo, [eos 015](http://www.flickr.com/photos/sacred_destinations/661835676/) by [Sacred Destinations](http://www.flickr.com/photos/sacred_destinations/) on [Flickr](http://flickr.com)
