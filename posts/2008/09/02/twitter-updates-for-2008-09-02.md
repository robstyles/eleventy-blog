---
layout: post.pug 
title: "Twitter Updates for 2008-09-02"
date: 2008-09-02
tags: ["post","tweet","twitter"]
legacy_wordpress_id: 271
---

<ul class="aktt_tweet_digest">
	<li>have designed caching strategy to try out, now just have to code it - using memcached. [#](http://twitter.com/mmmmmrob/statuses/905854058)</li>
	<li>I _am not_ currently saving Mr Rogers. <a href="http://savemisterrogers.com/" rel="nofollow">http://savemisterrogers.com/</a> [#](http://twitter.com/mmmmmrob/statuses/905861810)</li>
	<li>good day today, good design chats about caching, catching up with what's been done and just nattering :-) [#](http://twitter.com/mmmmmrob/statuses/906053032)</li>
	<li>Dragon's Den is as crap as usual so far tonight. Poor ideas with no IP protection, over-valued businesses, dull, dull, dull [#](http://twitter.com/mmmmmrob/statuses/906071772)</li>
	<li>@[MrJ1971](http://twitter.com/MrJ1971) not dull tv, just dull people and dull ideas [in reply to MrJ1971](http://twitter.com/MrJ1971/statuses/906087914) [#](http://twitter.com/mmmmmrob/statuses/906094327)</li>
	<li>@[kiyanwang](http://twitter.com/kiyanwang) interesting perspective on Google Browser from Luis Villa: <a href="http://tinyurl.com/5jlsky" rel="nofollow">http://tinyurl.com/5jlsky</a> [in reply to kiyanwang](http://twitter.com/kiyanwang/statuses/906565616) [#](http://twitter.com/mmmmmrob/statuses/906566358)</li>
	<li>why did Google choose 'Chrome' when that's already the name of a firefox subsystem? <a href="http://tinyurl.com/2bjge7" rel="nofollow">http://tinyurl.com/2bjge7</a> [#](http://twitter.com/mmmmmrob/statuses/906605931)</li>
	<li>Google name is bizarre given this: <a href="http://developer.mozilla.org/En/Chrome" rel="nofollow">http://developer.mozilla.org/En/Chrome</a> [#](http://twitter.com/mmmmmrob/statuses/906607777)</li>
	<li>awesome communication: <a href="http://tinyurl.com/5frq6f" rel="nofollow">http://tinyurl.com/5frq6f</a> [#](http://twitter.com/mmmmmrob/statuses/906636068)</li>
</ul>
