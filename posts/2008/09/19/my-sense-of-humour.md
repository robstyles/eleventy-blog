---
layout: post.pug 
title: "My Sense-of-Humour"
date: 2008-09-19
tags: ["post","Personal"]
legacy_wordpress_id: 285
---

<a href="http://xkcd.com" target="_blank">XKCD</a> is almost always brilliant. Today though struck a chord with me. Lots of people don't get my sense-of-humour and it gets me into trouble relatively often. Usually because it's destructive, derisive, insulting or disruptive. Anyhows... this is exactly what I have to stop myself from doing every time I see a staple gun.

<a href="http://xkcd.com/478/" target="target"><img src="http://imgs.xkcd.com/comics/the_staple_madness.png" /></a> 

<!-- excerpt -->

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:da2b98aa-671b-46da-bb97-cbdc318afc2f" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/xkcd" rel="tag">xkcd</a>,<a href="http://technorati.com/tags/humor" rel="tag">humor</a>,<a href="http://technorati.com/tags/humour" rel="tag">humour</a>,<a href="http://technorati.com/tags/staple%20gun" rel="tag">staple gun</a></div>
