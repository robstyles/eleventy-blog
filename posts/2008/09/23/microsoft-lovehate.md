---
layout: post.pug 
title: "Microsoft Love/Hate"
date: 2008-09-23
tags: ["post","Working at Talis"]
legacy_wordpress_id: 297
---

I'm often accused at work of being somewhat of a Microsoft hater. It's often fun, and easier, just to go along with it. The only difficulty with that is... it's simply not true.

A few years ago I was working at [Egg](http://www.egg.com) and was one of the key proponents of introducing ASP.Net and C#. At the time the existing infrastructure for the main customer site was also one I'd been involved in choosing - Netscape Application Server (NAS), a Java based app server and [Vignette StoryServer](http://en.wikipedia.org/wiki/StoryServer), a TCL/TK based content management server. After just 3 years of service both were looking under-loved by their respective vendors.

<!-- excerpt -->

Microsoft had been knocking on Egg's door for a long time, and with ASP.Net they presented a very interesting offering. Not only that, but by taking steps to work with ASP.Net we opened doors into Microsoft to get early demos of Longhorn - this is before the re-write, at a time when it was exciting and had far more innovation than was left by the time it shipped.

I even ended up (incorrectly quoted) in a [case-study for Visual Studio and .Net](http://www.microsoft.com/casestudies/casestudy.aspx?casestudyid=51672). Microsoft even sent a small film crew and interviewer to make a [short video on Egg's use of Visual Studio](http://www.microsoft.com/casestudies/resources/files/51672/Egg_Technical_300k.wvx) with us. Of course, the meat of the discussion was edited out, so it looks like all I said was &quot;Visual Studio is _Fantastic_&quot;.

So why am I writing this now? Well, the rather brilliant [Elliott has decided to learn C#](http://townx.org/blog/elliot/yeah-ive-been-learning-asp-net-c), but seems a little defensive about the decision. I just wanted to encourage him to go for it. C# is a very nicely designed language and the .Net framework is a comprehensive, elegant collection of things that makes doing almost anything easier.

At Talis we built some stuff using ASP.Net too, and it was good.

We're not using it now mostly because of cost. We can run much more cheaply for the kind of applications we're building using open-source products. The application we're currently piloting is written in PHP5, running in Apache on CentOS. Free, in every sense of that word. The only thing we pay for is an account on the [Talis Platform](http://www.talis.com/platform/), which handles all of our data storage needs. This keeps costs down and means we can sell the product for less (it's SaaS).

Is PHP as nice to write as C#? I don't think so, but then C# is not as nice to write as Ruby. Is PHP as fast as ASP.Net? I don't think so, although the way we write our apps usually has a bigger impact than the language. Is PHP the right tool for this job? Definitely.

Is C# still worth learning? Definitely.

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:a678b2fa-d7c4-4b20-a681-5713a0d602c2" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/Microsoft" rel="tag">Microsoft</a>,<a href="http://technorati.com/tags/ASP.Net" rel="tag">ASP.Net</a>,<a href="http://technorati.com/tags/C#" rel="tag">C#</a>,<a href="http://technorati.com/tags/PHP" rel="tag">PHP</a>,<a href="http://technorati.com/tags/Linux" rel="tag">Linux</a>,<a href="http://technorati.com/tags/open-source" rel="tag">open-source</a>,<a href="http://technorati.com/tags/TCO" rel="tag">TCO</a></div>
