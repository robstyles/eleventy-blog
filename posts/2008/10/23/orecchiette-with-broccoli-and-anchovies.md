---
layout: post.pug 
title: "Orecchiette with Broccoli and Anchovies"
date: 2008-10-23
tags: ["post","Food"]
legacy_wordpress_id: 324
---

I got a chain email forwarded a week or so ago that I came oh so close to actually joining in with... Then I remembered that no good can ever come of an email chain letter, no matter how good the idea.

So, I've decided to start an equivalent game of blog tag. The topic? One of my favourites - FOOD!

<!-- excerpt -->

I'm going to share with you one of my all time favourite, quick, easy week-day recipes and then tag five others in the hope they'll do the same.

 <h3>Orecchiette with Broccoli and Anchovies (serves 2)</h3> 

<img src="http://farm1.static.flickr.com/23/28980163_4d78faf02f_m.jpg">&nbsp;

First things first - please don't be afraid of the anchovies. The anchovies in here are in oil, not in salt, which means they're not like the dry, salty, crap you get on pizzas. In this recipe they work with the chilli to create a wonderfully warm and comforting meal. There's a heat and depth to the flavours that just makes you feel better about a bad day from the first mouthful to the last.

 <h4>Ingredients</h4> <ul> <li>250g Orecchiette pasta (or another short pasta, but the Orecchiette is best)</li> <li>1 medium head of broccoli, with a good main stalk</li> <li>2 garlic cloves</li> <li>50g can of anchovies in oil</li> <li>A large pinch of dried chilli flakes</li> <li>A substantial quantity of Parmesan, grated</li> <li>A knob of butter</li></ul> <h4>Method</h4> <ul> <li>Put a large pan of water on to boil, you need a pan large enough for the pasta and the broccoli florets, once chopped.</li> <li>Drain a generous tablespoon of the oil from the anchovies into a large frying pan and set on a medium-low heat.</li> <li>Chop the broccoli into small (one mouthful) florets and set aside.</li> <li>Peel or wash the stalk, trim off any dry woody bits and finely chop.</li> <li>Finely chop the garlic.</li> <li>Roughly chop the anchovies (yes, all of them, honestly, trust me).</li> <li>Set the pasta cooking, and set a timer for _three minutes less than the cooking time._</li> <li>Put the chopped broccoli stalks, garlic, anchovies and chilli in the frying pan.</li> <li>Fry the anchovy and broccoli mix gently, stirring occasionally to stop it sticking. If it starts to dry out, turn the heat to low and cover.</li> <li>Three minutes before the end of the pasta's cooking time throw in the broccoli florets to cook.</li> <li>Once cooked, drain the pasta and broccoli, holding back a little of the water and stir in with the anchovy and broccoli mix. Stir through some of the grated parmesan, drop in the knob of butter and spoon over a few tablespoons of the pasta water (you did remember to catch some didn't you?).</li> <li>Cover and leave for a minute.</li> <li>Serve with more of the parmesan over the top and good twist of black pepper.</li></ul> <h4>Drink</h4> 

A deep red wine goes well with this, something with some fruit to it. Maybe a Merlot, Cabernet Sauvignon or Pinotage.

 <h4>Tagging (as in the childrens' game, aka tig in the UK)</h4> 

Would the following people please step up and deliver a recipe and tag five further food-interested people:

 <ul> <li>[mauvedeity](http://uselessofblog.blogspot.com/) Because I bet the [heathen](http://uselessofblog.blogspot.com/2008/10/all-aboard-atheist-bus.html) eats some really tasty stuff.</li> <li>[nadeem.shabir](http://www.virtualchaos.co.uk/) Because he keeps promising to share family recipes (but failing to deliver).</li> <li>[Sarah B.](http://trafficlightmusings.blogspot.com/) Because she's a real foodie (hasn't got kids, so has time and money to cook proper).</li> <li>[Ross](http://dilettantes.code4lib.org/) Because an American perspective would be nice, maybe a nice pumpkin pie ;-).</li> <li>[Zach](http://www.zachbeauvais.com/) Because he lives in the countryside and might suggest something good to do with wild rabbit.</li></ul> 

Please remember to comment/trackback here so we can follow the thread and to tag a further five victims :-)

Photo: [_Orecchiette with Broccoli and Anchovies_](http://www.flickr.com/photos/su-lin/28980163/) by [su-lin](http://www.flickr.com/photos/su-lin/) on [Flickr](http://www.flickr.com/), licensed under Creative Commons Attribution-Noncommercial-No Derivative Works 2.0 Generic

 <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:f80d8cfb-c801-418f-84e6-8404d5ffa6e2" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/food" rel="tag">food</a>,<a href="http://technorati.com/tags/recipes" rel="tag">recipes</a>,<a href="http://technorati.com/tags/pasta" rel="tag">pasta</a>,<a href="http://technorati.com/tags/anchovy%20anchovies" rel="tag">anchovy anchovies</a>,<a href="http://technorati.com/tags/broccoli" rel="tag">broccoli</a>,<a href="http://technorati.com/tags/cooking" rel="tag">cooking</a>,<a href="http://technorati.com/tags/cookery" rel="tag">cookery</a>,<a href="http://technorati.com/tags/blog" rel="tag">blog</a></div>
