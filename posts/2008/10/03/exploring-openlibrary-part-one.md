---
layout: post.pug 
title: "Exploring OpenLibrary Part One"
date: 2008-10-03
tags: ["post","Internet Technical","Talis Technical","Semantic Web"]
legacy_wordpress_id: 308
---

This post also [appears on the n2 blog](http://blogs.talis.com/n2/archives/92).

I thought it was about time I got around to taking a better look at what might be possible with the OpenLibrary data.

<!-- excerpt -->

My plan is to try and convert it into meaningful RDF and see what we can find out about things along the way. The project is an own-time project mostly, so progress isn't likely to be very rapid. Let's see how it goes. I'll diary here as stuff gets done.

To save me typing loads of stuff out here, today's source code is tagged and in the n2 subversion as [day 1 of OpenLibrary](http://n2.talis.com/svn/playground/mmmmmrob/OpenLibrary/tags/day1).

Day one, 3rd October 2008, I downloaded the [authors data from OpenLibrary](http://openlibrary.org/static/jsondump/authors.json.gz) and unzipped it. I'm also downloading the [editions data from OpenLibrary](http://openlibrary.org/static/jsondump/editions.json.gz), but that's bigger (1.8Gb) so I'm playing with the author data while that comes down the tubes.

The data has been exported by OpenLibrary as [JSON](http://www.json.org/), so is pretty easy to work with. I'm going to write some PHP scripts on the command line to mess with it and it looks great for doing that.

Each line of the JSON in the authors file represents a single author, although some authors will have more than one entry. Taking a look at Iain Banks (aka Iain M Banks) we have the following entries:

```

{"name": "Banks, Iain", "personal_name": "Banks, Iain", "key": "\/a\/OL32312A", "birth_date": "1954", "type": {"key": "\/type\/type"}, "id": 81616}
{"name": "Banks, Iain.", "type": {"key": "\/type\/type"}, "id": 3011389, "key": "\/a\/OL954586A", "personal_name": "Banks, Iain."}
{"type": {"key": "\/type\/type"}, "id": 9897124, "key": "\/a\/OL2623466A", "name": "Iain Banks"}
{"type": {"key": "\/type\/type"}, "id": 9975649, "key": "\/a\/OL2645303A", "name": "Iain Banks         "}
{"type": {"key": "\/type\/type"}, "id": 10565263, "key": "\/a\/OL2774908A", "name": "IAIN M. BANKS"}
{"type": {"key": "\/type\/type"}, "id": 10626661, "key": "\/a\/OL2787336A", "name": "Iain M. Banks"}
{"type": {"key": "\/type\/type"}, "id": 12035518, "key": "\/a\/OL3127859A", "name": "Iain M Banks"}
{"type": {"key": "\/type\/type"}, "id": 12078804, "key": "\/a\/OL3137983A", "name": "Iain M Banks         "}
{"type": {"key": "\/type\/type"}, "id": 12177832, "key": "\/a\/OL3160648A", "name": "IAIN M.BANKS"}

```

In total the file contains 4,174,245 entries. First job is to get a more manageable set of data to work with. So, I wrote [a short script to extract 1 line in every 10 from a file](http://n2.talis.com/svn/playground/mmmmmrob/OpenLibrary/tags/day1/src/cli/keep1LineIn10.php). The resulting [sample author data file contains 417,424 entries](http://n2.talis.com/svn/playground/mmmmmrob/OpenLibrary/tags/day1/data/authors.1in10.json). This is more manageable for quick testing of what I'm doing.

So now we can start writing some code to produce some RDF. Given the size of these files, I need to stream the data in and out again in chunks. The easiest format I find for that is [turtle](http://www.w3.org/2007/02/turtle/primer/) which has the added benefit of being human readable. YMMV. Previously I've streamed stuff out using [n-triples](http://www.w3.org/2001/sw/RDFCore/ntriples/). That has some great benefits too, like being able to generate different parts of the graph, for the same subject, in different parts of the file then being them together using a simple command line sort. It's also a great format for chunking the resulting data into reasonable size files as breaking on whole lines doesn't break the graph, whereas with [rdf/xml](http://www.w3.org/TR/rdf-syntax-grammar/) and turtle it does.

So, I may end up dropping back to n-triples, but for now I'm going to use turtle.

I also like working on the command line and love the unix pipes model, so I'll be writing the cli (command line) tools to read from STDIN and write to STDOUT so I can mess with the data using grep, sed, awk, sort, uniq and so on.

First things first, Let's find out what's really in the authors data. Reading the json line by line and converting each line into an associative array is simple in PHP, so let's do that, [keep track of all the keys we find in the arrays and recurse into the nested arrays to look at them](http://n2.talis.com/svn/playground/mmmmmrob/OpenLibrary/tags/day1/src/cli/findArrayKeys.php) - then dump the result out. The arrays contain this set of keys:

```
alternate_names
alternate_names
alternate_names\1
alternate_names\2
alternate_names\3
bio
birth_date
comment
date
death_date
entity_type
fuller_name
id
key
location
name
numeration
personal_name
photograph
title
type
type\key
website
```

So, they have names, birth dates, death dates, alternate names and a few other bits and pieces. And they have a 'key' which turns out to be the resource part of the OpenLibrary url. That's means we can link back into OpenLibrary nice and easy. Going back to our previous Iain Banks examples, we want to create something like this for each one:

```

@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix bio: <http://vocab.org/bio/0.1/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .

<http://example.com/a/OL32312A>
	foaf:Name "Banks, Iain";
	foaf:primaryTopicOf <http://openlibrary.org/a/OL32312A>;
	bio:event <http://example.com/a/OL32312A#birth>;
	a foaf:Person .

<http://example.com/a/OL32312A#birth>
	bio:date "1954";
	a bio:Birth .

```

This gives us a foaf:Person for the author and tracks his birth date using a bio:Birth event. While tracking the birth as a separate entity may seem odd it gives the opportunity to say things about the birth itself. We'll model death dates the same way, for the same reason. I've written some [basic code to generate foaf from the OpenLibrary authors](http://n2.talis.com/svn/playground/mmmmmrob/OpenLibrary/tags/day1/src/cli/authorsToTriples.php).

Linking back to the OpenLibrary url has been done here using foaf:primaryTopicOf. I didn't use owl:sameAs because the url at OpenLibrary is that of a web page, whereas the uri here (http://example.com/a/OL32312A) represents a person. Clearly a person is not the same as a web page that contains information about them.

The only thing worrying me is that the uris we're using are constructed from OpenLibrary's keys. This makes matching them up with other data sources hard. Matching with other data sources requires a natural key, but there's not enough data in these author entries to create one. The best I can do is to create a natural key that will enable people to discover the group of authors that share a name.

```

@prefix mine: <http://example.com/mine/schema#> .
<http://example.com/names/banksiain>
	mine:name_of <http://example.com/a/OL32312A>;
	a mine:Name .

```

These uris will enable me to find authors that share the same name easily, either because they do share the same name or because they're duplicates. The natural key is simply the author's name with any casing, whitespace or punctuation stripped out. That might need to evolve as I start looking at the names in more detail later.

Next step is to look in more detail at the dates in here, we have some simple cases of trailing whitespace or trailing punctuation, but also some more interesting cases of approximate dates or possible ranges - these occur for historical authors mostly. The [complete list of distinct dates within the authors file](http://n2.talis.com/svn/playground/mmmmmrob/OpenLibrary/tags/day1/data/authors.unique_dates.txt) is in svn. If you know anything about dates, feel free to throw me some free advice on what to do with them...
