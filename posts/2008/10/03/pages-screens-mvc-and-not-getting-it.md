---
layout: post.pug 
title: "Pages, Screens, MVC and not getting it..."
date: 2008-10-03
tags: ["post","Internet Technical","Working at Talis","Talis Technical"]
legacy_wordpress_id: 287
---

<a title="MVC Web by mmmmmrob, on Flickr" href="http://www.flickr.com/photos/mmmmmrob/2908629023/"><img class="alignleft" title="MVC Web" src="http://farm4.static.flickr.com/3283/2908629023_be1e70313e_m.jpg" alt="MVC Web" width="240" height="235" /></a>

About two years ago my colleague [Ian Davis](http://iandavis.com) and I were talking about different approaches to building web applications. I was advocating that we use ASP.Net; The framework it provides for nesting controls within controls (server control and user controls) is very powerful. I was describing it as a component-centric approach where we could build pages rapidly by plugging controls together.

<!-- excerpt -->

Ian was describing a page-centric approach, and advocating XSLT (within PHP) as one of several possible solutions. He was suggesting that his approach was both simpler and that we could be more productive using it. Having spent two years working with ASP.Net I was not at all convinced.

Two years on and I think I finally get what he was saying. What can I say, I'm a slow learner. The difference in our opinions was based on two different underlying mental models.

The ASP.Net mental model is that of application software. It tries to bring the way we build windows software to the web. ASP.Net has much of the same feature set that we have if building a Windows Forms app; it's no coincidence that the two models are now called Windows Forms and Web Forms. In this model we think about the forms, or screens, that we have to build and consider the data on which they act as secondary - a parameter to the screen to tell it which user, or expense claim or whatever to load for editing.

In this mental model we end up focussing on the verbs of the problem. We end up with pages called 'edit.aspx', 'createFoo.aspx' and 'view.aspx'; where view is the in the verb form, not the noun. ASP.Net is not unique in this, the same model exists in JSP and many people use PHP this way - it's not specific to any technology, it's a style of thinking and writing.

Ian's mental model is different. Ian's mental model is that of the web. The term URL means Uniform _Resource_ Locator. It doesn't say Uniform _Function_ Locator. A URL is meant to refer to a noun, not a verb. This may seem like an esoteric or pedantic distinction to be making, but it affects the way we think about the structure of our applications and changing the way we think about solving a problem is always interesting.

If we think about URLs as being only nouns, no verbs, then we end up with a URL for every important thing in our site. Those URLs can then be bookmarked and linked easily. We can change code behind the scenes without changing the URLs as the URLs refer to objects that don't change rather than functions that do.

So if URLs refer to nouns, how do we build any kind of functionality? That's tied up in something else that Ian was saying a long time ago - when he asked me "What's the difference between a website and a web API?". My mental model, building web applications the way we build windows apps, was leading me to consider the UI and the API as different things. Ian was seeing them as one and the same. When I was using URIs refer to verbs I found this hard to conceptualise, but thinking about URIs as nouns it becomes clearer - That's what REST is all about. URIs are nouns and then the HTTP verbs give you your functionality.

That realisation and others from working on Linked Open Data means I now think they're one and the same too.

At Talis we've done a few projects this way. Most notably our platform, but also Project Cenote some time ago and a few internal research projects more recently. The clearest of these so far is the product I'm working on right now to support reading lists (read course reserves in the US) in Higher Education. We're currently in pilot with University of Plymouth, here's one of their lists on [Financial Accounting and Reporting](http://lists.lib.plymouth.ac.uk/lists/abf203). The app is built from the ground up as Linked Data and does all the usual content negotiation goodness. We still have work to do on putting in RDFa or micro-formats and cross references between the html and rdf views - so it's not totally linked data yet.

What I've found is that this approach to building web apps beats anything else I've worked with (In roughly chronoligical order - Lotus Domino, Netscape Application Server, PHP3, Vignette StoryServer, ASP, PHP4, ASP.Net, JSP, PHP5).

The model is inherently object-oriented, with every object (at least those of meaning to the outside world) having a URI and every object responding to the standard HTTP verbs, GET, PUT, POST, DELETE. This is object-orientation at the level of the web, not at the level of a server-side language. That's a very different thing to what JSP does, where internally the server-side code may be object-oriented, but the URIs refer to verbs, so look more procedural or perhaps functional.

It's also inherently MVC, with GET requests asking for a view (GET should never cause a change on the server) and PUT, POST and DELETE being handled by controllers. With MVC though, we typically think of that as happening in lots of classes in a single container, like ASP.Net or Tomcat or something like that. This comes from two factors in my experience. Firstly the friction between RDBMS models and object models and secondly the relatively poor performance of most databases. These two things combine to drive people to draw the model into objects alongside the views and controllers.

The result of this is usually that it's not clear how update behaviour should be divided between the model and the controllers and how display behaviour should be divided between the model and the views. As a result the whole thing becomes complex and confused. That doesn't even start to take into account the need for some kind of persistence layer that handles the necessary translation between object model and storage.

We've not done that. We've left the model in a store, in this case a [Talis Platform](http://talis.com/platform) store, but it could be any triple store. That's what the diagram at the top shows, the model staying seperate from views and controllers... and having no behaviour.

A simple example may help, how about tagging something within an application. We have the thing we're tagging, which we'll call http://example.com/resources/foo and the collection of tags attached to foo which we'll call http://example.com/resources/foo/tags. A http GET asking for /resources/foo would be routed to some view code which reads the model and renders a page showing information about foo, and would show the tags too of course. It would also render a form for adding a tag which simply posts the new tag to /resources/foo/tags.

The POST gets routed to some controller logic which is responsible for updating the model.

The UI response to the POST is to show /resources/foo again, which will now have the additional tag. Most web development approaches would simply return the HTML in response to the POST, but we can keep the controller code completely seperate from the view code by responding to a successful POST with a 303 See Other with a location of /resources/foo which will then re-display with the new tag added.

> "The response to the request can be found under a different URI and SHOULD be retrieved using a GET method on that resource. This method exists primarily to allow the output of a POST-activated script to redirect the user agent to a selected resource." [rfc2616](http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html)
This model is working extremely well for us in keeping the code short and very clear.

The way we route requests to code is through the use of a dispatcher, .htaccess in apache sends all requests (except those for which a file exists) to a dispatcher which uses a set of patterns to match the URI to a view or controller depending on the request being a GET or POST.

Ian has started [formalising this approach into a framework](http://iandavis.com/blog/2008/09/publishing-linked-data-with-php) he's called [Paget](http://code.google.com/p/paget/).
