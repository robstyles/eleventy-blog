---
layout: post.pug 
title: "Designing the Moment"
date: 2008-10-07
tags: ["post","Non-Fiction Book Review"]
legacy_wordpress_id: 316
---

[<img style="margin: 5px" src="http://ecx.images-amazon.com/images/I/41AfJf7CygL._SL500_AA240_.jpg" align="left">](http://www.amazon.com/Designing-Moment-Interface-Design-Concepts/dp/0321535081) Robert Hoekman Jr's sequel to _[Designing the Obvious](/2008/08/17/decidedly-obvious/)_, _[Designing the Moment](http://www.amazon.com/Designing-Moment-Interface-Design-Concepts/dp/0321535081)_ presents more insight into the steps that Hoekman uses to evolve designs from something difficult and obtuse to something that is foolproof (Poke yoke) and a pleasure to use.

The examples used are different to Designing the Obvious, and the justification is different. In this case creating a consistent sense of the application being pleasurable to use complementing his previous observations about obvious systems being more productive and making more money.

<!-- excerpt -->

Despite these differences we have two books that could easily have been one. The style of writing is the same, the publishing is identical and the techniques overlap substantially. Despite that both books are well worth reading. Mainly because the value comes mostly from the examples and practical application of techniques such as tabs, concertina interfaces and progressive disclosure.

This is firmly a practitioners book, as is its predecessor. The techniques are explained in ways that you could take and apply to what you're doing right now. What helps that is Hoekman's clear evidence-based engineering approach to this. While there is some creativity in the visual aspects, he applies various interaction design techniques as a science, not an art. This is key to it being repeatably successful.

For those working in Libraries, especially anyone developing public search interfaces, there's a great chapter on advanced search in which Hoekman explains both what is wrong with current approaches to advanced search and also the thinking he went through to produce an alternative - it takes all of 4 pages to explain, including several large pictures, so there's really no excuse for still having poor advanced search functionality.

Don't buy this book expecting something different to the first, it's more of the same - but still very worth reading.

 <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:1f12b927-f972-4be0-9fe2-6d502496b2cd" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/interaction%20design" rel="tag">interaction design</a>,<a href="http://technorati.com/tags/designing%20the%20moment" rel="tag">designing the moment</a>,<a href="http://technorati.com/tags/designing%20the%20obvious" rel="tag">designing the obvious</a>,<a href="http://technorati.com/tags/interface%20design" rel="tag">interface design</a>,<a href="http://technorati.com/tags/robert%20hoekman%20jr" rel="tag">robert hoekman jr</a>,<a href="http://technorati.com/tags/interaction" rel="tag">interaction</a></div>
