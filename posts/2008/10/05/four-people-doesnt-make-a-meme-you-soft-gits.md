---
layout: post.pug 
title: "four people doesn't make a meme you soft gits"
date: 2008-10-05
tags: ["post","Personal","Working at Talis"]
legacy_wordpress_id: 311
---

<img class="alignleft" style="margin: 5px;" title="Authentic Business, the book" src="http://www.authenticbusiness.co.uk/coversmall_jpg_media_public.aspx" alt="" width="150" height="224" />So, [I wrote about a great keynote by Gary Vaynerchuk at Web 2.0 Expo in New York](/2008/10/01/passion-personal-brand-and-doing-what-you-love/), saying that it was a greatly inspiring piece and well worth watching. It contains such great quotes as

> There is no reason in 2008 to do shit you hate, ‘cos you can lose just as much money being happy as hell.
[Nad decided to wade in with one of his usual mega-soft wishy washy group hug lets all live in a fucking commune posts](http://www.virtualchaos.co.uk/blog/2008/10/04/do-what-you-love/) (NB: I love the way Nad has the courage to open up about stuff, publish poetry on his blog and stuff - maybe I should lighten up a bit more too). Nad adds a great reference to [Paul Graham's _How to Do What You Love_](http://www.paulgraham.com/love.html) to the discussion and a couple of choice quotes.

[Rhys then decided to announce that "Do what you love" had achieved meme status](http://uselessofblog.blogspot.com/2008/10/meme-meme-meme.html). Perhaps a little early. &lt;sarcasm&gt;While clearly I am an international thought leader on many topics&lt;/sarcasm&gt;, if one swallow doesn't make a summer than three posts certainly don't constitute a meme.

<!-- excerpt -->

But then... [the venerable (yet completely unqualified, apparently) Danny Ayers joins in](http://hyperdata.org/blog/2008/10/04/meme-time/), also titling his post with the meme word. Danny leaves me unsure of whether he is agreeing or disagreeing with the premise, describing how he is simply compelled to do the things he does.

> So on paper I’m not remarkably insane (at worst alcoholic with elective bipolarity, teensy bit Aspergian maybe), but I do things because they need to be done, IMHO. Might be slow at getting them done, but the compulsion’s there. No grand plan either. I can explain why I think the Semantic Web can help mankind save itself, but that’s not the motivation. I do this, I do that, almost hand to mouth but on projects…that last years. Love never really comes into it, just some arbitrary compulsion (it certainly don’t get you laid).
The alcoholism, bi-polar issues and Aspergers I can certainly relate to, but putting that aside...

This compulsion sounds a lot like love to me, not necessarily easy, not necessarily something you can explain, but definitely a driving force that leads you to do stuff - often stuff you wouldn't otherwise have dreamed of doing.

Anyway, enough of that soft drivel. Stop navel gazing and get on with some work. Let's not just do what we love, let's build a business where people come to work because what the business is doing is what they're passionate about. That's what our library division has been when at its very best; that's what Ian's done with our platform division and that's what we need to do with our new division too.

To achieve that requires a deep understanding of what it is we believe in. A few months back I read a great book that is intended to help people get to grips with just that.

[Authentic Business by Neil Crofts](http://www.authenticbusiness.co.uk/discover/ABhtcarypb/) describes how businesses can run more sustainably and provide far more satisfaction if run in an authentic way. Authentic for Crofts means having a purpose (beyond making money) and to pursue that ethically, honestly and sustainably. Crofts introduces the book like this:

> Do you dream of stepping off the corporate treadmill? Do the politics and greed of corporate life leave you cold? It doesn't have to be like that. Authentic Business shows that business can be positive, fun and meaningful as well as profitable.
He's right, it doesn't have to be like that. The more I think about it though, and the more I read through Crofts examples of Yeo Valley, Howies and Dove Farm the more I think it's about caring about what the business is doing rather than about the business necessarily being "noble" in its cause. No matter how hard I try I couldn't get excited about bringing really great organic yoghurt to market.
