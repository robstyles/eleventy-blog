---
layout: post.pug 
title: "Betamax, VHS and RDF"
date: 2008-10-22
tags: ["post","Random Thought"]
legacy_wordpress_id: 320
---

I was chatting to a guy a few weeks ago, a Technical Account Manager at a reasonably good consultancy. We got chatting as we're both "in IT". I don't actually consider myself to be "in IT" but that's another story.

The conversation was somewhat one-sided, with this chap, let's call him Harry, wanting to tell me all about what he does and his illustrious career with a wide range of technologies. He wasn't interested in what I did, so I listened.

<!-- excerpt -->

Harry explained how the consultancy he works for is doing pretty well, despite the economic situation. His group, a team of technology specialists, were not doing so well, however. Harry doesn't understand why and we quickly moved on.

From not doing well Harry went on to detail his incredible career in technology. Putting in DEC equipment in the mid 80s (when everyone else was putting in PCs), networking several companies with Token Ring (in the late 90s when everyone else was putting in Ethernet), setting up large internal data centres based on Novell and/or IBM OS/2 (when everyone else was putting in Windows). Harry had even thrown out early copies of Microsoft Office in one company to put in Lotus 123 and AmiPro. Great decisions, choosing best-of-breed solutions from great suppliers.

The consistency of these "wrong" decisions seemed to have passed Harry by as he was saying how all of these technologies were "the best", but were subsequently beaten in the marketplace by inferior products. I suspect Harry still has a Betamax video recorder tucked away somewhere.

What's common across all of the products that succeeded is that they are superior in some way that the market defines, not in the way that Harry defined. They were successful in many respects simply because they were successful. That is, success begets success.

Many people are highly skeptical about the Semantic Web and RDF in particular, but in large part it seems to be in roughly the state the web was in the very early 90s. One of the browsers (Tabulator) is something that Tim Berners-Lee has written and is touting around as an example of what could be done, sites on the Semantic Web can still (just) be drawn on a single slide and lots of people are still looking at RDF and saying "it won't work".

But all of that misses the point. It will be successful if we make it successful. That is, it lives and dies not by how it compares to other approaches of representing data, but by how many people publish stuff this way.
