---
layout: post.pug 
title: "Is Effort a Myth?"
date: 2008-10-09
tags: ["post","Working at Talis"]
legacy_wordpress_id: 317
---

I hadn't bothered to read [Seth's Blog](http://sethgodin.typepad.com/seths_blog/) for a while, not sure why other than having 26,132 other unread posts in the feed reader.

But, scratching around for new and interesting things last night, I spotted his post asking _[Is effort a myth?](http://sethgodin.typepad.com/seths_blog/2008/10/is-effort-a-myt.html)_ and was intrigued. Of course, I was hoping, as you secretly are now as you click on the link, that it turns out it is a myth and that you can make loads of money, be hugely popular and find deep and long-lasting love without having to do anything at all.

<!-- excerpt -->

Unfortunately, it turns out you can't.

Arse.

In the absence of an effortless way to to achieve your every desire, Seth suggests

> 1. Delete 120 minutes a day of 'spare time' from your life. This can include TV, reading the newspaper, commuting, wasting time in social networks and meetings. Up to you.  <p>2. Spend the 120 minutes doing this instead:  <p>    * Exercise for thirty minutes.<br>    * Read relevant non-fiction (trade magazines, journals, business books, blogs, etc.)<br>    * Send three thank you notes.<br>    * Learn new digital techniques (spreadsheet macros, Firefox shortcuts, productivity tools, graphic design, html coding)<br>    * Volunteer.<br>    * Blog for five minutes about something you learned.<br>    * Give a speech once a month about something you don't currently know a lot about.  <p>3. Spend at least one weekend day doing absolutely nothing but being with people you love.  <p>4. Only spend money, for one year, on things you absolutely need to get by. Save the rest, relentlessly. 

These are great ideas and several folks at work do this kind of personal development routinely, others don't. Others are just plain lucky ;-)

 <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:7da51b44-d5a3-4f47-b7ae-159f4ec8b32b" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/Seth%20Godin" rel="tag">Seth Godin</a>,<a href="http://technorati.com/tags/Seths%20Blog" rel="tag">Seths Blog</a>,<a href="http://technorati.com/tags/Effort" rel="tag">Effort</a>,<a href="http://technorati.com/tags/Luck" rel="tag">Luck</a></div>
