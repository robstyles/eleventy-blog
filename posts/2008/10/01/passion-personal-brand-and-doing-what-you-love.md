---
layout: post.pug 
title: "Passion, personal brand, and doing what you love..."
date: 2008-10-01
tags: ["post","Personal","Working at Talis"]
legacy_wordpress_id: 300
---

Just found this great video of [Gary Vaynerchuk](http://garyvaynerchuk.com/) keynoting at [Web 2.0 Expo in New York](http://en.oreilly.com/webexny2008/public/content/home). I found it via the [Natural User Interface blog](http://www.multitouch.nl/).

Vaynerchuk's keynote is entitled "Building Personal Brand Within the Social Media Landscape" but the main thrust of it is that everyone should stop doing what they hate and do something they really love.

<!-- excerpt -->

<iframe width="560" height="315" src="https://www.youtube.com/embed/EhqZ0RU95d4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The timing of this is interesting as I've recently had a few conversations with folks at work about loving, or not, what they're doing. At Talis it can be hard to find what it is you really want to do; we expect people to self-organize and have a high degree of self-awareness.

When I worked at Egg, we found very strongly that people joining the company either loved it and thrived or hated it and didn't. This came down to one thing - was what Egg was doing something you were passionate about.

We've come round to a similar culture at Talis, one where we're all actively encouraged to find out what it is that we love doing and supported in getting into that role. Of course, we're not in just any business so if I decided I wanted to become a goat farmer then Talis and I would have to part company.

Knowing what it is that you love doing, and often that's based on [discovering and understanding your strengths](https://www.strengthsfinder.com/), is crucial to understanding if an organizational culture is right for you and even if the organization's business ambitions are right for you.

Having worked in companies that I've loved and in companies I loathed and in roles of both kinds within both kinds of company I'm starting to get clear on what I love doing. Right now my role involves bringing together really cool ideas like [Linked Data](http://linkeddata.org/) and great interaction design with existing problems in higher education. We're building lightweight stuff quickly and easily to test out ideas and constantly looking for the very essence of solutions. And more importantly than all of that, I'm working with people who are smarter than me and who inpsire me with great ideas.

That's what I love, but the very same culture can be experienced by others very differently.

Right now I've found something pretty close to what I love doing - you should do that too :-)
