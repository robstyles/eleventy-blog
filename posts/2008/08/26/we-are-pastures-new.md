---
layout: post.pug 
title: "We are pastures new..."
date: 2008-08-26
tags: ["post","Working at Talis"]
legacy_wordpress_id: 263
---

<a href="http://www.ldodds.com#me" target="_blank">Leigh Dodds</a>, author of <a href="http://www.ldodds.com/blog/" target="_blank">Lost Boy</a> and all-round good egg, has decided that <a href="http://www.ldodds.com/blog/archives/000333.html" target="_blank">joining our platform team as Programme Manager</a> offers him the challenges he's after next.

This is enormously good news for us :-)

<!-- excerpt -->

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:9b3fda90-dfcf-42e5-886f-984a7d95a739" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/working%20at%20talis" rel="tag">working at talis</a>,<a href="http://technorati.com/tags/talis" rel="tag">talis</a>,<a href="http://technorati.com/tags/leigh%20dodds" rel="tag">leigh dodds</a>,<a href="http://technorati.com/tags/ldodds" rel="tag">ldodds</a></div>
