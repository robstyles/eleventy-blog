---
layout: post.pug 
title: "Decidedly obvious"
date: 2008-08-17
tags: ["post","Non-Fiction Book Review","Interaction Design"]
legacy_wordpress_id: 247
---

<a href="http://www.amazon.co.uk/Designing-Obvious-Commonsense-Approach-Application/dp/032145345X/" target="_blank"><img style="margin: 5px 5px 5px 0px" height="210" alt="designing-the-obvious-cover.jpg" src="/assets/designing-the-obvious-cover.jpg" width="140" align="left" /></a>   

Just finished reading _Designing the Obvious_ by Robert Hoekman Jr. It turns out to be quite a simple book, an easy read, bringing together Hoekman's own ideas and thoughts with plenty of anecdotal lessons.

<!-- excerpt -->

Hoekman references all the usual usability heavyweights, Nielsen, Cooper, Krug and so on sometimes agreeing and sometimes offering alternatives views. Personas? Well, maybe, says Hoekman going on to explain that great software comes not from a deep understanding of the users, but from a deep understanding of the activities.

Under what Hoekman calls _Interface Surgery_ he covers the redesign of a form to make it obvious, introducing well thought out approaches to form validation and form layout. The things he suggest are, inline with the books title, obvious - but given that every form you ever use on the web gets this stuff wrong it can hardly be considered _common_ sense.

Usefully I stumbled across bits and pieces that are of immediate use. We're designing some stuff at work right now that requires interaction with a tree structure. We've been struggling a bit with this, with me advocating a fairly standard tree control yet all of us feeling that it doesn't quite work.

Hoekman provides a great explanation of why tree controls don't work. He rightly points out that while they are present in Windows Explorer they are not the default, and as most users don't change the defaults most users will not have come across the tree view regularly. He also decomposes the interactions used in a typical tree view and shows how complex they are. In short, don't use tree views - ever.

Hoekman doesn't leave us high and dry with navigation of these structures though. He describes the _as Columns_ view provided by OS X's Finder and contrasts the interactions of that view with those of the tree view. I'm sold on his explanations and will be looking to try that with the application we're working on.

Overall, good book, easy read, nicely packed with useful ideas.

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:0f6c3349-14e6-41fd-bbb1-caa7b26f2b17" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/interaction%20design" rel="tag">interaction design</a>,<a href="http://technorati.com/tags/designing%20the%20obvious" rel="tag">designing the obvious</a>,<a href="http://technorati.com/tags/interface%20design" rel="tag">interface design</a>,<a href="http://technorati.com/tags/robert%20hoekman%20jr" rel="tag">robert hoekman jr</a>,<a href="http://technorati.com/tags/interaction" rel="tag">interaction</a>,<a href="http://technorati.com/tags/interface" rel="tag">interface</a></div>
