---
layout: post.pug 
title: "Flickr Bashing"
date: 2008-08-19
tags: ["post","Other Technical"]
legacy_wordpress_id: 253
---

I spent some time a little while ago writing a bash interface to the Flickr api, using curl to handle the http interactions.

I was doing it because I have a backlog of around 11,000 photos that need sorting and uploading. To get them backed up I decided to simply upload them all marked as private and tagged as 'toProcess' so that I could then start to go through them online.

<!-- excerpt -->

I'd written most of the raw bash scripts to do the job, but hadn't put enough error handling in, or enough testing, to trust them with my live flickr account.

That was back at Christmas, so coming back to it fresh a thought occurred to me. At work we've been working with php a lot, and php works just dandy as a command line scripting language as well as a web language. Not only that, but php also has a great Flickr library already, it's tested and in production use - <a href="http://phpflickr.com/" target="_blank">phpFlickr</a> by <a href="http://dancoulter.com/" target="_blank">Dan Coulter</a>.

So, with all my photos already organised in folders...

```
#!/usr/bin/php
<?
$path = ini_get('include_path');
$myPath = dirname(__FILE__);
ini_set('include_path', $path . $PATH_SEPERATOR . $myPath);
ini_set('include_path', $path . $PATH_SEPERATOR . $myPath . DIRECTORY_SEPARATOR . 'phpFlickr-2.2.0');
require_once 'phpFlickr.php';
$f = new phpFlickr('_your api key here_', '_your secret here_');
$f-&gt;setToken('_your token here_');
$f-&gt;auth('write');
$searchPath = $myPath.DIRECTORY_SEPARATOR.'later/';
$uploadDir = opendir($searchPath);
$is_public = 0;
$is_friend = 0;
$is_family = 0;
while (false !== ($listing = readdir($uploadDir)))
{
  $isHidden = ((preg_match('/^\./', $listing)) &gt; 0) ? true : false;
  if (is_dir($searchPath.DIRECTORY_SEPARATOR.$listing) && !$isHidden)
  {
    echo $listing."\n";
    $year = (preg_match('/[0-9]{4}/', $listing)) ? substr($listing, 0, 4) : null;
    $setName = $listing;
    $setDir = opendir($searchPath.DIRECTORY_SEPARATOR.$listing);
    $tags = "$year \"$setName\" toprocess";
    while (false !== ($setListing = readdir($setDir)))
    {
      $isJpeg = ((preg_match('/\.jpg$/i', $setListing)) &gt; 0) ? true : false;
      if ($isJpeg)
      {
        $photoFilename = $searchPath.DIRECTORY_SEPARATOR.$listing.DIRECTORY_SEPARATOR.$setListing;
        $response = $f-&gt;async_upload($photoFilename, $setListing, '', $tags, $is_public, $is_friend, $is_family);
        echo $response."\t".$photoFilename."\n";
        rename($photoFilename, $photoFilename.'.done');
        //sleep(1);
        stream_set_blocking(STDIN, FALSE);
        $stdin = fread(STDIN, 1);
        if ($stdin != '')
        {
          echo "Exiting from stdin keypress\n";
          exit(0);
        }
      }
    }
  }
}
closedir($uploadDir);
?>
```

This script wanders through the folders uploading any jpegs it finds, tagging them with the name of the folder they came from as well as a year taken from the first four digits of the folder name. All so simple thanks to Dan Coulter's work.
<div id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:ec95c2ed-02b1-4010-ac90-7d7c2c6b48ba" class="wlWriterSmartContent" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a rel="tag" href="http://technorati.com/tags/flickr">flickr</a>,<a rel="tag" href="http://technorati.com/tags/bash">bash</a>,<a rel="tag" href="http://technorati.com/tags/php">php</a>,<a rel="tag" href="http://technorati.com/tags/dancoulter">dancoulter</a>,<a rel="tag" href="http://technorati.com/tags/phpflickr">phpflickr</a>,<a rel="tag" href="http://technorati.com/tags/upload">upload</a>,<a rel="tag" href="http://technorati.com/tags/batch">batch</a>,<a rel="tag" href="http://technorati.com/tags/bulk">bulk</a></div>
