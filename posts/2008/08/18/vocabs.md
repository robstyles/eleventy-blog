---
layout: post.pug 
title: "vocabs"
date: 2008-08-18
tags: ["post","Semantic Web"]
legacy_wordpress_id: 248
---

<a href="http://www.flickr.com/photos/paulmoody/225608884/" target="_blank"><img style="margin: 5px 5px 5px 0px" src="http://farm1.static.flickr.com/78/225608884_b859efa1da_m.jpg" align="left" /></a> <a href="http://www.virtualchaos.co.uk/blog/" target="_blank">Nadeem</a> and I have been working on several ontologies (for RDF) over the past few months and are intending to publish all of them.

The first to get published is an initial cut of <a href="http://purl.org/vocab/aiiso/schema#" target="_blank">AIISO</a> (pronounced ey-s-oh <a href="http://dictionary.reference.com/help/luna/Spell_pron_key.html" target="_blank">pronunciation key</a>), the <a href="http://purl.org/vocab/aiiso/schema#" target="_blank">Academic Institution Internal Structure Ontology</a>.

<!-- excerpt -->

We put this together really quickly to cover an internal need to document the departmental, school and faculty structures of higher education institutions. As of writing we know of two issues with it...

We named two of the predicates _knowledgeGrouping_ and _organisationalUnit_ after the things they link to. An _OrganisationalUnit_ is any kind of department, faculty etc and a _KnowledgeGrouping_ are collections of knowledge that get taught, things like modules and courses. The problem with the _organisationalUnit_ and _knowledgeGrouping_ properties is two fold, firstly they don't actually describe the relationship and secondly they don't give any indication of direction. So, if we say:

&lt;http://broadminster.org&gt; &lt;aiiso:organisationalUnit&gt; &lt;http://broadminster.org/faculty-of-science&gt;

it's clear to us as people that the faculty of science is a part of Broadminster, but it's not obvious from the ontology that's what's going on. We plan to change that to either use our own _partOf_ property or possibly re-use _dcterms:partOf_ within aiiso.

The other thing we failed to do was to make the appropriate links with <a href="http://xmlns.com/foaf/spec/" target="_blank">FOAF</a>. Aiiso's _OrganisalUnit_ is a specialisation of foaf:Group, so that needs to go into the ontology.

The other thing that's come up in conversation that we're fairly sure we've got right (though others disagree) is that the descriptions are somewhat self-referential. A faculty, for example, is described as follows:

> A Faculty is an OrganisationalUnit that represents a group of people recognised by an OrganisationalUnit as forming a cohesive group referred to by the organisation as a faculty.  

this defines the semantics of being a faculty as nothing more than 'because that's what you say you are'. This caused some debate internally about whether or not the semantics of being a faculty were consistent across institutions - does the term _faculty_ have the same meaning at MIT as it does at Harvard or Virginia?

Perhaps _faculty_ is reasonably consistent, but _college_ and _school_ certain vary substantially and are re-used in areas outside of higher education. My secondary school is certainly not the same kind of thing as the Winchester School of Art, a part of the University of Southampton.

I think the way to solve this is for aiiso to become clearer about its scope, its intention to describe higher education institutions and not other parts of the academic sector. That leaves others free to define ontologies for kindergarten, high school and the rest.

Photo: <a href="http://www.flickr.com/photos/paulmoody/225608884/" target="_blank">MIT's Stata Center night shot</a> by <a href="http://www.flickr.com/photos/paulmoody/" target="_blank">paul+photos=moody</a>

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:19337c06-8dd9-4847-8ddb-353ecfa11cdb" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/aiiso" rel="tag">aiiso</a>,<a href="http://technorati.com/tags/academic" rel="tag">academic</a>,<a href="http://technorati.com/tags/institution" rel="tag">institution</a>,<a href="http://technorati.com/tags/structure" rel="tag">structure</a>,<a href="http://technorati.com/tags/ontology" rel="tag">ontology</a>,<a href="http://technorati.com/tags/ontologies" rel="tag">ontologies</a>,<a href="http://technorati.com/tags/schema" rel="tag">schema</a>,<a href="http://technorati.com/tags/RDF" rel="tag">RDF</a>,<a href="http://technorati.com/tags/OWL" rel="tag">OWL</a>,<a href="http://technorati.com/tags/RDFS" rel="tag">RDFS</a></div>
