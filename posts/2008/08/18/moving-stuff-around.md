---
layout: post.pug 
title: "moving stuff around"
date: 2008-08-18
tags: ["post","Other Technical"]
legacy_wordpress_id: 249
---

Just spent the evening moving this blog over to Wordpress (from MovableType).

All seems to have gone well, though old permalinks will be broken right now, 'til I get to fix it.

<!-- excerpt -->
