---
layout: post.pug 
title: "Unread Blog Count..."
date: 2008-04-30
tags: ["post","Random Thought"]
legacy_wordpress_id: 240
---

Having grouped (read pigeon-holed) the blogs I subscribe to into groups, the current unread counts are:

<a title="Vienna - (10512 unread) by mmmmmrob, on Flickr" href="http://www.flickr.com/photos/mmmmmrob/2454375386/"><img height="265" alt="Vienna - (10512 unread)" src="http://farm3.static.flickr.com/2350/2454375386_e37ac1f138_o.jpg" width="289" /></a> 

<!-- excerpt -->

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:9b3b5705-1929-41d2-b58f-b26c56017633" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/blogs" rel="tag">blogs</a>,<a href="http://technorati.com/tags/unread%20count" rel="tag">unread count</a>,<a href="http://technorati.com/tags/too%20many" rel="tag">too many</a></div>
