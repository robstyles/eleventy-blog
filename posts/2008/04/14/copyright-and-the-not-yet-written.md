---
layout: post.pug 
title: "Copyright and the not-yet-written"
date: 2008-04-14
tags: ["post","IP Law"]
legacy_wordpress_id: 234
---

Various news sites are reporting on an interesting Copyright claim going through the New York courts right now. <a href="http://news.bbc.co.uk/1/hi/entertainment/7346093.stm" target="_blank">The BBC Says</a>:

> Author JK Rowling is to testify in a New York court this week over plans to publish an unofficial Harry Potter encyclopaedia.  

<!-- excerpt -->

The case centers on the suggestion that the encyclopaedia, composed from material on fan-site <a href="http://www.hp-lexicon.org/" target="_blank">The Harry Potter Lexicon</a> and original work by the author, Steve Vander Ark.

The Wall Street Journal's Law Blog has a very <a href="http://blogs.wsj.com/law/2008/04/14/harry-potter-lexicon-case-an-faq/?mod=WSJBlog" target="_blank">fair-use centric piece written by Dan Slater</a>, while others pit their language as &quot;JK Rowling in bid to defend Copyright&quot;. And this distinction is at the heart of the matter. To what extent does JK own the notion of Harry Potter and everything in the world she created and to what extent does she only own the books themselves.

The case will be bringing up some interesting arguments about just what Copyright protects.

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:be50c237-9a64-4d1d-960b-fad4cd9844bf" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/copyright" rel="tag">copyright</a>,<a href="http://technorati.com/tags/harry%20potter" rel="tag">harry potter</a>,<a href="http://technorati.com/tags/jk%20rowling" rel="tag">jk rowling</a>,<a href="http://technorati.com/tags/law" rel="tag">law</a>,<a href="http://technorati.com/tags/harry%20potter%20lexicon" rel="tag">harry potter lexicon</a>,<a href="http://technorati.com/tags/Rowling%20v%20RDR%20Books" rel="tag">Rowling v RDR Books</a></div>
