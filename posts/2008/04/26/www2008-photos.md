---
layout: post.pug 
title: "WWW2008 Photos"
date: 2008-04-26
tags: ["post","Personal"]
legacy_wordpress_id: 239
---

This week's gone so fast, a totally awesome conference, loads of really great people. Papers and papers to go back and read to really get into the meat of what was presented. 

 	There's loads more to say, as it starts to unpack in my head, but for now you'll have to make do with a selection of photos from various sessions, meals, drinks and ceremonies. More photos to come as Nad, Chris, Paul, Tom, Liv and I go out to explore a Beijing a little more before heading home. 

<!-- excerpt -->

 	The photos are on flickr, click to jump over, if you're logged in you can comment and/or tag them - feel free name anyone you know in the tags. 

 		<style type="text/css">
.Album { width: 450px; background: px; padding: 5px;}
.AlbumHeader { text-align:center; padding-left:0px; }
.AlbumPhoto { margin-bottom: 10px; }
.AlbumPhoto p { float: left; padding: 4px 4px 12px 4px; border: 1px solid #ddd; background: #fff; margin: 8px; }
.AlbumPhoto span { float: left; padding: 4px 4px 12px 4px; border: 1px solid #ddd; background: #fff; margin: 8px; }
.AlbumPhoto img { border: none; }
</style> 		<div class="Album"> 			<div class="AlbumHeader"></div><br clear="all"> 			<div class="AlbumPhoto"> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2430628947" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2229/2430628947_a5271fdd71_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2429873949" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3120/2429873949_a686bfd2b1_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2433392050" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2192/2433392050_0b5da91425_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2432978745" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2123/2432978745_8bf0f018f9_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2433795102" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2304/2433795102_9e1a6800cb_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2433794680" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2337/2433794680_51c4f3673d_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2432979207" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3177/2432979207_bfc62d9f08_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2432978083" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2152/2432978083_d7ac46f2c9_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2432978307" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2020/2432978307_567f95c427_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2433794480" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3038/2433794480_77f866d497_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2433794938" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3120/2433794938_b674a78f68_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2433793526" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3245/2433793526_a0493dca79_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2433794000" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3271/2433794000_bced19be46_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2432980293" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2397/2432980293_2324c3c484_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2435620656" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3067/2435620656_e4b7878208_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2434770643" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2163/2434770643_5bea303ef5_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2435608394" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3161/2435608394_1a7d4573ed_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2434781979" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2362/2434781979_9958384166_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2434793473" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2334/2434793473_a694cd887a_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2434789911" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2062/2434789911_fe5247629e_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2434784605" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3045/2434784605_18901a56b7_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2435615462" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2300/2435615462_4f331dc2d2_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2435618040" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3117/2435618040_03d168412f_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2435605418" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3168/2435605418_dc3af563f5_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2435618990" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3006/2435618990_880186925f_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2435595424" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2025/2435595424_26bfc6c7a6_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2435585618" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3103/2435585618_c1efcc8566_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2434780157" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2127/2434780157_b2f35205c7_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2435617098" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2245/2435617098_28296de9f4_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2434773857" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2227/2434773857_c71706dfb2_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2435593316" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3195/2435593316_117251668d_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2434804595" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2103/2434804595_3426e37d8f_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2436009694" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3086/2436009694_623e1e9675_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2437231915" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3160/2437231915_05e3d9be2a_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2438070246" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2288/2438070246_066f6518f2_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2437243569" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3028/2437243569_ec0e4f3114_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2438039542" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2097/2438039542_b5e2c94585_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2437209985" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2211/2437209985_d8136d7e32_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2437239397" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2039/2437239397_3d9289fc5e_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2438074020" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2034/2438074020_3596e2f906_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2437241613" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3071/2437241613_9f202faea6_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2437207049" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3162/2437207049_b8ae65a255_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2437215573" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2061/2437215573_82427efd4f_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2437238449" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2364/2437238449_5da4be89a7_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2438072254" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3146/2438072254_3f56fb07c3_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2438067408" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2341/2438067408_eaa1b5ec30_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2437201877" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2060/2437201877_0f3c8eff18_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2437223145" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2398/2437223145_1da15a9edb_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2437226469" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2335/2437226469_4515fde231_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2437233109" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2259/2437233109_3c09e95846_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2437236849" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2380/2437236849_205ab86c17_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2438056280" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2165/2438056280_03f61a155b_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2438051130" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2096/2438051130_4a4b31352f_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2438074848" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3258/2438074848_51a2b9640e_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2440443809" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2348/2440443809_2d313a6399_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2441196906" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3022/2441196906_a94fe1c9ae_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2440753333" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2006/2440753333_afd5a71b10_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2440829583" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3205/2440829583_f59057027a_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2440692053" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2063/2440692053_d7bb94d903_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2440781731" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3270/2440781731_f8e0b209ac_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2440857409" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3085/2440857409_a8ff428811_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2441490956" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2301/2441490956_353ac44ebb_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2441362394" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3119/2441362394_40a6d65340_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2440595407" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2107/2440595407_5310439f5d_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2440628425" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2084/2440628425_c183e3f349_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2440419417" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2166/2440419417_fda70c1d79_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2440393383" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2050/2440393383_d9d833a7cc_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2440725151" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2417/2440725151_3e79c41abd_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2440885193" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2051/2440885193_bfb7b3799f_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2441393730" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2260/2441393730_dd1e1191bd_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2441338518" target="_blank" class="flickrImage"><img border="0" src="http://farm3.static.flickr.com/2087/2441338518_fc67fde44a_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2441307748" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3048/2441307748_1576fbd820_s.jpg"></a> 				

 					<a href="http://www.flickr.com/photos/mmmmmrob/2441634644" target="_blank" class="flickrImage"><img border="0" src="http://farm4.static.flickr.com/3179/2441634644_abbfd2b263_s.jpg"></a> 				

 			</div><br clear="all"> 			<p align="right"> 				<font style="font-size:10px;color:#AABBCC;">Generated by <a href="http://webdev.yuan.cc/famaker.php" target="_blank" style="color:#AABBCC;text-decoration: underline;">Flickr Album Maker</a></font> 			</p> 		</div> <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:1d121777-3947-47d7-b81a-f744236a082e" style="display:inline; margin:0px; padding:0px 0px 0px 0px;">Technorati Tags: <a href="http://technorati.com/tags/Beijing" rel="tag">Beijing</a>,<a href="http://technorati.com/tags/WWW2008" rel="tag">WWW2008</a>,<a href="http://technorati.com/tags/PhotoAlbum" rel="tag">PhotoAlbum</a>,<a href="http://technorati.com/tags/Talis" rel="tag">Talis</a>,<a href="http://technorati.com/tags/LDOW2008" rel="tag">LDOW2008</a></div>
