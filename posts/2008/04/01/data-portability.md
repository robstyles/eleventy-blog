---
layout: post.pug 
title: "Data Portability"
date: 2008-04-01
tags: ["post","Internet Social Impact"]
legacy_wordpress_id: 226
---

<a href="http://www.dataportability.org/" target="_blank">Data Portability</a> is a great campaign, starting to gain some momentum, about ensuring the data you put into sites like facebook and linkedin is available for you to move between sites as you choose to move. Some major sites, including facebook have agreed to work with the group to develop standards for portable data, but still a long way to go.

So, dull bit done - there's a host of <a href="http://www.youtube.com/results?search_query=dataportability&amp;search_type=" target="_blank">videos going around promoting Data Portability</a>...

<!-- excerpt -->

Here are the best two (IMHO) so far...

Connect, Control, Share, Remix by Michael Pick

  <object width="425" height="355"><param name="movie" value="http://www.youtube.com/v/553DckaDiko&amp;hl=en"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/553DckaDiko&amp;hl=en" type="application/x-shockwave-flash" wmode="transparent" width="425" height="355"></embed></object>  

and Get Your Data Out! by (friend and colleague) Danny Ayers

  <object width="425" height="355"><param name="movie" value="http://www.youtube.com/v/6eGcsGPgUTw&amp;hl=en"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/6eGcsGPgUTw&amp;hl=en" type="application/x-shockwave-flash" wmode="transparent" width="425" height="355"></embed></object>  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:43522be3-d46b-4195-a94f-a234b4af9997" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/dataportability" rel="tag">dataportability</a>,<a href="http://technorati.com/tags/opendata" rel="tag">opendata</a>,<a href="http://technorati.com/tags/privacy" rel="tag">privacy</a>,<a href="http://technorati.com/tags/socialgraph" rel="tag">socialgraph</a>,<a href="http://technorati.com/tags/consumerrights" rel="tag">consumerrights</a></div>
