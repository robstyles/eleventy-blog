---
layout: post.pug 
title: "Yamaha TENORI-ON"
date: 2008-04-09
tags: ["post","Interaction Design"]
legacy_wordpress_id: 233
---

I love specialized controllers, providing an interaction metaphor that works with a keyboard and mouse is always sub-optimal as they are general input tools - that's one of the things that's so awesome about the iPhone, it allows the application to define a much more specialized interaction - look at the way different apps use the multi-touch. That's just the start, too. The iPhone has limited gestures and is 2D, I expect much more to happen with it as interaction designers get to grips with the underlying change in capability that multi-touch provides.

I wrote last year about <a href="/2007/02/16/more-touch-ui-videos" target="_blank">a whole host of multi-touch devices</a> that show off the cream of what's currently possible. One of the stand-outs for me was the <a href="http://www.youtube.com/watch?v=0O70FrnH2JU&amp;mode=related&amp;search=" target="_blank">JazzMutant Lemur, shown here as a music sequencer</a>.

<!-- excerpt -->

I've been keeping half-an-eye on music companies to see what comes out as keyboards, sequencers and digital DJing gear are all natural applications of specialized interfaces. A few days ago I spotted the Yamaha TENORI-ON. An LED based sequencing interface.

Here it is, in a short product demo

  <object width="425" height="355"><param name="movie" value="http://www.youtube.com/v/_SGwDhKTrwU&amp;hl=en"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/_SGwDhKTrwU&amp;hl=en" type="application/x-shockwave-flash" wmode="transparent" width="425" height="355"></embed></object>  And here it is being shown off by it's creator Toshio Iwai.  <object width="425" height="355"><param name="movie" value="http://www.youtube.com/v/6Ov-aqFe7hE&amp;hl=en"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/6Ov-aqFe7hE&amp;hl=en" type="application/x-shockwave-flash" wmode="transparent" width="425" height="355"></embed></object>  

I knew, when I saw it, that I'd seen the idea before. The concept is the same as the Monome devices sold in kit form:

  <object width="425" height="355"><param name="movie" value="http://www.youtube.com/v/F0A8xR8ieek&amp;hl=en"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/F0A8xR8ieek&amp;hl=en" type="application/x-shockwave-flash" wmode="transparent" width="425" height="355"></embed></object>  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:2503823a-06cb-459f-a4f8-20677629f15d" style="display:inline; margin:0px; padding:0px 0px 0px 0px;">Technorati Tags: <a href="http://technorati.com/tags/multi-touch" rel="tag">multi-touch</a>,<a href="http://technorati.com/tags/specialized%20interfaces" rel="tag">specialized interfaces</a>,<a href="http://technorati.com/tags/music" rel="tag">music</a>,<a href="http://technorati.com/tags/sequencing" rel="tag">sequencing</a>,<a href="http://technorati.com/tags/yamaha%20tenori-on" rel="tag">yamaha tenori-on</a></div>
