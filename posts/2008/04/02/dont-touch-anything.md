---
layout: post.pug 
title: "Don't touch anything!"
date: 2008-04-02
tags: ["post","Security And Privacy"]
legacy_wordpress_id: 227
---

Biometrics really annoy me. In a previous life building secure authentication systems for <a href="http://www.egg.com/" target="_blank">Egg</a>, a major internet bank, I did a great deal of research into biometrics. Not only are there issues for a substantial minority (think those with glaucoma, burns victims, those with no hands), whichever biometric you pick. But there is also the fundamental problem that you can't 'reset' a biometric in the same way as a password or a certificate.

Even more annoying is the way in which proponents seem to ignore even the most compelling evidence against biometrics - such as the obvious fact that your fingerprint is neither secure nor secret. Nor is it non-reproducible.

<!-- excerpt -->

Something that <a href="http://www.theregister.co.uk/2008/03/30/german_interior_minister_fingerprint_appropriated/" target="_blank">Germany's interior minister, Wolfgang Schauble has just found out</a>.

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:8f48708b-a506-44a5-bde5-c2ea235afda4" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/security" rel="tag">security</a>,<a href="http://technorati.com/tags/biometric" rel="tag">biometric</a>,<a href="http://technorati.com/tags/fingerprint" rel="tag">fingerprint</a>,<a href="http://technorati.com/tags/germany" rel="tag">germany</a>,<a href="http://technorati.com/tags/authentication" rel="tag">authentication</a></div>
