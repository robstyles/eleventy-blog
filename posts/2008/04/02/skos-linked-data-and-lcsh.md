---
layout: post.pug 
title: "SKOS, Linked Data and LCSH!"
date: 2008-04-02
tags: ["post","Semantic Web"]
legacy_wordpress_id: 228
---

The inimitable <a href="http://inkdroid.org/journal/" target="_blank">Ed Summers</a> has been working inside the <a href="http://www.loc.gov/" target="_blank">Library of Congress</a>, building examples and demonstrators of how LC could be getting themselves into the semantic web, the <a href="http://www.w3.org/DesignIssues/LinkedData.html" target="_blank">linked-data web</a>.

It appears he's got fed up of waiting for the support, permission and infrastructure he so richly deserves to get this data out there and he's been and gone and done something smart outside.

<!-- excerpt -->

<a href="http://lcsh.info" target="_blank">lcsh.info</a> is now a home where you can find a copy of the Library of Congress Subject Headings available in SKOS.

This is a great piece of work and fits in perfectly with the work I've been doing on <a href="/2008/01/25/marc-rdf-and-frbr" target="_blank">Semantic Marc</a>.

After much discussion with Ed he's provided two URI schemes, the primary scheme is based on the LC Control Number, and the second is based on the natural language term of the heading.

So, the LCSH/SKOS URIs for Beer (a subject close to my heart) are:

[http://lcsh.info/label/Beer](http://lcsh.info/label/Beer) which currently redirects to <a title="http://lcsh.info/sh85012832.html" href="http://lcsh.info/sh85012832">http://lcsh.info/sh85012832</a>

The concept URIs then do content negotiation to return either RDF or HTML representations.

The URIs based on the natural language term is something I've bent Ed's ear about constantly, mainly because of the way it makes it possible to link bibliographic data into the LCSH data without the need for a lookup, so I'm chuffed to see it. However, what I badgered Ed for was wrong.

After a long discussion with Tom Heath about stuff I now understand why my suggestion to Ed to simply redirect from the term to the LCCN based URI was wrong - using a redirect basically hides the relationship between the term and its control number form the data layer, leaving the meaning implicit in the HTTP conversation.

What Tom suggested, and I hope edsu can do is to provide a response to the term URI that explains its relationship with the LCCN URI.

Great work Ed.

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:5ffc7896-c832-4a50-ac2f-19ac755f733d" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/Semantic%20Web" rel="tag">Semantic Web</a>,<a href="http://technorati.com/tags/Linked%20Data" rel="tag">Linked Data</a>,<a href="http://technorati.com/tags/LDOW" rel="tag">LDOW</a>,<a href="http://technorati.com/tags/Opendata" rel="tag">Opendata</a>,<a href="http://technorati.com/tags/LCSH" rel="tag">LCSH</a>,<a href="http://technorati.com/tags/Library%20of%20Congress" rel="tag">Library of Congress</a>,<a href="http://technorati.com/tags/SKOS" rel="tag">SKOS</a>,<a href="http://technorati.com/tags/RDF" rel="tag">RDF</a></div>
