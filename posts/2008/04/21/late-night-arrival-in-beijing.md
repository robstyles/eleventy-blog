---
layout: post.pug 
title: "Late Night arrival in Beijing"
date: 2008-04-21
tags: ["post","Personal"]
legacy_wordpress_id: 237
---

<a href="http://www.flickr.com/photos/mmmmmrob/2429873949/" title="IMG_1650.JPG by mmmmmrob, on Flickr"><img src="http://farm4.static.flickr.com/3120/2429873949_a686bfd2b1_m.jpg" width="240" height="160" alt="IMG_1650.JPG" /></a>  

I landed in Beijing at 23:00 last night with [Chris](http://slashdotrobot.wordpress.com/), [Tom](http://my.opera.com/tomheath/blog/), [Nad](http://www.virtualchaos.co.uk/blog/) and [Paul](http://paulmiller.typepad.com/thinking_about_the_future/). We're here for WWW2008 - yep, work sent five of us to Beijing. How cool is that.

<!-- excerpt -->

It wasn't great to find we had no rooms when we arrived at the hotel - we finally got there at 02:00 this morning - it took us 3 hours taxiing around the airport, through immigration, picking up bags and getting a taxi to the hotel. Having no rooms wasn't fun, but watching how each of us chose to handle ourselves was really fun. 2am problems after 36 hours traveling apparently doesn't bring out the best in everyone! ;-) Nobody lost their rag - which was cool.

The hotel did the honorable thing and quickly got us into another hotel, paid for the cabs, accompanied us there and paid the bill, which was much more than they had to do. They then came over this morning and picked us up to move us into our nice, fresh rooms in the right hotel. Bad mistake, good recovery. The view of the Olympic stadium above was taken from my temporary room.

The next problem then, is how to get to see <a href="http://www.bbc.co.uk/iplayer/page/item/b00b07kw.shtml" target="_blank">Planet of the Ood</a> (this week's episode of Doctor Who, the link will expire sometime soon) which was on while I was sat at BHX waiting to board. Problems... I'm in China (known for restrictive networks)... BBC iPlayer doesn't like people outside the UK... BBC iPlayer doesn't allow downloads for OS X users...

So, the solution...

Grab a script that pretends to be an iPhone (<a href="http://po-ru.com/diary/keeping-up-with-iplayer-dl/" target="_blank">iplayer-dl by Paul Battley</a>), thus getting access to the non-DRM MP4 stream. VPN back to the UK, so on a UK network. Find that only specific traffic goes over VPN. Use VPN to SCP iplayer-dl script to a server in the UK. Download Planet of the Ood MP4 to UK Server. Use VPN to SCP Planet of the Ood back to laptop in China.

Getting to talk about Planet of the Ood with my 7 year old, from Beijing? Priceless.

 <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:b2feb1cc-3950-4494-8480-74b9536eb021" style="display:inline; margin:0px; padding:0px 0px 0px 0px;">Technorati Tags: <a href="http://technorati.com/tags/www2008" rel="tag">www2008</a>,<a href="http://technorati.com/tags/beijing" rel="tag">beijing</a>,<a href="http://technorati.com/tags/olympics" rel="tag">olympics</a>,<a href="http://technorati.com/tags/bbc%20iplayer" rel="tag">bbc iplayer</a>,<a href="http://technorati.com/tags/2008" rel="tag">2008</a></div>
