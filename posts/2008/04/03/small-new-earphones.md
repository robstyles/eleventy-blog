---
layout: post.pug 
title: "Small, New Earphones"
date: 2008-04-03
tags: ["post","New Toy"]
legacy_wordpress_id: 231
---

In contrast to a post I wrote a while ago on <a href="/2006/12/08/big-retro-headphones" target="_blank">Big Retro Headphones</a>...

I recently bought a pair of <a href="http://www.shure.com/PersonalAudio/Products/Earphones/SEModels/us_pa_se210_content" target="_blank">Shure SE210 earphones</a>. I've known about Shure for years - in the 90s I had a pair of <a href="http://www.shure.com/ProAudio/Products/PersonalMonitorSystems/us_pro_SCL5_content" target="_blank">Shure personal monitors</a> loaned to me for an evening and they just sound amazing. I'd given up all hope of ever owning a pair of anything like that due to to the cost.

<!-- excerpt -->

The SE210s, though, come in at a (comparatively) reasonable &#163;65 online (or a hundred quid if you go to the Apple store!).

To say I'm impressed with them would be an understatement. As they're noise isolating, they seal into your ear canal. At first that is pretty weird and it took me a few wearings to settle on the rubber tips I currently have fitted. Now I find I can pop them in easily and they're comfortable for a few hours at a time.

The sound, though, is why you'd spend more than a tenner on a pair of earphones - if you aren't interested in the sound then stick with whatever came with your iPod!

Listening to Girl I'm Gonna Miss You, Milli Vanilli, the detailed bass line that is such a major part of the overall sound, the SE210s manage to render the bass line clearly and keep it completely separate from the drum below it. Very nice.

Bass again listening to Ready Or Not by The Course, a dance track based on samples from The Fugees track of the same name. Clear vocals, dynamic synth and deep, rich, clear bass give this track new life after listening to the muddy noise it makes on cheap earphones.

Switching to Classical, a recording of the Intermezzo from Cavalleria Rusticana performed by the Orchestre de Paris sounds wonderful with the violins rising and falling as they play out the emotion. This is one of the aspects in which the noise isolation really excels - the removal of the background noise of the office allows the earphones to convey a greater range of volumes without the quiet portions becoming washed out. For amazing piano, try these with the 2nd Movement of Chopin's Piano Concerto No.1 in E Minor.

Putting all that bullshit about dynamics aside, what these headphones mean is a rediscovery of music in my collection that was just unlistenable in the office, train or plane using cheap earphones. That's worth the money.

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:8bf01911-c602-4d2d-8a41-6227369b1593" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/music" rel="tag">music</a>,<a href="http://technorati.com/tags/sound" rel="tag">sound</a>,<a href="http://technorati.com/tags/earphones" rel="tag">earphones</a>,<a href="http://technorati.com/tags/noise%20isolation" rel="tag">noise isolation</a>,<a href="http://technorati.com/tags/shure%20se210" rel="tag">shure se210</a>,<a href="http://technorati.com/tags/shure" rel="tag">shure</a>,<a href="http://technorati.com/tags/se210" rel="tag">se210</a></div>
