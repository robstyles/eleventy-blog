---
layout: post.pug 
title: "US Copyright Made Simple"
date: 2008-04-03
tags: ["post","Library Tech"]
legacy_wordpress_id: 230
---

The <a href="http://www.wo.ala.org/districtdispatch/?p=421" target="_blank">ALA are selling a lovely little Copyright slider</a> to simplify deciding if a work is in the public domain or not - I haven't held one, so can't comment on build quality, ease-of-use or even the usefulness of the device, but it seems like a great idea.

My first thought on seeing it was that it would be great for them to make this available as a PDF for people to download and make their own - after all the ALA is all about making sure everyone has work to do ;-)

<!-- excerpt -->

The content is licensed under a CC license, but I can't see from the photos which one - does anyone know?

My second thought after finding the instant gratification of a download was not a option was that maybe the format was a little stuffy - I'd much prefer to see an element of fun in the decision&#160; making process. Maybe this form factor would be better:

&#160;

<a href="http://www.flickr.com/photos/glueslabs/13011497/" title="The Cootie Catcher by glueslabs, on Flickr"><img src="http://farm1.static.flickr.com/9/13011497_38f34dd739_m.jpg" width="240" height="160" alt="The Cootie Catcher by glueslabs, on Flickr" /></a><br/>The Cootie Catcher by glueslabs, on Flickr. Licensed under CC-BY-NC

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:0df15118-7778-42fc-8e50-e9fc26179fe0" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/copyright" rel="tag">copyright</a>,<a href="http://technorati.com/tags/ala" rel="tag">ala</a>,<a href="http://technorati.com/tags/public%20domain" rel="tag">public domain</a>,<a href="http://technorati.com/tags/copyright%20status" rel="tag">copyright status</a>,<a href="http://technorati.com/tags/tools" rel="tag">tools</a></div>
