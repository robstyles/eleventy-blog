---
layout: post.pug 
title: "Asshole."
date: 2008-04-03
tags: ["post","Working at Talis","Non-Fiction Book Review"]
legacy_wordpress_id: 232
---

No, not you. At least I hope not.

Like Robert Sutton, I am a self-confessed asshole. That is, from time to time, I have the potential to act like an asshole to the people around me.

<!-- excerpt -->

Also like Robert Sutton I neither want to be considered an asshole nor do I want to work with people I consider to be assholes. So when I read (some time ago) that <a href="http://blog.guykawasaki.com/2006/10/you_have_to_lov.html" target="_blank">Bob Sutton had walked away from a publishing deal with Harvard Business School Press</a> because they wouldn't let him use the word 'asshole' in his title I thought &quot;assholes&quot;.

It's taken the best part of eighteen months for <a href="http://www.amazon.co.uk/Asshole-Rule-Civilised-Workplace-Surviving/dp/1847440002/ref=sr_1_2?ie=UTF8&amp;s=books&amp;qid=1207240516&amp;sr=1-2" target="_blank">The No Asshole Rule</a> to filter to the top of the pile of books next to my bed, so last week I finally read it. I should have read it eighteen months ago, when I found it :-(

The book covers a great amount of research into asshole statistics as well as anecdotal stories of assholedom. Sutton gives simple, highly loaded, tests to self-assess your own asshole score (I scored only moderately) and describes techniques to help you survive asshole encounters. The thrust throughout all the chapters of the book, though, is that nobody should have to put up with assholes, not at home, not at work, not anywhere. And I agree.

He quotes a good few people, Lars Dalgaard CEO of SuccessFactors amongst them:

> respect for the individual, no assholes - it's okay to have one, just don't be one ... because ... assholes stifle performance  

Sutton even includes a chapter on the virtues of being an asshole, albeit with a significant bias as he shows that the virtues are perceived rather than actual. He wraps that chapter up with:

> [life is] too short and too precious to spend our days surrounded by jerks. And despite my failures in this regard, I feel obligated to avoid inflicting my inner jerk on others. I wonder why so many assholes completely miss the fact that all we have on this earth are the days of our lives ... We all die in the end, and despite whatever 'rational' virtues assholes may enjoy, I prefer to avoid spending my days working with mean-spirited jerks and will continue to question why so many of us tolerate, justify, and glorify so much demeaning behaviour from so many people.  

Now that's a sentiment I can get behind.

Fortunately, working at Talis, we have a very good culture overall. People are supportive of each other and enjoy sharing knowledge, learning and getting better at what we do, together. In particular we have people in leadership positions who are (almost) never jerks let alone assholes. And that's a great example to set.

So, to all those who have tolerated me when I have not kept my inner-jerk under control, thank you.

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:f5ed26e7-98be-44f5-8a39-2e1d09b5f6d3" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/bobsutton" rel="tag">bobsutton</a>,<a href="http://technorati.com/tags/the%20no%20asshole%20rule" rel="tag">the no asshole rule</a>,<a href="http://technorati.com/tags/asshole" rel="tag">asshole</a>,<a href="http://technorati.com/tags/how%20many%20times%20can%20I%20use%20the%20word%20'asshole'" rel="tag">how many times can I use the word 'asshole'</a>,<a href="http://technorati.com/tags/work" rel="tag">work</a>,<a href="http://technorati.com/tags/office" rel="tag">office</a>,<a href="http://technorati.com/tags/culture" rel="tag">culture</a></div>
