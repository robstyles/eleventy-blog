---
layout: post.pug 
title: "oooooh, I'm on t'internet"
date: 2008-04-17
tags: ["post","Library Tech"]
legacy_wordpress_id: 236
---

The videos from Code4Lib Con 2008 are now up - thanks to the enormous efforts of Noel Pedens (master videography, library geek, artist and brewer).

Here's me presenting work on finding relationships in MARC data.

<!-- excerpt -->

<iframe src="https://archive.org/embed/code4lib.conf.2008.pres.Styles.MarcRelationships" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

Visit the <a href="http://code4lib.org/conference/2008/schedule" target="_blank">Code4Lib Conference 2008 schedule</a> for the full conference presentations.

&#160;

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:85b80d51-a065-4104-a762-4754dfc518be" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/code4lib" rel="tag">code4lib</a>,<a href="http://technorati.com/tags/code4lib2008" rel="tag">code4lib2008</a>,<a href="http://technorati.com/tags/mmmmmrob" rel="tag">mmmmmrob</a>,<a href="http://technorati.com/tags/rob%20styles" rel="tag">rob styles</a>,<a href="http://technorati.com/tags/semantic%20marc" rel="tag">semantic marc</a>,<a href="http://technorati.com/tags/MARC" rel="tag">MARC</a>,<a href="http://technorati.com/tags/MARC21" rel="tag">MARC21</a>,<a href="http://technorati.com/tags/video" rel="tag">video</a></div>
