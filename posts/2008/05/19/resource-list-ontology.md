---
layout: post.pug 
title: "Resource List Ontology"
date: 2008-05-19
tags: ["post","Ontologies"]
legacy_wordpress_id: 381
---

Working on our current project, [Nad](http://www.virtualchaos.co.uk) and I have been modelling resource lists - think course reading lists, but containing more than just reading material. A list is simply a collection of resources grouped into sections and annotated by the academic to give students guidance when using the list.

A resource list can take much more than just books. We're using Bibliontology to model the resources, so more or less anything can be accurately described. We've requested a couple of additions and are still debating those.

<!-- excerpt -->

The structure of the list is described using a new ontology which we've published on vocab.org, but with a purl.org base URI. The ontology lives here: [http://purl.org/vocab/resourcelist/schema](http://purl.org/vocab/resourcelist/schema)

It also works just fine with the AIISO ontology we published a few days ago - so you can say which parts of your institution are using which lists.
