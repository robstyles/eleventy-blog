---
layout: post.pug 
title: "http://1984, de-referencing George Orwell"
date: 2008-05-11
tags: ["post","Internet Social Impact"]
legacy_wordpress_id: 243
---

<a href="http://www.flickr.com/photos/duncan/225963043/" title="doublethink by duncan, on Flickr"><img src="http://farm1.static.flickr.com/85/225963043_f9ca528cea_m.jpg" width="240" height="180" alt="doublethink" /></a>

Winston sat at his usual table in the Chestnut Tree Cafe, it was unusually busy on this hot, sunny day. The waiter passed his table then returned with the gin bottle; filling his glass with the gin infused with cloves that the cafe was famous for. 

<!-- excerpt -->

The telescreen announced its imminent news of victory with a trumpet fare. It talked about further arrests of disidents, those who had commited crimes against Big Brother. Winston glanced across the room to Big Brother's kindly smiling face looking down at him from the wall, a poster from floor to ceiling filling the cafe with his benevolent presence. 

A commotion outside caused Winston to look out. The Thought Police marching past. They weren't coming into the cafe today, nobody here of interest he guessed. They marched on. 

The Brotherhood he now knew was real, but how could he really know how many others like him there were. O'Brien had told him he would only ever meet one or two others. He would do exactly as he was told. Follow orders. Was there really any hope of overthrowing The Party? He couldn't see how, but that didn't matter. Any act of rebellion however small felt great. 

Maps of the war were now scrolling across the telescreen, they showed Oceania's progress against Eurasia. Oceania was at war with Eurasia. Oceania had always been at war with Eurasia. 

Winston suddenly became aware of another person beside him. Had his face showed sign of what he was thinking? Julia sat down to join him. A swift gesture across the face of the telescreen made it go blank, the narrative in his earpiece stopped. &quot;You're playing that game again, love?&quot; asked Julia. &quot;Yes, you know how good it is&quot; he replied. Julia looked unimpressed. &quot;It sends me to sleep, stops my brain working.&quot; she responded dismissively. &quot;But it's so clever&quot; Winston defended. &quot;it's a view of what this world could have become!&quot; His eyes gleaming, Winston pulled a small book out of his bag. The inscription on the title-page ran: 

THE THEORY AND PRACTICE OF<br />   OLIGARCHICAL COLLECTIVISM 

by Emmanuel Goldstein 

Winston began reading: 

Chapter I 

Ignorance is Strength 

Throughout recorded time, and probably since the end of the Neolithic Age, there have been three kinds of people in the world, the High, the Middle, and the Low. They have been subdivided in many ways, they have borne countless different names, and their relative numbers, as well as their attitude towards one another, have varied from age to age: but the essential structure of society has never altered. Even after enormous upheavals and seemingly irrevocable changes, the same pattern has always reasserted itself, just as a gyroscope will always return to equilibrium, however far it is pushed one way or the other. 

The aims of these groups are entirely irreconcilable... 

Julia's eyes had glazed over, they always did when he talked to her about the tenets behind Big Brother, the massively multi-player game sweeping the world. He stopped reading but it was several seconds before Julia noticed he had stopped. &quot;It just doesn't grab me, sorry&quot; she apologised. 

The waiter arrived, smiled and exchanged pleasantaries. &quot;Sloe today Julia?&quot; he asked. &quot;Yes, please&quot; she replied. The cafe was famous for its clove gin, but the sloe gin was also excellent. In total they had more than 70 flavoured gins and many other drinks besides. When not drinking the gin Winston would often try one of the ever-changing supply of world beers that flowed through the cafe, carefully savouring each one and keeping notes in his online review diary. 

&quot;Oh, but look at this&quot;,  Julia's face lit up as she remembered why she had come to find him. It was a heavy lump of glass, curved on one side, flat on the other, making almost a hemisphere. There was a peculiar softness, as of rainwater, in both the colour and the texture of the glass. At the heart of it, magnified by the curved surface, there was a strange, pink, convoluted object that recalled a rose or a sea anemone. &quot;It's from Mr Charrington's,&quot; Julia went on, &quot;he said I could bring it round to show you. I want to find out all about it.&quot; 

She handed the glass to Winston who waved it across the small camera on the telescreen in front of him. The screen lit up as it took the code from the bottom of the glass and matched it with several photos of the object and a description. It was a paperweight, manufactured in Italy sometime in the early 1930s. The strange pink shape was Coral, not rare, but beautiful none the less. 

The telescreen was changing, new pictures were arriving and more information appearing alongside the initial description as Goldstein (Winston had named his search agent after one of the Game's main characters) trawled the semweb for more references. Within minutes he had a near complete history of the paperweight's manufacture, who had owned it over the years and how it related to other more or less rare collections. It wasn't really worth anything, semi-antiques like these were plentiful and Mr Charrington had a shop full of them, but it was just the kind of pretty thing that Julia loved to decorate their studio with. Winston smiled at her. &quot;I love you. Go and buy it.&quot; he said. 

As Julia left the cafe Winston heard the usual weekend commotion of the Thought Police returning. They were out-of-step now, not marching and there was a great deal of whooping and laughter. He sat back, glass in hand, to hear what had happened. In this mood they were bound to invade the cafe. He was right, around a dozen of them bounced up to the bar. Not one of them could have been more than 25 he thought to himself. Winston vaguely remembered a time in his childhood when people weren't that interested in politics, but not now. Groups of Thought Police, young activists holding politicians and large corporations to account were a common sight. The group laughed and joked, congratulating each other on the day's work. 

Winston pieced together the lively fragments of chatter to conclude they had managed to secure yet another resignation of a corrupt politician. He didn't catch the name or which of the many parties the poor chap had been part of. What he couldn't understand was how any politician thought they could do anything but serve their constituents when the Thought Police, like everyone else, could query every vote, business partnership, gift and expense claim. 

A broad smile broke across his face - maybe they thought it was all just a game. 

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:f6ee6a59-8495-4dcc-904a-bd4d1d50b85c" style="display:inline; margin:0px; padding:0px 0px 0px 0px;">Technorati Tags: <a href="http://technorati.com/tags/orwell" rel="tag">orwell</a>,<a href="http://technorati.com/tags/georgeorwell" rel="tag">georgeorwell</a>,<a href="http://technorati.com/tags/george%20orwell" rel="tag">george orwell</a>,<a href="http://technorati.com/tags/1984" rel="tag">1984</a>,<a href="http://technorati.com/tags/nineteen-eighty-four" rel="tag">nineteen-eighty-four</a>,<a href="http://technorati.com/tags/semweb" rel="tag">semweb</a>,<a href="http://technorati.com/tags/semantic%20web" rel="tag">semantic web</a>,<a href="http://technorati.com/tags/linked%20data" rel="tag">linked data</a>,<a href="http://technorati.com/tags/open%20data" rel="tag">open data</a></div>
