---
layout: post.pug 
title: "Emotional Intelligence in Signage"
date: 2008-05-13
tags: ["post","Interaction Design"]
legacy_wordpress_id: 244
---

Using the 20:20 format (also known as pecha-kucha) Daniel Pink, a staff editor at Wired talks about improving signage through the use of empathy. He has two tenets:

Demonstrate Empathy

<!-- excerpt -->

Encourage Empathy

With 20 slides at 20 seconds each he explains the concept clearly.

<object width="425" height="355"><param name="movie" value="http://www.youtube.com/v/9NZOt6BkhUg&amp;hl=en"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/9NZOt6BkhUg&amp;hl=en" type="application/x-shockwave-flash" wmode="transparent" width="425" height="355"></embed></object>

It strikes me that we could use these techniques when writing the messages that come out of our apps, making them more friendly. Isn't that what Flickr do with the kitten pictures they show during outages?

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:5c012271-bc5a-4c9c-8b76-8ba0244e1cbf" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/empathy" rel="tag">empathy</a>,<a href="http://technorati.com/tags/signage" rel="tag">signage</a>,<a href="http://technorati.com/tags/Daniel%20Pink" rel="tag">Daniel Pink</a>,<a href="http://technorati.com/tags/Wired" rel="tag">Wired</a>,<a href="http://technorati.com/tags/20:20" rel="tag">20:20</a>,<a href="http://technorati.com/tags/pecha-kucha" rel="tag">pecha-kucha</a>,<a href="http://technorati.com/tags/emotional%20intelligence" rel="tag">emotional intelligence</a></div>
