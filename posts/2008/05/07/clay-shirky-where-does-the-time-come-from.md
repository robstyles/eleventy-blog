---
layout: post.pug 
title: "Clay Shirky, Where does the time come from?"
date: 2008-05-07
tags: ["post","Internet Social Impact"]
legacy_wordpress_id: 242
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/AyoNHIl-QLQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/jNCblGv0zjU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<!-- excerpt -->

   <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:d68319e7-d50d-4c0e-98b6-76f46ea3d9fd" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/clay%20shirky" rel="tag">clay shirky</a>,<a href="http://technorati.com/tags/clayshirky" rel="tag">clayshirky</a>,<a href="http://technorati.com/tags/web2.0" rel="tag">web2.0</a>,<a href="http://technorati.com/tags/gin" rel="tag">gin</a>,<a href="http://technorati.com/tags/cognitive%20surplus" rel="tag">cognitive surplus</a>,<a href="http://technorati.com/tags/wikipedia" rel="tag">wikipedia</a></div>
