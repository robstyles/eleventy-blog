---
layout: post.pug 
title: "ldow2008 Open Data Commons"
date: 2008-05-01
tags: ["post","Intellectual Property","Open Data"]
legacy_wordpress_id: 399
---

Another paper from LDOW2008 that I worked on with Tom Heath and Paul Miller. The Open Data Commons licensing is about providing clear licensing for data shared on the web. It's not like Creative Commons because it is for data that doesn't qualify for Copyright protection, whereas Creative Commons relies on an underlying Copyright ownership.[
](http://events.linkeddata.org/ldow2008/papers/08-miller-styles-open-data-commons.pdf)

[Open Data Commons, A License for Open Data](http://events.linkeddata.org/ldow2008/papers/08-miller-styles-open-data-commons.pdf) is predominantly a position paper explaining what's been happening with Open Data Commons and its predecessor the Talis Community License.

<!-- excerpt -->
