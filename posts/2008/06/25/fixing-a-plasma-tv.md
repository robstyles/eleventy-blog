---
layout: post.pug 
title: "Fixing a plasma TV"
date: 2008-06-25
tags: ["post","Other Technical"]
legacy_wordpress_id: 245
---

My father-in-law very, very kindly donated me a plasma TV recently, a 32&quot; Phillips from a few years ago. It was refusing to switch on, the power LED on the front of the screen indicating what the manual calls &quot;protect&quot; mode. This means that the TV has a fault and the LED shows it by blinking red.

Finding information about stuff like this online is always annoying difficult due the variance in search terms. Is that LED blinking, flashing, cycling, going on and off or any of a number of other descriptions. I found several references to this problem on a mix of forums, but eventually found a thread describing <a href="http://www.avforums.com/forums/archive/index.php/t-400763.html" target="_blank">how to fix a phillips plasma tv with a flashing red led</a> on avforums.

<!-- excerpt -->

From there I managed to find that the type of TV I have is covered by the FM23 AC Service Manual, <a href="http://www.eserviceinfo.com/download.php?fileid=12034" target="_blank">available here in the annoying form of a 16 part rar file</a> which unrars to a single PDF.

Most of the successes in the thread seemed to have come from following Barbusa's instructions in the thread linked to above, detailing three capacitors that wear out on the main power board. Diagnosing this was based rather loosely on the fact that if I kept switching it off and back on every time it went into protect it would eventually power up and run just fine. I'm guessing that this comes from the caps managing to build up enough charge over several power cycles.

Capacitors like these are really cheap to replace, I bought some from a local Maplin for the grand total of &#163;1.14. I also bought a new soldering iron, a fine tipped butane one as Barbusa recommended for the job, bringing the bill to a lofty twenty quid - far less than I'd have to pay just to get someone to look at it for repair.

Armed with these new caps and the stupidity necessary to play at soldering inside a high voltage appliance I started stripping it down. Lying the screen flat on its front (on something soft) to remove the stand and screws from the back panel which gives us a great view of the insides - click for larger images.

<a title="internals by mmmmmrob, on Flickr" href="http://www.flickr.com/photos/mmmmmrob/2611573198/"><img height="128" alt="internals" src="http://farm4.static.flickr.com/3110/2611573198_da6601ca8b_m.jpg" width="240" /></a>

In the middle here I've outlined the main power board, it's the one with big capacitors, transformers and the two really big metal heat sinks (one black, one silver) running up the middle of the board.

<a title="power board connectors by mmmmmrob, on Flickr" href="http://www.flickr.com/photos/mmmmmrob/2611583566/"><img height="108" alt="power board connectors" src="http://farm4.static.flickr.com/3109/2611583566_9698213f9b_m.jpg" width="240" /></a>

To remove it, we first have to disconnect all the connectors, I took a few photos so I could put them back, but it seems the cabling routes and different sizes of connectors means that they will only fit one way. There are several screws all around the edge and one in the middle of the board, there a small torq fitting, the same as the case screws not sure what size these are, but they're the smallest torq I've got in my toolbox.

<a title="power board capacitors by mmmmmrob, on Flickr" href="http://www.flickr.com/photos/mmmmmrob/2611591232/"><img height="199" alt="power board capacitors" src="http://farm4.static.flickr.com/3122/2611591232_bcfd664af8_m.jpg" width="240" /></a>

Having removed the board we need to find capacitors 2662, 2663 and 2664. In my 32&quot; screen 2662 is a 1000&#956;F 25v 85&#176;C with 2663 and 2664 both 25v 100&#956;F 25v 85&#176;C. I took Barbusa's advice and bought 105&#176;C rated caps to deal better with the heat. For 2663 and 2664 I couldn't get the 25v caps, so bought the higher rated 50v ones that are fitted in the 42&quot; plasmas. I'm no expert, but thanks to some friendly advice in #electronics on freenode.net I was confident they would be safe.

<a title="Capacitors 2662, 2663 and 2664 by mmmmmrob, on Flickr" href="http://www.flickr.com/photos/mmmmmrob/2610762001/"><img height="107" alt="Capacitors 2662, 2663 and 2664" src="http://farm4.static.flickr.com/3108/2610762001_b5e7dbc92c_m.jpg" width="240" /></a>

Finding them is easy enough, the board is numbered, so with fingers on the capacitor and my new soldering iron on the joints I slowly pulled the caps out and replaced each one in turn.

<a title="underside of 2662, 2663 and 2664 by mmmmmrob, on Flickr" href="http://www.flickr.com/photos/mmmmmrob/2611603492/"><img height="138" alt="underside of 2662, 2663 and 2664" src="http://farm4.static.flickr.com/3125/2611603492_9106982270_m.jpg" width="240" /></a>

The numbering is on both sides of the board, here I have replaced 2662 and I'm just about to replace the other two.

Carefully putting everything back together - deep breath - it all works, powering up first time and running fine. Many thanks to the help of strangers :-&gt;

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:dbcd8618-72c1-45a7-8d53-465cb8df8ce4" style="padding-right: 0px; display: inline; padding-left: 0px; float: none; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/avforums" rel="tag">avforums</a>,<a href="http://technorati.com/tags/phillips" rel="tag">phillips</a>,<a href="http://technorati.com/tags/plasma" rel="tag">plasma</a>,<a href="http://technorati.com/tags/tv" rel="tag">tv</a>,<a href="http://technorati.com/tags/FM23%20AC" rel="tag">FM23 AC</a>,<a href="http://technorati.com/tags/FM24%20AB" rel="tag">FM24 AB</a>,<a href="http://technorati.com/tags/FM33%20AA" rel="tag">FM33 AA</a>,<a href="http://technorati.com/tags/faulty" rel="tag">faulty</a>,<a href="http://technorati.com/tags/repair" rel="tag">repair</a>,<a href="http://technorati.com/tags/fix" rel="tag">fix</a>,<a href="http://technorati.com/tags/capacitors" rel="tag">capacitors</a>,<a href="http://technorati.com/tags/blinking%20led" rel="tag">blinking led</a>,<a href="http://technorati.com/tags/flashing%20led" rel="tag">flashing led</a></div>
