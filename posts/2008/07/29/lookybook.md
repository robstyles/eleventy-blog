---
layout: post.pug 
title: "Lookybook"
date: 2008-07-29
tags: ["post","Internet Social Impact","Interaction Design"]
legacy_wordpress_id: 246
---

<a title="Lookybook | Home by mmmmmrob, on Flickr" href="http://www.flickr.com/photos/mmmmmrob/2714016505/"><img height="165" alt="Lookybook | Home" src="http://farm4.static.flickr.com/3237/2714016505_84440fe132_m.jpg" width="240" /></a>   

A friend emailed me a link to <a href="http://www.lookybook.com/" target="_blank">Lookybook</a> a little while ago and I've been meaning to blog it for a while.

<!-- excerpt -->

It's an interesting experiment as there are a good number of brilliant picture books to read with young children online in all their glory. Not low-res scans or just the first two pages; the full books in great big hi-res, page turning flash.

You can, and I do, sit at the computer and read these with the kids. I suspect, though, that most parents will buy more of these books as they find more and more great books full of big pictures of lorries for the 3 year old boy in their life.

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:6cb8bb67-0c81-45cf-aaf3-d9c9724f4ccd" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/books" rel="tag">books</a>,<a href="http://technorati.com/tags/children" rel="tag">children</a>,<a href="http://technorati.com/tags/Lookybook" rel="tag">Lookybook</a>,<a href="http://technorati.com/tags/web%20design%20examples" rel="tag">web design examples</a>,<a href="http://technorati.com/tags/childrens%20books" rel="tag">childrens books</a></div>
