---
layout: post.pug 
title: "What's in a name?"
date: 2008-03-18
tags: ["post"]
legacy_wordpress_id: 224
---

RePEc, Research Papers in Economics, have been doing some interesting work on [allowing the author community to self-specify variant forms of their name](http://repec.org/blog/?p=41).

This is interesting, the example they give is of John Maynard Keynes:

<!-- excerpt -->

> For John Maynard Keynes (who is not registered), such name variations could be:
> 
>     
> 
> John Maynard Keynes     <br />John M. Keynes      <br />John Keynes      <br />J. M. Keynes      <br />J. Keynes      <br />Keynes, John Maynard      <br />Keynes, John M.      <br />Keynes, John      <br />Keynes, J. M.      <br />Keynes, J.  

Unfortunately Keynes has not registered these himself, I'd love to see some examples from the 16,000 authors who have registered.

This data comes from published papers as well as monograph data, so the variance is a lot higher, I suspect, than occurs solely within libraries and the absence of years of birth (and death) makes it highly ambiguous. RePEc have published a list of [author name homonyms](http://ideas.repec.org/homonyms.html). Even within published papers on economics this is a reasonably problematic list.

I've been working on very similar issues with library data, [trying to analyze the meaning in marc data](/2008/01/25/marc-rdf-and-frbr).

One of the key things for me was developing techniques for dealing with ambiguity, allowing outside systems with limited data to receive the most specific answer possible, while giving less specific answers for less specific matches.

  <div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:7e0d87d3-34d4-4147-9e7f-7456531748eb" style="padding-right: 0px; display: inline; padding-left: 0px; padding-bottom: 0px; margin: 0px; padding-top: 0px">Technorati Tags: <a href="http://technorati.com/tags/marc" rel="tag">marc</a>,<a href="http://technorati.com/tags/library%20data" rel="tag">library data</a>,<a href="http://technorati.com/tags/semantics" rel="tag">semantics</a>,<a href="http://technorati.com/tags/homonyms" rel="tag">homonyms</a>,<a href="http://technorati.com/tags/author%20names" rel="tag">author names</a>,<a href="http://technorati.com/tags/RePEc" rel="tag">RePEc</a>,<a href="http://technorati.com/tags/Talis" rel="tag">Talis</a></div>
