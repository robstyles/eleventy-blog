---
layout: post.pug 
title: "SpiderFriend"
date: 2008-03-20
tags: ["post","Interaction Design"]
legacy_wordpress_id: 225
---

I love graph viewers and visualizers and being able to play with them on your own data is better than anything :-)

SpiderFriend is a visualizer for your facebook friends network, it graphs you and your immediate friends on the edge of a sphere with connections between y'all. Nice, but the visual treatment doesn't allow you to see clearly who knows who.

<!-- excerpt -->

I'd also like to see a way to explore the network, clicking on a friend and seeing their friends and so on. I guess facebook apps can't wander around that easily :-(

Take a look at the short video below or download and play yourself from <a title="http://www.butant.com/spiderfriend/" href="http://www.butant.com/spiderfriend/">http://www.butant.com/spiderfriend/</a>

<object width="425" height="355"><param name="movie" value="http://www.youtube.com/v/ixTy5otxL9I&amp;hl=en"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/ixTy5otxL9I&amp;hl=en" type="application/x-shockwave-flash" wmode="transparent" width="425" height="355"></embed></object>

<div class="wlWriterSmartContent" id="scid:0767317B-992E-4b12-91E0-4F059A8CECA8:41a526e8-81a8-4d90-9fb4-94c497796c93" style="display:inline; margin:0px; padding:0px 0px 0px 0px;">Technorati Tags: <a href="http://technorati.com/tags/spiderfriend" rel="tag">spiderfriend</a>,<a href="http://technorati.com/tags/facebook" rel="tag">facebook</a>,<a href="http://technorati.com/tags/visualizers" rel="tag">visualizers</a>,<a href="http://technorati.com/tags/socialgraph" rel="tag">socialgraph</a></div>
