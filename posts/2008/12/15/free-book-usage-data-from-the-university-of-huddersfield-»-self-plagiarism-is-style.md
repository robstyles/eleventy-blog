---
layout: post.pug 
title: "Free book usage data from the University of Huddersfield » \"Self-plagiarism is style\""
date: 2008-12-15
tags: ["post","Library Tech"]
legacy_wordpress_id: 368
---

> I'm very proud to announce that Library Services at the University of Huddersfield has just done something that would have perhaps been unthinkable a few years ago: we've just released a major portion of our book circulation and recommendation data under an [Open Data Commons](http://www.opendatacommons.org/)/[CC0](http://wiki.creativecommons.org/CCZero) licence. In total, there's data for over 80,000 titles derived from a pool of just under 3 million circulation transactions spanning a 13 year period.
[Free book usage data from the University of Huddersfield](http://www.daveyp.com/blog/archives/528).

This is great to see from my POV on two levels, first a great set of data that has the potential to be really useful and secondly a clear statement of the terms under which it's released.

<!-- excerpt -->

Awesome Dave Pattern.
