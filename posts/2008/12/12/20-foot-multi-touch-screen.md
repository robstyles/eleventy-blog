---
layout: post.pug 
title: "20 foot multi-touch screen"
date: 2008-12-12
tags: ["post","Other Technical","Interaction Design"]
legacy_wordpress_id: 366
---

I haven't posted anything on multitouch stuff for a while, mainly because I haven't seen anything that's really that new or exciting - I haven't been looking too hard either, so feel free to correct me in the comments.

Today, though, [Mike tweeted a link](http://twitter.com/dmje/status/1053081217) to a [video of NUI Group building a 20' multi-touch screen for an event in dubai](http://nuigroup.com/).

<!-- excerpt -->
