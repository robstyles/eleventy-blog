---
layout: post.pug 
title: "Paget, MVC, RMR and why words matter"
date: 2008-12-18
tags: ["post"]
legacy_wordpress_id: 371
---

I wrote a little while back about [Pages, Screens and MVC](/2008/10/03/pages-screens-mvc-and-not-getting-it/). The motivation for the post was to help me explain why my thoughts around software and the web had changed over time. It also tied in nicely with Ian's first post on Paget. [The second round of Paget](http://iandavis.com/blog/2008/11/paget-iteration-2) makes substantial changes and improves on the original design in many ways.

Following that Ian points us at Paul James's post, [Introducing the RMR Web Architecture](http://www.peej.co.uk/articles/rmr-architecture.html). [Ian says The Web is RMR not MVC](http://iandavis.com/blog/2008/12/the-web-is-rmr-not-mvc) and in the comments on that post we see some discussion about RMR being simply MVC using other names. We had a similar discussion over email internally.

<!-- excerpt -->

Naming things is important to how we think about them, as Paul James says:

> Alan, partly this is just a question of naming, but then a difference in naming can lead to a difference in thinking.
His second statement goes on to say what I said in [Pages, Screen, MVC and note getting it](/2008/10/03/pages-screens-mvc-and-not-getting-it/).

> As Ian points out above, it is more about binding actions to resources (models) rather than to controllers and of removing (or limiting) the need for routing. You say “The Controller is there to bind the system to HTTP”, but I feel that there should be no need for any binding as long as we work with HTTP to begin with rather than forcing our ways upon it.
Working with HTTP rather than forcing our ways upon it is very much the same thing as building something that is _of_ the web rather than simply _on_ the web.

The key reason I agree RMR is a better model is that in MVC the things we address with URIs are either views or controllers. As Andrew Davy comments:

> I think the danger of MVC is that unless you explicitly use it as Alan does you default into an RPC design. (ending up with “URIs” like /customer/1/delete .. shudder!)
And this is the crucks of it for me - URIs are _nouns_, not verbs and they address resources and representations of resources.

By thinking about the problem in terms of RMR rather than MVC we naturally change the way we structure the code to provide different representations or how we map particular methods to code that handles them. RMR provides a way of talking about the problem that is of the web rather than of SmallTalk. RMR provides a language and a way of thinking that doesn't [obscure the mechanics of the web](http://iandavis.com/blog/2007/09/mvc-obscures-the-mechanics-of-the-web).

That seems like more than just a change of words to me.
