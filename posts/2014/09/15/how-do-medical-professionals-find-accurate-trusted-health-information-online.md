---
layout: post.pug
title: "How do Medical Professionals Find Accurate, Trusted, Health Information Online?"
date: 2014-09-15
tags: ["post", "Meducation", "Internet Social Impact"]
---

**This blog post originally appeared on the [Meducation Company Blog](https://company.meducation.net/blog/2-How-do-Medical-Professionals-Find-Accurate-Trusted-Health-Information-Online)**

Medical professionals rely on accurate, trusted information much of which is found, and consumed, online. Little research has been done into the behaviour of those involved in healthcare when looking for and assessing healthcare information online.

We surveyed 313 medical professionals, medical students and non-medical professionals working in a health context to better understand how they find and consume healthcare information online. We have published the results and the associated data below.

<!-- excerpt -->

<p style=" margin: 12px auto 6px auto; font-family: Helvetica,Arial,Sans-serif; font-style: normal; font-variant: normal; font-weight: normal; font-size: 14px; line-height: normal; font-size-adjust: none; font-stretch: normal; -x-system-font: none; display: block;">   <a title="View How do Medical Professionals Find Accurate, Trusted Health Information Online? on Scribd" href="https://www.scribd.com/document/495159335/How-do-Medical-Professionals-Find-Accurate-Trusted-Health-Information-Online#from_embed"  style="text-decoration: underline;" >How do Medical Professionals Find Accurate, Trusted Health Information Online?</a> by <a title="View mmmmmrob's profile on Scribd" href="https://www.scribd.com/user/1498290/mmmmmrob#from_embed"  style="text-decoration: underline;" >mmmmmrob</a> on Scribd</p><iframe class="scribd_iframe_embed" title="How do Medical Professionals Find Accurate, Trusted Health Information Online?" src="https://www.scribd.com/embeds/495159335/content?start_page=1&view_mode=scroll&access_key=key-Y6Rz70wq332sObWFAdyi" data-auto-height="true" data-aspect-ratio="0.7080062794348508" scrolling="no" id="doc_80453" width="100%" height="600" frameborder="0"></iframe><script type="text/javascript">(function() { var scribd = document.createElement("script"); scribd.type = "text/javascript"; scribd.async = true; scribd.src = "https://www.scribd.com/javascripts/embed_code/inject.js"; var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(scribd, s); })();</script>
