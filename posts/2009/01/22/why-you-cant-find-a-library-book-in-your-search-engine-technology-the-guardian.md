---
layout: post.pug 
title: "Why you can't find a library book in your search engine | Technology | The Guardian"
date: 2009-01-22
tags: ["post","Library Tech","Intellectual Property"]
legacy_wordpress_id: 416
---

Wendy Grossman, in The Guardian, covers the difficulties of libraries publishing their catalogue data online.

> Despite the internet's origins as an academic network, when it comes to finding a book, e-commerce rules. Put any book title into your favourite search engine, and the hits will be dominated by commercial sites run by retailers, publishers, even authors. But even with your postcode, you won't find the nearest library where you can borrow that book. (The exception is Google Books, and even that is limited.)
via [Why you can't find a library book in your search engine | Technology | The Guardian](http://www.guardian.co.uk/technology/2009/jan/22/library-search-engines-books).

I get a namecheck and a quote at the end:

<!-- excerpt -->

> Rob Styles, a programme manager for Talis's data services, says: "The main reason I think libraries need freedom to innovate is because we don't know what they're going to look like".
