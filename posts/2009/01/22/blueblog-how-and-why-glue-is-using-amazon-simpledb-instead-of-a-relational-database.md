---
layout: post.pug 
title: "BlueBlog: How and Why Glue is Using Amazon SimpleDB instead of a Relational Database"
date: 2009-01-22
tags: ["post","Semantic Web"]
legacy_wordpress_id: 418
---

Alex blogs over at Adaptive Blue about their use of Amazon's SimpleDB to power their browser add-on Glue.

The post is interesting, and the comments useful. What I noticed, though, is that they're using natural keys...

<!-- excerpt -->

> The solution that Glue uses relies on data duplication. Each Person and each Thing in our system has a unique key. In the case of a Person, the key is the username. In the case of a Thing, the key is a combination of the type, its name and an attribute, like author for a book or director for a movie, which provides a way to disambiguate among the objects that have the same type and the same name.
via [BlueBlog: How and Why Glue is Using Amazon SimpleDB instead of a Relational Database](http://blog.adaptiveblue.com/?p=1145).
