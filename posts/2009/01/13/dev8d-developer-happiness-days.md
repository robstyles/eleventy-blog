---
layout: post.pug 
title: "dev8D - Developer Happiness Days"
date: 2009-01-13
tags: ["post","Library Tech"]
legacy_wordpress_id: 387
---

[dev8D - Developer Happiness Days](http://www.dev8d.org/). 9-13 February 2009, London

JISC is running a Developer Happiness Days meet, sort of like a 4 day hackfest, come code4lib type thing.

<!-- excerpt -->

> Over four intensive days we're bringing together the  						cream of the crop of educational software developers  						along with coders from other sectors, users, and  						technological tinkerers in an exciting new forum.
> 
> Share your skills and knowledge with the coding  						community in a stimulating and fun environment and come  						away with new skills, fresh contacts – and you might  						even win a prize.
Sounds like it will be a great few days.
