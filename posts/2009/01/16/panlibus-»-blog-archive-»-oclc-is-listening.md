---
layout: post.pug 
title: "Panlibus » Blog Archive » OCLC is listening."
date: 2009-01-16
tags: ["post","Library Tech"]
legacy_wordpress_id: 397
---

Further to my previous posts on OCLC's record use policy:

<a title="OCLC, Record Usage, Copyright, Contracts and the Law" rel="bookmark" href="/2008/11/06/oclc-record-usage-copyright-contracts-and-the-law/">OCLC, Record Usage, Copyright, Contracts and the Law</a>

<!-- excerpt -->

<a title="More OCLC Policy…" rel="bookmark" href="/2008/11/14/more-oclc-policy/">More OCLC Policy…</a>

<a title="Schroedinger’s WorldCat" rel="bookmark" href="/2008/11/18/schroedingers_worldcat/">Schroedinger’s WorldCat</a>

I'm just posted this over on panlibus:

[Panlibus » Blog Archive » OCLC is listening.](http://blogs.talis.com/panlibus/archives/2009/01/oclc-is-listening.php)
