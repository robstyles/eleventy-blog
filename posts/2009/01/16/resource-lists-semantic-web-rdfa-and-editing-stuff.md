---
layout: post.pug 
title: "Resource Lists, Semantic Web, RDFa and Editing Stuff"
date: 2009-01-16
tags: ["post","Internet Technical","Software Engineering","Working at Talis","Talis Technical","Semantic Web"]
legacy_wordpress_id: 392
---

Some of the work I've been doing over the past few months has been on a [resource lists product that helps lecturers and students make best use of the educational material for their courses](http://www.talis.com/aspire/).

One of the problems we hoped to address really well was the editing of lists. Historically products that do this have been deemed cumbersome and difficult by academic staff who will often produce lists as simple documents in Word or the like.

<!-- excerpt -->

We wanted to make an editing interface that really worked for the academic community so they could keep the lists as accurate and current as they wanted.

Chris Clarke, our Programme Manager, and Fiona Grieg, one of our pilot customers, describe the work in a [W3C case study](http://www.w3.org/2001/sw/sweo/public/UseCases/Talis/). [Ivan Hermann then picks up on one of the way we decided to implement editing using RDFa within the HTML DOM](http://ivan-herman.name/2009/01/14/a-different-usage-of-rdfa/). In the case study Chris describes it like this:

> The interface to build or edit lists uses a WYSIWYG metaphor implemented in Javascript operating over RDFa markup, allowing the user to drag and drop resources and edit data quickly, without the need to round trip back to the server on completion of each operation. The user’s actions of moving, adding, grouping or editing resources directly manipulate the RDFa model within the page. When the user has finished editing, they hit a save button which serialises the RDFa model in the page into an RDF/XML model which is submitted back to the server. The server then performs a delta on the incoming model with that in the persistent store. Any changes identified are applied to the store, and the next view of the list will reflect the user’s updates.
This approach has several advantages. First, as [Andrew says](http://www.astilla.co.uk/blog/2009/01/w3c-case-study/)

> One thing I hadn’t used until recently was RDFa.  We’ve used it on one of the main admin pages in our new product and it’s made what was initially quite a complex problem much simpler to implement.
The problem that's made simpler is this - WYSIWYG editing of the page was best done using DOM manipulation techniques, and most easily using existing libraries such as prototype. But what was being edited isn't really the visual document, it is the underlying RDF model. Trying to keep a version of the model in a JS array or something in synch with the changes happening in the DOM seemed to be a difficult (and potentially bug-ridden) option.

By using RDFa we can distribute the model through the DOM and have the model updated by virtue of having updated the DOM itself. Andrew describes this process nicely:

> Currently using Jeni Tennison’s RDFQuery library to parse an RDF model out of an XHTML+RDFa page we can mix this with our own code and end up with something that allows complex WYSIWYG editing on a reading list.  We use RDFQuery to parse an initial model out of the page with JavaScript and then the user can start modifying the page in a WYSIWYG style.   They can drag new sections onto the list, drag items from their library of bookmarked resources onto the list and re-order sections and items on the list.  All this is done in the browser with just a few AJAX calls behind the scenes to pull in data for newly added items where required.   At the end of the process, when the Save button is pressed, we can submit the ‘before’ and ‘after’ models to our back-end logic which builds a Changeset from before and after models and persists this to a data store on the Talis Platform.

> Building a Changeset from the two RDF models makes quite a complex problem relatively straightforward.  The complexity now just being in the WYSIWYG interface and the dynamic updating of the RDFa in the page as new items are added or re-arranged.
As Andrew describes, the editing starts by extracting a copy of the model. This allows the browser to maintain before and after models. This is useful as when the before and after get posted to the server the before can be used to spot if there have been editing conflicts with someone else doing a concurrent edit - this is an improvement to how Chris described it in the case study.

There are some gotchas in this approach though. Firstly, some of the nodes have two-way links:
<p class="code">&lt;http://example.com/lists/foo&gt; &lt;http://purl.org/vocab/resourcelist/schema#contains&gt; &lt;http://example.com/items/bar&gt;
&lt;http://example.com/items/bar&gt; &lt;http://purl.org/vocab/resourcelist/schema#list&gt; &lt;http://example.com/lists/foo&gt;

So that the relationship from the list to the item gets removed when the item is deleted from the DOM we use the [@rev](http://www.w3.org/TR/rdfa-syntax/#relValues) attribute. This allows us to put the relationship from the list to the item with the item, rather than with the list.

The second issue is that we use rdf:Seq to maintain the ordering of the lists, so when the order changes in the DOM we have to do a quick traversal of the DOM changing the sequence predicates (_1, _2 etc) to match the new visual order.

Neither of these were difficult problems to solve :-)

My thanks go out to [Jeni Tennison](http://www.jenitennison.com/) who helped me get the initial prototype of this approach working while we were at [Swig back in Novemeber](http://swig.networkedplanet.com/november2008.html).
