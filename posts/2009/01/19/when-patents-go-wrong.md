---
layout: post.pug 
title: "When Patents Go Wrong..."
date: 2009-01-19
tags: ["post","Intellectual Property"]
legacy_wordpress_id: 400
---

_Warning, Patent Rant follows._
<a title="91019234.pdf (page 15 of 19) by mmmmmrob, on Flickr" href="http://www.flickr.com/photos/mmmmmrob/3209003483/"><img class="alignleft" src="http://farm4.static.flickr.com/3490/3209003483_2338829cbf_m.jpg" alt="91019234.pdf (page 15 of 19)" width="164" height="240" /></a>
Sure, everyone knows about high profile patents like [Amazon One-Click](http://en.wikipedia.org/wiki/Amazon_one_click_patent), but what about the effect of less prominent cases?

A patent is a monopoly over an invention - in order to encourage innovation, patents are granted to inventors so they are assured of an income from the invention. Inventions usually make money either through the sale or licensing of the patent or through production of a product that makes use of the invention. The patent prevents others from simply copying the idea. Unlike Copyright, however, patents cover the idea even if the second person came up with the idea completely independently.

<!-- excerpt -->

But why am I writing about this now? Well, I've come to the end of the line with a child's clock. Yep, you read that right.

Back in 1991 [Julian Renton designed a clock for children](http://www.bunnyclock.com/bunnyclockbackground.html). The intention was simple, provide children who were too young to tell the time a visual indication of whether or not they should be awake. This is a great idea and one that, as a father of three, I whole-heartedly support.

[He patented the clock](http://www.wipo.int/pctdb/en/fetch.jsp?SEARCH_IA=GB1991000890&amp;DBSELECT=PCT&amp;C=10&amp;TOTAL=12&amp;IDB=0&amp;TYPE_FIELD=256&amp;SERVER_TYPE=19-10&amp;QUERY=(FP%2Fclock+AND+FP%2Fchildren)+&amp;ELEMENT_SET=B&amp;START=1&amp;SORT=41249674-KEY&amp;RESULT=11&amp;DISP=25&amp;FORM=SEP-0%2FHITNUM%2CB-ENG%2CDP%2CMC%2CAN%2CPA%2CABSUM-ENG&amp;IDOC=440141&amp;IA=GB1991000890&amp;LANG=ENG&amp;DISPLAY=STATUS).

All would be fine and dandy if he, or a licensed manufacturer, had gone on to produce a product based on the patent that both worked in practice and was sound value-for-money. Unfortunately that's not what happened. Without the patent the idea would have been open for several manufacturers to pick up the idea and produce competing versions. This would have had the usual market benefits of encouraging the development of better products as well as driving cost down. The patent prevents this.

So, we're left with just [Sleep Time Bunny](http://www.bunnyclock.com/).

Now, as far as Julian's concerned, the patent system has worked very well. He has a nice little business selling Sleep Time Bunny directly over the internet and through some shops. The usual street price for Sleep Time Bunny is a little under £20. Bear in mind that, other than patent-protected bunny face, this is the same complexity as a standard alarm clock. It's also been manufactured with cost very much in mind - you can tell. I would suspect the manufacturing cost doesn't exceed £2 per clock - and it is possible to buy unbranded alarm clocks that appear to be the same quality for around £4. Julian should be making a healthy profit on the sale of each clock.

But as a consumer, the patent protection has delivered me poor value-for-money and resulted in no consumer choice. The problem for me, as a consumer, is that the idea has been allowed to run as a monopoly, thus requiring no innovation or development to make the product better or cheaper. Take one of the most obvious problems for a children's alarm clock:

A 2½ year old, the stated lower end of the age range for the clock, will often go to bed around 7pm and be expected to stay in bed until 7am the following morning. This is obviously something that people ask a lot as makes it onto Bunny Clock's FAQ:

> Bunny Clock Q. I tried to set Bunny Clock to sleep at 7.00pm to wake at 7.00am but the Bunny won't stay asleep. Why is this?

> Bunny Clock A. The waking time selection is set with the normal alarm set hand. However, the alarm mechanism, as with all alarm clocks, works on a 12 hour cycle and so you are effectively trying to set Bunny Clock to sleep when it wants to wake. If you want Bunny to sleep for 12 or more hours you will need to adjust the wake setting at a later time – possibly just before you go to bed.
Right, so for a very common case the clock simply doesn't work. Notice the aside there, "as with all alarm clocks". Not all alarm clocks are marketed for young children, not all alarm clocks are patented. How much product development has gone into solving that problem in the past 17 years? Zero. Because there is no need to solve it. The product has no immediate competitors.

What the patent doesn't prevent is someone designing a product that solves the exact same problem and competes in the exact same space, as long as it doesn't infringe on any of the claims made for Sleep Time Bunny. And that's exactly what someone should do. Please someone.
