---
layout: post.pug 
title: "TechCrunch Tablet Update: Prototype B"
date: 2009-01-19
tags: ["post","New Toy"]
legacy_wordpress_id: 401
---

It's like a big iPhone, but not a phone, so more like a big 'i' then...

[TechCrunch Tablet Update: Prototype B](http://www.techcrunch.com/2009/01/19/techcrunch-tablet-update-prototype-b/).

<!-- excerpt -->

I don't know if I'd buy this one, but I would buy something like this - if it were good enough, and cheap enough...

[courtesy of iand](http://twitter.com/iand/status/1130712196)
