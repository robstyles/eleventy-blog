---
layout: post.pug 
title: "shameless..."
date: 2009-01-07
tags: ["post"]
legacy_wordpress_id: 383
---

In a shameless attempt to keep your attention, and get some people to comment, during this period of limited blogging I pose you the following scruple...

You stumble across some photographs, online, of a colleague. The photographs are of them naked. It is not clear from the context if the person in the photographs knows they have been posted online or not. What do you do?

<!-- excerpt -->

I should add that this has not happened to me. This was the subject of conversation between some folks I was over-hearing. Judging by the look on one of the faces it had happened to him...
