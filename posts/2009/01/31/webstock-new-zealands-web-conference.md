---
layout: post.pug 
title: "Webstock - New Zealand's web conference"
date: 2009-01-31
tags: ["post","event"]
legacy_wordpress_id: 426
---

While wandering around searching for interesting semwebby bits and pieces I stumbled across [Webstock - New Zealand's web conference](http://www.webstock.org.nz/). The programme looks great, with Ze Frank, Matt Biddulph, Tom Coates, Toby Segaran and Heather Champ amongst others. This looks like an awesome line-up.

Shame it's on the opposite side of the planet in just over 2 weeks, otherwise I'd be trying to wangle a trip. I wonder if Matt Biddulph has space in his luggage for little old me?

<!-- excerpt -->
