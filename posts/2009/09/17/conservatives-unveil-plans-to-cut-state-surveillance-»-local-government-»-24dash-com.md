---
layout: post.pug 
title: "Conservatives unveil plans to cut state surveillance » Local Government » 24dash.com"
date: 2009-09-17
tags: ["post","Security And Privacy"]
legacy_wordpress_id: 560
---

> A future Conservative government would drastically reduce the size of the "Big Brother" state.
> 
> Were they to win the election, the Tories would slash database projects and roll back the "snooping" powers given to officials.
> 
> In future, Whitehall departments would face tougher privacy rules to protect the individual against loss of their personal data, shadow justice secretary Dominic Grieve will say today.
via [Conservatives unveil plans to cut state surveillance » Local Government » 24dash.com](http://www.24dash.com/news/Local_Government/2009-09-16-Conservatives-unveil-plans-to-cut-state-surveillance).

<!-- excerpt -->
