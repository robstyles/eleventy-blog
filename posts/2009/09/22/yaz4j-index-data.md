---
layout: post.pug 
title: "yaz4j | Index Data"
date: 2009-09-22
tags: ["post","Library Tech"]
legacy_wordpress_id: 562
---

> yaz4j is a toolkit for Java which includes a wrapper for the ZOOM API of YAZ. This allows developers to write Z39.50/SRU clients in Java. yaz4j supports both search and scan. See the javadoc for details.
from [yaz4j | Index Data](http://www.indexdata.com/yaz4j).

I wrote Yaz4J a couple of years ago when I needed a robust Z39.50 client. The underlying work is done by Index Data's Yaz library, wrapped for use in Java using JNI (and yes, JNI does work fine and yes it does work cross-platform, we have it running on Linux, Windows and OS X). I hadn't ever found the time to properly structure and mavenise the code or release it properly so it's very pleasing that Adam Dickmeiss and Mike Taylor from Index Data along with Juan Cayetano have tidied it all up and published it under a home on Index Data's site.

<!-- excerpt -->

:-)
