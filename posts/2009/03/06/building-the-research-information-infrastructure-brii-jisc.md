---
layout: post.pug 
title: "Building the Research Information Infrastructure (BRII) : JISC"
date: 2009-03-06
tags: ["post","Semantic Web","Ontologies"]
legacy_wordpress_id: 457
---

[AIISO](http://purl.org/vocab/aiiso), [AIISO-Roles](http://purl.org/vocab/aiiso-roles) and [Participation](http://purl.org/vocab/participation), ontologies I developed with others at Talis and at VoCamp are picked up by JISC's [Building the Research Information Infrastructure (BRII) : JISC](http://www.jisc.ac.uk/whatwedo/programmes/institutionalinnovation/brii.aspx).

<!-- excerpt -->
