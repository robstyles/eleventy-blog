---
layout: post.pug 
title: "JISCmail - DC-RDA Archives"
date: 2009-03-10
tags: ["post","Library Tech","Semantic Web"]
legacy_wordpress_id: 460
---

[JISCmail - Alistair Miles releases some of his work on RDF, RDA, FRBR and LOC data](https://www.jiscmail.ac.uk/cgi-bin/webadmin?A2=ind0903&amp;L=DC-RDA&amp;T=0&amp;F=&amp;S=&amp;P=49).

<!-- excerpt -->
