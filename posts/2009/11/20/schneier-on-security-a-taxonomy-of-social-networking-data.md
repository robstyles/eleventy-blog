---
layout: post.pug 
title: "Schneier on Security: A Taxonomy of Social Networking Data"
date: 2009-11-20
tags: ["post","Internet Social Impact","Security And Privacy","Open Data","Ontologies"]
legacy_wordpress_id: 590
---

> A Taxonomy of Social Networking Data

> At the Internet Governance Forum in Sharm El Sheikh this week, there was a conversation on social networking data. Someone made the point that there are several different types of data, and it would be useful to separate them. This is my taxonomy of social networking data.
from [Schneier on Security: A Taxonomy of Social Networking Data](http://www.schneier.com/blog/archives/2009/11/a_taxonomy_of_s.html).

Follow the link for a useful breakdown of data in any community site or service.

<!-- excerpt -->
