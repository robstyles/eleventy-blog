---
layout: post.pug 
title: "Government Data, Openness and Making Money"
date: 2009-11-20
tags: ["post","Internet Social Impact","Semantic Web","Open Data"]
legacy_wordpress_id: 592
---

Over on the [UK Government Data Developers group](http://groups.google.com/group/uk-government-data-developers/) there's been a great discussion about openness, innovation and how Government makes money from its data; and of course if it _should_ make money. I can't link to the discussion as the group is closed - sign up, it's a great group.

Philosophically there's always the stance that Government data has _already_ been paid for by the public through general taxation.

<!-- excerpt -->

[Tim Berners-Lee even says so in his guest piece for Times Online](http://www.timesonline.co.uk/tol/comment/columnists/guest_contributors/article6920761.ece).

> This is data that has already been collected and paid for by the taxpayer, and the internet allows it to be distributed much more cheaply than before. Governments can unlock its value by simply letting people use it.
While that's true, the role of Government is to maximise the return we get on our taxes so if more money can be made from the assets we have then surely we should.

This is where discussion breaks of into various arguments as to where on the spectrum licensing of Government data should sit, and how open to re-use it should be.

The discussion covers notions of Copyleft licensing, attribution, commercial and non-commercial use as well as models of innovation.

What I always come back to is the notion that to make money you have to have something that is not "open", a scarce resource. I have a blog post talking about that in the context of software and the web that's been drafted but not finished for some time, so I'm coming at this from a point of existing thinking.

To make money something has to be closed.

In the case of creative works, the thing that is closed is the right to produce copies (closed through Copyright law). An author makes money by selling that right (or a limited subset of it) to a publisher who makes money from exploiting the right to copy. The publisher has exclusivity.

In the case of open source software companies the dominant model is support and consultancy. They make money by exploiting the specialist knowledge they have in their heads - a careful balance exists for companies doing this between making the product great and needing the support revenue. This balance leads to other monetization strategies, like using the closed nature of being the only place to go for that software to sell default slots in the software (think search boxes), or advertising.

In the case of closed-source commercial software it is the code, the product itself, that remains closed.

Commercial organisations with data assets have to keep the data closed in order to make money. The Government, however, does not. The Government can give the data away for free because it has something else that is closed - the UK economy. To be a part of the (legitimate) UK economy you have to pay taxes, giving the UK a 20% to 40% share of all profits.

If people find ways to make money using Government data those taxes dwarf any potential licensing fee - can you imagine a commercial data provider asking for up to 40% of a company's profit as the cost of a data license?

This is why it makes sense for the Government to make data available with as few restrictions as possible - ultimately that means Public Domain.

That seems to be the direction the mailing list is heading thanks to some great contributors. If open data, government data and innovation interest you then [sign up and join in](http://groups.google.com/group/uk-government-data-developers/).
