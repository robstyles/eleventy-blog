---
layout: post.pug 
title: "Six bottle of wine later and I can tell you, it's pretty good stuff."
date: 2009-11-05
tags: ["post","Review"]
legacy_wordpress_id: 576
---

Thanks to [Documentally tipping us off](http://documental.ly/a-new-way-to-enjoy-wine) on Twitter and his blog my wife and I received a gratuitous couple of [FreshCase](http://www.freshcasewine.com/)s, one red, one white.

These claim to be the next generation of winebox and they are rather nicely designed. The floppy cardboard normally surrounding the tap on a wine box (and the digging around in the box for the tap with just two fingers) is replaced by a smart plastic moulding that, once pressed, releases the tap into position.

<!-- excerpt -->

The tap for the red, a rather nice [Nottage Hill Cabernet Shiraz](http://www.freshcasewine.com/our_wines_index.aspx), is where you'd expect it, front of the case down low. The white, a very crisp [Nottage Hill Chardonnay](http://www.freshcasewine.com/our_wines_index.aspx), rather cleverly has the tap on the base, making it a perfect fit lying down in the fridge. With a little extra thought they could have made the handle asymmetric and had it hold the box at a slight angle, but they haven't.

These cases only landed in the shop on November 1st, so it's nice to get my hands on these so soon, and free is a great price. I'm told they hit the shelves at £19.99, making them £6.66 a bottle equivalent. That price seems high to me with so many bottles on half-price or 3 for a tenner offers. At £9.99 these would be too god to be true though - the Nottage Hill is definitely more a £7 bottle than a £3 one.

The cases themselves are a really nice design, they remind me, in proportion and style, of the boxes whisky bottle come in. That's a bonus if you're putting these out for a party - unlike most wine boxes these don't look cheapskate. Of course, stripping down the box to squeeze the last glass out of the foil bag _will_ still make you look cheap, or desperate.

Documentally went as far as to record a video showing you the FreshCase in some detail so I figured I'd just share that with you.

<iframe src="https://player.vimeo.com/video/7209269" width="640" height="352" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

[Hardy&#039;s Nottage Hill FreshCase](https://vimeo.com/7209269) from [Documentally](https://vimeo.com/documentally) on [Vimeo](https://vimeo.com).

The most obvious downside is that the size of a bottle acts as a limit to what we drink. Take away that barrier of opening (or rather not opening) the second bottle and the wine seems to run out very quickly indeed. That alone will probably keep me buying the more limiting 75cl bottles.
