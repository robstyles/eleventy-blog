---
layout: post.pug 
title: "101 Things I Learned in Interaction Design School"
date: 2009-11-25
tags: ["post","Interaction Design"]
legacy_wordpress_id: 601
---

> 101 Things I Learned in Interaction Design School
> 
> A set of short, easily digested learnings from the world of Interaction Design, inspired by "101 Things I Learned in Architecture School", by Matthew Frederick
from [101 Things I Learned in Interaction Design School](http://www.ixd101.com/).

<!-- excerpt -->
