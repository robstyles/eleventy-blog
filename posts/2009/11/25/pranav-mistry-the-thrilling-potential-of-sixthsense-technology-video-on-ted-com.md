---
layout: post.pug 
title: "Pranav Mistry: The thrilling potential of SixthSense technology | Video on TED.com"
date: 2009-11-25
tags: ["post","Interaction Design"]
legacy_wordpress_id: 603
---

> At TEDIndia, Pranav Mistry demos several tools that help the physical world interact with the world of data -- including a deep look at his SixthSense device and a new, paradigm-shifting paper "laptop." In an onstage Q&A, Mistry says he'll open-source the software behind SixthSense, to open its possibilities to all.
from [Pranav Mistry: The thrilling potential of SixthSense technology | Video on TED.com](http://www.ted.com/talks/pranav_mistry_the_thrilling_potential_of_sixthsense_technology.html).

<!-- excerpt -->

<div style="max-width:854px"><div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://embed.ted.com/talks/pranav_mistry_the_thrilling_potential_of_sixthsense_technology" width="854" height="480" style="position:absolute;left:0;top:0;width:100%;height:100%" frameborder="0" scrolling="no" allowfullscreen></iframe></div></div>
