---
layout: post.pug 
title: "QOOQ - Le premier coach culinaire tactile"
date: 2009-11-27
tags: ["post","Interaction Design"]
legacy_wordpress_id: 605
---

Tablets and multi-touch hardware are becoming more mainstream, and the release of Windows 7 will drive yet more. There hasn't been much in the way of product design going into the tablets I've seen so far, which is why so many people keep hoping for an Apple tablet.

French company [Unowhy](http://www.unowhy.com/) are taking a different approach though, releasing a tablet targeted at the kitchen, with the sale driven by content - recipes and training videos from top french chefs.

<!-- excerpt -->

<a title="QOOQ - Le premier coach culinaire tactile by mmmmmrob, on Flickr" href="http://www.flickr.com/photos/mmmmmrob/4137397287/"><img src="http://farm3.static.flickr.com/2653/4137397287_fa976599c7_o.jpg" alt="QOOQ - Le premier coach culinaire tactile" width="639" height="403" /></a>

[QOOQ - Le premier coach culinaire tactile](http://qooq.com/).

The physical design looks really good, if the price is right I could see these selling well and possibly prompting a targeted linux distro for it.
