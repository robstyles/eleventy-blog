---
layout: post.pug 
title: "When you really want empty files..."
date: 2009-11-27
tags: ["post","commands I have issued"]
legacy_wordpress_id: 607
---

ls | sed -e 's%^%cat /dev/null &gt; %' | bash

* NB: Dangerous.

<!-- excerpt -->
