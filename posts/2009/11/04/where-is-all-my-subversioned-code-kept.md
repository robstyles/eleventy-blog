---
layout: post.pug 
title: "Where is all my subversioned code kept?"
date: 2009-11-04
tags: ["post","commands I have issued"]
legacy_wordpress_id: 573
---

find . -type d | grep -v "/\.svn/" | grep -v "/\.svn$" | xargs svn info | grep "^URL"

<!-- excerpt -->
