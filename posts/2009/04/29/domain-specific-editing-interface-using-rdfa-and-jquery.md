---
layout: post.pug 
title: "Domain Specific Editing Interface using RDFa and jQuery"
date: 2009-04-29
tags: ["post","Internet Technical","Software Engineering","Talis Technical","Semantic Web"]
legacy_wordpress_id: 476
---

I wrote back in January about <a title="Resource Lists, Semantic Web, RDFa and Editing Stuff" rel="bookmark" href="/2009/01/16/resource-lists-semantic-web-rdfa-and-editing-stuff/">Resource Lists, Semantic Web, RDFa and Editing Stuff</a>. This was based on work we'd done in <a title="Resource Lists, Semantic Web, RDFa and Editing Stuff" rel="bookmark" href="/2009/01/16/resource-lists-semantic-web-rdfa-and-editing-stuff/"></a>[Talis Aspire](http://www.talis.com/aspire/).

Several people suggested this should be written up as a fuller paper, so [Nad](http://www.virtualchaos.co.uk/blog/), [Jeni](http://jenitennison.com) and I wrote it up as a paper for the [SFSW 2009 workshop](http://www.semanticscripting.org/SFSW2009/). It's been accepted and will be published there, but unfortunately due to work priorities that have come up we won't be able to attend.

<!-- excerpt -->

A draft of the paper is here: [A Pattern for Domain Speciﬁc Editing Interfaces Using Embedded RDFa and HTML Manipulation Tools.](/assets/embeddedrdfaediting-sfsw2009.pdf)

The camera ready copy will be published in the conference proceedings. Feedback welcomed.
