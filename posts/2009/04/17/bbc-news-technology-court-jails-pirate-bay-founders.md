---
layout: post.pug 
title: "BBC NEWS | Technology | Court jails Pirate Bay founders"
date: 2009-04-17
tags: ["post","Internet Social Impact","IP Law","Intellectual Property"]
legacy_wordpress_id: 470
---

A court in Sweden has jailed four men behind The Pirate Bay (TPB), the world's most high-profile file-sharing website, in a landmark case.

via [BBC NEWS | Technology | Court jails Pirate Bay founders](http://news.bbc.co.uk/1/hi/technology/8003799.stm).

<!-- excerpt -->

:-(
