---
layout: post.pug 
title: "Kyocera Concept Phone | Fubiz™"
date: 2009-04-17
tags: ["post","Interaction Design"]
legacy_wordpress_id: 468
---

La marque Kyocera vient de présenter ce concept de téléphone avec écran OLED pliable en trois parties tel un portefeuille. Doté d’un clavier, de boutons rétro-éclairés et entièrement propulsé par l’énergie cinétique. Plus d’images du projet dans la suite.

Kyocera presents this concept phone using a flexible OLED tri-fold screen, like a purse. With a keyboard, backlit keys and powered by movement. More photos of the project to come in the future.

<!-- excerpt -->

[Photos of the Kyocera Concept Phone at Fubiz™](http://www.fubiz.net/2009/04/17/kyocera-concept-phone/).
