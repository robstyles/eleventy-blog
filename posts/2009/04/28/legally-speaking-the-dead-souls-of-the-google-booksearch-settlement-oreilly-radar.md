---
layout: post.pug 
title: "Legally Speaking: The Dead Souls of the Google Booksearch Settlement - O'Reilly Radar"
date: 2009-04-28
tags: ["post","Internet Social Impact","IP Law","Intellectual Property"]
legacy_wordpress_id: 474
---

> In the short run, the Google Book Search settlement will unquestionably bring about greater access to books collected by major research libraries over the years. But it is very worrisome that this agreement, which was negotiated in secret by Google and a few lawyers working for the Authors Guild and AAP (who will, by the way, get up to $45.5 million in fees for their work on the settlement—more than all of the authors combined!), will create two complementary monopolies with exclusive rights over a research corpus of this magnitude. Monopolies are prone to engage in many abuses.
> 
> The Book Search agreement is not really a settlement of a dispute over whether scanning books to index them is fair use. It is a major restructuring of the book industry’s future without meaningful government oversight. The market for digitized orphan books could be competitive, but will not be if this settlement is approved as is.
from [Legally Speaking: The Dead Souls of the Google Booksearch Settlement - O'Reilly Radar](http://radar.oreilly.com/2009/04/legally-speaking-the-dead-soul.html).
