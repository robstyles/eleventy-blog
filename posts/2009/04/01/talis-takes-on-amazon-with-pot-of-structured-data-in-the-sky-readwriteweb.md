---
layout: post.pug 
title: "Talis Takes on Amazon With Pot of Structured Data in the Sky - ReadWriteWeb"
date: 2009-04-01
tags: ["post","Working at Talis"]
legacy_wordpress_id: 464
---

[Talis Takes on Amazon With Pot of Structured Data in the Sky - ReadWriteWeb](http://www.readwriteweb.com/archives/talis_takes_on_amazon_with_pot_of_structured_data.php).

Nice write-up of our [Talis Connected Commons](http://blogs.talis.com/n2/cc) programme.

<!-- excerpt -->
