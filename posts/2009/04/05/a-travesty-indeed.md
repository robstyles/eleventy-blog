---
layout: post.pug 
title: "a travesty indeed"
date: 2009-04-05
tags: ["post","Internet Social Impact","IP Law"]
legacy_wordpress_id: 466
---

[open...: HADOPI Law Passed - by 12 Votes to 4](http://opendotdotdot.blogspot.com/2009/04/hadopi-law-passed-by-12-votes-to-4.html).

The French passed a law that forces ISPs to withdraw internet access based upon accusations of infringement by Copyright holders.

<!-- excerpt -->

This is what [many in New Zealand protested about and got delayed](/2009/02/16/join-the-internet-blackout-protest-against-guilt-upon-accusation-laws-in-nz-%E2%80%94-creative-freedom-foundation-creativefreedomorgnz/) if not completely withdrawn.
