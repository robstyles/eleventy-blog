---
layout: post.pug 
title: "Interview with the Twitter DJ Traktor App’s Co-Creator at djtechtools.com"
date: 2009-10-16
tags: ["post","Internet Social Impact"]
legacy_wordpress_id: 564
---

> On the surface, Twitter DJ seems like a gracious gesture from a DJ to solve the age-old problem of fans not knowing what the amazing track they’re hearing is called and who made it, as well as a boon for often small-time music producers to get some well-deserved props.
from [Interview with the Twitter DJ Traktor App’s Co-Creator at djtechtools.com](http://www.djtechtools.com/2009/10/16/interview-with-the-twitter-dj-traktor-apps-co-creator/).

Nice integration of Traktor DJ software and Twitter - part of a growing trend that makes apps more native to the web.

<!-- excerpt -->
