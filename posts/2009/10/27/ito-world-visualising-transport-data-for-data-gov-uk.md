---
layout: post.pug 
title: "Ito World: Visualising Transport Data for Data.gov.uk"
date: 2009-10-27
tags: ["post","Semantic Web","Open Data"]
legacy_wordpress_id: 566
---

> It can be hard to make meaningful information from huge amounts of data, a graph and a table doesn't always communicate all it should do. We have been working hard on technology to visualise big datasets into compelling stories that humans can understand. We were really pleased with what we came up with in just one and a half days, see for yourself
from [Ito World: Visualising Transport Data for Data.gov.uk](http://itoworld.blogspot.com/2009/10/visualising-transport-data-for.html).

Nice work on visualizing traffic data.

<!-- excerpt -->
