---
layout: post.pug 
title: "Interactive storefront display | ARvertising news"
date: 2009-10-28
tags: ["post","Other Technical","Interaction Design"]
legacy_wordpress_id: 570
---

> As you walk down the street you are approached by a dog. He is on his guard trying to discern your intentions. He will follow you and interpret your gestures as friendly or aggressive. He will try to engage you in a relationship and get you to pay attention to him.
from [Interactive storefront display | ARvertising news](http://www.arvertising.com/news/2009/10/interactive-storefront-display/).

Computer generated dog, reacts to real-world passers-by.

<!-- excerpt -->
