---
layout: post.pug 
title: "ShelterIt - My digital think-tank: On identity"
date: 2009-10-28
tags: ["post","Library Tech","Semantic Web"]
legacy_wordpress_id: 568
---

> Did you notice what just happened? I used used an URI as an identifier for a subject. If you popped that URI into your browser, it will take you to WikiPedia's article on the book and provide a lot of info there in human prose about this book, and this would make it rather easy for Bob to say that, yes indeed, that's the same book I've got. So now we've got me and Bob agreeing that we have the same book.
from [ShelterIt - My digital think-tank: On identity](http://shelter.nu/blog/2009/10/on-identity.html).

Great piece by Alexander Johannesen about the future of library data, semantic web and the difficulties of getting from here to there.

<!-- excerpt -->
