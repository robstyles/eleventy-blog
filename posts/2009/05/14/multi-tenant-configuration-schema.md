---
layout: post.pug 
title: "Multi-Tenant Configuration Schema"
date: 2009-05-14
tags: ["post","Working at Talis","Talis Technical","Semantic Web","Ontologies"]
legacy_wordpress_id: 486
---

Are you writing multi-tenant software? Are you using RDF at all? Do you want to keep track of your tenants?

You might want to comment on the first draft of the new [Multi-Tenant Configuration Schema](http://vocab.org/multi-tenant-configuration/schema).

<!-- excerpt -->

This schema attempts to describe a simple set of concepts and relationships about tenants within a multi-tenant software system. It avoids anything that would constitute application configuration, but will happily co-exist with classes and properties to do that. The documentation is sparse currently, awaiting questions and comment so that I can expand on areas that require further explanation. Comment here, or email me.
