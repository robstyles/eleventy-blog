---
layout: post.pug 
title: "Wendy’s Blog: Legal Tags » Theater of the DMCA Anticircumvention Hearings"
date: 2009-05-11
tags: ["post","IP Law","Intellectual Property"]
legacy_wordpress_id: 482
---

> The proceedings jumped the line to farce when Fritz Attaway and a colleague from the MPAA pulled out a cinematic demonstration of just how to camcord a movie from your television screen. (You start with a $900 HD video camera, a tripod, a flat-screen television, and a room that can be completely darkened.) [Tim Vollmer captured the whole scene](http://www.engadget.com/2009/05/07/mpaa-suggests-teachers-videotape-tvs-instead-of-ripping-dvds-se/) on a video of his own. Mind you, this is the same industry that has lobbied to make a crime of camcording in movie theaters, telling us how to frame shots properly from the television. (As [Fred Benenson notes](http://fredbenenson.com/blog/2009/05/07/the-staggering-hypocrisy-of-the-mpaa/), they’re also demonstrating DRM’s impossibility of closing the “analog hole.”)
from [Wendy’s Blog: Legal Tags » Theater of the DMCA Anticircumvention Hearings](http://wendy.seltzer.org/blog/archives/2009/05/08/theater-of-the-dmca-anticircumvention-hearings.html).
