---
layout: post.pug 
title: "Scripting and Development for the Semantic Web (SFSW2009)"
date: 2009-05-19
tags: ["post","Internet Technical","Semantic Web"]
legacy_wordpress_id: 489
---

The following papers have been accepted for SFSW2009:
<ul>
	<li>Christoph Lange :  [Krextor — An Extensible XML-&gt;RDF Extraction Framework](http://www.semanticscripting.org/SFSW2009/short_2.pdf)</li>
	<li>Eugenio Tacchini, Andreas Schultz and Christian Bizer :  [Experiments with Wikipedia Cross-Language Data Fusion](http://www.semanticscripting.org/SFSW2009/full_3.pdf)</li>
	<li>Jouni Tuominen, Tomi Kauppinen, Kim Viljanen and Eero Hyvönen :  [Ontology-Based Query Expansion Widget for Information Retrieval](http://www.semanticscripting.org/SFSW2009/short_1.pdf)</li>
	<li>Laura Dragan, Knud Möller, Siegfried Handschuh, Oszkar Ambrus and Sebastian Trueg :  [Converging Web and Desktop Data with Konduit](http://www.semanticscripting.org/SFSW2009/full_4.pdf)</li>
	<li>Mariano Rico, David Camacho and Oscar Corcho :  [Macros vs. scripting in VPOET](http://www.semanticscripting.org/SFSW2009/short_4.pdf)</li>
	<li>Norman Gray, Tony Linde and Kona Andrews :  [SKUA — Retrofitting Semantics](http://www.semanticscripting.org/SFSW2009/short_5.pdf)</li>
	<li>Pierre-Antoine Champin :  [Tal4Rdf: lightweight presentation for the Semantic Web](http://www.semanticscripting.org/SFSW2009/full_1.pdf)</li>
	<li>Rob Styles, Nadeem Shabir and Jeni Tennison :  [A Pattern for Domain Speciﬁc Editing Interfaces Using Embedded RDFa and HTML Manipulation Tools ](http://www.semanticscripting.org/SFSW2009/full_2.pdf)</li>
	<li>Stéphane Corlosquet, Richard Cyganiak, Axel Polleres and Stefan Decker :  [RDFa in Drupal: Bringing Cheese to the Web of Data](http://www.semanticscripting.org/SFSW2009/short_3.pdf)</li>
</ul>
from [Scripting and Development for the Semantic Web (SFSW2009)](http://www.semanticscripting.org/SFSW2009/#papers).

Looks like a great line-up. As neither Nad, Jeni nor I are able to attend our paper will be presented (briefly) by Chris Clarke.
