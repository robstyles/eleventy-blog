---
layout: post.pug 
title: "The Evolution of Cell Phone Design Between 1983-2009 | Webdesigner Depot"
date: 2009-05-22
tags: ["post","Interaction Design"]
legacy_wordpress_id: 491
---

> Cell phones have evolved immensely since 1983, both in design and function.
> 
> From the Motorola DynaTAC, that power symbol that Michael Douglas wielded so forcefully in the movie “Wall Street”, to the iPhone 3G, which can take a picture, play a video, or run one of the thousands applications available from the Apple Store.
> 
> There are thousands of models of cell phones that have hit the streets between 1983 and now.
> 
> We’ve picked a few of the more popular and unusual ones to take you through the history of this device that most of us consider a part of our everyday lives.
from [The Evolution of Cell Phone Design Between 1983-2009 | Webdesigner Depot](http://www.webdesignerdepot.com/2009/05/the-evolution-of-cell-phone-design-between-1983-2009/).

<!-- excerpt -->
