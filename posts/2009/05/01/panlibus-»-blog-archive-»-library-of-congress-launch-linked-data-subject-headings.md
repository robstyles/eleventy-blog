---
layout: post.pug 
title: "Panlibus » Blog Archive » Library of Congress launch Linked Data Subject Headings"
date: 2009-05-01
tags: ["post","Internet Technical","Library Tech","Semantic Web","Open Data"]
legacy_wordpress_id: 480
---

Agree with this summary from Richard

> On the surface, to those not yet bought in to the potential of Linked Data, and especially [Linked Open Data](http://linkeddata.org/), this may seem like an interesting but not necessarily massive leap forward.   I believe that what underpins the fairly simple functional user interface they provide will gradually become core to bibliographic data becoming a first-class citizen in the web of data.
> 
> Overnight this uri ‘[http://id.loc.gov/authorities/sh85042531](http://id.loc.gov/authorities/sh85042531#concept)’ has now become the globally available, machine and human readable, reliable source for the description for the subject heading of ‘Elephants’ containing links to its related terms (in a way that both machines and humans can navigate).  This means that system developers and integrators can rely upon that link to represent a concept, not necessarily the way they want to [locally] describe it.  This should facilitate the ability for disparate systems and services to simply share concepts and therefore understanding – one of the basic principles behind the Semantic Web.
from [Panlibus » Blog Archive » Library of Congress launch Linked Data Subject Headings](http://blogs.talis.com/panlibus/archives/2009/05/library-of-congress-launch-linked-data-subject-headings.php).

Great to see LoC doing this stuff and getting it out there.
