---
layout: post.pug 
title: "Sir Tim Berners-Lee to advise the Government on public information delivery - PublicTechnology.net"
date: 2009-06-15
tags: ["post","Internet Social Impact","Semantic Web","Open Data"]
legacy_wordpress_id: 496
---

From: [Sir Tim Berners-Lee to advise the Government on public information delivery - PublicTechnology.net](http://www.publictechnology.net/print.php?sid=20307)

> The Prime Minister has announced the appointment of the man credited with inventing the World Wide Web, Sir Tim Berners-Lee as expert adviser on public information delivery. The announcement was part of a statement on constitutional reform made in the House of Commons this afternoon.
> 
> Sir Tim Berners-Lee, who is currently director of the World Wide Web Consortium which overseas the web's continued development. He will head a panel of experts who will advise the Minister for the Cabinet Office on how government can best use the internet to make non-personal public data as widely available as possible.
> 
> He will oversee the work to create a single online point of access for government held public data and develop proposals to extend access to data from the wider public sector, including selecting and implementing common standards. He will also help drive the use of the internet to improve government consultation processes.

TimBL talked about this at TED2009 and the video is below:

<!-- excerpt -->

<div style="max-width:854px"><div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://embed.ted.com/talks/tim_berners_lee_the_next_web" width="854" height="480" style="position:absolute;left:0;top:0;width:100%;height:100%" frameborder="0" scrolling="no" allowfullscreen></iframe></div></div>

This is fantastic news, of course. Ambitious timescales, following the lead of the Obama administration, opening up government data for re-use as well as public oversight. All very good things.

The technical challenges in doing this will be very interesting. First off, the service will undoubtedly by <a class="zem_slink" title="Linked Data" rel="wikipedia" href="http://en.wikipedia.org/wiki/Linked_Data">Linked Data</a> - the pattern of the Semantic Web or Web of Data. TimBL has been describing the efforts of the Linked Open Data community as "the web done right" for some time now. Linked data is also the approach taken by the US administration and is really starting to gather pace just like the early days of the document web. That will be interesting to see as it's a different discipline to developing a basic html site with a different set of balances and trade-offs in the data modeling, granularity, URI design and so on.

Second up will be scaling to meet the traffic demand. As both a high profile linked data service and <a class="zem_slink" title="Her Majesty's Government" rel="homepage" href="http://www.number10.gov.uk/">UK government</a> data it will be highly in demand from day one. Coping with peak traffic loads is not technically difficult as long as someone has their eye on that ball from the start. It's likely that demand for this data will be global, at least from those exploring what has been published, so traffic could get very high indeed. One of the aspects that might make this easier is that it will almost certainly be read-only for the foreseeable future, and that allows far more flexibility (and simplicity) in the approach to scaling.

Talking of it being read-only... Being a high profile data-source there will need to be a focus on securing it, not to prevent access, but to prevent unauthorised changes. Given the current atmosphere surrounding MPs expense claims and the level of voting in the recent European parliament elections it seems obvious that this will be a target for disgruntled and technically adept individuals both here and abroad. The read-only nature of the service helps make this easier, as does the linked data approach as that is the same in many security respects to the web of documents we have today - that is, securing it is well understood.

Definitely a project to watch closely.

[Disclosure - I work for <a title="Talis" rel="homepage" href="http://www.talis.com/">Talis</a>, a software company that offers a <a title="Talis Platform" href="http://www.talis.com/platform/">semantic web platform</a> for doing this kind of publishing]
