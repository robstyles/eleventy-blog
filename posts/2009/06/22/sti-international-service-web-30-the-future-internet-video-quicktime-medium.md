---
layout: post.pug 
title: "STI International - Service Web 3.0 - The Future Internet Video - Quicktime - medium"
date: 2009-06-22
tags: ["post","Internet Social Impact","Working at Talis","Semantic Web"]
legacy_wordpress_id: 500
---

This video explains really well what I've been doing the past few years at Talis.

<object classid="clsid:02bf25d5-8c17-4b23-bc80-d3488abddc6b" width="540" height="322" codebase="http://www.apple.com/qtactivex/qtplugin.cab#version=6,0,2,0"><param name="src" value="http://www.sti2.org/images/stories/videos/sti_qt_540_320.mov" /><param name="autoplay" value="false" /><param name="scale" value="Aspect" /><embed type="video/quicktime" width="540" height="322" src="http://www.sti2.org/images/stories/videos/sti_qt_540_320.mov" scale="Aspect" autoplay="false"></embed></object>

<!-- excerpt -->

the original can be found at [STI International - Service Web 3.0 - The Future Internet Video - Quicktime - medium](http://www.sti2.org/service-web-3-0-the-future-internet-mov-medium#).
