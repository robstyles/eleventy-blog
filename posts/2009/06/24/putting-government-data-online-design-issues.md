---
layout: post.pug 
title: "Putting Government Data online - Design Issues"
date: 2009-06-24
tags: ["post","Internet Social Impact","Semantic Web","Open Data"]
legacy_wordpress_id: 502
---

> Government data is being put online to increase accountability, contribute valuable information about the world, and to enable government, the country, and the world to function more efficiently. All of these purposes are served by putting the information on the Web as Linked Data. Start with the "low-hanging fruit". Whatever else, the raw data should be made available as soon as possible. Preferably, it should be put up as Linked Data. As a third priority, it should be linked to other sources. As a lower priority, nice user interfaces should be made to it -- if interested communities outside government have not already done it. The Linked Data technology, unlike any other technology, allows any data communication to be composed of many mixed vocabularies. Each vocabulary is from a community, be it international, national, state or local; or specific to an industry sector. This optimizes the usual trade-off between the expense and difficulty of getting wide agreement, and the practicality of working in a smaller community. Effort toward interoperability can be spend where most needed, making the evolution with time smoother and more productive.
from Tim Berners-Lee [Putting Government Data online - Design Issues](http://www.w3.org/DesignIssues/GovData.html).
