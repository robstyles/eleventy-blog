---
layout: post.pug 
title: "data and anti-data"
date: 2009-06-03
tags: ["post","Internet Technical","Working at Talis","Talis Technical","commands I have issued","Semantic Web"]
legacy_wordpress_id: 493
---

php -r "include 'moriarty/moriarty.inc.php'; include 'moriarty/changeset.class.php'; \$data=file_get_contents('megarecord.rdf.xml'); \$cs = new ChangeSet(array('before'=&gt; \$data)) ; echo \$cs-&gt;to_rdfxml();" &gt; removal_changeset.rdf.xml

<!-- excerpt -->
