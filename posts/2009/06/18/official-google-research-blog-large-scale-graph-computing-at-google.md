---
layout: post.pug 
title: "Official Google Research Blog: Large-scale graph computing at Google"
date: 2009-06-18
tags: ["post","Software Engineering"]
legacy_wordpress_id: 498
---

from [Official Google Research Blog: Large-scale graph computing at Google](http://googleresearch.blogspot.com/2009/06/large-scale-graph-computing-at-google.html).

> If you squint the right way, you will notice that graphs are everywhere. For example, social networks, popularized by Web 2.0, are graphs that describe relationships among people. Transportation routes create a graph of physical connections among geographical locations. Paths of disease outbreaks form a graph, as do games among soccer teams, computer network topologies, and citations among scientific papers. Perhaps the most pervasive graph is the web itself, where documents are vertices and links are edges. Mining the web has become an important branch of information technology, and at least one major Internet company has been founded upon this graph.
Just like Map/Reduce, Logic programming or OO, having more ways of thinking about a problem is a good thing :-)

<!-- excerpt -->
