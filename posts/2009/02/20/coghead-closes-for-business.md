---
layout: post.pug 
title: "Coghead closes for business"
date: 2009-02-20
tags: ["post","Internet Technical","Software Engineering","Internet Social Impact","Software Business"]
legacy_wordpress_id: 452
---

With the announcement that [Coghead, a really very smart app development platform, is closing its doors](http://blogs.zdnet.com/collaboration/?p=349) it's worth thinking about how you can protect yourself from the inevitable disappearance of a service.

Of course, there are all the obvious business type due diligence activities like ensuring that the company has sufficient funds, understanding how
your subscription covers the cost (or doesn't) of what you're using and so on, but all these can do is make you feel more comfortable - they can't provide real protection. To be protected you need 4 key things - if you have these 4 things you can, if necessary, move to hosting it yourself.
<ol>
	<li>URLs within your own domain.</li>
Both you and your customers will bookmark parts of the app, email links, embed links in documents, build excel spreadsheets that download the data and so on and so on. You need to control the DNS for the host that is running your tenancy in the SaaS service. Without this you have no way to redirect your customers if you need to run the software somewhere else.

This is, really, the most important thing. You can re-create the data and the content, you can even re-write the application if you have to, but if you lose all the links then you will simple disappear.
	<li>Regular exports of your data.</li>
You may not get much notice of changes in a SaaS service. When you find they are having outages, going bust or simply disappear is not the time to work out how to get your data back out. Automate a regular export of your data so you know you can't lose too much. Coghead allowed for that and are giving people time to get their data out.
	<li>Regular exports of your application.</li>
Having invested a lot in working out the write processes, rules and flows to make best use of your app you want to be able to export that too. This needs to be exportable in a form that can be re-imported somewhere else. Coghead hasn't allowed for this, meaning that Coghead customers will have to re-write their apps based on a human reading of the Coghead definitions. Which brings me on to my next point...
	<li>The code.</li>
You want to be able to take the exact same code that was running SaaS and install it on your own servers, install the exported code and data and update your DNS. Without the code you simply can't do that. Making the code open-source may be a problem as others could establish equivalent services very quickly, but the software industry has had ways to deal with this problem through escrow and licensing for several decades. The code in escrow would be my absolute minimum.</ol>
SaaS and PaaS (Platform as a Service) providers promote a business model based on economies of scale, lower cost of ownership, improved availability, support and community. These things are all true even if they meet the four needs above - but the priorities for these needs are with the customer, not with the provider. That's because meeting these four needs makes the development of a SaaS product harder and it also makes it harder for any individual customer to get setup. We certainly don't meet all four with our SaaS and PaaS offerings at work yet, but I am confident that we'll get there - and we're not closing our doors any time soon ;-)
