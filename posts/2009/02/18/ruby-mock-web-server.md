---
layout: post.pug 
title: "Ruby Mock Web Server"
date: 2009-02-18
tags: ["post","Internet Technical","Software Engineering","Other Technical","Working at Talis","Talis Technical"]
legacy_wordpress_id: 447
---

I spent the afternoon today working with Sarndeep, our very smart automated test guy. He's been working on extending what we can do with rspec to cover testing of some more interesting things.

Last week he and [Elliot](http://townx.org/) put together a great set of tests using [MailTrap](http://rubymatt.rubyforge.org/mailtrap/) to confirm that we're sending the right mails to the right addresses under the right conditions. Nice tests to have for a web app that generates email in a few cases.

<!-- excerpt -->

This afternoon we were working on a mock web server. We use a lot of RESTful services in what we're doing and being able to test our app for its handling of error conditions is important. We've had a static web server set up for a while, this has particular requests and responses configured in it, but we've not really liked it because the responses are all separate from the tests and the server is another apache vhost that has to be setup when you first checkout the app.

So, we'd decided a while ago that we wanted to put in a little Ruby based web server that we could control from within the rspec tests and that's what we built a first cut of this afternoon.

```

```
require File.expand_path(File.dirname(__FILE__) + "/../Helper")
require 'rubygems'
require 'rack'
require 'thin'
class MockServer
  def initialize()
    @expectations = []
  end
  def register(env, response)
    @expectations << [env, response]
  end
  def clear()
    @expectations = []
  end
  def call(env)
    #puts "starting call\n"
    @expectations.each_with_index do |expectation,index|
      expectationEnv = expectation[0]
      response = expectation[1]
      matched = false
      #puts "index #{index} is #{expectationEnv} contains #{response}\n\n"
      expectationEnv.each do |envKey, value|
        puts "trying to match #{envKey}, #{value}\n"
        matched = true
        if value != env[envKey]
          matched = false
          break
        end
      end
      if matched
        @expectations.delete_at(index)
        return response
      end
    end
    #puts "ending call\n"
  end
end
mockServer = MockServer.new()
mockServer.register( { 'REQUEST_METHOD' => 'GET' }, [ 200, { 'Content-Type' => 'text/plain', 'Content-Length' => '11' }, [ 'Hello World' ]])
mockServer.register( { 'REQUEST_METHOD' => 'GET' }, [ 200, { 'Content-Type' => 'text/plain', 'Content-Length' => '11' }, [ 'Hello Again' ]])
Rack::Handler::Thin.run(mockServer, :Port => 4000)
```

```

The MockServer implements the [Rack](http://rack.rubyforge.org/) interface so it can work within the [Thin](http://code.macournoyer.com/thin/) web server from inside the rspec tests. The expectations are registered with the MockServer and the first parameter is simply a hashtable in the same format as the [Rack Environment](http://rack.rubyforge.org/doc/files/SPEC.html). You only specify the entries that you care about, any that you don't specify are not compared with the request. Expectations don't have to occur in order (expect where the environment you give is ambiguous, in which case they match first in first matched).

As a first venture into writing more in Ruby than an rspec test I have to say I found it pretty sweet - There was only one issue with getting at array indices that tripped me up, but [Ross](http://dilettantes.code4lib.org/) helped me out with that and it was pretty quickly sorted.

Plans for this include putting in a verify() and making it thread safe so that multiple requests can come in parallel. Any other suggestions (including improvements on my non-idiomatic code) very gratefully received.
