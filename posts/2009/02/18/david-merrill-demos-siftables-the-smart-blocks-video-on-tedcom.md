---
layout: post.pug 
title: "David Merrill demos Siftables, the smart blocks | Video on TED.com"
date: 2009-02-18
tags: ["post","Interaction Design"]
legacy_wordpress_id: 445
---

[David Merrill demos Siftables, the smart blocks | Video on TED.com](http://www.ted.com/index.php/talks/david_merrill_demos_siftables_the_smart_blocks.html).

<div style="max-width:854px"><div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://embed.ted.com/talks/david_merrill_toy_tiles_that_talk_to_each_other" width="854" height="480" style="position:absolute;left:0;top:0;width:100%;height:100%" frameborder="0" scrolling="no" allowfullscreen></iframe></div></div>

<!-- excerpt -->

Siftables are really interesting, very similar to [Reactables](http://mtg.upf.edu/reactable/) which I blogged about while talking about [more multi-touch UI](/2007/02/07/more-multi-touch-ui/) last year.

Siftables are generic blocks, each has a wireless connection to a nearby computer, a proximity sensor so it can identify other siftables nearby and a small screen. The manipulation of the blocks at this stage seems a little bit slow, but the work shown with kids gives a really clear indication of the advantages of this kind of tactile interface.

I want some.
