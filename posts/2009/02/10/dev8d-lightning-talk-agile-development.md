---
layout: post.pug 
title: "dev8D | Lightning talk: Agile Development"
date: 2009-02-10
tags: ["post","Software Engineering"]
legacy_wordpress_id: 430
---

This is a great post on agile development coming out from the JISC Dev8d days.

> Example from the floor, Matthew: what worked well in a commercial company I was working for where we practiced extreme coding and used agile principles was: no code ownership (bound by strict rules), test-based development, rules about simplicity, never refactoring until you have to, stand up meetings, whiteboard designs, iterations so could find out when you’d messed something up almost immediately, everything had to have unit tests, there has to be a lot of trust in the system (you have to know that someone is not going to break your code)

<!-- excerpt -->

> Graham: building trust is central.
via [dev8D | Lightning talk: Agile Development](http://dev8d.jiscinvolve.org/2009/02/10/lightning-talk-agile-development/).

The quote above from Matthew and Graham mirrors exactly my experience - when we do those things well, and are disciplined about it and trust each other the things work out well. When we do less of those things then things turn out less well.

Graham is [Graham Klyne](http://www.ninebynine.net/People/GKDetail.html) who I've met a few times at various meets like [Vocamp 2008 in Oxford](/2008/09/24/vocamp-2008-oxford/). He and his team are doing clever things with pictures of flies and semweb technologies.
