---
layout: post.pug 
title: "Join the Internet Blackout - Protest Against Guilt Upon Accusation Laws in NZ — Creative Freedom Foundation (creativefreedom.org.nz)"
date: 2009-02-16
tags: ["post","IP Law","Intellectual Property"]
legacy_wordpress_id: 433
---

Update: The NZ government have suspended the introduction of Section 92a (via [MiramarMike](http://work.miramarmike.co.nz/2009/02/this-website-is-blacked-out.html))

An interesting campaign to 'blackout' your online presence to campaign for change to one of NZ's clauses started today.[ Protest Against Guilt Upon Accusation Laws in NZ — Creative Freedom Foundation (creativefreedom.org.nz)](http://creativefreedom.org.nz/blackout.html). I spotted this a few days ago thanks to [Mike Brown](http://twitter.com/maupuia) who tweeted about it.

<!-- excerpt -->

What's interesting about the law is that it changes the presumption of guilt quite significantly. Currently in most Copyright jurisdictions if someone is infringing your copyright then the first thing you'd do (after asking them politely to stop) is take out an injunction against them. This involves persuading the court that you have enough of a case that the (alleged) infringer should be told to stop until the case is heard. The bar for getting an injunction is, then, quite high.

What [Section 92 of the Copyright Amendment Act](http://www.legislation.govt.nz/act/public/2008/0027/latest/link.aspx?id=DLM346240#DLM346240) does is compels ISPs (and there is a broad definition of that term in the law) to take down sites or revoke internet access when an accusation of infringement is made. The clause looks like this:

> <h4>Internet service provider liability</h4>
> <h5>92A Internet service provider must have policy for terminating accounts of repeat infringers</h5>
> <ul>
> 	<li>(1) An Internet service provider must adopt and reasonably implement a policy that provides for termination, in appropriate circumstances, of the account with that Internet service provider of a repeat infringer.</li>
> 	<li>(2) In subsection (1), repeat infringer means a person who repeatedly infringes the copyright in a work by using 1 or more of the Internet services of the Internet service provider to do a restricted act without the consent of the copyright owner.</li>
> </ul>
The potential downsides of a law like this are many, but one of the biggest is the impact it is likely to have on fair-use. Fair use is not explicitly defined and is clarified by case law, though some common examples are often postulated - parody, criticism, illustration are often quoted. There are also tests around the commercial impact of the use.

What this means is that Copyright is not absolute, it's a negotiation between creators and the state to strike a balance that is most effective for the country's cultural and economic prosperity. This clause changes that for internet based uses by preventing that negotiation and also by makes people more fearful by increasing the immediate penalty for an accusation of infringement from very little to the loss of internet service. That could be enough to close many small businesses.

The reasoning behind the bill is one of practicality. Those with large catalogs of Copyright works, such as the music labels, are having a really tough time preventing copying on the internet (because teh internet is one big copying machine). The reason is that the current laws make pursuing people difficult and expensive as the RIAA have found out in the states. The solution in Section 92, though, may be a little heavy handed. ISPs are likely to comply with the law, and the cheapest thing for them to do is simply take down anything they're asked to. ISPs are a commodity, they don't have big profit margins to use up helping you keep your content up online.

[Labour <abbr title="Minister in Parliament">MP</abbr> Judith Tizard is quoted as saying](http://creativefreedom.org.nz/s92.html)

> It is easier for ISPs, Internet Service Providers, to cut off anyone who ___might___ be breaking the law.
Now, this seems to be a more and more common perception. That it would be too much trouble to ask a copyright holder to file suit and that ISPs look perfectly placed to handle issues. What that misses though is that ISPs are not at all equipped to perform any kind of arbitration, so with an individual customer on one side and a large, wealthy corporate lawyer on the other the ISP will always play it safe.

If this were happening in the US then I wouldn't even have blogged about it, but it seems odd to me that this is happening at almost exactly the same time, and in the same city, as [Webstock](http://www.webstock.org.nz/), one of the best web conferences in the world.
