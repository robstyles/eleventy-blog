---
layout: post.pug 
title: "Linked Data on the Web (LDOW2009) - Workshop at WWW2009, Madrid, Spain"
date: 2009-02-13
tags: ["post","event"]
legacy_wordpress_id: 442
---

I've been asked to be on the programme committee for [LDOW2009](http://events.linkeddata.org/ldow2009/#committee). It looks set to another great workshop and the three papers I've been assigned to review all look brilliantly interesting. Unfortunately not allowed to blog about them :-(

<!-- excerpt -->
