---
layout: post.pug 
title: "We're hiring..."
date: 2009-07-15
tags: ["post","Working at Talis"]
legacy_wordpress_id: 515
---

Fancy a job building great web apps? Interested in being an early part of publishing large amounts of data on the semantic web? Want to help build fantastically useful search interfaces to be used by millions of people? [We're hiring](http://www.talis.com/careers/).

We're looking for a Web Application Technical Lead who knows how to build great web interfaces and wants to get into the next wave of the web, Linked Data and the semantic web.

<!-- excerpt -->

The role is to lead the development of Talis Prism, a flagship product for us and for our customers. Those customers are the biggest public and academic libraries in the UK, so Prism gets used by millions of people all over the country every day.

[The job spec](http://www.talis.com/careers/documents/web_application_lead.pdf) (pdf) gives you more detail, but one of the things we ask is that you take a pop at answering any _two_ from the following three questions.
<ol>
	<li>Ensuring web applications work effectively across different browsers is hard. Explain how
you would go about ensuring a web application functions correctly with Yahoo's list of A-
grade browsers, covering both development and testing approaches.</li>
	<li>URIs play a very significant role in the way a site appears on the web; WordPress blogs, for
example, have a variety of URI schemes they can use. HttpRange-14 adds further
implications for the use of # based URI schemes. Outline a URI scheme for a car dealership
website and explain the trade-offs made?</li>
	<li>If you were asked to write a book based on your technical expertise, what would the title be
and what chapters would it contain?</li>
</ol>
Now, because I'm really friendly (and because it's my blog), I'll give you some pointers on what we might be looking for.

With question 1, you've got to recognise that Prism is a SaaS product with a frequent release cycle, releasing to the live service once a month currently. That means any answer that talks about specs, manual test plans or requirement documents isn't going to get you very far. Think about what you'd need to do if we wanted to do continuous deployment - from checkin to release in less than 30 minutes say?

On question 2 we'll be looking for your understanding of how HTTP URIs work and how different choices work differently with browser caching, proxy servers and server-side code. If you don't know what HttpRange-14 is then read the [draft tag finding on dereferencing HTTP URIs](http://www.w3.org/2001/tag/doc/httpRange-14/2007-05-31/HttpRange-14). Take a look at [How to Publish Linked Data on the Web](http://linkeddata.org/docs/how-to-publish).

Question 3, if it cropped up in a book on interviews and job applications, would be answered as "an ideal opportunity to re-present the information on your CV". That's because most people who interview haven't really read your CV, so you have to say things several times. We will have read your CV, we'll have gone through it with a fine tooth comb actually, checking all the dates and cross-referencing the technologies listed. We'll have checked out all the sites and companies you list - even if you don't give us links to them. We like to know who we're interviewing, so we'll have googled you and looked you up on Facebook, LinkedIn, Twitter and anywhere else we think you might hang out. Please don't feel stressed about that, we're not going to be upset if there's a photo of you drunk at a party or if you once tweeted the F word. So, there's no need for the book you'd write to be a game of buzzword bingo, we're just curious about what excites and motivates you.

All in all though, we're looking for great people to come and help us do great stuff. Get in touch!
