---
layout: post.pug 
title: "Paul Miller is right... and so is Ian Davis"
date: 2009-07-20
tags: ["post","Semantic Web"]
legacy_wordpress_id: 519
---

<a class="zem_slink" title="Paul Miller" rel="blog" href="http://cloudofdata.com/">Paul Miller</a>, a good friend and ex-colleague, has been having a tough time arguing that perhaps [Linked Data doesn't need RDF](http://cloudofdata.com/2009/07/does-linked-data-need-rdf/). Don't misunderstand that, he thinks RDF is a Good Thing and Best Practice for <a class="zem_slink" title="Linked Data" rel="wikipedia" href="http://en.wikipedia.org/wiki/Linked_Data">Linked Data</a>. But he thinks a dogmatic stance is unhelpful.

> The problem, I contend, comes when well-meaning and knowledgeable advocates of both Linked Data and RDF conflate the two and infer, imply or assert that ‘Linked Data’ can only be Linked Data if expressed in RDF.
> 
> This dogmatism makes me deeply uncomfortable, and I find myself unable to agree with the underlying premise.
In the twitter stream that Paul links to there is some comment reminding people that RDF can take many forms, not just RDF/XML.

> kidehen: @andypowe11 re. #rdf, it's the data model for #linkeddata based #metadata. Remember #rdf != RDF/XML, no escaping RDF model re. #linkeddata.
Ian Davis (my boss) took a strong stance saying that if things weren't RDF then they weren't linked data. Perhaps the very thing Paul sees as a dogmatic stance. Ironic as Ian is far from dogmatic. But Ian is defending the term Linked Data, not saying that's the only way to publish data on the web...

> TallTed: @iand "I think LD better for many cases, but there are times i'd rather hv a spreadsheet." What? Can a spreadsheet not hold #LinkedData?
Well, it seems to me both Paul and Ian are right to a strong degree and are essentially arguing over only one thing - the meaning of the term Linked Data.

Paul quote Tim Berners-Lee's design note on Linked Data:

<!-- excerpt -->

> 1. Use URIs as names for things
> 
> 2. Use HTTP URIs so that people can look up those names
> 
> 3. When someone looks up a URI, provide useful information, __using the standards (RDF, SPARQL)__
> 
> 4. Include links to other URIs. so that they can discover more things.
The emphasis is Paul's. I would emphasise a different point:

> __4. Include links to other URIs. so that they can discover more things.__
And in point four lies the reason that Ian is saying a spreadsheet isn't Linked Data, even if it's on the web and even if it's linked _to_. The only standard for describing how one resource relates to others using URIs is RDF. Sure, you can put URIs into a spreadsheet, but there is no standard interpretation of what the sheets, rows and columns mean. Sure, you can put URIs into a CSV file, but again, there is no standard interpretation of what the fields mean.

The end result of that is data published on the web that can be linked _to_ but not _from_.

At this early time, though, Paul argues that what we really want is to get more and more data published and open. We all agree on that, I know. Ian does for sure, he runs [Data Incubator](http://dataincubator.org/) for exactly that reason - well, that and helping show those publishing spreadsheets and CSV why they should move to RDF and Linked Data.

In the comments on Paul's post Justin (another senior manager at <a class="zem_slink" title="Talis Group" rel="homepage" href="http://www.talis.com/">Talis</a>) says:

> Yes the same mistake was made with the rise of the web.
> 
> Once you had URIs and HTTP you already had plain text which is a perfectly good way to encode content. By adopting the STANDARD convention of HTML, all sort of existing text based formats with their various mark ups were locked out. That locked out a lot of content that already existed and required anyone who wanted to play to convert existing content into a html format.
> 
> Of course it did have the small side effect that to consume web content you only needed a browser that understood one convention i.e. html.
> 
> The same is true of RDF. XML is the equivalent of ascii in this regard.
And that's the point. XML is the equivalent of ASCII, as is a spreadsheet or a CSV file, not because they're simple, but because they have no mechanism for embedding the relationships and links necessary to link out from your data. Yes, they can contain URIs and clients can decide to make those into links, but there is no way to describe the meaning.

I agree with both side of this argument - If it isn't RDF then it isn't Linked Data, but I wouldn't keep pushing that point if someone was willing to publish data yet unable or unwilling to publish RDF (in any of its many forms).
