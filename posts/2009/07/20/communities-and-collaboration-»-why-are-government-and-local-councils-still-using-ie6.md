---
layout: post.pug 
title: "Communities and Collaboration » Why are Government and Local Councils still using IE6?"
date: 2009-07-20
tags: ["post","Internet Social Impact"]
legacy_wordpress_id: 517
---

Steve Dale pushing question of why local and central government is still using IE6.

> The latest information on IE6 market share is just over 12%. I’m betting that a good proportion of this 12% is public sector workers who continue to be poorly served by their IT departments and CIOs who don’t see the browser as being an important component in improving user productivity.
from [Communities and Collaboration » Why are Government and Local Councils still using IE6?](http://steve-dale.net/2009/07/20/why-are-government-and-local-councils-still-using-ie6/).

<!-- excerpt -->
