---
layout: post.pug 
title: "OTTO - Controllerism Instrument at djtechtools.com"
date: 2009-07-02
tags: ["post","Interaction Design"]
legacy_wordpress_id: 508
---

from [OTTO - Controllerism Instrument at djtechtools.com](http://www.djtechtools.com/2009/07/02/otto-beat-slicer/):

> Controllerism continues to take small leaps forward as the software and techniques improve but the giant steps are going to happen in the realm of performance interfaces. Without a solid controller surface that has been designed to play like an instrument we wont be able to leave the realm of noodling and enter the fabled land of flow.
Specialised controllers continue to evolve and this is a really interesting, focussed, loop controller.

<!-- excerpt -->
