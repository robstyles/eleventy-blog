---
layout: post.pug 
title: "What else? « Web of Data"
date: 2009-07-21
tags: ["post","Semantic Web"]
legacy_wordpress_id: 521
---

great explanation from Dan Brickley:

> The non-RDF bits of the data Web are – roughly – going to be the leaves on the tree. The bit that links it all together will be, as you say, the typed links, loose structuring and so on that come with RDF. This is also roughly analagous to the HTML Web: you find JPEGs, WAVs, flash files and so on linked in from the HTML Web, but the thing that hangs it all together isn’t flash or audio files, it’s the linky extensible format: HTML. For data, we’ll see more RDF than HTML (or RDFa bridging the two). But we needn’t panic if people put non-RDF data up online…. it’s still better than nothing. And as the LOD scene has shown, it can often easily be processed and republished by others. People worry too much! :)
from [What else? « Web of Data](http://webofdata.wordpress.com/2009/07/20/what-else/#comment-132).

<!-- excerpt -->
