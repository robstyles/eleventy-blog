---
layout: post.pug 
title: "Sharing and re-use of catalogue records: what are the legal implications? : Information Environment Team"
date: 2009-07-29
tags: ["post","Internet Social Impact","IP Law","Intellectual Property"]
legacy_wordpress_id: 530
---

> The records in a university library catalogue typically have many different origins: created by the library, obtained from a national library or a book supplier etc. So, who ‘owns’ them? And what are the legal implications of making them available to others when this involves copying, transferring them into different formats, etc.?
> 
> The JISC has just commissioned a study to explore some of these issues as they apply to UK university libraries and to provide practical guidance to library managers who may be interested in making their catalogue records available in new ways. Outcomes are expected by the end of 2009.
from [Sharing and re-use of catalogue records: what are the legal implications? : Information Environment Team](http://infteam.jiscinvolve.org/2009/07/29/sharing-and-re-use-of-catalogue-records-what-are-the-legal-implications/).

<!-- excerpt -->
