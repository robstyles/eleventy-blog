---
layout: post.pug 
title: "Vanish: Enhancing the Privacy of the Web with Self-Destructing Data"
date: 2009-07-27
tags: ["post","Internet Social Impact","Security And Privacy"]
legacy_wordpress_id: 528
---

> Computing and communicating through the Web makes it virtually impossible to leave the past behind. College Facebook posts or pictures can resurface during a job interview; a lost or stolen laptop can expose personal photos or messages; or a legal investigation can subpoena the entire contents of a home or work computer, uncovering incriminating or just embarrassing details from the past.
> 
> Vanish is a research system designed to give users control over the lifetime of personal data stored on the web or in the cloud. Specifically, all copies of Vanish encrypted data — even archived or cached copies — will become permanently unreadable at a specific time, without any action on the part of the user or any third party or centralized service.
from [Vanish: Enhancing the Privacy of the Web with Self-Destructing Data](http://vanish.cs.washington.edu/).

<!-- excerpt -->
