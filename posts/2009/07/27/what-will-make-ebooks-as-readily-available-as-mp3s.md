---
layout: post.pug 
title: "What will make eBooks as readily available as MP3s?"
date: 2009-07-27
tags: ["post"]
legacy_wordpress_id: 526
---

[caption id="" align="alignleft" width="500" caption="Printing Press by Thomas Hawk, Licensed cc-nc"][<img title="Printing Press by Thomas Hawk" src="http://farm1.static.flickr.com/76/172495285_9e2ba5bf28.jpg" alt="Printing Press by Thomas Hawk, Licensed cc-nc" width="500" height="316" />](http://www.flickr.com/photos/thomashawk/172495285/)[/caption]

I was talking to a colleague recently about ebooks and the lack of access to course text books electronically. I asked why he thought that was, and he suggested that we were waiting for digital rights management to be sorted out - he meant that in his view we were waiting for DRM technology to be strong enough to protect publishers' intellectual property rights.

<!-- excerpt -->

This struck me as interesting, as that certainly wasn't the case with music where DRM has been struggling (and failing) to catch up for some time. Then, last week, came the news that [Amazon had recalled a book it had previously sold](http://www.guardian.co.uk/technology/2009/jul/17/amazon-kindle-1984), at the publisher's behest, deleting it from everyone's Kindle and refunding them. [James Grimmelmann reminds us he warned of Amazon's terms](http://laboratorium.net/archive/2009/07/19/from_the_laboratorium_archives_dont_buy_that_kindl) and suggests we need new laws around digital property rights.

We'd also been discussing this at work, in the context of how digital music has disrupted things and attempting to predict how and what ebooks will disrupt and when. Then, Roy Tennant pops up saying [Print is SO Not Dead](http://www.libraryjournal.com/blog/1090000309/post/1300046930.html). All of these got me thinking more about it.

The major trigger for digital music was the MP3 player, some cheap, some cool, both hardware and software. People bought MP3 players instead of CD, Mini-disc and cassette because they were smaller, could hold more music and had better battery life. Initially you put music onto them by ripping the CDs you already owned. i.e. there was a cheap, easy way to get digital music onto them from your existing media. We'll leave [the legality of ripping CDs](http://betweenlawyers.corante.com/archives/2005/06/06/ipods_and_timeshifting_fair_use_personal_use_and_the_digital_copyright_morass.php) to others.

It was this ability to get music into digital file form that led to online music sharing, and subsequently to the publishing of non-DRM MP3 files from major record labels. The ease with which music could be made available in digital form for anyone to use is what changed the recording industries business and gave consumers what they wanted - cheap, DRM-free music from their favourite artists.

DRM didn't work for music for many reasons, not least because of the ease with which people could get hold of DRM-free copies. Other contributing factors included the profusion of cheap MP3 players, these players meant people didn't just want DRM-free music, but_needed_ it because their cheap players wouldn't play DRM. Those cheap players wouldn't implement DRM because of the increased hardware cost of supporting it as well as the licensing cost of many of the schemes. Remember, we're not talking about a £200 iPod here, we're talking about a £5 USB stick with a headphone socket and 4 buttons.

The per unit manufacturing cost of an ebook reader is much higher, they'll sell in smaller volumes, they have more parts including a good screen and use newer technologies rather than off-the-shelf components. The proportion of cost that DRM would impose is a much smaller part of the total unit cost than it was for MP3 players.

A good few good ebook readers have come out over the past year or so including the first and second generation kindles, the BeBook and recently the Samsung Papyrus. All very nice and very capable. [Plastic Logic](http://www.plasticlogic.com/) are on the brink of launching a nice new, very lightweight plastic reader.

But there's still something missing - the books.

That's not to say nothing's progressing - a great number of books are available and I've not heard anyone complaining that they couldn't get anything, but it's still a tiny drop in the ocean, [Barnes &amp; Noble launching a store with 700,000 books](http://digital-scholarship.org/digitalkoans/2009/07/21/barnes-nobles-ebookstore-offers-over-700000-e-book-titles/), compared to Kindle's "[Over 300,000 eBooks, Newspapers, Magazines, and Blogs](http://www.amazon.com/kindle-store-ebooks-newspapers-blogs/b?ie=UTF8&amp;node=133141011)". To put that in context, the [Library of Congress alone which has 141,847,810 items in it's catalogue](http://www.loc.gov/about/generalinfo.html).

And I have a stack of books on my desk, real ones with paper pages. And no way to easily get these onto my laptop.

This is due to several asymmetries. In music, the music is recorded and a player has always been required to reproduce the sound, whether analogue or digital. Books have had many advances in the production side, but not on the consumer side - books have never needed a player.

The second asymmetry is of the display and input of computers. [Bill Buxton](http://www.billbuxton.com/) talks about this a lot when explaining why computers are still on the periphery of life, rather than integrated through it. Essentially this comes down to the issue that the display on my laptop can't also see - there is no easy way to put a physical book into the computer.

So where does that leave the take-up of ebooks? The publishers seem to be in the same position the record industry was in some time ago, but without the driver to change. With music, consumers were able to say "if you won't do this, we'll do it ourselves" but with books that isn't as easy. There aren't students out there copying text books to give to their fellow students.

So without an obvious source of DRM-free ebooks - ones that people really want to read - and DRM as a much lower part of the manufacturing cost it seems unlikely that we're going to see cheap, non-DRM ebook readers being taken up by lots of people.

So, in the absence of consumer-led digitisation of everyone's _existing_ collections, and assuming Google's book scans don't become freely available, what reason do publishers have to really support open and flexible digital publishing? None that I can see.

So this is where DRM may actually come in useful - in providing the mechanism that allows publishers to release those precious digital copies into the marketplace.
