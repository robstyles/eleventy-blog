---
layout: post.pug 
title: "Excel RDF"
date: 2009-07-22
tags: ["post","Semantic Web"]
legacy_wordpress_id: 524
---

<h3>Introduction</h3>
When world's collide sometimes things happen that can be useful. This is _not_ one of those useful things, but a collision of two worlds none-the-less...

ExcelRDF is a proposed serialisation for RDF using the Microsoft Excel Spreadsheet format. This work was inspired by the discussions in the semantic web community about Linked Data and whether or not it mandates the use of RDF. This document is not trying to prove a point, insult anyone or come down on either side of the argument. I just noticed that it hadn't been done and it didn't seem too difficult. Of course, that it hadn't been done should have been enough of a warning to me that it is not, in any sense, desirable.
<h3>Conventions used in this document</h3>
The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in [RFC 2119](http://www.ietf.org/rfc/rfc2119.txt).
<h3>Overview</h3>
When a server receives a HTTP or HTTPS request for a resource that is described in RDF and the client indicates that it is willing to accept content of type application/vnd.ms-excel the server MAY respond with a Microsoft Excel spreadsheet meeting the following conventions.
<ul>
	<li>The spreadsheet MUST contain one or more sheets that meet the following conventions.</li>
	<li>A sheet SHOULD contain zero or more rows of RDF data.</li>
	<li>Column A of each non-empty row of a sheet SHOULD contain a URI indicating the Subject of a statement.</li>
	<li>Column B of each non-empty row of a sheet SHOULD contain a URI indicating the Property of a statement.</li>
	<li>Column C MAY contain either a URI or a literal value as the Object of a statement.</li>
	<li>If Column C contains a literal value then Column D MAY contain a language identifier in accordance with [IETF BCP 47](http://www.w3.org/TR/REC-xml/#RFC1766).</li>
	<li>If Column C contains a literal value then Column E MAY contain a type specifier indicating the type of the literal value.</li>
</ul>
<h3>Example</h3>
The attached example Microsoft Excel spreadsheet contains [RDF from the dbpedia project describing Annette Island Airport](http://dbpedia.org/resource/Annette_Island_Airport) using the conventions described above.

[ExcelRDF Example File](/assets/excelrdf.xls)
<h3>Use Cases</h3>
ExcelRDF may be useful where it is desirable to produce charts showing characteristics of a dataset, such as the relative distribution of types within a dataset. Perhaps analysing the count of particular properties. I can think of no obvious way to assess graph characteristics such as linkiness, but you could do things like word counts in literals, or working out how of the literal data is in French.

ExcelRDF may be useful where a specific contract, policy or agreement means that the data must be delivered as an Excel spreadsheet while the underlying data is more useful in RDF.

<!-- excerpt -->

ExcelRDF may be useful if you wish to be deliberately obtuse.
