---
layout: post.pug 
title: "Agile Coach Code of Conduct - What To Fix"
date: 2009-07-30
tags: ["post","Software Business"]
legacy_wordpress_id: 532
---

> A while back I started on an Agile Coach Code of Conduct. I noticed that after coaching for a while I started to forget basic principles that should be part of every coaching engagement.
> 
> So I put this list together to help me (and others) remember what coaching is all about.
> 
> Like everything in agile, it's an ideal, not something you can ever perfectly do.
> <ol>
> 	<li>I will always remember that my teams are full of intelligent professionals acting in the best way that they can for project completion</li>
> 	<li> I will not direct or follow, but lead the team by example, which means walking a fine line between participation and observation</li>
> 	<li> I will always remember that I am a guest on each team, and will strive to behave respectfully to my hosts as much as I can</li>
> </ol>
read the full list of Daniel Markham's 12 principles at [Agile Coach Code of Conduct - What To Fix](http://www.whattofix.com/blog/archives/2009/05/agile-coach-cod.php).

<!-- excerpt -->
