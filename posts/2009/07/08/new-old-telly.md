---
layout: post.pug 
title: "New (old) Telly"
date: 2009-07-08
tags: ["post","Personal"]
legacy_wordpress_id: 510
---

So, after my [success fixing a Philips Plamsa TV](/2008/06/25/fixing-a-plasma-tv) my dad is giving me his Sony Bravia LCD KDL32V. It's not working following lightning storms and has been written off by the insurers following a £350 repair quote, but we all know that £350 buys you a handful of capacitors and maybe a triode or three from a pro repair shop.

Hence I pick it up tomorrow to take a look at :-)

<!-- excerpt -->

__Update__ 10/07/2009: picked it up yesterday and spent the evening looking at it. Some folks on #electronics (mr_boo, SpeedEvil and kludge) helped me work out that the broken bits were very likely the expensive silicon on the primary side of the power supply - difficult to replace and the main control chip, a Sony CXD9841P would cost me about £20.

So, having worked out it's not a nice easy case of swapping out a couple of transistors or caps, I decided to hunt down a whole board. Obvious approach was to find the same model on ebay that had suffered an encounter with a wii controller, there were a couple about but nothing really cheap or really close. Next step then was to try and find a new power board.

The original quote, from [BSS](http://bss-online.co.uk/) had quoted £180 for the board, plus a whole load of labour bringing a total over £300! Next try, [Audio Technical Services](http://www.audiotechnical.co.uk/) who refused to quote me a price because Sony would remove their Authorised status (and stop sending them business) if they sold an internal component to an end customer. Fantastic, good business Sony, thanks.

So, a quick google for the exact part number brought up [SJS Television Services](http://www.sjstv.co.uk/) who have the board in stock. A cheeky call to owner Stuart and he helped my plight to keep costs down by agreeing the board, inc vat and p&amp;p for £75. Really nice guy, shame he isn't local or I might have been able to point more people to him.

Now I just have to be patient for the part to arrive :-)
