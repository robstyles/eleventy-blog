---
layout: post.pug 
title: "Chase Jarvis Blog: Uber-Cool Video Projections On Buildings"
date: 2009-08-17
tags: ["post"]
legacy_wordpress_id: 553
---

> There's been a lot of hot fuss lately about what's possible with new projection media, especially in urban environments, onto building facades, etc. Last time I was in Paris there was similar stuff emerging on building walls in the Marais, but this seems to be evolving quickly and really taking off. Impressive live performance here in this video from NuFormer.

<!-- excerpt -->

<iframe src="https://player.vimeo.com/video/5677879?title=0&byline=0&portrait=0" width="640" height="368" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

[NuFormer, 3D video mapping projection - Live performance in Zierikzee, the Netherlands - 2009](https://vimeo.com/5677879) from [NuFormer](https://vimeo.com/nuformer) on [Vimeo](https://vimeo.com).

from [Chase Jarvis Blog: Uber-Cool Video Projections On Buildings](http://blog.chasejarvis.com/blog/2009/08/uber-cool-video-projections-on.html).

Interaction of 3D modelled world, projected onto the same building as in the model, allows awesome effects as the building is brought to life. Reminded me of the opening of the Atlantis Hotel, Dubai, which made similar attempts to use projection and lighting to change the building but failed to really make use of the building itself IMHO.

<iframe width="560" height="315" src="https://www.youtube.com/embed/7AcSs2Sp2hI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
