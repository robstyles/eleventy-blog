---
layout: post.pug 
title: "If you use numbers..."
date: 2009-08-13
tags: ["post","Random Thought","Semantic Web"]
legacy_wordpress_id: 549
---

<a title="If you use numbers... by mmmmmrob, on Flickr" href="http://www.flickr.com/photos/mmmmmrob/3817789964/"><img src="http://farm3.static.flickr.com/2674/3817789964_97bde9f138.jpg" alt="If you use numbers..." width="500" height="322" /></a>

Still not sure what's for the best, but the idea that opaque URIs are better because they're language independent doesn't ring true for me. A word is just as opaque as a GUID if you don't speak the language, but for those who can read it may be far clearer and easier to work with.

<!-- excerpt -->
