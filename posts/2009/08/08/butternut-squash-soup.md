---
layout: post.pug 
title: "Butternut Squash Soup"
date: 2009-08-08
tags: ["post","Food"]
legacy_wordpress_id: 541
---

<a title="IMG_6694 by mmmmmrob, on Flickr" href="http://www.flickr.com/photos/mmmmmrob/3801814214/"><img src="http://farm4.static.flickr.com/3453/3801814214_4c75c5736b.jpg" alt="IMG_6694" width="500" height="333" /></a>
<div id="setThumbs-indv3800967733_div">
<div>
<div id="setThumbs">
<div id="setThumbs-indv3800967733_div"><span id="photo_thumb3800967733"><a title="IMG_6677" href="http://www.flickr.com/photos/mmmmmrob/3800967733/in/set-72157621856959921/"><img src="http://farm4.static.flickr.com/3481/3800967733_e99871c53b_s.jpg" alt="IMG_6677" width="75" height="75" /></a></span></div>
<div id="setThumbs-indv3801790960_div"><span id="photo_thumb3801790960"><a title="IMG_6679" href="http://www.flickr.com/photos/mmmmmrob/3801790960/in/set-72157621856959921/"><img src="http://farm3.static.flickr.com/2522/3801790960_8ddb366fb8_s.jpg" alt="IMG_6679" width="75" height="75" /></a></span></div>
<div id="setThumbs-indv3801794182_div"><span id="photo_thumb3801794182"><a title="IMG_6681" href="http://www.flickr.com/photos/mmmmmrob/3801794182/in/set-72157621856959921/"><img src="http://farm4.static.flickr.com/3472/3801794182_29cac01f54_s.jpg" alt="IMG_6681" width="75" height="75" /></a></span></div>
<div id="setThumbs-indv3801797314_div"><span id="photo_thumb3801797314"><a title="IMG_6683" href="http://www.flickr.com/photos/mmmmmrob/3801797314/in/set-72157621856959921/"><img src="http://farm3.static.flickr.com/2530/3801797314_f24771be88_s.jpg" alt="IMG_6683" width="75" height="75" /></a></span></div>
<div id="setThumbs-indv3801800434_div"><span id="photo_thumb3801800434"><a title="IMG_6684" href="http://www.flickr.com/photos/mmmmmrob/3801800434/in/set-72157621856959921/"><img src="http://farm4.static.flickr.com/3538/3801800434_8a0d0d43d8_s.jpg" alt="IMG_6684" width="75" height="75" /></a></span></div>
<div id="setThumbs-indv3801802086_div"><span id="photo_thumb3801802086"><a title="IMG_6686" href="http://www.flickr.com/photos/mmmmmrob/3801802086/in/set-72157621856959921/"><img src="http://farm3.static.flickr.com/2670/3801802086_3555043911_s.jpg" alt="IMG_6686" width="75" height="75" /></a></span></div>
<div id="setThumbs-indv3800986429_div"><span id="photo_thumb3800986429"><a title="IMG_6689" href="http://www.flickr.com/photos/mmmmmrob/3800986429/in/set-72157621856959921/"><img src="http://farm4.static.flickr.com/3443/3800986429_73e4828ff7_s.jpg" alt="IMG_6689" width="75" height="75" /></a></span></div>
<div id="setThumbs-indv3801809526_div"><span id="photo_thumb3801809526"><a title="IMG_6691" href="http://www.flickr.com/photos/mmmmmrob/3801809526/in/set-72157621856959921/"><img src="http://farm4.static.flickr.com/3441/3801809526_5dbc246230_s.jpg" alt="IMG_6691" width="75" height="75" /></a></span></div>
<div id="setThumbs-indv3801811184_div"><span id="photo_thumb3801811184"><a title="IMG_6692" href="http://www.flickr.com/photos/mmmmmrob/3801811184/in/set-72157621856959921/"><img src="http://farm3.static.flickr.com/2644/3801811184_375cb43a1c_s.jpg" alt="IMG_6692" width="75" height="75" /></a></span></div>
<div id="setThumbs-indv3801814214_div"><span id="photo_thumb3801814214"><a title="IMG_6694" href="http://www.flickr.com/photos/mmmmmrob/3801814214/in/set-72157621856959921/"><img src="http://farm4.static.flickr.com/3453/3801814214_4c75c5736b_s.jpg" alt="IMG_6694" width="75" height="75" /></a></span></div>
</div>
</div>
</div>
This is a really simple tasty soup that will serve 4 as a starter, or 2 as a good lunch with a solid bread. The curry powder brings out the flavour of the squash and adds a depth and warmth to it.

You'll need:

<!-- excerpt -->

1 tbsp Olive Oil
1x Onion
1 tbsp Mild Curry Powder
1x Butternut Squash
1 pint Vegetable Stock
Salt and Black Pepper

To do:

Slice the onion and pop in a heavy-based saucepan over a medium heat, stirring every few minutes.

While the onions are softening, peel and dice the squash. Take the seeds out, of course.

Once the onions are soft and have some colour, add the curry powder and fry gently for a minute.

Add the diced squash and cover with the pint of stock, top up with water to cover if necessary.

Simmer gently for 10 minutes, a bit longer if the squash isn't nice and soft.

Leave to cool enough to put in your blender, blend until smooth. Season with salt and black pepper to taste.

Serve with a swirl of cream and a little fresh coriander.

Enjoy
