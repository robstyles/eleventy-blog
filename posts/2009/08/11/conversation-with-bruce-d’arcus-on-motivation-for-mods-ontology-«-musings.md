---
layout: post.pug 
title: "Conversation with Bruce D’Arcus on Motivation for MODS Ontology « Musings"
date: 2009-08-11
tags: ["post","Library Tech","Semantic Web","Ontologies"]
legacy_wordpress_id: 547
---

> The problem from my standpoint is that MODS has some really odd, library-specific, design choices that I don’t think map very well to the wider world. A central concept like mods:name, with mods:role as a child of that, really makes no sense, and conflicts with more common modeling you see in DC, FRBR ,etc.
> 
> It’s semantics are also really loose.
> 
> So you have to ask yourself, just how linked could a MODS view in RDF really be?
from [Conversation with Bruce D’Arcus on Motivation for MODS Ontology](http://www.chrisfrymann.com/2009/08/05/conversation-with-bruce-darcus-on-motivation-for-mods-ontology/).

<!-- excerpt -->
