---
layout: post.pug 
title: "Carsonified » Top 10 UX Myths"
date: 2009-08-27
tags: ["post","Interaction Design"]
legacy_wordpress_id: 558
---

> There was an early time on the web when everything important needed to be ‘above the fold’; the area seen in a typical browser before any scrolling took place. This is now much less relevant.
Read the Top 10 UX Design Myths over on [Carsonified » Top 10 UX Myths](http://carsonified.com/blog/design/top-10-ux-myths/).

<!-- excerpt -->
