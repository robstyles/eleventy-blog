---
layout: post.pug 
title: "Intensional and Extensional Sets"
date: 2009-08-19
tags: ["post","Semantic Web","Ontologies"]
legacy_wordpress_id: 556
---

> One of my collegaues called the other day and asked if we still relied on the distinction between intensional and extensional sets (really intensionally and extensionally defined sets).  Yes, even more so now.
from [Intensional and Extensional Sets](http://www.semanticuniverse.com/blogs-intensional-and-extensional-sets.html).

If you don't know the difference (I didn't) then it's worth reading.

<!-- excerpt -->
