---
layout: post.pug 
title: "How to Correctly Open a Banana"
date: 2009-08-14
tags: ["post","Other Technical","Internet Social Impact"]
legacy_wordpress_id: 551
---

> So, it turns out me and everyone I know have opened a Banana the wrong way our entire lives. I have always thought that banana’s could only be opened that one way and always been rejecting any other way of doing it. But the living learn and me included. When I watched this the first time I thought to myself that this guy has to be joking. What’s the joke in the whole clip…but how wrong I was. It actually turned out to be a much more efficient way of pealing the banana to get to the actual fruit.

<iframe width="560" height="315" src="https://www.youtube.com/embed/nBJV56WUDng" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<!-- excerpt -->

from [How to Correctly Open a Banana – Monkey… « Bit Rebels](http://www.bitrebels.com/geek/how-to-correctly-open-a-banana-monkey-style/).
