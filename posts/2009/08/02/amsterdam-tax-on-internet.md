---
layout: post.pug 
title: "Amsterdam Tax on Internet to Support Newspapers"
date: 2009-08-02
tags: ["post","Internet Social Impact"]
legacy_wordpress_id: 534
---

> <h2 class="summary"><span onmouseover="_tipon(this)" onmouseout="_tipoff()">AMSTERDAM - A levy imposed Internet, with the proceeds benefiting innovation of traditional media like newspapers.</span></h2>
> <img class="art_image" src="http://media.nu.nl/m/m1dzc58akt5t.jpg" alt="" width="120" height="120" />
> 
> <span onmouseover="_tipon(this)" onmouseout="_tipoff()"><span class="google-src-text" style="direction: ltr; text-align: left;">Dat is een van de aanbevelingen van de commissie-Brinkman, die zich in opdracht van minister Ronald Plasterk (Onderwijs, Cultuur en Wetenschappen) boog over de toekomst van de Nederlandse dagbladsector.</span> This is one of the recommendations of the committee Brinkman, who is in command of Minister Ronald Plasterk (Education, Culture and Science) arc about the future of the Dutch newspaper industry.</span>
> 
> <span onmouseover="_tipon(this)" onmouseout="_tipoff()"><span class="google-src-text" style="direction: ltr; text-align: left;">Volgens het rapport, dat dinsdag in Den Haag wordt gepubliceerd, moet worden gedacht aan een "opslag van enkele euro's per jaar op de internetaansluiting voor de Nederlandse huishoudens".</span> According to the report Tuesday in The Hague that is published should be given to a "storage of a few euros per year on the Internet for the Dutch households.</span> <span onmouseover="_tipon(this)" onmouseout="_tipoff()"><span class="google-src-text" style="direction: ltr; text-align: left;">De maatregel kan ongeveer 20 miljoen euro opleveren.</span> The measure could yield around 20 million euros.</span>
[Google Translation: Tax on Internet to support newspapers](http://translate.google.com/translate?prev=hp&amp;hl=nl&amp;js=n&amp;u=http%3A%2F%2Fwww.nu.nl%2Finternet%2F2027713%2Fheffing-op-internetaansluiting-om-kranten-te-steunen.html&amp;sl=auto&amp;tl=en&amp;history_state0=).

This is an interesting quandary. Should new technology be taxed to support business models that get disrupted to evolve?

<!-- excerpt -->

I'm not actually sure that's what they're suggesting. One of the problems that newspapers face is that there is no good way for them to charge. With the notable exception of the New York Times, newspapers have found they have to publish articles online and have no way to make up for the loss in revenue from their print business.

If this money props up failing newspapers then that would be bad, but if it is invested in developing a working, de-centralised micropayments approach that would be a good thing.
