---
layout: post.pug 
title: "New ID cards are supposed to be 'unforgeable'"
date: 2009-08-06
tags: ["post","Security And Privacy"]
legacy_wordpress_id: 538
---

> Within 12 minutes of laying his hands on [a uk government foreign nationals biometric ID card], Laurie had made a clone.
from [New ID cards are supposed to be 'unforgeable' - but it took our expert 12 minutes to clone one, and programme it with false data | Mail Online](http://www.dailymail.co.uk/news/article-1204641/New-ID-cards-supposed-unforgeable--took-expert-12-minutes-clone-programme-false-data.html).

<!-- excerpt -->
