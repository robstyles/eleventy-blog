---
layout: post.pug 
title: "Disappeared..."
date: 2005-05-05
tags: ["post","Personal"]
legacy_wordpress_id: 81
---

Well, I disappeared for a while. Things are very busy and very interesting at my place of work currently. We're running two projects with tight deadlines, both of which are now going quite nicely and we're trying to adopt more agile approaches.

A few of us are blogging on our experiences introducing new agile practices - as we go. Learning such lessons as... don't introduce refactoring until after you've introduced developer testing. Andy (colleague) is attempting to procure some lava lamps for the LLPDCA (Lava-Lamp Powered Developer Conscience Alerting) aka CruiseControl.

<!-- excerpt -->
