---
layout: post.pug 
title: "Agile != Flexible"
date: 2005-05-05
tags: ["post","Resourcing"]
legacy_wordpress_id: 82
---

We had a retrospective today, just a small one post iteration. Sam Tunnicliffe (a very smart cookie, picking up .Net far faster than I did) suggested that this Agile approach, with its focus on communication, doing the next most important thing, relying on your team mates for pairing in order to write better code and checking in and our frequently doesn't gel with flexible working and home working.

I agree with him, but I find it interesting that the two companies I've worked for in an Agile way both have a strong focus on other "great place to work" concerns.

<!-- excerpt -->

We talked a bit about the opportunity to "spike" things on your own and bring back that learning, but that often doesn't fit as the highest priority piece of work, so is more "something to do".

I'm fairly biased - I prefer to be in the office, with two small children and another on the way I don't have the luxury of being able to sit down quietly; I get jumped on by about 3 stone of energetic child. Nor do I have a great answer to the problem. It may prove interesting to see who decides they want to work from home and who decides they must communicate.
