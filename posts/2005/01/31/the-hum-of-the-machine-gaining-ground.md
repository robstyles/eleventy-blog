---
layout: post.pug 
title: "The Hum of The Machine gaining ground"
date: 2005-01-31
tags: ["post","Enterprise Architecture"]
legacy_wordpress_id: 79
---

previously I wrote about [The Hum of The Machine](/2004/03/09/the-hum-of-the-machine), an idea I'd like to see^H^H^Hhear much more of.

Seems <a href="http://blogs.pragprog.com/cgi-bin/pragdave.cgi/Random/FanNoise.rdoc">PragDave has picked up on people doing it for real and even has it 'by default' on his Apple G5<a>.

<!-- excerpt -->
