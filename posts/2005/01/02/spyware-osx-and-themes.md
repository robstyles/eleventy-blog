---
layout: post.pug 
title: "Spyware, OSX and Themes"
date: 2005-01-02
tags: ["post","Random Thought"]
legacy_wordpress_id: 76
---

Apple's have been more secure than PCs for about as long as I can remember. It's generally acknowledged, though, that a main reason for that is the lack of value in attacking one.

Seriously, writing a virus, some spyware or other piece of trojan software for the Mac would be pretty pointless with the market penetration they currently have. That's not a dig - I'm a big Mac fan. I drool over the 17" Powerbook whenever I pass one and if virtual pc for the mac were just that bit faster then I would seriuously consider it. But if you want market penetration for a piece of malignant code it's not the platform to exploit.

<!-- excerpt -->

With corporate and even home machines getting slowly more secure, the use of social engineering attacks, such as the email phishing scams for bank details, become more and more prevalent. One such misrepresentation attack that's been around for ages, but appears to be on the increase is the use of flash, DHTML and other dynamic web content designed specifically to look like system dialogs. FUIs - Fake User Interface dialogs.

If you've spent any time at all on less reputable sites, for whatever reason, you'll have seen them. Big exclamation icons with phrases like "you computer is infected with spyware, click here to disinfect". Which should really read "this is an advert from a malicious spyware writer, click here to have your machine hijacked and/or infected". If you want to see what I mean, [look at examples of what was probably the first major campiagn of this type, by Bonzi](http://www.lukins.com/bonzi/index.php?pid=banners). Oh, and the [subsequent settlement to a class action lawsuit filed in Washington](http://www.lukins.com/bonzi/index.php?pid=settlement).

So, apart from the obvious benefit that a Mac isn't vulnerable to the same exploits as a PC (a benefit you can get most of by browsing with Firefox instead of IE) there is another benefit. Everything on the Mac </i>looks</i> different. The window frames, the maximize, minimize and close buttons, the grey bevel buttons all look very different to a PC. This make it obvious to anyone using a Mac that the little dialog is an imposter and not part of the system.

As a poster on [MetaFilter](http://www.metafilter.com/mefi/22065) says so eloquently:

> Of course, we Mac users are nothing but amused by those bogus "error" messages because, well... they don't look like error messages to _us_, they look like cheap attempts to trick bumbling PC users into clicking through someplace they wouldn't otherwise want to go...

You can achieve this effect on your PC, making it easier to recognise threats visually, by installing a skinning tool such as [WindowBlinds](http://www.stardock.com/products/windowblinds/). Making your windows look different to Windows could make the difference between clicking a dialog and not for many users.

[Other, more traditional, tips & tricks can be found on Bruce Schnier's blog](http://www.schneier.com/blog/archives/2004/12/safe_personal_c.html).
