---
layout: post.pug 
title: "Comment Spam"
date: 2005-01-10
tags: ["post","Blog on Blog"]
legacy_wordpress_id: 78
---

I've always kept comments switched off on my online ramblings due to the threat of abuse. Now, it appears, [Six Apart](http://www.sixapart.com/) (writers of [Movable Type](http://www.movabletype.org/) which powers my blog and [TypePad](http://www.typepad.com/), the hosted blog service) have introduced [TypeKey](https://www.typekey.com/). TypeKey is an online identity specifically designed for commenting on blogs and is integrated with Movable Type.

Great - now I just have to work out how to install it and how to edit all the comment templates to make them fruity.

<!-- excerpt -->
