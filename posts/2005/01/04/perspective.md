---
layout: post.pug 
title: "Perspective"
date: 2005-01-04
tags: ["post","Personal"]
legacy_wordpress_id: 77
---

<script type="text/javascript" src="http://www.makepovertyhistory.org/whiteband_small_right.js"></script>I'm gonna take some stick for this for being a heartless man. But it's not going to stop me saying it. But before I do let me just say that I am not heartless. Far from it. I have been deeply touched by the South East Asian earthquake. I watched the children swept away. I watched the mother run into the sea trying to save her children. On News 24, safely on my sofa - just like you.

It made me cry.

<!-- excerpt -->

Then I watched the Vicar of Dibley on New Year's Day. It made me cry. Richard Curtis is a writer of enormous talent.

For those who missed it, [a good description can be found on the BBC's site](http://www.bbc.co.uk/comedy/vicarofdibley/newyear.shtml).

Follow the link at the bottom to _Make Poverty History_ and read up:

> Over the next 12 months a series of landmark meetings are taking place where world leaders can finally stop 30,000 people dying needlessly every day, just because they?re poor.

__30,000 people dying needlessly every day__

If 150,000 people die as a result of the Tsunami, that's just 5 average days of death due to Poverty. Poverty that we can stop; that we can change. Death that is so routine, so mundane, that it fails to make the news any more. That's why we had </i>Break The Chains</i> back in 2000, _Band Aid_ too many years ago and _Band Aid 20_ this year.

This is mad and we must stop it.
