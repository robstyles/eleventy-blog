---
layout: post.pug 
title: "Dry Stone Wall"
date: 2005-07-10
tags: ["post","[grid::blogpaper]"]
legacy_wordpress_id: 88
---

So, each week I'm posting one of my own photos, sized for use on your desktop. Join in by bloggin your own photos and include the phrase [grid::blogpaper] in your post - then everyone can google for them.

This week's BlogPaper is called Dry Stone Wall:

<!-- excerpt -->

<img src="/assets/wallpaper/DryStoneWall_160x120.jpg" />

Download one here: [1600x1200](/assets/wallpaper/DryStoneWall_1600x1200.jpg), [1280x1024](/assets/wallpaper/DryStoneWall_1280x1024.jpg), [1024x768](/assets/wallpaper/DryStoneWall_1024x768.jpg), [800x600](/assets/wallpaper/DryStoneWall_800x600.jpg)
