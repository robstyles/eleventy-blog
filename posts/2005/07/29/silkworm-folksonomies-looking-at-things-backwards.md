---
layout: post.pug 
title: "Silkworm, Folksonomies, looking at things backwards"
date: 2005-07-29
tags: ["post","Internet Social Impact"]
legacy_wordpress_id: 92
---

A colleague of mine has been looking at Folksonomies recently and it's likely to feautre more and more in our work, but his post on [Reversed folksonomy](http://wonderfulworldofmrc.blogspot.com/2005/07/reversed-folksonomy.html) is really interesting. This is very much the culture of [Silkworm](http://silkworm.talis.com), a networking project I'm involved in.

Something interesting always happens when you look at things backwards. The stuff above is a great example; a number of years ago I was penetration testing (legitimately) and developed the reverse brute force attack for gaining network access - passwords are secret, usernames are known. A brute force attack tries every password against a known name. I gained access by trying a few likely (i.e. probably known) passwords against every possible username. It would be interesting to know just how many online banks would be vulnerable to that?

<!-- excerpt -->

You see, look at things backwards and something interesting falls out.

update: typo corrected; "A bruce force attack tries every password against a known name." This is not to be confused with a Bruce Forsyth attack which [David Leigh-Fellows](http://www.twelve71.org/blogs/dave/) has just pointed out to me involves attempting access using jokes about your mother-in-law.
