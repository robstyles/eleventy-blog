---
layout: post.pug 
title: "Can't we all just get on?"
date: 2005-07-25
tags: ["post","Resourcing"]
legacy_wordpress_id: 90
---

:: The names have been changed to protect the innocent

I blogged a while ago on [professionalism](/2004/08/10/what-is-professionalism-really-about) and received some interesting comments, some encouraging, some less so.

The the other day I was having a deep conversation with a colleague who was upset at having being told he was rather too intense. This got me back to thinking about the best teams I've worked in and what the relationships were like there.

<!-- excerpt -->

As some of you know, I spent quite a few years at Egg and in the early days there we had some fairly intense times. One of the reasons I like XP is that it's the closest thing I've come across to a formalisation of "great people do great things" which was Egg's methodology in those post-launch days.

The team I joined at Egg back in 1998 was great fun, but we had arguments. Some scorchers. I remember a colleague telling one of the exec of Egg to leave the room - in just two words - and never being sanctioned for it. And if I had a quid for every time someone used the f-word I'd be worth more than Egg is now.

But Egg wasn't the only place. At a much smaller finance house, as part of a team punching much more than its weight, I saw arguments between the IT Director and a developer that would make your hair curl. That team - a team that argued about everything - built systems that got mentions in the New York Times, Cosmopolitan, The Times and many other high profile publications.

When I think back to many of my bext experiences, the times I learnt most, was most productive and the team I was part of achieved the most impressive things the people I'm still in touch with and the people I would hire like a shot today are the ones who would tell the CEO he was wrong, and not necessarily politely.

So, as we set off on the journey of trying to build a great team of great people to do great things, I'm wondering if we should see arguments (real ones where we say "fuck" a lot) as a good sign, a bad sign or a necessary evil; the inevitable side-effect of passion, our number one recruitment criteria.

I hope a good sign as I saw a message to our CEO today that started "I really don't agree"...
