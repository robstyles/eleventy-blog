---
layout: post.pug 
title: "Striped Leaves"
date: 2005-07-25
tags: ["post","[grid::blogpaper]"]
legacy_wordpress_id: 91
---

This week's BlogPaper is the only one, so far, that I've altered digitally other than to crop and resize. This was taken in low light with a fast film speed so ended up grainy. I did a simple motion blur ,totally vertical, to blur the grain into vertical stripes.

Anyway, Striped Leaves is below.

<!-- excerpt -->

<img src="/assets/wallpaper/StripedLeaves_160x120.jpg" />

Download one here: [1600x1200](/assets/wallpaper/StripedLeaves_1600x1200.jpg), [1280x1024](/assets/wallpaper/StripedLeaves_1280x1024.jpg), [1024x768](/assets/wallpaper/StripedLeaves_1024x768.jpg), [800x600](/assets/wallpaper/StripedLeaves_800x600.jpg)
