---
layout: post.pug 
title: "Paper your screen, Blue Timber"
date: 2005-07-03
tags: ["post","[grid::blogpaper]"]
legacy_wordpress_id: 86
---

I've been taking photos since I was little. I started with my Dad's camera and my first photo was a great holiday snap in which our family dog, Honey, takes centre-stage and my dad appears alongside her in full 70s kodacolor... less his head.

My technique has improved little since then, but with advent of cameras smarter than I am and, more recently, affordable digital SLRs my luck has changed a little. My camera allows me to take several hundred photos on a card, virtually cost free, So I'm now able to apply the monkeys & typewriters approach.

<!-- excerpt -->

Anyway, a few of my shots have come out quite nicely so I'm doing two things. Firstly I'm going to post one of my shots, sized to common desktop resolutions, to my blog every so often as I get a moment. Secondly, I'm going to invite everyone else to do the same and to add the phrase [grid::blogpaper] so we can all google for the grid.

This week's is called Blue Timber:

<img src="/assets/wallpaper/BlueTimber_160x120.jpg" />

Download one here: [1600x1200](/assets/wallpaper/BlueTimber_1600x1200.jpg), [1280x1024](/assets/wallpaper/BlueTimber_1280x1024.jpg), [1024x768](/assets/wallpaper/BlueTimber_1024x768.jpg), [800x600](/assets/wallpaper/BlueTimber_800x600.jpg)
