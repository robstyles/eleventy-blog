---
layout: post.pug 
title: "Red Arrows Corkscrew"
date: 2005-07-18
tags: ["post","[grid::blogpaper]"]
legacy_wordpress_id: 89
---

So, each week I'm posting one of my own photos, sized for use on your desktop. Join in by blogging your own photos and include the phrase [grid::blogpaper] in your post - then everyone can google for them.

A few weeks ago I took my boy and a friend, Jake, to Cosford airshow. The cameras went with us and this photo of The Red Arrows Corkscrew was taken by Jake.

<!-- excerpt -->

<img src="/assets/wallpaper/RedArrowsCorkscrew_160x120.jpg" />

Download one here: [1600x1200](/assets/wallpaper/RedArrowsCorkscrew_1600x1200.jpg), [1280x1024](/assets/wallpaper/RedArrowsCorkscrew_1280x1024.jpg), [1024x768](/assets/wallpaper/RedArrowsCorkscrew_1024x768.jpg), [800x600](/assets/wallpaper/RedArrowsCorkscrew_800x600.jpg)
