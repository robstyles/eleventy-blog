---
layout: post.pug 
title: "Baptised"
date: 2005-06-05
tags: ["post","Personal"]
legacy_wordpress_id: 83
---

I've been a Christian for 4 and-a-half years now; although my involvement with Christian friends and my interest in knowing more about God has been much longer. Well, today I became baptised; a very interesting and beneficial experience...

<!-- excerpt -->

Firstly, let me set the scene a little. Baptism has been around for a very long time, well before Christ it was used as part of some Jewish traditions of washing. John the Baptist then started preaching a Baptism of repentance; for the cleasning of your spiritual self and as a way of stepping forwards in your life with a clean start. Today, we use it in the same way. As a symbolic act of cleansing and re-birth. What's different, at least amongst my Christian friends, is that many are active Christians often involved in teaching and acts of mission of some kind long before they become baptised. That's true for all four of us who were baptised today.

So, the scene: a baptist church in a city centre near my home had kindly allowed us (a house church) to use their facilities; a 1920s building with custom build baptistry. The baptistry is like a victorian swimming pool, but shrunk to lilliputian proportions; just long enough for a six footer to be fully immersed without cracking a head on the edge.

My family were there, they're not Christian. A select handful of friends were there, also not all Christian. So the baptism was my way of publicly declaring an intention to follow God that I've been trying to live up to for years already.
