---
layout: post.pug 
title: "On a lighter note..."
date: 2005-06-05
tags: ["post","Music"]
legacy_wordpress_id: 84
---

I had the great fortune of being reminded of [Nizlopi](http://www.nizlopi.com/) today. I've got the album already, I've had a while and it's brilliant.

They're just releasing their first single and you _must_ buy it! Why? because it's brilliant. A wonderful blend of sounds and memories; that's why the album's called "Half these songs are about you..."

<!-- excerpt -->

Anyway, the single's called JCB and can be found, along with the amazing video for it, at [http://www.jcbsong.co.uk/](http://www.jcbsong.co.uk/).

Buy It ! Now !
