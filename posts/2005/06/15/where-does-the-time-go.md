---
layout: post.pug 
title: "Where does the time go..."
date: 2005-06-15
tags: ["post","Enterprise Architecture"]
legacy_wordpress_id: 85
---

Busy times, child number three on the way, due at the end of next month; along with trying to buy and sell houses and work has been great fun. I want to blog about some of the technicalities of what we've been building, and will try to get around to that, but in the meantime we're talking about some of it at:

[http://silkworm.talis.com](http://silkworm.talis.com)

<!-- excerpt -->

Essentially, we're working on services out in the cloud that provide content discovery, interoperability and access services for content providers. I can highly recommend the white paper by our CTO Justin, available for download from above.

more coming soon.
