---
layout: post.pug 
title: "Standing on the Shoulders of Giants."
date: 2005-02-08
tags: ["post","Software Engineering"]
legacy_wordpress_id: 80
---

Building on the successes of larger complimentary and competitive organisations has to be a sound strategy. Leveraging other people?s investment to make money yourself has to be a good move. This is so often referred to as Standing on the Shoulders of Giants.

But what?s the best way to do that with software? The mental image of a normal person like you or I standing, one foot on each shoulder of some gargantuan Neanderthal, is evocative and illustrative, but can be interpreted too literally.

<!-- excerpt -->

__One giant good. Two giants better.__

So, if standing on one giant is a good idea, surely standing on two is better. Yes and no. If the two giants choose different directions there will be very precarious moment as you teeter back and forth trying to work out which giant to stand on and which to step off. Sometimes, if you fail to move quickly enough, this can result in a long fall to the ground in the gap left between the giants.

You may also find that your initial position of only one foot on this giant still leaves you with a difficult move to reposition with one foot on each shoulder. And, of course, you may find that the other shoulder isn?t free, instead occupied by a competitor, standing one foot on your giant and one foot on another.

Standing on the shoulders of giants also limits you to the number of giants you can stand on ? for most of us the practical limit is just two.

__Giant Headwear__

If I were a giant, standing in the commercial world, I?d probably encourage smaller players to stand on me. That way if another giant comes along with a big club and clobbers me I?ll be protected. In fact, several smaller players perched upon my sturdy shoulders would make a very effective helmet. And, if one of them suffers I?ll be able to protect myself as long as I can keep getting new club-fodder to climb on up there.

__Standing alongside giants.__

Perhaps a better approach would be to stand behind Giants. Or alongside them in a position that leaves you free to dive for cover behind those mighty legs. Thinking about it, I can stand behind many giants. I can run between them, standing alongside each giant as and when necessary. There?s a risk, of course, that I might get trampled. I?ll have to keep my eyes open for giant feet, but with any luck they?ll be a giant to stand alongside whenever I feel vulnerable.

__What?__

So, what am I getting at? Well, standing on the shoulders of giants is a sane and worthy vision. But it is all too easy to translate it directly into software architecture. Take database integration as an example; tying product to a single database limits your customer base to those who find that choice acceptable. Working with many protects you from that concern. Of course, if you?re a giant you don?t have to worry too much.

The equivalent of standing alongside giants is to build platform independent systems, running on Windows and Unix. Write BizTalk or Sharepoint components to allow customers of those products to work seamlessly with yours ? but build the same things for BEA Process Portal and IBM?s WebSphere Business Integration Server too.

If you can only afford to stand next to one giant that?s fine, but don?t stand on his shoulders.
