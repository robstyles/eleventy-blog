---
layout: post.pug 
title: "The Bartimaeus Trilogy"
date: 2005-12-29
tags: ["post","Fiction Book Review"]
legacy_wordpress_id: 105
---

This trilogy by Jonathan Stroud consists of _The Amulet of Samarkand_, _The Golem's Eye_ and _Ptolemy's Gate_. They chronicle the adventures of a talented young magician called Nathaniel and the witty Djinni Bartimaeus whom Natahaniel has, just, managed to control. The books are set in a magical version of London run by magicians and travels a little to other countries. The other main character who features is Kitty, part of a resistance movement working against the rule of the magicians. Along with the human inhabitants are a suitable number and variety of creatures from the tiny and snivelling ??? to the living clay monster Golems.

I found the first of these book, The Amulet of Samarkand, while looking for something less vacuous than Harry Potter. This trilogy falls into the category of children's fiction also, which sits nicely with my attention span. I waited anxiously for the second and found it well worth the wait - not a copy of the first with different monsters as the Harry Potter series has been so far.

<!-- excerpt -->

The third part of the trilogy is sat next to my bed waiting for me to finish some non-fiction books before I really get into it as I know it will take over my reading time as soon as I open it.

Would I recommend these? Definitiely.

["The Amulet of Samarkand" by Jonathan Stroud, Doubleday Oct 2003, ISBN: 0385605994](http://www.bartimaeustrilogy.com/)
["The Golem's Eye" by Jonathan Stroud, Doubleday Oct 2004, ISBN: 038560615X](http://www.bartimaeustrilogy.com/)
["Ptolemy's Gate" by Jonathan Stroud, Doubleday Sept 2005, ISBN: 0385606168](http://www.bartimaeustrilogy.com/)
