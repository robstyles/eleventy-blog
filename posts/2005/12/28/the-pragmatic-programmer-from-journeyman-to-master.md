---
layout: post.pug 
title: "The Pragmatic Programmer: From Journeyman to Master"
date: 2005-12-28
tags: ["post","Non-Fiction Book Review"]
legacy_wordpress_id: 104
---

I was introduced to this book several years ago by some [Thoughtworkers](http://www.thoughtworks.com) I had the luck to be working with. After a bit of a dip in my passion for writing software this book really brought back my interest in being professional about writing great stuff.

I was chuffed when the Geek Book Club I started a few months ago decided on it as the first book we should read as a group. It was a second reading for me and I can say that it's just as good second time around. The book is full of sensible explanations of practical tips. Many of them are things that we all _know_ we should do, but somehow don't like "The Cat Ate My Source Code". But other aspects challenge our thinking about some stuff more deeply; like the discussion that ensued around Domain Specific Languages and their good (and bad) uses that our group had while reading Chapter 2.

<!-- excerpt -->

So much of this book is the foundation of _individual_ agile practices (if there are such things) that it makes sense for everyone to read it. Interestingly it's also on [Joel Spolsky's Software Management Reading List](http://www.joelonsoftware.com/items/2005/11/22.html) which is aimed at those wishing to manage software development rather than  those wishing to do software development.

["The Pragmatic Programmer: From Journeyman to Master" by Andrew Hunt and David Thomas, Addison-Wesley Oct 1999, ISBN: 020161622X](http://www.pragmaticprogrammer.com/ppbook/index.shtml)
