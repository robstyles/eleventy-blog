---
layout: post.pug 
title: "Well who then?"
date: 2005-12-12
tags: ["post","Software Engineering"]
legacy_wordpress_id: 103
---

[Ok/Cancel has a great comic this week](http://www.ok-cancel.com/comic/117.html) that I feel points at something more fundamental.

Who "owns" the design. The design, in this posting, being what the application must do and how it should do it. Now, many would say that requirements are written by Analysts, and I would agree, but requirements aren't a design.

<!-- excerpt -->

Often the Technical Lead feels she owns it, but surely that's the technical design? Maybe the Interaction Designer? Well, I'm sure Tom Chi would be the first to say that they can only design what's been decided on.

At Microsoft it falls to the Program Manager (note the spelling, Program, not Programme) and that makes sense in many ways but most organisations don't have anyone with that title.

At Apple it's simple; it's Steve Jobs.

So where does/should it sit? Being able to boil down the essence of a problem or opportunity that many people have and present a solution that meets that need is an unusual gift.
