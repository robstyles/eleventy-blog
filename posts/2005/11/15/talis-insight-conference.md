---
layout: post.pug 
title: "Talis Insight Conference"
date: 2005-11-15
tags: ["post","Working at Talis"]
legacy_wordpress_id: 102
---

I'm at [Talis' Insight Conference](http://www.talis.com/insight2005/index.shtml) (I work for Talis) today and tomorrow and so far I'm having a great time.

Our CEO [Dave Errington](http://daviderrington.typepad.com/serendipity/) put on a fantastic keynote...

<!-- excerpt -->

He demo'd several of our new product developments and talking about openness, particpation, integration, co-operation and so on.

We're doing a lot right now around how Libraries fit with Web 2.0 and that's a topic I've just co-presented on with my colleague [Ian Davis](http://iandavis.com/).

It's great to focus on what you've achieved in a year and conferences help us do that internally as well as externally.

This year we've built a new bibliographic data service, converted millions of bibliographic records from one format to another and put ourselves in much better shape technically. Then, of course, we've hired the very great Jingye Luo who's been incredibly productive building our new Syndication Platform.

One of my long-time friends recently asked me why I was working for Talis and the answer, for me, was easy. We're doing some of the biggest, coolest web stuff that I know of. We're not just working with Web Services in an enterprise context, we're putting some big, useful services out on the open web. There are few places doing that at the scale we are worldwide, let alone in the UK. On top of that, because of that, and driving that I have peers I can really learn from. Where else would I work?

Today is a good day :-)
