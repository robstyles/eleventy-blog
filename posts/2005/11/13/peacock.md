---
layout: post.pug 
title: "Peacock"
date: 2005-11-13
tags: ["post","[grid::blogpaper]"]
legacy_wordpress_id: 101
---

So, <strike>each week</strike> sometimes I'm posting one of my own photos, sized for use on your desktop. Join in by blogging your own photos and include the phrase [grid::blogpaper] in your post - then everyone can google for them.

This week's BlogPaper is called Peacock and was taken at Warwick Castle:

<!-- excerpt -->

<img src="/assets/wallpaper/Peacock_160x120.jpg" />

Download one here: [1600x1200](/assets/wallpaper/Peacock_1600x1200.jpg), [1280x1024](/assets/wallpaper/Peacock_1280x1024.jpg), [1024x768](/assets/wallpaper/Peacock_1024x768.jpg), [800x600](/assets/wallpaper/Peacock_800x600.jpg)
