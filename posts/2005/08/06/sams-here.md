---
layout: post.pug 
title: "Sam's Here"
date: 2005-08-06
tags: ["post","Personal"]
legacy_wordpress_id: 94
---

Friday morning at 6.33am my third child arrived. Sam's a big boy, just half an ounce short of ten pounds. Despite this my wife delivered on just gas & air in a little over three and a half hours. Bloody hell she's amazing. She was totally in control the whole time, following the midwife's instructions to the point of managing to not push when told not to...

<!-- excerpt -->

Sam's arrival was something I was really looking forward to, but was also ver anxious about, as it had so much in common with the arrival of my first son, Joe. Sam's due date was Joe's birthday; sam arrived twelve days late though, pushing him into August. He's a boy, they have that in common of course, and Carrie felt the same in many ways through her pregnancy.

When Joe arrived he had some difficulties and had to be whisked away to the neo-natal unit. While I went with him, I wasn't able to really get close to him as he needed nursing help and I didn't get much time holding him until later the next day. Carrie got hardly any time with him until several days later.

Sam's arrival was so different. He arrived very healthy and alert; eyes open very soon after landing on Carrie's chest and I had him skin to skin on me for about an hour while Carrie was sorted out. That's the greatest thing, close contact with your new baby and I found myself in tears both because of how wonderful it was to hold Sam, but also a feeling of sadness at not having been able to hold Joe in the same way.

So, Sam's four days old now and we're loving having him. Holding him is so great; he just spends his time looking at everything going on around him. And, of course, we've had more [Poo](/2004/12/17/poo) to deal with.
