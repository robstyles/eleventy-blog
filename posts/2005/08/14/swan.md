---
layout: post.pug 
title: "Swan"
date: 2005-08-14
tags: ["post","[grid::blogpaper]"]
legacy_wordpress_id: 95
---

So, each week I'm posting one of my own photos, sized for use on your desktop. Join in by blogging your own photos and include the phrase [grid::blogpaper] in your post - then everyone can google for them.

This week's BlogPaper is called Swan:

<!-- excerpt -->

<img src="/assets/wallpaper/Swan_160x120.jpg" />

Download one here: [1600x1200](/assets/wallpaper/Swan_1600x1200.jpg), [1280x1024](/assets/wallpaper/Swan_1280x1024.jpg), [1024x768](/assets/wallpaper/Swan_1024x768.jpg), [800x600](/assets/wallpaper/Swan_800x600.jpg)
