---
layout: post.pug 
title: "Aardvark, BDUF and Getting your story straight"
date: 2005-08-18
tags: ["post","Software Engineering"]
legacy_wordpress_id: 96
---

I've got a huge amount of time for [Joel](http://www.joelonsoftware.com/). His attitude to businesss, developers, workspace and more are great, but sometimes he writes something that makes me cringe.

Yesterday, he published the spec for Copilot, codenamed project Aardvark, for the world to see. For that I'm truly greatful - it's always good to see what others are doing.

<!-- excerpt -->

But in the surrounding blurb, he says:

> As I worked through the screens that would be needed to allow either party to initiate the process, I realized that Aardvark would be just as useful, and radically simpler, if the helper was required to start the whole process. Making this change in the spec took an hour or two. If we had made this change in code, it would have added weeks to the schedule. I can’t tell you how strongly I believe in Big Design Up Front, which the proponents of Extreme Programming consider anathema. I have consistently saved time and made better products by using BDUF and I’m proud to use it, no matter what the XP fanatics claim. They’re just wrong on this point and I can’t be any clearer than that.

What XP "fanatics" say is that you don't need to do BDUF on how you build the thing. What Joel's talking about is BDUF on _what_ you build. Many XPers call this "getting your story straight".

[Rachel Davies](http://www.agilexp.com/) has [spoken about this](http://www.twelve71.org/blogs/rachel/archives/000014.html) with respect to stories having a cost element. [Fuller document](http://www.bcs-oops.org.uk/twiki/pub/SPA/OT2004/20040330_GetStoryStraight.doc) is on BCS' wiki.

[Olivier Lafontan](http://maclaf.blogspot.com/) talks about this in his excellent post where he discusses eXtreme Analysis and [the NoVNoS Framework](http://maclaf.blogspot.com/2004/11/release-2-novnos-frameworktoo-radical.html)

And back at XPDay3, David Leigh Fellows and Richard Watt [presented on Acceptance Test Driven Development](http://xpday3.xpday.org/slides/ATDD.ppt), giving a clear view on how "getting your story straight" preceedes estimates and planning.

Anyway, here's [Joel's useful post with the spec in](http://www.joelonsoftware.com/articles/AardvarkSpec.html)...

Updated: [Anthony Williams has picked up on the same point](http://www.livejournal.com/users/anthony_w/6916.html). Anthony sites "design at the last responsible moment" as his headline and that's a great mantra also. The term missing from Anthony's post, though, is Interaction Design.

This, in my mind, is the biggest hurdle. Joel is talking about refining the Interaction Design of his application. I have long said that this is the major difference between a traditional requirement and a story. A story is a fragment of interaction design; the XP process is predicated on the story being written by someone who knows what that interaction should be, but alos on allowing them to change their mind.
