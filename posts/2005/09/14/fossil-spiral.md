---
layout: post.pug 
title: "Fossil Spiral"
date: 2005-09-14
tags: ["post","[grid::blogpaper]"]
legacy_wordpress_id: 99
---

So, <strike>each week</strike> sometimes I'm posting one of my own photos, sized for use on your desktop. Join in by blogging your own photos and include the phrase [grid::blogpaper] in your post - then everyone can google for them.

I took this one, along with a good handful more, on a private beach near Lyme Regis. The fossils down there are amazing, but gettig harder and harder to find as they get hunted down and taken home by holiday makers :-(

<!-- excerpt -->

This week's BlogPaper is called FossilSpiral:

<img src="/assets/wallpaper/FossilSpiral_160x120.jpg" />

Download one here: [1600x1200](/assets/wallpaper/FossilSpiral_1600x1200.jpg), [1280x1024](/assets/wallpaper/FossilSpiral_1280x1024.jpg), [1024x768](/assets/wallpaper/FossilSpiral_1024x768.jpg), [800x600](/assets/wallpaper/FossilSpiral_800x600.jpg)
