---
layout: post.pug 
title: "Domain Driven Design"
date: 2003-06-21
tags: ["post","Software Engineering"]
legacy_wordpress_id: 19
---

I've been talking to a few friends for a while about adding object modelling into XP stories and having an overall view of the project held as an interaction diagram (often called a storyboard) and backed up by an object model that describes the problem domain.

Anyway, when I mentioned this to Richard Watt and Martin Fowler of Thoughtworks they suggested that I take a look at a book due out soon called _Domain Driven Design, Tackling Complexity in The Heart of Software by Eric Evans_ which talks about a very similar concept.

<!-- excerpt -->

Evans talks about the use of a single _Ubiquitous Language_ that is used by the whole team - customer and all. That is, you educate your customer in Object Modelling, to some extent.

[http://domaindrivendesign.org/book/](http://domaindrivendesign.org/book/)

Well worth a read.
