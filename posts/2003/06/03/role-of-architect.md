---
layout: post.pug 
title: "Role of Architect"
date: 2003-06-03
tags: ["post","Enterprise Architecture"]
legacy_wordpress_id: 18
---

What a wonderfully eclectic group we are! It is clear that we all bring many different levels of ability and many different experiences to the mix here. While this is the lifeblood of any organisation it can have a damaging side-effect. If people are not self-aware and self-evaluating there is a real risk that the same things that have been done before keep getting done again and again, regardless of their suitability.

<!-- excerpt -->

The common suggestion that if you ask three architects for a solution you'll get three different solutions holds very true, but it doesn't have to. Many people take the approach of "I must have seen this before and all I need to do is apply the pattern I used last time" and this leads to solutions copied from experience rather than designed in the light of experience. Unfortunately, as time progresses this approach becomes self-enforcing and practitioners become more and more adept at applying fewer and fewer approaches. In short people stagnate and ultimately become a one-trick pony. This can be true for organisations as well as people, of course.

It is this "toolkit" approach that leads to the three architects giving three different answers. Designing in the light of experience, with all of the relevant information to hand, leads people to arrive at the same conclusions. The problem is that it is a lot more difficult. You actually have to understand the problem you're designing a solution for and the technology you're implementing with - properly - rather than giving it just a cursory glance.

You can see this problem manifest itself whenever you see C# that's written in a very Java style or vice versa. You can see it in the way Oracle is the answer to any problem; if you're an Oracle consultant. You can see it where many different fragments of technologies do different parts of a task. This often comes from the "I know how to do that bit in VB, but not that bit, so we'll use .Net for that bit" approach to design.

But the problem doesn't end there. Assuming that several architects can agree on an approach this then has to be communicated. Many organisations use UML for this. I use UML a lot. But UML isn't good for designing solutions in environments where the development community doesn't understand UML and isn't practiced at Object-Oriented design. The reason it's not good in this environment is because the process should be a chain - Understand Requirements -> Design Solution -> Implement Design. But if the design is communicated in language that the implementors don't understand then the obvious thing for them to do is to go back up the chain to the requirements and implement anything they can that seems to meet those needs.

A solution design, if it requires sign-off from a customer, needs to do two things. Firstly it needs to inform the customer of what the solution is and how it meets their need, in language they can understand. Second it needs to tell the implementors, clearly, what they have to build. UML doesn't have any tools in it to describe the structure of user interactions or user interface, so can't define what the user will get in terms they care about or understand. Models designed out-of-context of code are also, always, rife with errors, ommissions and glitches which means they don't specify the implementation, they only give a flavour. This reduces the value of a design very significantly and can be dangerous if the design is placed under change control processes. If a customer has signed off on an incorrect design then that gives excellent grounds for rebuking an implementation, or implementor, for not following it.

By going back to the requirements, the solution certainly won't implement the design, making the design work a waste of effort, increasing project risk and demotivating those involved.

The communication barrier that can exist when designers and implementors are perceived as seperate groups manifests itself in other ways too. The perception that once the design is complete the designers job is over is a common one. When the inevitable mistakes, conflicts, inconsistencies and misinterpretations are spotted the person most able to make changes and tradeoffs to the design is no longer around, leaving implementors who didn't understood the original design and are not best equipped to make design decisions, to make changes as they see fit, usually with visibility of only a small part of the overall solution.

Communication barriers exist in other directions too. As UML design artefacts don't tell the customer what they want to know, the customer and the business analysis communities are often left wondering what the design really means. The link between requirements and design is opaque, a black art open only to those in the UML elite. In many organisations this leads to architects being elevated to a position of worship, but in many more it leads to architects being seen as an irrelevance. In one organisation I've worked with they are even referred to as "gobs on sticks". This latter case is, of course, even more likely when the implementors use the requirements to produce the solution rather than the design.

This seperation of designer and implementor causes compromise by the implemetor for other good reasons, after all the designer isn't the one with skin in the game, he doesn't get called out at 3am when systems go down, that glory is saved for the coders. This leads to the support and deployment functions having no respect for architects as clearly they know nothing about how the systems actually work.

Over time the seperation exacerbates further. As the designers know less and less about the implementation their designs become less and less relevant and they become less and less trusted or involved.

Solving these problems requires the breaking down of those communication barriers. Implementors must be shown the design in language they understand, they live in code and that is where you have to meet them in order to be most effective. Respect cannot be commanded in the code world, it must be earned by showing prowess, by writing better code and by helping others write better code. Customers and analysts only care about how the software looks and feels. They care about performance because slow systems are frustrating or because they cost more in hardware, they don't care about classes, interfaces or queues. They live in the interface of the software and that's where we must meet them, with mockups, screenshots and prototypes.

In short, an Architect is not a distinct role. In order to be successful an architect must be business analyst, interaction designer, implementor, customer advocate, coach and mentor. In order to be successful an architect has to be both transparent and worthy of worship.

Update:

[Martin Fowler, Who Needs An Architect?, IEEE, July/August 2003](http://www.martinfowler.com/ieeeSoftware/whoNeedsArchitect.pdf) is a great article.

[Architects Don't Code anti-pattern on C2.com](http://c2.com/cgi/wiki?ArchitectsDontCode)
