---
layout: post.pug 
title: "XP, Customer as Interaction Designer"
date: 2003-03-12
tags: ["post","Software Engineering"]
legacy_wordpress_id: 16
---

I've been a fan of Alan Cooper for some time, having picked up his book _About Face_ in 1995 and read it cover to cover in just a few days.

But having been working with eXtreme Programming, and having the customer as part of the team, it seems to me XP may have forgotten some of the lessons Cooper espouses.

<!-- excerpt -->

Alan Cooper essentially holds the Interaction Designer up, in both _The Inmates Are Running The Asylum_ and _About Face 2.0_ as the absolute arbiter of system design. The one person who has the skills to understand what the customer needs, to design it and to ensure that what gets built matches the design. Well, he would say that, he's an Interaction Design guru.

I'm not sure I'd go that, but I would dispute the XP mantra that the customer knows what they need. Some may know what they want and some may even have the skills to design basic solutions, but that isn't enough to build great solutions.

My thoughts on this were greatly clarified a few days ago when we started a new project, with a new customer. The real customer doesn't want to get her hands dirty by sitting with developers and "certainly hasn't the time to be there every day". So, she's given us a proxy customer. The proxy happens to have a background as analyst and interaction designer. A god send.

The role of customer has become her full-time focus and she has the time and energy to run round brand, security, and business teams gathering and resolving requirements in order to present the development team with just-in-time decisions. And crucially she acts as arbiter and talks to the development team with a single, clear voice.

So why, back in January 2002, did [Kent Beck and Alan Cooper have such a fierce debate](http://www.fawcette.com/interviews/beck_cooper/default.asp)?

I have no idea. But the more I consider combining the ideas of Goal-Directed Design (Alan Cooper's methodology) with Agile development methods the more they seem to gel.

The use of Personas allows the team to prioritise and focus on the most important stories for the primary persona. The Interaction Designer as Customer gives the development team the single voice they need. And the skills of the Interaction Designer lead to more complete, consistent and coherent stories due to their solution design ability.

Of course, Interaction Design also benefits from XP. For example, Interaction Design is usually done, to completion, before any development starts. This is based on Alan Cooper's assumption that writing code is like pouring concrete, but XP says code can change. This means that, through short iterations and refactoring the Interaction Designer can get immediate feedback on their ideas, ensuring that they actually meet the customer's needs.

The other issue that combining these disciplines can help address is the adoption of XP in software houses producing shrinkwrap where access to real customers is usually difficult and the customers you have are certainly not the people who should be designing the product.

Sounds like a win-win to me.
