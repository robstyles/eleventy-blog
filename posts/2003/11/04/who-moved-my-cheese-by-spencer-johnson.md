---
layout: post.pug 
title: "Who moved my cheese? by Spencer Johnson"
date: 2003-11-04
tags: ["post","Personal"]
legacy_wordpress_id: 23
---

Who moved my cheese? by Spencer Johnson

What a fantastic book. ([http://www.amazon.co.uk/exec/obidos/ASIN/0740737023/ref=sr_aps_books_1_1/202-9329566-9272657](http://www.amazon.co.uk/exec/obidos/ASIN/0740737023/ref=sr_aps_books_1_1/202-9329566-9272657)) Spencer Johnson talks in a wonderfully childish, condescending, even patronising way about how we all, as individuals, cope with and manage change. His book has taken many organisations by storm. It took almost all of Hong Kong by storm.

But what do you do when people are in denial? When people blame others, constantly, for their own aversion to change?

<!-- excerpt -->

Have you ever come across situations where you're trying to make change and you keep getting told by those around you that "Operations won't like that" or "Production will never let you do that" or even "Yeah, great, but the developers are all too stupid to do that properly"? I have and I find that when you challenge these statements, when you go and talk to "Operations", "Production" or the "developers" you find that they are more than happy with your suggestions.

So one of two things could be happening, either the person objecting on another group's behalf is not fully up to speed on that other group's feelings or they actually voicing their own objection. By going back and explaining that everyone else is happy with the move you often find that the objections shift to "oh, but you'll never get that past security" or, if no other avenues are open the objector will, grudgingly accept the change.

This kind of smoke screening and abdication of opinion is rife where I work and destroys the organisation's ability to effect change. It destroys it in several ways, firstly by stifling debate as the group that has objections foisted on it is always in absentia the debate has to stop while the objection is checked. Secondly, the objector's primary concerns and cause for objection is rarely ever voiced, leaving the objector in a bad place where they can neither accept and buy into the change nor can they argue against it. In my epxerience this leads to whispering campaigns and other de-railing activities and, ultimately, teamicide.

But why do people do this? Are people so insecure that they are not prepared to state their own objections? Are they so averse to change that they will object to any change, regardless of its value? Or is it just a shorthand that we should accept and interpret?

If it is a shorthand then what is it short for?

Candidates must be:

"I don't like what you're suggesting, but don't feel able to argue against you";
"I didn't come up with that idea (or have a different idea), but I can't explain why I don't like yours";
"I'm scared of any change, but have no specifics that I can use to stop this one";

Surely there are others, but as I'm not a psychologist I should stop considering the deep psychological motivations and think about how to stop it. The best way I've found is to stop proxied opinions dead in their tracks. Introduce a new rule "You may speak on your own behalf", but that creates a difficulty when a subject is being discussed in the absence of a genuinely interested and relevant party. So needs to be supplemented with "Don't talk about stuff unless _all_ the relevant groups are present".

When these two rules are enforced, you'll get a few meetings where you have to postpone, or you have to extend the invite list a little wider, but the benefit is that those delays and interests become explicit and it becomes harder for people to de-rail change without good reason.
