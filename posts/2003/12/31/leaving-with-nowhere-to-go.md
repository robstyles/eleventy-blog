---
layout: post.pug 
title: "Leaving with nowhere to go"
date: 2003-12-31
tags: ["post","Personal"]
legacy_wordpress_id: 26
---

Well, it's the end of the year. Well, almost. I've just come home from Egg for the last time as an employee. I'm sure I'll drop back in to see a few people, but I'll be off the payroll as of December 31st. That's kinda scary, but it gives me some time to find the right thing to go to next.

<!-- excerpt -->
