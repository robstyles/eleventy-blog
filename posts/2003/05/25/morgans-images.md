---
layout: post.pug 
title: "Morgan's Images"
date: 2003-05-25
tags: ["post",".Net Technical"]
legacy_wordpress_id: 17
---

[Morgan Skinner's written up some nice stuff on color quantization](http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dnaspp/html/colorquant.asp), mapping using both a palette based approach and an octree based algorithm which is also very nice.

This follows on from [work I did with him back in 2002](/2002/05/22/dynamic-image-generation), and adds to it.

<!-- excerpt -->
