---
layout: post.pug 
title: "eXtreme Programming"
date: 2003-01-31
tags: ["post","Software Engineering"]
legacy_wordpress_id: 15
---

Dave Fellows, one of the guys I work with, has been banging on about this thing called XP, eXtreme Programming. Emboldened by a very smart guy called Baker he's persuaded us to try it out.

I have to say it looks pretty good - lightweight specs in the form of short user stories; always having the customer on site to answer questions and specifics and having all of the development disciplines in one place, sharing what they're doing.

<!-- excerpt -->

Sounds good.
