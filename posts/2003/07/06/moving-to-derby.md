---
layout: post.pug 
title: "Moving to Derby"
date: 2003-07-06
tags: ["post","Personal"]
legacy_wordpress_id: 20
---

Well, we've finally moved to Derby (Egg that is, not me). That means an hour and a half commute each way for a while at least.

The new office is pretty impressive. Half of a large tin shed, but with nice desks, new machines with plenty of grunt and I managed to get dual-monitors into the spec for all developers' machines. If you haven't tried being 'Tommy Two Tellies' as Morgan Skinner has insisted on calling me for the past 6 months then I strongly recommend it. [Microsoft Research](http://research.microsoft.com) are doing some measurements of productivity gains for high concentration tasks, such as development work, so look there for some stats soon.

<!-- excerpt -->

The biggest problem for me is that our business colleagues have brought with them a hang up from the other building, a call centre. They like piped music, in the background all day, every day. This makes pair-programming much harder as two people focussed on a problem and communicating well are often disturbed by one or other noticing a track they either like or dislike. Ho hum, maybe one day people will realise how much developers need to concentrate.

If I do this for the next six months I'll have been at Egg for five years. That's a long time and I think it might be time to look for a new challenge.
