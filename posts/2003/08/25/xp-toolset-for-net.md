---
layout: post.pug 
title: "XP Toolset for .Net"
date: 2003-08-25
tags: ["post",".Net Technical"]
legacy_wordpress_id: 22
---

We've started doing TDD and Continuous Integration a bit more formally and have brought together a toolset to support this on the .Net platform. For reference this is currently:

Visual Studio.Net - IDE
Visual SourceSafe - Configuration Management
nUnit - Unit Test Framework
CruiseControl.Net - Continuous Integration Process
nAnt - .Net Build Tool
FXCop - Coding Standards validator
DevPartner Profiler - Performance Analysis and Test Coverage

<!-- excerpt -->
