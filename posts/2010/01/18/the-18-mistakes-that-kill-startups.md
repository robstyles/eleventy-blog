---
layout: post.pug 
title: "The 18 Mistakes That Kill Startups"
date: 2010-01-18
tags: ["post","Software Business"]
legacy_wordpress_id: 609
---

> when I think about what killed most of the startups in the e-commerce business back in the 90s, it was bad programmers. A lot of those companies were started by business guys who thought the way startups worked was that you had some clever idea and then hired programmers to implement it. That's actually much harder than it sounds—almost impossibly hard in fact—because business guys can't tell which are the good programmers. They don't even get a shot at the best ones, because no one really good wants a job implementing the vision of a business guy.
from [The 18 Mistakes That Kill Startups](http://www.paulgraham.com/startupmistakes.html) by Paul Graham.

<!-- excerpt -->
