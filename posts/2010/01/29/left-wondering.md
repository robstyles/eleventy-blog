---
layout: post.pug 
title: "left wondering..."
date: 2010-01-29
tags: ["post","Personal"]
legacy_wordpress_id: 617
---

He's a nice chap, the man across from me on the train, jolly as we share a 'What do you do?' over the tops of our laptops. Mine a Mac with stickers on, his an old corporate HP struggling to boot.

His top button done up, tie pulled tight, pink pin-stripe running through the dark blue of his suit; me in my worn jeans.

<!-- excerpt -->

"What do you do?" I ask. "I'm a head hunter" he replies. "Oh, what sector?" I ask. "Big industry; Power, Energy, Oil and Gas" he says, smiling.

"That must be interesting, do you do much in renewables?" I ask trying to turn the conversation to something I'd be very interested to hear about. "Oh no, there's nothing in renewables, it's just a distraction" he says dismissively. He goes on... "I just finished reading a report, renewables are fine to make us look good but they can't provide anything like enough power for the needs of somewhere like the UK. For the big companies like Shell, BP, they're just a distraction."

"and all this suggestion that hydrocarbons are running out isn't true, the oil companies are happy for people to think that as it keeps the prices high, but a project I recently hired for has found millions of barrels just off Brazil. There's plenty of it out there."

I sit back, wondering if he has kids; if he has noticed the chaotic weather or the news; if he watched <a class="zem_slink" title="The Age of Stupid" rel="wikipedia" href="http://en.wikipedia.org/wiki/The_Age_of_Stupid">The Age of Stupid</a>. I resist asking.

I am left saddened and wondering, do we have any chance at all.
