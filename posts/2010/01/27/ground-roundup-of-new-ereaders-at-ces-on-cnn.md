---
layout: post.pug 
title: "Ground roundup of new eReaders at CES on CNN"
date: 2010-01-27
tags: ["post","Library Tech"]
legacy_wordpress_id: 615
---

> Las Vegas, Nevada (CNN) -- The first generation of electronic readers had little more than black-and-white text. The second generation had black-and-white text, simple graphics and Web connectivity.
> 
> Glimpses of the third generation are on display this week at the International Consumer Electronics Show, where manufacturers are previewing e-readers with color screens, interactive graphics and magazine-style layouts.
from [Bold new e-readers grab attention at CES - CNN.com](http://www.cnn.com/2010/TECH/01/08/ces.ereader/index.html).

<!-- excerpt -->
