---
layout: post.pug 
title: "Hacking Into Your Account is as Easy as 123456"
date: 2010-01-23
tags: ["post","Security And Privacy"]
legacy_wordpress_id: 612
---

> in reality, it's as easy as "123456". And if that doesn't work, we'd suggest trying "12345", next.
from [Hacking Into Your Account is as Easy as 123456](http://www.readwriteweb.com/archives/hacking_into_your_account_is_as_easy_as_123456.php).

<!-- excerpt -->
