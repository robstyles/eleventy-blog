---
layout: post.pug 
title: "No Named Graphs — Period."
date: 2010-10-14
tags: ["post","commands I have issued"]
legacy_wordpress_id: 640
---

cat quads.nq | sed -e "s/^\(\S\+\)\s\+\(\S\+\)\s\+\(.*\)\s\+\(\S\+\)\s\+\./\1 \2 \3 ./" &gt; triples.nt

Someone with regex and sed (or awk) foo may well be able to improve on this.

<!-- excerpt -->
