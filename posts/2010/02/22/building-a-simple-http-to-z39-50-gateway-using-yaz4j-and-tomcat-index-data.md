---
layout: post.pug 
title: "Building a simple HTTP-to-Z39.50 gateway using Yaz4j and Tomcat | Index Data"
date: 2010-02-22
tags: ["post","Library Tech"]
legacy_wordpress_id: 619
---

> [Yaz4J](http://www.indexdata.com/yaz4j) is a wrapper library over the  client-specific parts of YAZ, a C-based Z39.50 toolkit, and allows you to use  the ZOOM API directly from Java. Initial version of Yaz4j has been written by Rob Styles from [Talis](http://www.talis.com/) and the project is now  developed and maintained at IndexData. [ZOOM](http://zoom.z3950.org/api/zoom-1.4.html) is a relatively straightforward  API and with a few lines of code you can write a basic application that can  establish connection to a Z39.50 server.  Here we will try to build a very simple HTTP-to-Z3950 gateway using yaz4j and  the Java Servlet technology.
from [Building a simple HTTP-to-Z39.50 gateway using Yaz4j and Tomcat | Index Data](http://www.indexdata.com/blog/2010/02/building-simple-http-z3950-gateway-using-yaz4j-and-tomcat).

I write Yaz4J a couple of years ago now and it's great to see it getting some use outside of Talis.

<!-- excerpt -->
