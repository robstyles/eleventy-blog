---
layout: post.pug 
title: "You're not the one and only..."
date: 2010-06-24
tags: ["post","Software Business","Semantic Web"]
legacy_wordpress_id: 630
---

The chorus of [Chesney Hawkes](http://www.youtube.com/watch?v=z8f2mW1GFSI)' song goes "I am the one and only", a huge pop hit with teenage girls in the 1990s, but what does that have to do with [SemTech 2010](http://semtech2010.semanticuniverse.com/)?

I was in the exhibit space yesterday evening and there was so much really interesting stuff. I had some really great conversations. Talking about storage implementations with [Franz](http://www.franz.com/) and [revelytix](http://www.revelytix.com/) (and drinking their excellent margaritas), looking at vertical search with [Semantifi](http://www.semantifi.com/) and having a great discussion about scaling with the guys from [Oracle](http://www.oracle.com/technology/tech/semantic_technologies/index.html).

<!-- excerpt -->

A really useful exhibition of some great technology companies in the semweb space.

So why the Chesney reference? Well, several of the exhibitors started out with

> we're the _only_ end-user semantic web application available today
and

> we have the _first_ foo bar baz server that does blah blah blah
and

> we are the _first and only_ semantic search widget linker
and all I could hear in my head every time it was said was Chesney... "You are the one and only" only they're not.

For all of the exhibitors that said they were first or only I had serious doubts, having seen other things very similar. Maybe their 'first' was very specific — I was the first blogger at SemTech to write a summary of the first two days that included a reference to Colibri...

The problem with these statements is that they are damaging, how much depends on the listener. If the listener is new to the semweb and believe the claim then it makes our _market_ look niche, immature and specialist. If the listener is informed and does not believe the claim it makes _your business_ look like marketeers who will lie to impress people. Either way it's not a positive outcome. Please stop.
