---
layout: post.pug 
title: "Flatten it, without collisions"
date: 2010-07-12
tags: ["post","commands I have issued"]
legacy_wordpress_id: 634
---

find ./from -type f | awk '{ str=$0; sub(/\.\//, "", str); gsub(/\//, "-", str); print "mv " $0 " ./to/" str }' | bash

<!-- excerpt -->
