---
layout: post.pug 
title: "Introducing the Web of Data"
date: 2010-11-03
tags: ["post","Working at Talis"]
legacy_wordpress_id: 643
---

** [This post originally appeared on Talis' Platform Consulting Blog](http://blogs.talis.com/platform-consulting/2010/11/02/web-of-data/) **

So, the blog is fairly new, but we've been here a while. For those of you who know us already you may know that Talis is more than 40 years old!

<!-- excerpt -->

During that time the company has seen many changes in the technology landscape and has been at the forefront of many changes.

Linked Data is not too much different. We've been doing Linked Data and Semantic Web stuff for several years now. We think we've learned some lessons along the way.

If you've been to one of our open days, or paid really close attention to our branding, you'll have noticed the strapline shared innovation™. We like to share what we're doing and have been a little lax at talking about our consulting work here — expect that to change. :)

In the meantime I wanted to point to something we've been sharing for a while; course materials for learning about Linked Data. We originally designed this course for government departments working with data.gov.uk, refined based on our experience there and went on to deliver it to many teams throughout the BBC.

It's now been delivered dozens of times to interested groups and inside companies with no previous knowledge who want to get into this technology fast.

In the spirit of sharing, the materials are freely available on the web and licensed under the Creative Commons Attribution License (CC-By).

Take a look and let us know what you think:

<a title="Introduction to the Web of Data Training Course" href="http://bit.ly/intro-to-web-of-data">http://bit.ly/intro-to-web-of-data</a>
