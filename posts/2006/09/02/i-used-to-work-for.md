---
layout: post.pug 
title: "I used to work for..."
date: 2006-09-02
tags: ["post","Personal"]
legacy_wordpress_id: 120
---

Xansa. I haven't bothered to blog at all about my time there as I only lasted 6 months, and it was two years ago. The account I worked on had a particular culture to it that I found I couldn't work with. I'm not going to expand on that here for the same reason I haven't blogged about it before - there are some great folks there trying really, really, hard to do some great stuff, but there's a lot of difficulties that make it difficult for them to.

So, why am I posting this now? Well, this strange thing keeps happening; every few months or so I get a clump of searches for Xansa and variants of it showing up in the logs - from inside Xansa's network.

<!-- excerpt -->

So... Hello Xansans (or whatever the correct plural noun is). I wish you all well. May your god smile on you and shower you with good fortune. :->
