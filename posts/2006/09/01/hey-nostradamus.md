---
layout: post.pug 
title: "hey, nostradamus"
date: 2006-09-01
tags: ["post","Fiction Book Review"]
legacy_wordpress_id: 119
---

This book is a great book. I started reading Douglas Coupland when a friend handed me Girlfriend in A Coma and I couldn't put it down. That was several years ago and, building software for a living, I had to read Microserfs. I guess I'll get around to reading JPods soon enough.

But this book get a strong eight-out-of-ten. It's different; in the way that Pulp Fiction was different and Isaac Asimmov's The Culture series is different. It doesn't feel like anything else I've read. Ever.

<!-- excerpt -->

So what's it about? Well, Cheryl, Jason, Heather and Reg. These four people form the chapters of this book and each chapter is written as a stream of consciousness from each of them. They don't overlap the same events in the same way as the Gospels, they kind of move on in time gently, and sometimes less gently. I guess the most obvious thing is that each character is talking about losing people. This could be depressing and dark, and in parts it is a little dark, but I found it made me wonder more and more about what goes on in other's heads.
