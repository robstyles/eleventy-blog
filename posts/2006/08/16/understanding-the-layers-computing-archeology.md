---
layout: post.pug 
title: "Understanding The Layers, Computing Archeology"
date: 2006-08-16
tags: ["post","Other Technical"]
legacy_wordpress_id: 118
---

Over at Outgoing, <a title="Outgoing: UNIMARC" href="http://outgoing.typepad.com/outgoing/2006/08/unimarc.html">Thom's been looking at Marc</a> a bit. Not a surprise really, as <a title="OCLC" href="http://www.oclc.org/">OCLC</a> use it quite a bit.

When I first joined <a title="Talis" href="http://www.talis.com">Talis</a>, almost two years ago, I wrote about <a title="The promises (and arrogance) of youth" href="/2004/11/24/the-promises-and-arrogance-of-youth">The promises (and arrogance) of youth</a>, a comment about how most, if not all, of what we try to do has been done before and often better.

<!-- excerpt -->

Not long after posting that I started using some existing libraries for parsing marc records, but found some interesting things in the records that didn't fit with the libraries, like alphanumeric tags. Not only that, but it seemed that the format looked very general for just library use so I dug through the layers a bit. This sometimes feels like the computing equivalent of archeology, looking for artefacts that indicate the history of the code. For example, whenever you see the EBCDIC character encoding you may well find an IBM mainframe used to lurk around there some time ago.

Only a little digging led me to two equivalent standards, <a title="ISO2709 Standards Documents" href="http://www.iso.org/iso/en/CatalogueDetailPage.CatalogueDetail?CSNUMBER=7675&ICS1=35&ICS2=240&ICS3=30">ISO2709</a> and <a title="ANSI/NISO Z39.2 Standards Document (pdf)" href="http://www.niso.org/standards/resources/Z39-2.pdf?CFID=9006868&CFTOKEN=88305210">ANSI/NISO Z39.2 (pdf)</a>. For those familiar with Z39.50 it should be no surprise that there was also a Z standard for the interchange format that underpins MARC.

Like the US Constitution, the <a title="About Talis" href="http://www.talis.com/about_talis/corporate.shtml">forefathers</a> of our domain helped <a title="Talis Developer Network" href="http://www.talis.com/tdn/">develop</a> and <a title="Talis: Mashing Up The Library Competition" href="http://www.talis.com/news/press/press_releases.shtml#no074">implement</a> these standards in a spirit of <a title="Talis Source: The Right To Contribute" href="http://www.talis.com/source/blog/2006/05/the_right_to_contribute.html">freedom</a> and <a title="Panlibus: Looking forward to WorldCat.org" href="http://blogs.talis.com/panlibus/archives/2006/07/looking_forward.php">openness</a> that has allowed many to share knowledge widely over the decades since. That's why I work for <a title="Talis" href="http://www.talis.com">Talis</a>.
