---
layout: post.pug 
title: "Worse is Better"
date: 2006-06-07
tags: ["post","Software Business"]
legacy_wordpress_id: 115
---

Thom Hickey over at [Outgoing](http://outgoing.typepad.com/outgoing/) posted a snippet about the old addage <a title="Outgoing: Worse is better?" href="http://outgoing.typepad.com/outgoing/2006/05/worse_is_better.html"> Worse is better</a> - or "Crap is the new black" as I prefer.

Thom says:

<!-- excerpt -->

> People often accept this as the reason why VHS triumphed over Betamax, and why people use Microsoft products. <snip> In most cases it isn't so much that one is better than another that is important, it is how broadly useful the technology is.

And here's the nub of it I think. It's not that one thing is _absolutely_ better or worse, it depends on your criteria. That's why [programming] language wars are so futile, languages are useful for what they allow you to do and how easy they make it. Nobody know why VHS won over Betamax for sure, it may be record time and licensing as Thom suggests, but it may be that VHS players were smaller and, hence, more discreet under the telly. Or it may have been that the VHS manufacturers offered slightly more opportunity for the retailer to make a few extra pennies.

Going beyond products, it's about making sure your motivations and your customers are aligned. In the case of VHS, Mrs Miggins motivation is not to have a large ugly black Betamax machine in her lounge. She will, however tolerate the smaller VHS player. In the case of SGI it wasn't that anybody said they didn't like being able to [render 3D teapots](http://www.sgi.com/products/software/opengl/examples/samples/images/teapot.jpg), it's just that cost was a more important factor - the same was true of the original Sony transistor radio.

What's key in our world, metadata, is to understand what it is used for, what can it be used for, what do customers want to use it for. Answering these questions can give us an indication of their motivations and can lead us to understand if our own motivations are aligned with them or, ultimately, in confilct.
