---
layout: post.pug 
title: "Why I'm not blogging"
date: 2006-06-07
tags: ["post","Personal"]
legacy_wordpress_id: 114
---

So, it's ages since I last blogged anything and even then a lot of was [BlogPaper](/tags/grid::blogpaper/)... So what's up with that?

Well, I try to blog things that genuinely contribute to the world. Things like [Poo](/2004/12/17/poo) and [What is professionalism really about?](/2004/08/10/what-is-professionalism-really-about) Writing things like that contribute gives me a sense of meaning, of being able to help others.

<!-- excerpt -->

What I've tried to avoid is a link-fest to unrelated things I spotted on other people's blogs - you have [Technorati](http://www.technorati.com/) for that - or short thoughts on things flying round the blogosphere. If I haven't got time enough to research and understand stuff I generally won't blog it, Tim O'Reilly justifies that for me:

<a title="O'Reilly Radar > Web 2.0 Service Mark Controversy (Tim responding this time)" href="http://radar.oreilly.com/archives/2006/05/web_20_service_mark_controvers.html">O'Reilly Radar > Web 2.0 Service Mark Controversy (Tim responding this time)</a>

The other reason is that a lot of what I'm doing right now is massively interesting, [way too interesting to blog](http://www.technorati.com/tags/talis) it ;-)

I'll be back with some photos soon...
