---
layout: post.pug 
title: "Big, Retro Headphones"
date: 2006-12-08
tags: ["post","Personal"]
legacy_wordpress_id: 132
---

I just read over this report:

[Sound Output Levels of the iPod and Other MP3 Players: Is There Potential Risk to Hearing?](http://www.hearingconservation.org/docs/virtualPressRoom/portnuff.htm)

<!-- excerpt -->

and it's great. I bought a pair of [Sennheiser headphones](http://www.sennheiser.co.uk/uk/icm.nsf/root/04478) a couple of years back mainly because I enjoy my music and wanted to hear it without it being massacred by earbuds.

I [wrote some visualisations for Windows Media Player](/2004/04/04/better-bars-2) to see what different codecs were doing to the frequency range too...

So, now when I sit in the office looking über cool in my big white retro looking headphones I know that in just a few short years I'll be laughing at you poor deaf b*st*rds.

Updated: It's just been pointed out to me that I may be laughing but you won't be able to hear me so it won't matter.
