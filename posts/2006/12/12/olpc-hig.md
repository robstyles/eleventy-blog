---
layout: post.pug 
title: "OLPC HIG"
date: 2006-12-12
tags: ["post","Interaction Design"]
legacy_wordpress_id: 133
---

The OLPC (One Laptop Per Child) Project has releases [Human Interface Guidelines for the machine](http://wiki.laptop.org/go/OLPC_Human_Interface_Guidelines).

This is a great piece of thinking and is, aongst other things, the first really good application of [Fitts Law](http://en.wikipedia.org/wiki/Fitts'_law) that I've seen.

<!-- excerpt -->

I was going to write some stuff up about it, but [Mikes Journal has done a great job already](http://plan99.net/~mike/blog/2006/12/10/olpc-is-genius/).

If you're into Interaction Design, you might want to follow [Nadeem's occasional ramblings](http://www.virtualchaos.co.uk/blog/2006/12/05/programmers-are-generally-bad-at-user-interface-design/) too.

As he says, [we are currently looking](http://www.talis.com/about_talis/careers/careers.shtml) for talent in this area.
