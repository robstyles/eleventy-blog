---
layout: post.pug 
title: "ah, now I can read"
date: 2006-12-01
tags: ["post","Other Technical","Blog on Blog"]
legacy_wordpress_id: 129
---

I've been using [FeedReader](http://www.feedreader.com/) for a while (I prefer email style new to river of news) and it works really well. The interface is calm and it fits the way I work. But a few months back I upgraded my laptop to a higher res one, 1920x1200 in just 15.4". This looks great, but I do end up making text larger in a few of my apps. I wanted to change that text size in FeedReader and I couldn't find it anywhere. Then I remembered that the preview pane in FeedReader is just an embedded browser - I wonder... Sure enough, C:\Program Files\FeedReader30\stylesheet contains atom.xsl, emailstyle.xsl and custom(delicious.xml); a quick tweak from

```
body {
font-family: verdana, tahoma;
font-size: 0.7em;
line-height: 1.3em;
padding: 0;
margin: 0;
}
```

to

<!-- excerpt -->

```
body {
font-family: verdana, tahoma;
font-size: 1.0em;
line-height: 1.3em;
padding: 0;
margin: 0;
}
```

in both atom and emailstyle and all looks lovely :-&gt;
