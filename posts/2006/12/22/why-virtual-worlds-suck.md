---
layout: post.pug 
title: "Why virtual worlds suck..."
date: 2006-12-22
tags: ["post","Random Thought","Internet Social Impact"]
legacy_wordpress_id: 138
---

And why they don't have to...

So, I've been spending too much time in [Second Life](http://secondlife.com/) and broadly agree with Nad's "[The problem with Second Life](http://www.virtualchaos.co.uk/blog/2006/12/12/the-problem-with-second-life-is-it-just-sucks/)".

<!-- excerpt -->

But why does it suck and what would I want instead? Well first I think Second Life is too much like real-life - the physical characteristics of the world are too similiar too restrictive. Sure, you can teleport; great. And you can fly, albeit very, very slowly. But broadly the things that slow you down IRL are the same things that slow you down ISL.

There are things that aren't right yet, like how you show search results as objects, for example, but there are others too. ISL today my search results look like a whole load of blobs; arbitrary objects created by the search interface for me to rifle through. This isn't the fault of those producing search interfaces, but the fault of the engine's themselves.

The good news is we don't have to look far for a more imaginative solution. In The Matrix<sup>1</sup> Neo needs guns, so guns arrive. He doesn't move within the world, the world moves for him. That's the kind of virtual world I want to play in.

<embed src="http://www.youtube.com/v/Y70vcs3oV14" width="600" height="350" type="application/x-shockwave-flash"></embed>

In real-life when you want to, say, arrange books on the shelves of a library you have to choose a filing scheme - there are plenty around and they each serve a different blend of different needs. In a virtual library we could ditch the shelves altogether, but if we kept the shelves we could let people re-sort the shelves to meet their own needs - instantly and on-demand.

For that to work for many people at the same time that would mean the world that others are seeing would be slightly different from what I am seeing. That is to say that the while you and I may be stood next to the 'same' shelves, the books may be arranged differently for me than for you.&nbsp;That might bring usability problems with it too at times, but at least it would be lifting the restrictions of this world.

Imagine, this is like being able to all sit around one TV, but watch your own programmes! I've wanted to do that since I was six; imagine the childhood fights that would stop!

If you think about it this is what websites do all the time, if we used a search perspective rather than a browse of the shelves, we could see a shelf as analogous to the search results page of a search engine - that's seen by millions of people at the same time, but contains results for each of us. What if we could do the same for the shelves? A hundred of us could be stood looking at virtual shelves, seeing different results upon them.

Any aspect of the world could vary, the same corridor of shelving could be 50 feet long for me and only 20 feet long for you - there would be some work to do in mapping where people are stood within each others' spaces, but that's not too hard. Imagine what that could mean though. When I search for books now, I could get my own shelf of results - everyone stood in the search "room" would only see their own shelf. I could easily judge how many results I have by the length of the shelf. Perhaps there would be a way for me to collaboratively search and manipulate results with other people I invite to help me.

AFAIK the current virtual worlds don't support this kind of personalised, run-time, programmatic remodelling of the physical space. And that's why they suck - they're too much like this world.

<sup>1</sup> This clip, The Matrix, Copyright 1999, Warner Bros
