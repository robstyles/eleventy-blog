---
layout: post.pug 
title: "Tidy or Organised"
date: 2006-12-21
tags: ["post","Software Engineering"]
legacy_wordpress_id: 137
---

Rachel Davies (she's a top-notch agile coach if you're looking for one) [discusses the term _refactoring_](http://twelve71.typepad.com/rachel/2006/12/why_call_it_ref.html) and suggests it may be unhelpful.

She suggests calling it _Tidying Up_ instead as then it's clear what we're doing.

I agree with Rachel whole-heartedly that the term is somewhat grandiose when talking about the work with non-technical project members, but I don't think it's just tidying up - it's _organising_.

<!-- excerpt -->

The disctinction I make comes from conversation with colleagues about home life, rather than work life. Those moments at home when you tidy up and that means putting several ramshackle piles of paper into one neatly stacked pile, putting all the books back on the book-shelf and doing some dusting and hoovering. Organising, on the other hand, is a much more cerebral activity - maybe sorting that pile of papers into several piles; one for the car insurance, MOT certificate and service history and others for house-stuff, work-stuff etc. You might even decide to buy some colour-coded folders for them all!

While a lot of what we need to do with code is tidying up - last week I re-sorted the entries in a (rather large) configuration file so there was some meaning to the order; I also spent some time re-formatting large sections of code to keep the indentation and whitespace sensible, as well as changing the offending IDE settings to keep things straight in future. This is all tidying up; it doesn't change the meaning conveyed by the code.

Re-factoring (rob waves an arm ostentatiously as he rolls the term across his palette) on the other hand is about putting more meaning into the code. It's about making the code more _organised_, rather than just _tidier_.

The [Alpha list of refactorings](http://www.refactoring.com/catalog/index.html), maintained by Martin Fowler, lists a great number of different refactoring patterns. There are things like _Add Parameter_, which allows a method to _say that it needs more information from its caller_, Collapse Hierarchy, which combines two not very different classes into one, and so on. These are about making the code describe the problem more clearly by organising the code into meaningful piles.

In my experience this is more than _tidying up_.
