---
layout: post.pug 
title: "Metaverse"
date: 2006-12-14
tags: ["post","Internet Social Impact"]
legacy_wordpress_id: 136
---

One of the first things that frustrated me about Second Life was the restrictions on real-estate that are necessary for Linden Labs business model. Having to buy land in a virtual world seems bizarre, unless you're the one selling it, of course.

What I want is an open-source world server where I can set up my own world and use hyperlinks to join the boundaries of my world others - like a kind of web of virtual worlds.

<!-- excerpt -->

As none of my ideas are original, it was of no surprise to me to find this... <a title="Open Source Metaverse Project" href="http://metaverse.sourceforge.net/index.html">Open Source Metaverse Project</a>
