---
layout: post.pug 
title: "Offshoring..."
date: 2006-12-13
tags: ["post","Software Business"]
legacy_wordpress_id: 135
---

Over at Virtual Chaos, [Nad's been rambling about offshoring and outsourcing](http://www.virtualchaos.co.uk/blog/2006/12/13/outsourcing-developers-abroad-is-it-really-a-good-idea/).

> The problem though, is that writing code isn’t something you can translate into an assembly line. What I think the people pushing this type of outsourcing failed to comprehend, and seemingly still dont understand is that farming out development overseas doesn’t lead to innovation. ... Someone famously once said _every line of code is a design decision_, I’m struggling to remember who it was [insert clever guys name here]. But that single statement embodies for me what the real problem is with outsourcing projects abroad.

<!-- excerpt -->

I don't really see this as about innovation directly. I think it's all about design. There are plenty of software projects that aren't about innovation; they're about cost-reduction or about refreshing technologies (which is generally about cost-reduction) or about well, cost-reduction. Most of what IT departments in enterprise are asked to do is cost reduction - hence most of what enterprise will outsource is also about cost-reduction. Innovation isn't really a factor.

The indirect quote that Nad references about code being design could well have been from [Code as Design: Three Essays by Jack W. Reeves](http://www.developerdotstar.com/mag/articles/reeves_design_main.html). Jack's discussions parallel what Nad is saying; that writing code is the act of designing something, not the act of manufacturing something. "Tooling up" is done by the compiler and manufacture is when you press lots of CDs, or on-demand when folks download your RPM or MSI.

The answer you arrive at about outsourcing (and offshoring is just outsourcing with extra big communication barriers and some cultural differences thrown in) will depend on what part of the [software universe](http://www.joelonsoftware.com/articles/FiveWorlds.html) you feel you're in. For us, [we](http://www.talis.com/) write commercial software that we sell and maintain for a number of years, the quality of code is important.

But that's not always the case for everyone. Say you have a legacy application that was not well-written to start with, it's built on top of old, unsupported languages (say, Java 1.2?) and you need to keep running it with minor changes for a few more years. There is no innovation to be done. The design decisions in-the-small aren't that important to you as they'll be better than the ones made last time! Your team in the office are de-moralised by the very mention of the application's name and you're over-stretched on new projects that are innovative... Surely that's a contender for off-shoring?
