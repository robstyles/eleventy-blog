---
layout: post.pug 
title: "So Proud."
date: 2006-01-25
tags: ["post","[grid::fatherhood]"]
legacy_wordpress_id: 111
---

In order to learn about how the world works we need two things, the ability to notice that stuff is the way it is and the ability to find or develop an explanation for the way things are. These are things we try to assess when looking for talent to hire.

I was so proud last night becuase my (eldest) boy, aged 5 (and a half) asked what in my mind is a seminal question.

<!-- excerpt -->

> Why do zips all have YKK written on them?

Maybe I should add that to the list of questions to ask candidates.
