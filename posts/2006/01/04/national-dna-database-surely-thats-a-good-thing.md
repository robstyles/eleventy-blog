---
layout: post.pug 
title: "National DNA Database \"surely that's a good thing\""
date: 2006-01-04
tags: ["post","Security And Privacy"]
legacy_wordpress_id: 108
---

I've been concerned about the current government's stance on many things, but today Lord Mackenzie of Framwellgate was defending the National DNA Database, used by Police to log and search through DNA samples when investigating crimes.

<!-- excerpt -->

[Lord Mackenzie of Framwellgate](http://www.lordmackenzie.com/) appeared on Radio 4's Today program this morning alongside [Simon Davies Director of Privacy International](http://www.privacyinternational.org/); [interviewed by James Naughtie](http://www.bbc.co.uk/radio4/today/listenagain/ram/today5_dna2_20060104.ram).

There was discussion about the Home Office statement that convictions supported by the use of DNA evidence had increased four-fold over the last five years. The stance that Lord Mackenzie takes is fairly typical of the stance many take - this success rate can be improved by having everyone's DNA in there and if you haven't got anything to hide then you have nothing to fear. Unfortunately this simply isn't true.

> John: the argument goes if you haven't done anything wrong you've no reason to fear your DNA being in the database.
> 
> LM: I understand that and it's a question of balance at the end of the day ... what we find is that when people are arrested for say drunk driving and are given a blood test we find they've committed a rape or a murder 20 years ago, 15 years ago ior even a string of rapes or murders that's got to be good news Jim, surely.

Lord Mackenzie's language really belies the truth and is about demonising any opposition rather than furthering the debate. Why? Well, anyone who argues against Lord Mackenzie's point is saying that catching murderers and rapists is a bad thing - how can you argue against that? Well, you have to ask "at what cost?"

[The Department for Transports figures](http://www.dft.gov.uk/stellent/groups/dft_transstats/documents/downloadable/dft_transstats_041304.pdf) (Table 2d, Page 29)  suggest that the number of people who either failed a breath test or refused to give a sample (in 2003) was in the region of 106,000 (for England and Wales). That's a lot of people - with the population being about 52.5 million that's 2 offences per 1,000 population.

Murders, on the other hand, ran at 854 for roughly, but not quite, the same period - according to the [Home Office Crime Statistics](http://www.crimestatistics.org.uk/tool/Default.asp?region=0&force=0&cdrp=0&l1=6&l2=1&l3=27&sub=0&v=24). That's just 0.02 offences per 1,000 population. As the vast majority, a national average of 76% [according to The Times](http://www.timesonline.co.uk/article/0,,2-1884801_2,00.html), of these are solved that leaves us with 205, or 0.003 offences per 1,000 population.

So, we try to detect unsolved murders, i.e. find an unfound murderer -  just 0.0003 percent of the population by taking and storing the DNA for 0.002 per cent of the population with samples based on a completely unrelated crime.

Is it me or does this make no sense?
