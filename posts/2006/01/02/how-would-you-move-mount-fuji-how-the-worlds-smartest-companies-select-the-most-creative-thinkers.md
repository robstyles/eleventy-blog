---
layout: post.pug 
title: "How Would You Move Mount Fuji? How the World's Smartest Companies Select the Most Creative Thinkers"
date: 2006-01-02
tags: ["post","Non-Fiction Book Review"]
legacy_wordpress_id: 107
---

I've been interested in how to recruit really good people for a long time. I started helping in interviews and assessments in my first job after graduating and have been involved heavily in most of my roles since. So this book has been on my reading list for a while and finally made it to the top this holidays - I confess I was looking for some puzzles to counteract the alcohol intake...

<!-- excerpt -->

What's most interesting about this book is the quantity of history and research in it. A book that said "Microsoft use puzzles and here they are" would have been extremely "so what", but this book tackles some really good questions like where the puzzles came from and some that have been discredited. IQ Tests, for example, have a far more shady past than I realised.

William Poundstone's style of writing is fluid and relaxed, very easy to read and the puzlzles are thrown in throughout the book - a nice format as most people reading this will probably have at least some interest in the puzzles for their own sake. Most of the types of puzzle discussed are far more straightforward than the infamous title question which is [almost certainly impossible to answer well](http://discuss.fogcreek.com/joelonsoftware/?cmd=show&ixPost=42089) and this leads to some intersting thoughts about what puzzle solving abilities do or don't show about a candidate.

Perhaps what they show the most is that people who like solving puzzles like to find, compete with and hire like-minded puzzle solvers.

I'll pass this around the office when I get back in as we're in the process of really trying to pin down how we attract, identify, retain and develop the best people - this book isn't the answer, but it's definitely a part.

"How Would You Move Mount Fuji? How the World's Smartest Companies Select the Most Creative Thinkers" By William Poundstone, Little, Brown and Company, 2004, ISBN: 0316778494
