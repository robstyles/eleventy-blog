---
layout: post.pug 
title: "Dirty Thieving B*st*rd"
date: 2006-11-30
tags: ["post","Software Engineering","Working at Talis"]
legacy_wordpress_id: 128
---

Yesterday at work I was having a pleasant few minutes chat with [Nadeem](http://www.virtualchaos.co.uk/blog/) over coffee - a break from refactoring our acceptance test suite and we go talking about [Gordon Ramsay's Kitchen Nightmares](http://www.channel4.com/ramsay). A subject close to my heart as a when Ramsay first started doing Kitchen Nightmares a colleague compared him to me, while reviewing code.

Like Ramsay I am passionately against over complicated things and that sometimes means I come across a little strong.

<!-- excerpt -->

Anyway, we finished up with me saying that I was going to blog about it, so [what do I find when FeedReader opens up this morning](http://www.virtualchaos.co.uk/blog/2006/11/30/gordon-ramsay-could-teach-software-engineers-a-thing-or-two/)?

Very nicely written Nad.
