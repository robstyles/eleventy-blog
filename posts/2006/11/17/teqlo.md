---
layout: post.pug 
title: "Teqlo"
date: 2006-11-17
tags: ["post","Internet Technical"]
legacy_wordpress_id: 123
---

I've been watching Teqlo for a little while (since before the renamed from Abgenial Systems) and they look very interesting. They've been pretty coy about what they're doing other than letting us know they're broadly about letting you combine web services into applications without doing any programming. Anyways, they [finally got a video out](http://blog.teqlo.com/?p=22) showing precisely... nothing. Although the UI looks like it'll be quite pretty...

I've signed up for the preview, so I hope dissing the video doesn't stop them sending me a login...

<!-- excerpt -->
