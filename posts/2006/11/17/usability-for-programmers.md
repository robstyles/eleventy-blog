---
layout: post.pug 
title: "Usability for Programmers"
date: 2006-11-17
tags: ["post","Software Engineering"]
legacy_wordpress_id: 122
---

At work we have a geek book club, a little while ago we all read [Joel Spolsky's User Interface Design for Programmer's](http://cenote.talis.com/isbn/9781893115941), which is a longer, printed version of [this earlier online work](http://www.joelonsoftware.com/uibook/chapters/fog0000000057.html). Joel does a great job of explaining the basic of use interaction, provides a few rules-of-thumb and some well-written anecdotes.

What's struck me back in the office, though, is that the phrase can be read two ways, as teaching programmer's how to design interfaces or about how to design interface that programmers will use.

<!-- excerpt -->

This set me thinking about other stuff we've been reading, lie [Paul Graham's Hackers &amp; Painters](http://www.paulgraham.com/hp.html), [The Pragmatic Programmer](http://www.pragmaticprogrammer.com/ppbook/index.shtml), [Object Thinking](http://www.microsoft.com/mspress/books/6820.aspx) [Code as Design](http://www.developerdotstar.com/mag/articles/reeves_design_main.html) and other bits and pieces. A common thread of all of these is that writing code is about describing a solution to a problem, rather than simply providing a set of instructions to a computer.

Then this week a few of us were working on checkout and build processes for one of our newer codebases, trying to make it easier for ourselves to work on and it struck me that we were really designing software for each other and that a big part of best-practice and common approaches is really just about usability and interactiopn design.

Take, for example, any one of the popular c/c++ open-source applications and you'll find it has an sutomake based configure script which generates a makefile that has 'clean', 'build', 'all' and 'install' targets in it. i.e. it follows the standard pattern. This is no different to using standard menus, buttons and other controls in a Win32 GUI - it's easier to use.

So, mental note to self:

Consider anyone who will be working with code I'm writing as a class of user and design the development interface with as much care as the user interface.
