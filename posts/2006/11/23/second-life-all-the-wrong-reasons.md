---
layout: post.pug 
title: "Second Life... All the wrong reasons"
date: 2006-11-23
tags: ["post","Internet Social Impact"]
legacy_wordpress_id: 125
---

Warren Ellis posts a [colourful description of his drop into Second Life](http://www.warrenellis.com/?p=3329). This is an extreme position. I'm a non-paying user of SL and have managed to avoid being harangued in any of these ways.

At [work](www.talis.com) we've looked at opportunities for SL, we've even [sponsored some initiatives](http://blogs.talis.com/panlibus/archives/2006/11/talis_helps_bri.php) and we even have our own office there.

<!-- excerpt -->

In SL, there are opportunities to do things just not possible in first life. The most obvious is simply flying and tele-porting around the world; but many folks are setting up businesses too. One of my colleagues, of all the things to do, has a nice little routine of finding a nice little sitting spot (where he gets paid $1 Linden for every 5 minutes he sits) and when he's earned enough he'll hop up and wander over to the virtual one-armed bandits and feed them his virtual earnings.

While there is so [much](http://www.infoisland.org/) [great](http://secondlife.reuters.com/) [stuff](http://blogs.electricsheepcompany.com/sheep/) [going](http://zdnet.com.com/1606-2_2-6099774.html) on the only money being made seems to be on Gold-Rush businesses. During the gold-rush those who made money weren't digging for gold, they were selling houses and land and clothes and shovels; all of the SL millionaire stories I've heard fit into that category - selling real-estate, buildings and avatar designs. It's a virtual world and still the most common businesses are based on how we look.

If it's going to go anywhere we have to find business models that are about doing business in a virtual world, bot about the world itself.

Updated to add: Nad has expanded on this to note [his own feelings](http://www.virtualchaos.co.uk/blog/2006/12/12/the-problem-with-second-life-is-it-just-sucks/)...
