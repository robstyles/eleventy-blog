---
layout: post.pug 
title: "Things are always the way they are because they got that way."
date: 2006-11-23
tags: ["post","Software Engineering"]
legacy_wordpress_id: 126
---

<a title="How things got that way" href="http://www.toolshed.com/blog/articles/2006/08/19/how-things-got-that-way">How things got that way</a>

Andy Hunt's (Pragmatic Programmer) blogs a quick post about how "things are always the way they are because they got that way".

<!-- excerpt -->

I always try to look at what I am checking in and ask "does it improve or weaken the overall codebase?" I've never come across a situation where the honest answer is that my contribution is neutral; I'm either improving the code for the next person who checks it out or I'm making it worse. If the team all make the code just a little worse each check-in it soon adds up.
