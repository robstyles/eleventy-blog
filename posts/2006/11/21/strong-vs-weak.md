---
layout: post.pug 
title: "Strong vs Weak"
date: 2006-11-21
tags: ["post","Internet Technical"]
legacy_wordpress_id: 124
---

My colleague Ian Davis [pointed at](http://iandavis.com/blog/2006/11/strong-typing-vs-the-web) a [great post by Nelson Minar](http://www.somebits.com/weblog/tech/bad/whySoapSucks.html). 

> The deeper problem with SOAP is strong typing. WSDL accomplishes its magic via XML Schema and strongly typed messages. But strong typing is a bad choice for loosely coupled distributed systems. The moment you need to change anything, the type signature changes and all the clients that were built to your earlier protocol spec break. And I don’t just mean major semantic changes break things, but cosmetic things like accepting a 64 bit int where you use used to only accept 32 bit ints, or making a parameter optional. SOAP, in practice, is incredibly brittle. If you’re building a web service for the world to use, you need to make it flexible and loose and a bit sloppy. Strong typing is the wrong choice. 

<!-- excerpt -->

This difference in capability and philosophy betwen weak and strong typing is still something I'm getting a handle on. On the one hand I like the flexibility of wekly typed languages like Smalltalk and more recently Ruby, but on the other hand strong typing gives us great gifts like Intellisense and compile-time type checking (which I still find useful when I'm using someone elses API).

I've been watching [Teqlo](http://teqlo.com/) for a while (haven't had my preview key yet though) and it seems [they also appreciating strong typing](http://blog.teqlo.com/?p=23)...

The questions in my mind are:

Do Teqlo know something I don't about the trends for web services and typing?

Could Teqlo do what they're trying to without strong typing?

If web services go weakly typed will Teqlo have a business?
