---
layout: post.pug 
title: "Physical World Hyperlinks"
date: 2006-11-24
tags: ["post","Internet Social Impact"]
legacy_wordpress_id: 127
---

The Pondering Primate has been chattering away about PWHs, or [Physical World Hyperlinks](http://theponderingprimate.blogspot.com/2006/11/physical-world-hyperlinks-next-big.html) for ages. The potential is obvious.

In [my world](http://www.talis.com), many of our customers use RFID on their books and self-service terminals from intellident, 3M or other partners. This is the same thing as a PWH in many ways, apart from one key thing. They're local, proprietary and constrained in purpose.

<!-- excerpt -->

I had thought the ISBN13, ISBN joining the EAN world, might help but I've since discovered that EAN get reclaimed and re-used once a product is not being retailed. So in 20 years I might find an old tin of beans, check out the EAN and find it's been re-issued to Oakley sun glasses. Permanence in Hyperlinks is important, so this isn't ideal.

[TMS looks like a really nice new approach](http://tms.com.ph/blog/). It's a machine-readable tag designed to be read by a phone camera - how long before I have glasses that can read them?
