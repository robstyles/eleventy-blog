---
layout: post.pug 
title: "If It Was Good Enough for Shakespeare"
date: 2006-02-02
tags: ["post","Personal"]
legacy_wordpress_id: 112
---

I've been accepted to speak at [Waterfall2006](http://www.waterfall2006.com/) with the following session:

[If It Was Good Enough for Shakespeare: A Fresh Look at the Need for Talent in Software Engineering](http://www.waterfall2006.com/styles.html)

<!-- excerpt -->

:-)
