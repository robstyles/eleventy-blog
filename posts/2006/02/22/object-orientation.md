---
layout: post.pug 
title: "Object-Orientation"
date: 2006-02-22
tags: ["post","Software Engineering"]
legacy_wordpress_id: 113
---

There are a number of things that I look for in assessing if code has been written in an object-oriented way or in a procedural way - I've got a document somewhere that I wrote at a previous job, but this [brief piece about "getters" by Martin Fowler](http://martinfowler.com/bliki/GetterEradicator.html) is well worth reading.

Fowler assumes a fair amount of prior knowledge in his writing. For example, for this piece to make sense you need to know that "getters" is a term used for methods on Java classes that simply expose a field on the object.

<!-- excerpt -->
