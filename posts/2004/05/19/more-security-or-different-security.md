---
layout: post.pug 
title: "More Security or Different Security"
date: 2004-05-19
tags: ["post","Enterprise Architecture"]
legacy_wordpress_id: 52
---

We've just been having a discussion at work about the benefits of Impersonation and Delegation in .Net. That is, the ability of an application to perform actions using the identity of the human user driving them.

For web applications, web services, database access etc this can be very useful, giving a trail throughout a multi-tier application showing which user performed an action.

The obvious perception is that this is, of course, more secure. But that's just not true...

<!-- excerpt -->

If you impersonate a user when doing things like database access then what you're left with is an audit trail that says "Alice updated row 971 in Table Foo", but if you go and ask Alice she'll quite clearly tell you she did no such thing. Nor should she be able to.

More worrying is the situation where you ask Alice and she says "Oh, yeah, that web app you wrote is so bad. I just set up an ODBC link and I put my stuff straight in using Access".

If this system is impersonating Alice when accessing databases then Alice's account requires access rights which she clearly shouldn't have.

Extend this to a situation where there are several client systems all talking to the same database, via a shared data access layer, or service obviously. Now we have a situation where an update happened in Alice's name, but we have no idea how. So we end up writing tracking logic in so we know which system did what - a homegrown audit trail.

Now, a mailicuous application can, without knowing any credentials, do things in the database using a genuine user's identity, perhaps using the 'application name' of another application and do damage that is difficult to trace.

If we weren't impersonating this would not be possible, but we would have to track which user actions triggered which database events.

This brings me to the problem at hand. Impersonation isn't enough. We need to know two things. Who or What is performing the action and Who the action is being performed for or on behalf of. A dual-authentication, knowing both the application and the user.

Without knowing both parts securely, Impersonation gives you a different audit trail, not necessarily a better one.
