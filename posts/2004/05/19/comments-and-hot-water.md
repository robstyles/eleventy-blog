---
layout: post.pug 
title: "Comments and Hot Water"
date: 2004-05-19
tags: ["post","Software Engineering"]
legacy_wordpress_id: 51
---

"...and they don't even comment their code..." said Brian as he explained noisily why the project he was code reviewing was poor enough to get someone fired.

"But comments are evil" I said, defending code that was not mine and I had never seen. "Comments are an excuse for writing unreadable code that nobody other than the writer can understand ... and they go out-of-date quicker than you can say 'jack robinson' so you can't trust them even if they're there."

<!-- excerpt -->

As the looks of incredulity greeted me from around the office I realised that, perhaps, I hadn't done enough groundwork before making this statement.

"It's like those 'Very Hot Water' signs you get in office bathrooms" I went on. "The problem is that the water is too bloody hot, " I aspoused "but instead of fixing the water temperature somebody, almost certainly in a meeting where the guy who controls the water temerature wasn't present, orders the installation of the evil signs".

Still ranting on I point out that "the whole purpose of which is to present a defence in the event that someone gets scalded and decides to sue".

"They actually turn the water temerature up to kill any germs in the pipes" a small voice threw across the room.

But the point still stands. Stop insisting developers write comments and start insisting they write _human_ readable code.
