---
layout: post.pug 
title: "Must learn to look deeper"
date: 2004-05-05
tags: ["post","Personal"]
legacy_wordpress_id: 49
---

I always enjoy mind puzzles like [The Monty Hall Problem](http://www.cut-the-knot.org/hall.shtml) which I discovered recently while reading [The Curious Incident of the Dog in the Night-time](http://www.amazon.co.uk/exec/obidos/ASIN/0099450259/ref=sr_aps_books_1_1/202-7244728-2339047). Many of my friends like these puzzles too. Yes, I am a geek.

But one of the obvious ones I've always thought I knew, as it's easy, I've just found out I got wrong...

<!-- excerpt -->

_You are in a boat floating on a lake, with a rock and a log. If you throw the log into the water will the water level rise, fall or stay the same? What about if you throw the rock in?_

I've always thought the answer was easy - the water level stays the same as both the rock and the log have already displaced their weight in water because they are in the boat.

Hmmmm, I'm not smart enough to have thought deeper. As a good friend pointed out to me:

The boat is floating with both rock and log inside it. Net effect is that both the rock and log are floating and displace their weight in water. When the log is thrown into the water it floats, continuing to displace its weight in water and the water level stays the same. The rock on the other hand sinks...

When it sinks its weight is no longer supported by the water, as it is when the rock is in the boat, and the rock no longer displaces its weight in water, only displacing its volume in water.

As it is significantly heavier than water the volume of the stone will be less than the volume of water of equal weight, so the water level will fall.

That's not difficult, it's obvious if you think about it properly.

... must learn to look deeper ...
