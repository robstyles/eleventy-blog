---
layout: post.pug 
title: "Data Latency and Astronomy"
date: 2004-03-22
tags: ["post","Enterprise Architecture"]
legacy_wordpress_id: 43
---

I found these posts on [Pat Helland's Blog](http://blogs.msdn.com/pathelland/), actually, I didn't, [Dave did](http://www.twelve71.com/dave/archives/000492.html), but hey.

[SOA is like the Night Sky...](http://blogs.msdn.com/pathelland/archive/2004/03/18/91825.aspx)
[It's All in a Name: What's a Service?](http://blogs.msdn.com/pathelland/archive/2004/03/11/88058.aspx)

The timing was really interesting...

<!-- excerpt -->

because I've been conversing about a similiar problem on the project I'm on. We have three organisation, Alice, Bob and Charlie. Alice and Bob both have contact with each other and a number of shared clients; they both keep their own records about those clients in their own databases. Charlie is responsible for analysing Alice's and Bob's data, ensuring they reconcile and producing statistical analysis from it.

The process, however, is that Alice and Bob are contractually obliged to provide their data to Charlie, but not to accept any changes back. As both Alice and Bob are also limited in their technology capabilities the route that has always been taken is for them both to provide a dump of their data "whenever they've made some changes".

Charlie is also considering providing dumps of data back to both Alice and Bob.

As both Alice and Bob are, in effect, working on caches of subsets of the data that Charlie has (although they don't think of it that way) the latency of starlight image is very instructuive here. Alice and Bob only know what the system _used to be like_.

My suggestion is that when Alice and Bob send through their changes they say what they want to change from... i.e.

Alice: "The data said 'baz' when I looked at it, please change it to 'quux' for me"
Charlie: "Yeah, the value is still 'baz', I'll change it"
Bob: "The data said 'baz' when I looked at it, please change it to 'pre' for me"
Charlie: "Sorry, the data has been changed to 'quux' since then, try again"

I'm sure there is a name for this pattern somewhere, it can't be new. The only problem I have now is to find an easy way for Alice and Bob to produce these change logs from inside their systems...
