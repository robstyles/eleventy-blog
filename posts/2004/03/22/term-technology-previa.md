---
layout: post.pug 
title: "Term: Technology Previa"
date: 2004-03-22
tags: ["post","Enterprise Architecture"]
legacy_wordpress_id: 42
---

As in ["Placenta Previa"](http://www.google.com/search?num=50&hl=en&lr=lang_en&ie=UTF-8&oe=UTF-8&newwindow=1&q=placenta+previa) where the placenta is positioned to arrive before the child. Technology Previa is the positioning of a technology to arrive before the requirement. ;-)

<!-- excerpt -->
