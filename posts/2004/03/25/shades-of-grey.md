---
layout: post.pug 
title: "Shades of Grey"
date: 2004-03-25
tags: ["post","Personal"]
legacy_wordpress_id: 45
---

I have a large and legitimate CD collection - all of which (even the copy protected ones) have been ripped as WMAs (no religous alignment to MP3) and shipped onto my 60Gb player. But today, on Channel 4 news of all places, I heard about an album that intrigued me - The Grey Album by DJ Danger Mouse. Even his name is stolen from a UK children's TV show.

So I did what any one with an ounce of nouse would do - I went and downloaded it to hear what all the fuss was about. Took me about three minutes to find it and about an hour for BitTorrent to download it.

<!-- excerpt -->

What I don't understand is what EMI's action is based on. They claim to be "protecting The Beatles' prior work" on which the sounds on The Grey Album are openly derived. But when you listen to it you can clearly tell that it is a new, different and un-related work. Danger Mouse (Brian Burton) dis-assembled the tracks on The Beatles' The White Album and used fragments of sound like snares, kick drums and bass notes.

At what point will the RIAA attempt to copyright the very Do-Re-Me-Fa-So-La-Te-Do away from innovative new talent. Stupid buggers should have just signed him.
