---
layout: post.pug 
title: "How Dumb are some people?"
date: 2004-03-01
tags: ["post","Personal"]
legacy_wordpress_id: 34
---

According to an interesting [news item on The Boston Channel](http://www.thebostonchannel.com/news/2875665/detail.html) several managers of Wendy's restaurants were gullible enough to ask employees to strip search on the basis of just a phone call.

This is interesting from a security perspective as it highlights that many, many people will simply believe you are whoever you say. Worrying really.

<!-- excerpt -->
