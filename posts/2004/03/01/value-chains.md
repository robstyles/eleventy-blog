---
layout: post.pug 
title: "Value Chains"
date: 2004-03-01
tags: ["post","Enterprise Architecture"]
legacy_wordpress_id: 33
---

Dale Emery has a [new post on Value Chains](http://www.dhemery.com/cwd/2004/02/values.html). I didn't find it myself, [Alan Francis spotted it](http://www.twelve71.com/archives/000417.html)...

I thought it was interesting as it reminded me of a process I took a team of mine through a while ago. We derived the needs of an enterprise architecture in a direct and evidenced way from the company's brand values through a value chain analysis.

<!-- excerpt -->

This approach led to a few surprising outcomes and some challenges for us as a strategy team. For example, the brand values of 'modern', 'innovative' and 'adventurous' took us down the route of requiring elements that publicly supported EAPs or Beta programmes; that we needed to be able to spot and adopt new ideas and technologies very quickly.

The brand values of 'trustworthy', 'reliable' and 'predictable' however led us down a route of tried and tested solutions, potentially with big vendors to facilitate support.

Conflicts like this only become apparent when you perform something like a value chain analysis and can be a real eye-opener as to how well your enterprise IT strategy is supporting the organisations culture and direction.

We found techniques and tools to address this specific conflict and the organisation is faster moving and better supported as a result. Participating in EAPs, using beta sofware and exploring new ideas while maintaining the integrity of it's core systems through deployment patterns we designed to facilitate experimentation. But if we hadn't done the value-chain analysis we wouldn't have even known it needed doing.
