---
layout: post.pug 
title: "New Job"
date: 2004-03-16
tags: ["post","Personal"]
legacy_wordpress_id: 40
---

Tomorrow is the big day, I get to start my new job working on .Net projects with a consultancy here in the Midlands. I'm really looking forward to getting my brain back in gear and getting up-to-speed with what the team's working on. I'll try to keep the blog up-to-date with what we're doing.

<!-- excerpt -->
