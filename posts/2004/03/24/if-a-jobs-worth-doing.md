---
layout: post.pug 
title: "If a job's worth doing..."
date: 2004-03-24
tags: ["post","Personal"]
legacy_wordpress_id: 44
---

When I was at Uni we watched every year as the University grounds-keepers re-seeded and tended the large lawn next to the Students' Union. Fencing it off in the spring and lovingly watering it and spreading seed. The end result was a beautiful, lush lawn, obviously well-loved.

Then, in early summer each year, we held a large open-air concert and beer festival on the lawn over two days. We built a huge stage, lighting and PA rigs, burieds cables the full length of the lawn for the front-of-house sound and lighting desks and then had around 2,000 people sit, stand, dance, wrestle, shag, drink beer and destroy the lawn.

<!-- excerpt -->

The following days would see the grounds-keepers re-patching the turf, seeding, watering and desperately trying to love that lawn back into life. A fruitless task as it never quite got back to it's former glory until the next year.

This led to us using the phrase "If a job's worth doing, it's worth doing twice" with respect to many of the University staff.

As I sit here and look at an architecture that was lovingly crafted by a guy no longer here I can see how it too has been danced on and had beer spilt on it by the pressures of delivering a project. The job now is to forgive that, as the grounds-keepers forgave us students, and lovingly try to re-seed and patch the holes in the turf.
