---
layout: post.pug 
title: "ASP.Net Traditional Web Anti-Pattern"
date: 2004-03-03
tags: ["post","Enterprise Architecture"]
legacy_wordpress_id: 35
---

ASP.Net introduces some new concepts in Web Development that many aren't familiar with. I've come across enough projects done badly in the same way that I've concluded this must be an anti-pattern...

"The ASP.Net Traditional Web Anti-Pattern" is what web developers coming from all web backgrounds Perl/CGI, ASP/COM, JSP, PHP and others all seem to build if not given adequate time, guidance or training in .Net Web Development.

<!-- excerpt -->

That shouldn't be a surprise, of course. Microsoft have moved the goal-posts. When introducing .Net one of Microsoft's major objectives was to bring the WinForms and Web Forms development models together. They did this by applying as many of the windows forms concepts to WebForms as they could.

The concepts are generally new to the web. ASP.Net introduces the idea of 'postback' and event-driven development to the web in a far more consistent and holisitc manner than seen before.

But, they left all the old hooks for getting at post data, querystrings and cookies in place. So when developers steeped in handling these manually are given an ASP.Net development to do they naturally build things the same way they always have; with html controls and code to parse the form data.

This misses the value of ASP.Net though, with it's web controls, event plumbing and state management making life so much easier and more powerful.

So, next time you're parsing form post data, querystrings or cookies in ASP.Net ask yourself if you're actually implementing "The ASP.Net Traditional Web Anti-Pattern".
