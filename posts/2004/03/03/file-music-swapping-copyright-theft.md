---
layout: post.pug 
title: "File & Music Swapping, Copyright theft?"
date: 2004-03-03
tags: ["post","Personal"]
legacy_wordpress_id: 36
---

As news comes in, unsurprisingly, that [Washington is considering technological solutions to copyright infringement on file swapping networks](http://news.com.com/2100-1025_3-5168505.html?type=pt&part=inv&tag=feed&subj=news) I finally find myself galvanised to write about the subject.

All in all, I think the DMCA ([UCLA Summary](http://www.gseis.ucla.edu/iclp/dmca1.htm)) and the EUCD ([Stand Summary](http://www.stand.org.uk/weblog/archive/2002/08/21/000243.php)) are based on falsehoods of a massive scale.

<!-- excerpt -->

The [RIAA describes copyright infringement of music in these terms](http://www.riaa.com/issues/piracy/default.asp):

_
"Piracy" generally refers to the illegal duplication and distribution of sound recordings. There are four specific categories of music piracy:
<ul><li>Pirate recordings are the unauthorized duplication of only the sound of legitimate recordings, as opposed to all the packaging, i.e. the original art, label, title, sequencing, combination of titles etc. This includes mixed tapes and compilation CDs featuring one or more artists.</li>
<li>Counterfeit recordings are unauthorized recordings of the prerecorded sound as well as the unauthorized duplication of original artwork, label, trademark and packaging.</li>
<li>Bootleg recordings (or underground recordings) are the unauthorized recordings of live concerts, or musical broadcasts on radio or television.</li>
<li>Online piracy is the unauthorized uploading of a copyrighted sound recording and making it available to the public, or downloading a sound recording from an Internet site, even if the recording isn't resold. Online piracy may now also include certain uses of "streaming" technologies from the Internet.</li></ul>_

These definitions are fairly sound and I'll use them in my arguments against.
<ul><li>First, _Pirate Recordings_. Anyone who has a pirate recording probably got given it by a friend. As they don't have the accompanying artwork and so on and are usually on a gold disc they are very obvious. I don't believe this damages the record industry at all as generally people have limited funds with which to buy music and would not otherwise have this album. Not only that, but my experience of people who have albums like this is that they also have a significant genuine collection and will often buy the album, at the sacrifice of another purchase, in order to maintain the look of that collection. The music industry loses little or nothing by this practice.</li>
<li>Second, _Counterfeit Recordings_ are designed to look close to the real thing and are retailed on market stalls, car boots and the other usual suspect locations. This is serious criminal activity and the people involved will be making a substantial living at this. The people buying them know that they are either counterfeit or stolen and are equally culpable. The industry loses money on this.</li>
<li>Third, _Bootleg Recordings_ are as old as recording gear. Destroy this and you destroy our musical heritage. Some of the most prized recordings of such artists as The Beatles and The Rolling Stones are bootlegs. Nowadays it may be possible to get a quality recording surreptitiously, but when The Beatles were bootlegged on an 8-track the size of a briefcase that need a mains supply I find it hard to believe it was anywhere other than the sound engineer's desk when it happened.</li>
<li>Fourth, _Online Piracy_ has many people involved. Ranging from those who have unwittingly bought "protected" CDs to the school children collecting MP3s like people used to collect football cards, Top Trumps or [Garbage Pail Kids](http://www.garbagepailkidsworld.com/). These people aren't costing the industry anything. Most of the 'collectors' are pulling down more MP3s than they could listen to end-to-end in their lifetime. We're talking about children in the playground saying "I've got more Gigs than you". And those using file swapping servcies to get MP3s of genuine CDs they have bought and are therefore legally entitled to copy to MP3 can hardly be seen as villains.</li></ul>

So, my concern is this: The measures being discussed and implemented to protect copyright material both technologically and legally, things such as Cactus Data Shield, the DMCA and EUCD are not here to stop piracy. The only real pirates are the counterfeiters and they won't be stopped by Cactus or by new laws.

The measures being taken are being taken to increase profits for the record industry, not by reducing piracy, but by increasing levies on their customers. What will happen if copy protection succeeds in preventing you from ripping it to MP3? Will it stop commercial pirates? No. Will it stop you from listening to music you own on CD on your iPod? Yes. Then you'll have to buy it again as MP3 from an online store. That's what it's all about; charging consumers more because the RIAA knows full-well it's easier to do that than to stop the real pirates.

Once you realise that three out of the foure types of piracy that the RIAA talk about, while wrong, illegal and not to be condoned, cost the record industry little it is clear they're aiming the wrong weapons at the wrong targets.

And, at the risk of being jailed under the EUCD, if you own a copy-protected CD from BMG you can rip it quite easily by first copying it onto a cd-rw using your CD recording software - copying only the audio tracks, not the data track, and then ripping the cd-rw. I'm guessing a good many people have done this already as when you do this the cd-rw gets recognised and automatically labelled by any software using an online music catalog. We're all criminals now.
