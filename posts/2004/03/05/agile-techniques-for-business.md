---
layout: post.pug 
title: "Agile Techniques for Business"
date: 2004-03-05
tags: ["post","Enterprise Architecture"]
legacy_wordpress_id: 37
---

Dave Fellows, a partner in crime on one or two projects has a note on his blog about _[releasing a business proposition but delaying the build of supporting IT infrastructure until just before its required](http://www.twelve71.com/dave/archives/000452.html)_

This reminded of the heady days of Egg during the dotcom boom...

<!-- excerpt -->

Launching products and building systems faster than people could keep up with. We destroyed a few people during those days, but the buzz was amazing.

Anyway, it reminded me specificallyof the launch of Egg Card. The release dates were crucial for that product as a number of competitors were planning similar, but inferior, product launches around the same time and we desperately wanted to be first.

The priority, inline with that was to build the supporting literature and the account opening and card issuing processes first. Followed by the card activation processes and finally the account servicing functions. What made this a higher risk vernture than maybe a more established bank would have taken was that we launched the account opening processes before the card activation services had been built. Time available was, therefore, the time it took for an account to be opened, the customer to sign and return the Consumer Credit Agreement and a card to be embossed and posted. About ten days.

28 days following the first card activations was the deadline for account servicing. Based on the fact that Egg Card has online statements and a credit card company can't debit your account for the balance on the card unless you've had opportunity to view your statement. Critical Deadline.

So, JIT systems for a live product has been done. I can't say I can recommend it. There are 3 axes to software engineering in real world situations, Scope, Quality and Time. As many of us know, if you fix time and scope at points that create this level of pressure then the only thing that can happen is a reduction in quality.
