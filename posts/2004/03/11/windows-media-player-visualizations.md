---
layout: post.pug 
title: "Windows Media Player Visualizations"
date: 2004-03-11
tags: ["post","Other Technical"]
legacy_wordpress_id: 39
---

I finally got around to writing a better bar type visualization for Windows Media Player. It took me a day to run through some examples and write this one up.

<!-- excerpt -->

It differs from the usuals as it uses a root-mean-square (RMS) algorithm to work out volumes of the bands based on the RMS of the included, finer, bands.

It also changes it's shape and size as you switch to skin or full-screen mode or re-size the window.

[Download Better Bars Visualization](/assets/code/wmpbars/BetterBars.dll)

__Update: I suggest you download the newer version... [Better Bars 2](/2004/04/04/better-bars-2)__

Save the file in the Visualizations folder of Windows Media Player, usually _C:\Program Files\Windows Media Player\Visualizations_ then run _regsvr32 betterbars.dll_ from a command line in that folder. You should now have 'Better Bars' as a Visualization in WMP.

Source is available to anyone who asks, especially if you can help me work out why it forms a stronger volume in the lower frequency bands than the higher ones... I think it's down to WMP's ability to discern the frequencies - lower frequencies can be spotted at lower sample rates, and I think that's why the very top end tends to be empty.

<div class="media">[Listening to: Easy Listening Superstar - Le Hammond Inferno - Switched on: Cool Sound of TV Advertising Disc 2 (02:59)]</div>
