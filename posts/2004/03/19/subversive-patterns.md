---
layout: post.pug 
title: "Subversive Patterns"
date: 2004-03-19
tags: ["post","Enterprise Architecture"]
legacy_wordpress_id: 41
---

Well, we all do a jolly good job at making the right technology decisions and having a good go at building great systems for our customers, but from time to time you get one of those situations where some functionality has been chosen or bought and mandated despite it's lack of suitability.

You know the situation, flashy sales demo is followed by a purchase, the CTO/Strategy Team/Architecture Group/Project Sponsor hands you the box and says "we're going to use this for our foobits logic processing".

Well, here are two subversive patterns I've used to tackle the problem...

<!-- excerpt -->

Pre-Requisites: pinch of salt.

The first pattern is for situations where you need the lack of suitability to be visible; perhaps to get the product withdrawn or perhaps to pass the pain of the product back to the selector. In this case the most appropriate pattern is the 'Zero-Touch' pattern.

In this you simply install and configure as standard a deployment of the platform as possible. Perhaps even allowing the vendor to perform the deployment design and sign it off. You perform no integration. Allow the users to switch from one to the other, or provide simple links between the two for things like web apps.

<a href="/assets/diagrams/zero-touch-pattern.html" onclick="window.open('/assets/diagrams/zero-touch-pattern.html','popup','width=342,height=455,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false"><img src="/assets/diagrams/zero-touch-pattern-thumb.gif" width="75" height="99" border="0" /></a>

If you're right and the product is unsuitable then this will result in the project failing. The system will hopefully not get past UAT or pilot, but even if it doesn get into live the lack of integration will likely kill it later. Of course, if youre wrong and have to accept the product you may have a lot of work to do tidying up the mess created by the two systems being seperate.

The second pattern is what I like to call the 'False-Touch' pattern. This is for use where the technology is unworkable but you need the project to succeed and you maybe wish to hide the misguided selection.

<a href="/assets/diagrams/false-touch-pattern.html" onclick="window.open('/assets/diagrams/false-touch-pattern.html','popup','width=342,height=455,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false"><img src="/assets/diagrams/false-touch-pattern-thumb.gif" width="75" height="99" border="0" /></a>

In this pattern you sit the new system alongside your existing systems in exactly the same way you would if you were genuinely using the system. Then, within the existing system, you make calls to the new system that provide just enough accuracy to make it appear to work. The key, though, is that you ignore all responses and build your own implementation in the existing system. Ideally calls to the duff system are done asynchronously, further reducing any dependance.

The elegance of this pattern is that the duff system becomes irrelevant and because the production system maintains it's performance and stability profiles it is very unlikely anyone will ever investigate which bit is actually doing the work.
