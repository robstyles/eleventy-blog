---
layout: post.pug 
title: "Poo"
date: 2004-12-17
tags: ["post","[grid::fatherhood]"]
legacy_wordpress_id: 74
---

[Alan Francis has started a wonderful Grid Blog about Fatherhood](http://www.twelve71.org/blogs/alan/archives/000679.html) and I thought I'd add some thoughts in the vain of "things nobody will tell you".

Poo.

<!-- excerpt -->

There are various things people won't tell you about children's poo. Firstly, when you get your baby home, the poo is thick, dark stuff called merconium. It doesn't smell, it's a bit like spinach that's been boiled a bit too long.

Then it turns into baby poo. It's the texture of pebbledash and usually smelss of sick. It's not nice, but the interesting thing is that it doesn't seem to upset you. It upsets other people, and other people's childrens' poo will upset you, but in this regard your children are an extension of you. You can usually tell what your little one's been eating too. Often questions like "What the **** did you feed her?" will follow a day when Daddy's been in charge of food.

Then, somewhere betwen 18 months old and 2 years something very disconcerting happens. You open a nappy to find something different. The baby and toddler poo give way to a small but perfectly formed log. A little adult turd. Get used to this, you'll still be seeing these, albeit in the potty and then the loo, for a few years yet.

As Alan says, sacrifice is everything. Who else would expect you to wipe their bum for them?
