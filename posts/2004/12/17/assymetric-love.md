---
layout: post.pug 
title: "Assymetric Love"
date: 2004-12-17
tags: ["post","[grid::fatherhood]"]
legacy_wordpress_id: 75
---

I need to start this entry by stating clearly that I love my parents very much and have throughout my life.

But...

<!-- excerpt -->

There is an enormous difference to the love I feel for my children. I would die for them without a second thought if that were required. That's what makes work, poo, hobbies etc seem unimportant in comparison.

When you come to realize this, the sense of [Richard's post on "Ask Your Parents"](http://cookiemoo.kgbinternet.com/blog/archives/2004/12/q_what_advice_w.html) becomes even more apparent. For most of us, there are two people in this world who love us far more than we love them; who worked tirelessly for years to support you; who want to see you succeed at everything you try and above all else want to see you happy.

You love your kids more than you love your parents. Now think about how much your parents love you.
