---
layout: post.pug 
title: "not so luuuuurvely"
date: 2004-12-03
tags: ["post","Random Thought"]
legacy_wordpress_id: 73
---

thinking about this here screensaver a bit more I've decided this is a seriously flawed piece of kit.

Firstly, the idea is flawed. Spamer economics is based on very small, sometimes infinitesimally small margins, this applies to their mailings and is likely to apply to their hosting also. Even at its peak this screensaver is not likely to really impact them.

<!-- excerpt -->

Secondly, the implementation is flawed. It clearly sends junk. Not only does this make it more likely to infringe regulation of some kind or another by being a deliberate attack, it also means that it's content will be easy to filter - trivial in fact.

Thirdly, the implementation doesn't send any kind of legitimate HTTP request. As most ISPs host many web sites on each machine they rely on a HTTP Host header in each request to identify whose site you're asking for. As Make Love Not Spam doesn't identify which site it's asking for, and clearly identifies itself as a non-legitimate requestor it is unlikely that costs will ever be attrributed to the spammer. Not to mention the fact that the degradation in service will be affecting all the poor sites who, through no fault of their own, happen to be hosted alongside one of the targeted sites.

Finally - aren't the spammers pissing away enough of the internet's bandwidth without us pissing it away too? This isn't the way.
