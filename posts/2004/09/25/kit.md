---
layout: post.pug 
title: "Kit"
date: 2004-09-25
tags: ["post","Software Engineering"]
legacy_wordpress_id: 63
---

The Effect of Sound Tools for Developers

When we want to develop sound software, you know the stuff when you see it, great interface, fast response, really solid, the importance of tooling people up correctly is beyond measure. We (Developers) all know that, but often development organisations find it difficult to argue the point and justify the tools they need.

Consider this (they're all real)...

<!-- excerpt -->

Company A expects their developers to produce stunning web-based interfaces combining server-side and client-side scripts, using flash animation and streaming technologies. But the internet access and machine build the developers have blocks all these things. They're not allowed to install other browsers (they only have IE) even. Many of the best sites for script fragments and CSS guidance are blocked as "they contain code". What effect does this have on developers' ability to quickly find solutions to the problem? Do you think the best of the best of the best want to work in that environment? I don't think so.

Company B has decided to adopt .Net as its development platform and to migrate a lot of old ASP and VB across to .Net over a substantial period. However, the developers laptops do not have enough disk space to have both versions of Visual Studio installed and MSDN on top, so the developers have to do without immediately accessible help. The laptops also have a screen resolution of just 1024x768, which means that it is not possible to have both code and solution explorer on screen simultaneously. The developers work in very sub-optimal ways as a result.

Company C expects its developers to work on mission-critical, high availability, robust systems, but the laptops they are issued with for development crash regularly due to the age of the hardware and the mass of "audit agents" installed.

An initiative to introduce re-factoring at Company D, led by the Chief Architect can't attract enough budget to buy a $400 enterprise license for a refactoring tool. Refactoring has obviously appeared in _Ivory Towers, The Journal for Architects_ recently, but not in _Software Budget Management_. This gives a clear message to the developers. Re-factoring is worth less than $400.

Of course not the whole world is like this. [Joel Spolksy runs a great environment](http://www.joelonsoftware.com/articles/BionicOffice.html). But we can all see that Joel is unusual. [Egg takes a very different approach](http://www.reflex.co.uk/Egg-Case-Study-3185) to Joel, choosing to house its development teams in a busy open-plan environment with harsh arificial lights and piped music. But each developer has a high-spec machine on their desk, dual 18" flat screen displays and plenty of memory and drive space. At the time I left they also had a pair of playstations linked up to play GT3, with proper steering wheels, pedals, racing seats etc.

Company E did an interesting thing. They gave all the developers nice, shiny high-spec machines with CD-RW drives, but then locked down all the USB ports to prevent the use of USB storage devices. Many of the developers were used to using the USB ports on their old machines to charge MP3 players, use their own USB peripherals etc. The develoeprs found this patronising, but it also damaged the credibility of the security teams who set the policy.

So why do developers get such a raw deal? The only reason I can think is trust. Microsoft trusts their employees enormously and techies working in Redmond and other non-sales sites have their own office. They can connect to the network, they have USB.

But in corporates all over the world techies are percieved as an awkward expense, a resource that has to be tightly controlled and managed - or it will spend all day surfing.

For those of us fed up of working this way, XP can offer some help. When a customer sees how much more information they need to give to make a development run smoothly they start to see how difficult developing software is. When they've seen a team work hard and have developed relationships with the coders they start to see what's being surfed (MSDN) rather than just that "the browser's always open". And, importantly, when they see the development tools and they sit next to a dev as it takes 7 minutes to build the application they realise that bigger screens and more memory are not just new toys.
