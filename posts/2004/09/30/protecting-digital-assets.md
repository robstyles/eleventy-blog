---
layout: post.pug 
title: "Protecting Digital Assets"
date: 2004-09-30
tags: ["post","Random Thought"]
legacy_wordpress_id: 64
---

The success of un-restricted file-swapping services such as the original Napster and, more recently ED2K, Source Exchange, Kad and the odd other rely on two key points for their success. If the music industry _really_ want to stop piracy it should be surprisingly easy...

<!-- excerpt -->

The two things they rely on are:

A critical mass of users.
A critical mass of content.

This means a few things - typically networks seek to encourage people to share files. Some rank the download requests of multiple downloaders by their upload score. This makes sense as it increase the amount of content. They also seek to provide an environment of trust, encouraging users to recommend it to new users. This relies on a high degree of anonymity.

Stopping file sharing of any volume then becomes simply an attack on either of those two needs.

For example, a critical mass of content means good content that you can find. It needs to be correctly labelled, virus-free, not corrupted etc. It is a relatively trivial task to change the content to junk ratio by uploading lots and lots of junk. Take a movie, say _I,Robot_ as an example - this is an attractive movie that will be wanted by many people for illegal download and if there are one or two different versions to choose from it's a simple and quick task to download one. But what if there were 972 different versions and the first one you download is corrupt, as is the second; the third download attempt results in an incorrectly filed pirate copy of _Disney's Fantasia_ or the promotional video for _Disney Land Paris_. That experience would destroy a network very quickly, but the only technique that could solve it, content checking, would also put the network in a difficult position as it could then enforce copyright.

The same could easily apply to users. Trust only works when the chance of a stranger doing you harm is very low and the degree of anonymity you have is high. A technique that could be used to change this dynamic in file-sharing networks could be easily implemented - the police already do this in children's chatrooms, participating in the chat posing as children, and have had some high-profile successes catching paedophiles. With the funds that the RIAA has available I would suspect this would be a much more cost-effective deterrent than subpoenas against ISPs and file-swapping networks who, by design, don't record identifiable information.

Attack either the content, or the anonymity and trust of users and file-swapping networks become difficult for anything other than small fry. So why aren't the RIAA doing this? Surely they are.
