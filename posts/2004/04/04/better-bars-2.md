---
layout: post.pug 
title: "Better Bars 2"
date: 2004-04-04
tags: ["post","Other Technical"]
legacy_wordpress_id: 46
---

I finally got around to adding a couple more visualizations to Better Bars; the first being a stereo version of the standard 'scope' and the second being a trailing scan of the sound, looking something like an ultrasound. If you try [Better Bars 2](/assets/code/wmpbars/BetterBars2.dll) then I'd appreciate some feedback on the performance of 'Scan' as it's using BitBlt and seems to be a bit slow on my machine if you run it full screen.

Again, installation instructions are:

<!-- excerpt -->

Save the file in the Visualizations folder of Windows Media Player, usually _C:\Program Files\Windows Media Player\Visualizations_ then run _regsvr32 betterbars2.dll_ from a command line in that folder. You should now have 'Better Bars' as a Visualization in WMP.

[update: 05.04.2004.10:00 I should have checked this better before posting it - 'Scan' bombs out after running for a while as my off-screen buffer gets garbage collected while I'm not looking. I'll fix that tonight, hopefully.]

[update: 05.04.2004.20:39 Fixed it. When writing these, it turns out to be a good idea not to use the global namespace to store handles to things unless you want them garbage collected for you. :-)]
