---
layout: post.pug 
title: "Better Better Bars"
date: 2004-04-06
tags: ["post","Other Technical"]
legacy_wordpress_id: 47
---

My younger brother has explained to me some of the intricacies of perceptual sound and yada-yada-yada

> you'll typically find that if the band for 18khz is showing any appreciable
> level, that level will be reflected louder, lower down.  Sounds with an
> 18khz component will almost certainly have matching (in terms of volume
> envelope) components across a wide range (in linear terms) of frequencies in
> the top end.  For example - a hi-hat stills sounds like a hi-hat if you
> low-pass filter it at 10k, the higher components aren't really doing
> anything different for our purposes.
> ...
> So limiting your graph to say 60 > 12k or so would still be representing all
> the zingy top and hefty bottom visually, but would do it more accurately (by
> doing it less accurately, if you know what i mean!?!?).

So expect to see these ideas appearing in BetterBars.dll in a few weeks or so when I next get chance to touch it.

<!-- excerpt -->
