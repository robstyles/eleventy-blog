---
layout: post.pug 
title: "New Toy"
date: 2004-11-14
tags: ["post","Personal"]
legacy_wordpress_id: 68
---

Last weekend saw an impulse purchase of Canon's new EOS 20D Digital SLR. Well, hardly impulse actually. I'd been thinking about buying the 10D for some time, but had never quite convinced myself; then I heard the spec for the 20D, jumping up to 8 mega pixels. I've checked and they're all there.

<!-- excerpt -->

I've been a Canon fan for many years and have a film EOS and a couple of lenses already, so didn't even consider the Nikon D70 or anything one elses range really, so I can't say this is unbiased, but I love it.

For ages now I've had a 35mm SLR and a digital compact. Having the two combine in this large, black, solid camera is a joy.

I've paired it with a 1GB compact flash card, so can take a few hundred full res photos before having to pop the card and zap them across to the PC. I wish I'd done it ages ago.

What really made me smile was when I put one of the photos I'd just taken on the new EOS with a 10MP scan of a 35mm negative that I'd done using my Minolta Dimage film scanner. The difference in clarity was stunning, the film scanner pulls up all the grain of the film, showing what a poor cousin all but the slowest films have now become.

[Here's a little snap I took to try it out](/assets/photos/D20Samples/leaves.jpeg) but be warned, it's 3.42MB.
