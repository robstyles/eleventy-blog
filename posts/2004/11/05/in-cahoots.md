---
layout: post.pug 
title: "in cahoots"
date: 2004-11-05
tags: ["post","Random Thought"]
legacy_wordpress_id: 67
---

This morning's news includes an item about Internet bank Cahoot, run by the Abbey. Internet banking is something I know a little about, and listening to Tim Sawyer, head of Cahoot, had alarm bells ringing.

A wonderful quote posted on the BBC has Tim saying "We did not fail as an organisation because there was no risk of financial loss...". I wonder if Cahoot customers agree? Or the Data Protection Act?

<!-- excerpt -->

If you walked into a bank and asked for a balance on accounts you'd be asked for ID and if you weren't it would be considered serious, so why does Tim not think that this was a failure? The bank's legal responsibility is not only to protect your money, but also your information.

He also went on to talk about how, in order to see someone elses account details, you'd have to know their "confidential" customer identification. One of the commonly misunderstood fundamentals of username password systems such as Cahoot's is that the username is not part of the secret. You have to assume it is known. I hope this was just bluff and blunder by a non-detail management professional rather than Cahoot's real security model.

But more worrying is that bugs like this (which was a very simple breach) show that the developers working on this "browser based secure internet banking application" are actually building it in the same way they'd write a guestbook for their geocities homepage.

One of the problems of the web is that the barrier to entry for simple sites is so low, yet the complexity of writing genuine browser delivered applications is very high. Akin to writing complex win32 apps and certainly more complex than writing windows apps in VB.
