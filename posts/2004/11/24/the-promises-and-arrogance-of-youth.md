---
layout: post.pug 
title: "The promises (and arrogance) of youth"
date: 2004-11-24
tags: ["post","Software Engineering"]
legacy_wordpress_id: 71
---

I'm working on a product team building the next generation of a very large metadata storage solution core to library systems. I've been about 6 weeks now and have spent a good chunk of that time looking at the standards and protocols that are in use in the environment.

The first is _"a computer protocol that can be implemented on any platform, defines a standard way for two computers to communicate for the purpose of information retrieval"_ - the [ANSI/NISO standard for Z39.50](http://www.niso.org/z39.50/z3950.html). The interesting thing, having been working with web services since 1999, is that this standard was ratified in 1988 and by 1992 had a second ratified version. Even more interesting is that it is both more efficient than SOAP and delivers on 90% of the promises made by SOAP, which is probably more than SOAP does.

<!-- excerpt -->

The second _"defines a data format that emerged from a Library of Congress-led initiative that began thirty years ago. It provides the mechanism by which computers exchange, use, and interpret bibliographic information, and its data elements make up the foundation of most library catalogs used today"_. This is [MARC](http://www.loc.gov/marc/), MAchine-Readable Cataloging, and delivers a structured format general enough to exchange any data. The first definition of this standard was made by the Library of Congress in 19... 66.

Both of these standards (and you pass MARC records over Z39.50 if you want to know the relationship) were formed at a time when many of us were, in the case of MARC, not born and in the case of Z39.50 still at school.

Now, everything goes in cycles, but this is quite amusing. These are great standards that deliver on inter-platform and inter-vendor interoperability and have been established and underpinning our libraries for decades.

Wow, it's way too easy to think our thoughts are new.
