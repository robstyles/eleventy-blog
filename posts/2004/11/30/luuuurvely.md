---
layout: post.pug 
title: "luuuurvely"
date: 2004-11-30
tags: ["post","Random Thought"]
legacy_wordpress_id: 72
---

[BBC](http://news.bbc.co.uk/2/hi/technology/4051553.stm) via [Clarke Ching](http://www.clarkeching.com/2004/11/make_love_not_s.html) via [alan francis](http://www.twelve71.org/blogs/alan/archives/000659.html):

> Internet portal Lycos has made a screensaver that endlessly requests data from sites that sell the goods and services mentioned in spam e-mail.M</a>
> <p class="quote">Lycos hopes it will make the monthly bandwidth bills of spammers soar by keeping their servers running flat out.

__Update:__ I did start to wonder if this was for real. I mean, there are probably laws that this screensaver violates - or should. But I took a look at what it's doing. I turned on the logging on my firewall and it really does visit the sites, it makes several requests...

<!-- excerpt -->

```

```

so, being suspicious I wondered if the domains might be bogus...

```

Domain Name 	 ANYSOFT.BIZ
Domain ID 	D8128287-BIZ
Sponsoring Registrar 	GANDI SARL
Sponsoring Registrar IANA ID 	81
Domain Status 	ok
Registrant ID 	O-876962-GANDI
Registrant Name 	Sergey Gachichiladze
Registrant Organization 	Sergey Gachichiladze
Registrant Address1 	11, Ulan-Bator St.
Registrant City 	Moscow
Registrant Postal Code 	117142
Registrant Country 	Russian Federation
Registrant Country Code 	RU
Registrant Phone Number 	+7.0957899432
Registrant Email 	whois@hqlists.com
Administrative Contact ID 	SG1094-GANDI
Administrative Contact Name 	Sergey Gachichiladze
Administrative Contact Address1 	11, Ulan-Bator St.
Administrative Contact City 	Moscow
```

The screensaver appears to send junk messages such as:

<p class ="code">
&lt;makeLOVEnotSPAM&gt;6Ad;&amp;o2RbS\{)Q&amp;{q/&lt;TN;z%?E|9uXv%%;m~C,dA}7.jGqD;|ym14Bck#N&aT[B+T&lt;/makeLOVEnotSPAM&gt;
&lt;/TN;z%?E|9uXv%%;m~C,dA}7.jGqD;|ym14Bck#N&aT[B+T&lt;/makeLOVEnotSPAM&gt;&lt;/makeLOVEnotSPAM&gt;.
</p>

So, surprisingly, judging by [Starring](http://corporate.starring.se/), the company who came up with it:

> Our business concept is to<br><br>help companies __do__ things that give them something to __say__.

it appears to be for real.
