---
layout: post.pug 
title: "Advice for Interviewees"
date: 2004-11-23
tags: ["post","Personal"]
legacy_wordpress_id: 70
---

The signal-to-noise ratio of technical recruitment amazes me. The number of CVs I see where the technologies listed don't match the must-haves of the job spec is astounding.

<!-- excerpt -->

What I also find interesting is the lack of knowledge interviewees appear to have on technologies listed on their CVs. I've done this myself, everything's relative. When interviewing a few months back for a technical mentoring role it became very clear to me (during the interview) that my level of knowledge of a particular technology really wasn't going to cut it. The intervewers were very pleasant about it and I left feeling it was the wrong role for me.

But, I changed my CV by removing stuff I wasn't prepared to really get down and dirty with. I would suggest you do the same. Exaggerating on your CV makes you look stupid and it makes the interview frustrating.

When interviewing for technical staff I make an effort to look into the technical prowess listed on CVs. If it says, for example, five years of Delphi experience then I'll spend some time on Google finding examples of Delphi code and looking at it. I'll write down some thoughts and questions (and the right answers just in case I forget). For example, Delphi has an interesting way of doing constructor chaining - I'd expect an interviewee to know that.

If the CV lists extensive experience of networking and web applications then I'll maybe choose to ask some questions about how HTTP works, and maybe some about Firewalls and Proxy servers.

When recruiting I'm looking for people I can takes seriously and that means being able to answer questions robustly. Otherwise, why is the technology on your CV?

Perhaps this relates to the [argument recently between Charles and Charles](http://news.bbc.co.uk/1/hi/uk/4029541.stm) in that it's about applying for jobs appropriate to your experience and current levels of ability.

Like Charles, I'm not saying that people shouldn't stretch or shouldn't aim high. But if the role states that a deep understanding of RDBMSs is required then you'd better be able to tell the difference between a clustered and nonclustered indexes.
