---
layout: post.pug 
title: "Use Cases and iPod"
date: 2004-11-23
tags: ["post","Software Engineering"]
legacy_wordpress_id: 69
---

Talking with some friends a few days ago we were talking about the value of use cases, amongst other things.

My product team are in the process of identifying user classes, looking at defining personas for them and getting really clear on the use cases required.

<!-- excerpt -->

A colleague of mine, Ian Corns, is an excellent analyst, new to the analysis game and gaining a head of steam very quickly. He's been reading [Wiegers book on software requirements](http://www.amazon.co.uk/exec/obidos/search-handle-form/202-4007310-1937414) which appears (not having read it) to be very sound.

But I'm keen to avoid one of the main thrusts of processes like RUP and the Rational approach and also espoused by a new found friend of ours,  [Ian Alexander](http://www.amazon.co.uk/exec/obidos/ASIN/0470861940/ianalexswebsiteb) - Traceability. And that's what we were talking about.

My previous employer, a services company, had a chief architect who designed an enormously contrived UML "world" solely on the basis that requirements should be traceable through to lines of code. Woah, what an idea. Maybe, in some world that I'm not living in that may be true, but I think it misses the point.

Product companies dream of having a product as successful as the iPod, people have talked for a long time about the apple factor and apple appeal. So the question is: Will you get an iPod if your focus is on Use Cases and traceability?

I don't believe you will, I think you'll get something very different. By isolating each use case and focusing on the traceability you establish the criteria that is being measured. And if you don't measure what you value you end up valuing what you measure.

To make traceability measurable, use cases and their realizations start to form a one-to-one mapping. "I've done that use case, see here's the screen" whether at the design or build stage.

But the iPod clearly doesn't do that. The iPod has a set of capabilities - a wheel and a centre button that drive a very adaptive menu. The behaviour of the iPod changes depending on where in the iPod you are. Can you imagine the many-to-many mapping diagram for the use cases of an iPod and the components within it? It may not even be measurable.

My Creative Zen 60Gb player is a different story. You can map the use cases of that almost one-to-one with the physical controls. It's horrible. I've hated it since the day I bought it (buy an iPod instead). The sound quality is awesome; it plays WMAs (of which I have many) and it's only a little larger than an iPod. But it's horrible: it takes two hands to use; operations often take several trips through the menu to achieve and it's ugly.

How do Use Cases and traceability allow me to define a product that users will love, not just one that meets the use cases?

The answer, of course, is Interaction Design. As Alan Cooper so beautifully puts it in [The Inmates are Running the Asylum](http://www.amazon.co.uk/exec/obidos/ASIN/0672326140/qid=1101244457/ref=sr_8_xs_ap_i1_xgl/202-4007310-1937414), the whole industry is crap and interaction designers are the only people who can save us.

And I think that's so true. Almost. Well, maybe, if you've had a beer or two.

But the point he raises is very real. So many of us work in environments where a requirements document is handed to a developer to work from. In some cases an [architect](http://martinfowler.com/ieeeSoftware/whoNeedsArchitect.pdf) may have pinned some technical constraints or ramblings to it, but broadly speaking there is little or no Interaction or UI design.

So, why do we get crap instead of an iPod? Rhetorical Question.
