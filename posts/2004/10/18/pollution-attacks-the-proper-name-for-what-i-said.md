---
layout: post.pug 
title: "Pollution Attacks, the proper name for what I said"
date: 2004-10-18
tags: ["post","Random Thought"]
legacy_wordpress_id: 65
---

After [my previous post](/2004/09/30/protecting-digital-assets) I got a number of comments back, one pointing me at [a company (allegedly) involved in this](http://www.overpeer.com/), and the term "Pollution Attack" which describes one of the two attacks I described in the previous post.

I also discovered [this article about the structure of KaZaA](http://cis.poly.edu/~ross/papers/UnderstandingKaZaA.pdf) which I found interesting. It also mentions pollution attacks and the use of published lists of known genuine files and their content hashes. This uses the [Sig2Dat](http://www.google.com/search?q=sig2dat) tool to generate a KaZaA hash for any file.

<!-- excerpt -->
