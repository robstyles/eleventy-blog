---
layout: post.pug 
title: "A fool with a tool is still a fool"
date: 2004-10-25
tags: ["post","Software Engineering"]
legacy_wordpress_id: 66
---

Further to my [recent posting on Kit](/2004/09/25/kit) I found myself amused by [an advert running here in the UK from Barclays called Money Trees III](http://www.davidreviews.com/SearchResults.asp?SearchText=Barclays+Money+Trees+III). The advert, features Donald Sutherland and Gary Oldman talking about growing money trees and is a fabulous outline of any skilled work...

> Gary Oldman: So, where do I start? Plant a penny? Plenty of water? Shazzam. I got me a money tree?
> 
> Donald Sutherland: No no no, you gotta follow it up, use the right tools; guy I knew was a bonafide Arborist.
> 
> Gary Oldman: What?
> 
> Donald Sutherland: Arh, never mind. Let's just say he had every gizmo going. You had a job, needed a certain tool, bingo he had it. And he'd never fob y'off with long handled secaturs when what you needed was a vine lopper.
> 
> Gary Oldman: Well if it's down to the tools, that's easy, I could grow me a money tree if I had the right tools.
> 
> Donald Sutherland: You think so? Wrench and a tub of grease doesn't make y'an engineer you know.

What amused me was the way Donald Sutherland switches tack at the end and reminds me so much of the meetings everyone has great anecdotes about... You know, the ones where you try to explain why you need the re-factoring tool and the book.
