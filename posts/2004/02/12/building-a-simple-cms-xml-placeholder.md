---
layout: post.pug 
title: "Building a simple CMS XML Placeholder"
date: 2004-02-12
tags: ["post",".Net Technical"]
legacy_wordpress_id: 28
---

I've just been down to see some guys in the middle of nowhere... Beautiful little office on a deer park, with views over fields and so incredibly quiet. A company called Torchbox who are getting up-to-speed with MS CMS.<br />
<br />
While I was there the big question was why one of their placeholders wasn't working, which I think we figured, but I also put together a very basic XML Placeholder control to act as a base for them. I thought it would be worth dropping it up here for the record.<br />
<br />

<!-- excerpt -->

```

using Microsoft.ContentManagement.Publishing.Extensions.Placeholders;
using Microsoft.ContentManagement.WebControls;
using System.Web;
using System.Web.UI.WebControls;

namespace Foo.ContentManagementServer.Extensions
{
public class SimpleXmlPlaceholderControl : BasePlaceholderControl
{
public SimpleXmlPlaceholderControl()
{
EnableViewState = false;
}

protected Literal PresentationControl;
protected TextBox AuthoringControl;

protected XmlPlaceholder BoundXmlPlaceholder
{
get
{
return BoundPlaceholder as XmlPlaceholder;
}
}

protected override void CreatePresentationChildControls(
BaseModeContainer presentationContainer)
{
PresentationControl = new Literal();
PresentationControl.ID = "PresentationControl";
Controls.Add(PresentationControl);
}

protected override void CreateAuthoringChildControls(
BaseModeContainer authoringContainer)
{
AuthoringControl = new TextBox();
AuthoringControl.ID = "AuthoringControl";
Controls.Add(AuthoringControl);
}

protected override void LoadPlaceholderContentForPresentation(
PlaceholderControlEventArgs e)
{
EnsureChildControls();
PresentationControl.Text = HttpUtility.HtmlEncode(
BoundXmlPlaceholder.XmlAsString);
}

protected override void LoadPlaceholderContentForAuthoring(
PlaceholderControlEventArgs e)
{
EnsureChildControls();
AuthoringControl.Text = BoundXmlPlaceholder.XmlAsString;
}

protected override void SavePlaceholderContent(
PlaceholderControlSaveEventArgs e)
{
EnsureChildControls();
BoundXmlPlaceholder.XmlAsString = AuthoringControl.Text;
}
}
}

```

<br />
<br />
[Download file](/assets/code/SimpleXmlPlaceholderControl.cs)
