---
layout: post.pug 
title: "Learn Programming in 21 Days"
date: 2004-02-29
tags: ["post"]
legacy_wordpress_id: 32
---

I found this linked from [Simple Geek](http://www.simplegeek.com/) (Chris Anderson's Blog)

[http://www.norvig.com/21-days.html](http://www.norvig.com/21-days.html)

<!-- excerpt -->

I hadn't read it before, but it backs up, I think, even further the notion that you have to recruit [Grey Matter rather than Grey Hair](/1999/11/04/grey-hair-versus-grey-matter).
