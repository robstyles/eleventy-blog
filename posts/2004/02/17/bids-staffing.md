---
layout: post.pug 
title: "Bids & Staffing"
date: 2004-02-17
tags: ["post","Resourcing"]
legacy_wordpress_id: 29
---

I've had two calls recently from people looking to bring in resource for MS CMS that seem to tie up. One was from an organisation who needed to line up resource in order to show they could do the work. The second, a few days later, came from another organisation who had just won the bid and needed to find resource quickly - before it became obvious to the client they had none.

This is a really extreme case of JIT resourcing, and strikes me as very high risk. It also suggests that extending the idea of [Grey Matter versus Grey Hair](/1999/11/04/grey-hair-versus-grey-matter) to your bid selection process makes sense.

<!-- excerpt -->
