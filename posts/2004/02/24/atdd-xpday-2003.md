---
layout: post.pug 
title: "ATDD XPDay 2003"
date: 2004-02-24
tags: ["post","Personal"]
legacy_wordpress_id: 30
---

<a href="/assets/photos/XPDay3/owen-rogers.html" onclick="window.open('/assets/photos/XPDay3/owen-rogers.html','popup','width=450,height=338,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false"><img src="/assets/photos/XPDay3/owen-rogers-thumb.jpg" width="75" height="56" border="0" alt="Owen Rogers presenting AT vs UT at XP Day 2003"/></a>

Owen Rogers (Thoughtworks) presenting our ATDD Test Framework thoughts at XPDay 2003
Courtesy of Tom Poppendieck, [Tom's other photos of the day](http://www.poppendieck.com/photogallery/XP_Day_2003.htm). Fortunately, I didn't make it into frame - I'm off to the left.

<a href="/assets/photos/XPDay3/david-leigh-fellows.html" onclick="window.open('/assets/photos/XPDay3/david-leigh-fellows.html','popup','width=450,height=338,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false"><img src="/assets/photos/XPDay3/david-leigh-fellows-thumb.jpg" width="75" height="56" border="0" alt="David Leigh-Fellows, arms wide, offering to hug everyone at the same time"/></a>

<!-- excerpt -->

Dave Fellows showing how he hugs all the members of his development team at once. ;-)

[Here are the slides](/assets/slides/xpday/ATvUT(XPDay3).ppt)
