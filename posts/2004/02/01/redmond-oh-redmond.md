---
layout: post.pug 
title: "Redmond, oh Redmond"
date: 2004-02-01
tags: ["post","Personal"]
legacy_wordpress_id: 27
---

Well, I've just got back from a week in Redmond. It rained.

I thoroughly enjoyed myself, being interviewed by some of the world's finest techies at Microsoft's main campus in Redmond.

<!-- excerpt -->

I got grilled by nine people, each individually, over two days. Each interview lasting for between an hour and two and a half. Questions ranging from 'Design a switch for a car sunroof' to 'Redesign this very obscure piece of Outlook'.

None of the expected 'you have two buckets, measure a quart' type questions, apart from one:

```
Starting with both hands of a clock at midday,
how soon after will the hands first form a ninety degree angle
```

And no, it's not 3 o'clock.
