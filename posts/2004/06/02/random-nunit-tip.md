---
layout: post.pug 
title: "Random NUnit Tip"
date: 2004-06-02
tags: ["post",".Net Technical"]
legacy_wordpress_id: 53
---

Often when writing NUnit tests I find myself wanting to output some stuff to the Console in order to make sense of a failing test. But, you don't want the console window filling up with rubbish when the test is passing, so instead of doing this...

```

<!-- excerpt -->

[Test]
public void DoesSomething()
{
string expected = "some expected result";
object someObject = new SomeObject();
string actual = someObject.DoesSomething();
Console.WriteLine(expected);
Console.WriteLine("----");
Console.WriteLine(actual);
AssertEquals("SomeObject should do something", expected, actual);
}

```

you can do this...

```

[Test]
public void DoesSomething()
{
string expected = "some expected result";
object someObject = new SomeObject();
string actual = someObject.DoesSomething();
try
{
AssertEquals("SomeObject should do something", expected, actual);
}
catch(AssertionException)
{
Console.WriteLine(expected);
Console.WriteLine("----");
Console.WriteLine(actual);
throw;
}
}

```

Then the Console output only shows up when the test fails. Nice.
