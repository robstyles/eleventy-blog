---
layout: post.pug 
title: "Agile and User-Centred"
date: 2004-06-07
tags: ["post","Software Engineering"]
legacy_wordpress_id: 54
---

I just came across [this great article talking about user-centred design in agile processes](http://www.syntagm.co.uk/design/articles/ucd-xp03.pdf).

I [wrote something along the same lines last year](/2003/03/12/xp-customer-as-interaction-designer), but it's always worth re-visiting and very good to hear others talking about the same issues.

So, I thought I'd re-visit it.

<!-- excerpt -->

Apart from the fantastic diagram of the mythical perfect user in this document it also points out some other common flaws in agile projects. Allowing developers to decide what, when and how UI elements manifest themselves.

This can be changed easily within an agile project if you have an interaction designer on board. The user stories can be extended using Interaction Diagrams (I recommend [JJG's](http://www.jjg.net/ia/visvocab/)) and screenshots, either full or detail, to show exactly how the interaction and interface must be constructed.

This comes from a common misconception in agile projects - that 'no big design upfront' means no design upfront. Upfront design is crucial to any project, even agile ones, but you only do enough to start work.

Agile projects also assume that the customer knows what they want. In projects where interaction and interface design are critical, arguably all, this is probably not viable as very few customers have a high enough degree of design knowledge or ability. Often, of course, there are many stakeholders expected to use the system, and often the customer assigned is not able to represent all of them.

So, for the customer to act as designer and state clearly in the user stories what is outwardly required of the software, they must have excellent interaction design skills. They will almost certainly have to do some reasonable thinking about what the system needs to look like and how it will be used; again these are interaction design skills.

So why not employ an interaction designer to play that role?
