---
layout: post.pug 
title: "Worse is Better, or Worse now is better than better later"
date: 2004-06-23
tags: ["post","Software Engineering"]
legacy_wordpress_id: 55
---

[Worse is Better by Richard Gabriel](http://www.jwz.org/doc/worse-is-better.html)

The paper above is old news for many people, but in my continued search for 'better' ways to build 'better' software I only just came across this paper. In many ways this can relate to Agile methodologies in which delivering value as soon as possible is key to the success of the project. XP uses short iterations, SCRUM uses sprints, although there are some big differences in the way these two work.

<!-- excerpt -->

The main difference, in my experience, is quality. Whereas Gabriel's description implies building compromised software in order to be first to market and to be as portable as possible my experience of XPers is that they want very much to do the right thing.

This is where much debate springs up about what exactly we mean by 'the simplest thing'. The simplest thing almost never means the quickest thing or the easiest thing.

To parody Gabriel's conversation between two famous people, one from MIT and one from Berkeley...

There were two famous people, one from a London bank steeped in Prince2 consultancy and one from a project in Hemel, with a history (albeit brief) in XP. The two met one day at OOPSLA...

The Prince2 guy had been reading all about XP and was interested in how it solved the problem of getting your ass sued of when a project failed, but he couldn't find any documentation standards and processes for XP that would mitigate that risk.

The Hemel guy replied that the XP community was aware of the potential problem, but that their solution was to ensure that all projects succeeded but sometimes a project might make demands of the customer in order to do this. So, a correct customer in XP would have to hear these demands and take responsibility for the success of the project.

The Prince2 guy argued that this solution was wrong, the Prince2 community had spent years carefully managing their customers' expectations to ensure the customer had no wish to be in any way involved in the project. He argued that the XP way would lead to the customer seeing just how bad everyone is at developing software and how much we had to drop the quality in order to meet the deadline.

The Hemel guy said that the XP solution was right. The customer was asking for a bespoke solution and in any other line of bespoke craftsmanship, hand-built cars, architect designed houses or designer clothes, the customer was intimately involved in the process from inception right through to completion.

The Prince2 guy then muttered that sometimes it takes a tough man to make a tender chicken, but the Hemel guy didn't understand (I'm not sure I do either).

Peter Gabriel's discussion then goes on to explain how worse-is-better is better, but XP can be considered as taking that further. XP says worse-is-better, but only along certain axes, and only those axes the customer chooses.

The overall quality of a system is defined by its non-functional behaviour: Availability; Cost of ownership; Maintainability; Data integrity; Development cost; Extensibility; Flexibility; Functionality; Leveragability/Reuse; Operability; Performance; Portability; Compliance; Robustness; Scalability; Installability.

Within XP the customer can specify these. Perhaps a particular feature must respond witin a certain time, the performance axis. Perhaps this is specified upfront as part of the acceptance criteria for a story or perhaps the functionality of the story is implemented and the customer subsequently decides it is too slow for their needs. The customer can decide exactly how much to spend on making it faster. They can decide if they want that function to go faster or they want a different function added instead.

Worse is better - use the customer's defintion of worse and better.
