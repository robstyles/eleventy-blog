---
layout: post.pug 
title: "In the land of the blind..."
date: 2004-06-23
tags: ["post","Personal"]
legacy_wordpress_id: 56
---

In the land of the blind the one eyed man is king, but the blind man shouting "I can see, I can see" will also gather many followers.

<!-- excerpt -->
