---
layout: post.pug 
title: "Certificates Still Not Valid"
date: 2004-08-16
tags: ["post","Other Technical"]
legacy_wordpress_id: 60
---

Back in 2001 I wrote about [the differences of PKI and passwords](/2001/01/27/securesummit-pki) after speaking at Secure Summit. [Bruce Schneier talks about the situation in Internet Banking today](http://www.schneier.com/crypto-gram-0408.html#8), with most still using simple passwords.

This makes perfect sense because, still, certificates are vulnerable to theft and brute force attack.

<!-- excerpt -->

There is an interesting attack on passwords, however, that most sites don't consider or protect against. If you only care about geting into _an_ account, not a specific account then a brute force attack against name, rather than password is very viable. Simply take a password, or passwords, that somebody is bound to have then run through usernames to find the user who has it.

I came across a company recently who had actually made this form of attack very simple. They had decided that usernames based on your real name were obviously a risk so all usernames were instead a five letter random sequence, making it trivial to now run through and find a user (any random five letters) with pa55w0rd as their password.
