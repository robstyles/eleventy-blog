---
layout: post.pug 
title: "Gumption Traps"
date: 2004-08-18
tags: ["post","Personal"]
legacy_wordpress_id: 61
---

[Ivan Moore](http://ivan.truemesh.com/) and [Rachel Davies](http://www.twelve71.com/rachel/) are [talking about Gumtion Traps again](http://www.twelve71.com/rachel/archives/000686.html), which caused me to realise that every time I encounter a gumption trap in my work I start talking about them. It inevitably ends up as either:

a) the other party has heard about and undertsands Gumption Traps and we start listing our number one traps or

<!-- excerpt -->

b) the other person has not heard about Gumption Traps and I start explaining about this great book (which I haven't even read) called [Zen and the Art of Motorcycle Maintenance](http://www.amazon.co.uk/exec/obidos/ASIN/0099322617/qid=1092845009/ref=sr_8_xs_ap_i1_xgl/026-0356300-5770835) (Pirsig, 1974) and how he talks about having the right tools for the job and all the other things that get in the way of the real objective.

So, all in all, Rachel, Ivan I'd just like to say a great big thank you for Gumption Traps, the biggest bloody Gumption Trap of all.

;-)
