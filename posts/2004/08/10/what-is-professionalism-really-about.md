---
layout: post.pug 
title: "What is professionalism really about?"
date: 2004-08-10
tags: ["post","Software Engineering"]
legacy_wordpress_id: 59
---

Professionalism is defined by the dictionaries as the "expertness characteristic of a professional" or the "pursuit of an activity as an occupation". We often talk about the quality of what we do and measure the professionalism of our work against others - although we often use different terms the overall meaning is Alice's work is more professional than Bob's.

So in Software, what is professionalism about?

<!-- excerpt -->

Or what should it be about. We're in the business of writing software. Product centric software houses are measured on the professionalism of their product - how robust Windows is, or how quickly the menus respond in Word. The professionalism of mass market product is quantifiable and has a close relationship to the usability and non-functional performance of the product. In office environments all over the world people's only exposure to Microsoft is the software; many will never see a CD, box or manual - only the application.

But in the bespoke software industry, or services companies, the measures often seem very different.

Is it important to wear a suit and tie? Social convention is that professionals dress smartly, suited and booted as a good friend of mine says. But does this level of formality have a positive or negative impact on the quality of the resulting software? It appears to have a negative impact; making the dress formal makes the meeting formal which makes the communication formal and formality takes precedence over clarity, exploration and debate. [Psychology Today has a very short comment on the subject](http://www.psychologytoday.com/htdocs/prod/ptoarticle/pto-19950301-000013.asp) pointing out that less formal dress leads to less formal discussion and more innovative and explorative communication; a better bond and trust between participants. Surely that clearer communication leads to better requirements, clearer priorities, more consistent decisions and ultimately better software.

Is it important that all documents are formatted to look the same and are structured in the same way? While branding is definitely a key part of a professional image, is dictating section headings down to 4 levels positive or negative? Having read through a good number of design documents recently it appears that dictating structure in this way has a major negative impact. Rather than really considering what information is needed by the audience of the document the template leads us to adopt a "fill in the form" mentality in which we consider only the heading we're writing for now, failing to consider the document as whole. This means that irrelevant details get included for the sake of having something under a heading and major points are lost in the noise or missed out all together.

Is the language of communication important? Of course, always, everyone knows that. But what is 'professional language'. Recently I've been told that humour in documents is unprofessional and that everything must be written in the third person. What bearing do these factors have on the quality of the end product? [Joel On Software talks about humor [sic] in documents](http://www.joelonsoftware.com/printerFriendly/articles/fog0000000033.html) in a sensible way. He points out that the purpose of the document is to communicate the information it contains. If the document is difficult to read, dry and formal then it will not communicate to the best of its ability. This will lower the quality of the end result; the software.

Let's try applying those measures to a different industry... You call a plasterer, he arrives in a suit, spends two hours documenting your requirements then leaves. Two weeks later a document arrives in the post; the document is beautifully bound and has a long list of other plasterers who have reviewed the document. "The Dining Room of (number) 352 Your Street, located on the ground floor at the front of the house, entrance off the hall, from hereon referred to as the room, is to be plastered using Plaster Supplies grade A plaster product mixed at a ratio of no less than 1 part plaster to 10 parts common tap water, to be provided by the customer on request from Joe Plasterer, or equivalent representative thereof...". You sign the document; with no idea of the correctness of the product selection or the mixing ratios of plaster. The plasterer arrives and does the work, your room is plastered, you pay him, he leaves, you're happy.

Now, what criteria do you judge him on? No plasterer in the world works this way because you judge him on the quality of his plastering and possibly a few other aspects of his approach, like his respect for you and your home. Ultimately, what you care about is the quality of the plastering.

So in our industry what is professionalism? I think it's the courage to do the things that will result in more professional software; that's what we're being paid to produce, after all.
