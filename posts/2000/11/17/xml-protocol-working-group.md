---
layout: post.pug 
title: "XML Protocol Working Group"
date: 2000-11-17
tags: ["post","Enterprise Architecture"]
legacy_wordpress_id: 7
---

The W3C have set up a working group to take on SOAP. Represented are IBM, Sun, HP, Vignette, Netscape, Lotus and, of course, Microsoft.

http://www.w3.org/2000/xp/Group/

<!-- excerpt -->
