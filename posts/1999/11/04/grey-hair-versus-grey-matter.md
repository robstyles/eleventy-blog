---
layout: post.pug 
title: "Grey Hair versus Grey Matter"
date: 1999-11-04
tags: ["post","Resourcing"]
legacy_wordpress_id: 5
---

I've just been through the process of writing guidelines and tests for recruiting developers into Egg. The challenge is this: How do you ensure that you get great people, rather than just good ones?

<!-- excerpt -->

This problem occurs everywhere, but in a fast-paced environment where good people are already running at speed the costs of bringing in bad people is compounded and can cause entire teams to stall. In the worst cases it can cause teamicide.

Some of our HR guys started telling us about interview techniques to drive out which bits of the CV are true and which may be exaggerated, but it doesn't tell you if they can do the job or not. And to avoid the catch-22 of needing experience to get a job to get the experience I felt we needed something that showed whether somebody had the right thinking rather than the right experience.

So, Paul Umbers and I, put together some questions to try and sift the wheat from the chaff. I'll not list the specific questions, but try to give an overview of the types of question.

__General Knowledge__
Ask questions designed to look at how well-read the applicant is in the fields they're applying for. For Internet developers, ask them to list five different web browsers - anyone can list two, most web developers can list four and good ones can list five. For a DBA, ask them what their opinion is on PostgreSQL or some other RDBMS they don't cliam to know. Ask software engineers to list half-a-dozen IDEs and so on.

__Design Questions__
Pick a suitable problem for the type of design decisions the applicant will face each day. This might be a piece of Interface or API design or it might be broader. Try picking something unusual, like 'What would be better than Fish Fingers?' and ask the applicant to work through their answer.

__Maths__
Writing code is often a mathematical task. Working out how many times a loop will execute, or the level of nesting of a function call and making those functions more efficient often requires rearranging equations or rearranging the way you're thinking about them. Ask questions like how to find the centre of a circle, those old questions about two trains leaving New York and Seattle and so on.

__Puzzles__
Puzzles are always fun. Try the 'Tower of Hanoi' or the 'Fox, Hen, Corn' problem, asking the applicant to explain the process as they go. These not only give you a good idea of their mental agility, but also their willingness to attack a problem and possibly fail in front of an audience.

__Coding__
Coding problems are obviously worth exploring with an applicant for a development role, but be careful that you pay attention to the important things. Logic, attention to detail, ability to explain their thinking, readable code are all important things; missing a semi-colon is not.

__Bug/Style Spotting__
A technique brought up by Paul Chesterton, as a result of a member of his team checking in a 100 line C function with 97 bugs and style issues. This technique relies simply on giving the applicant a copy of the code and asking them to talk through as many issues as they can see with it. Even non-C coders will spot many, such as absence of comments, unused or uninitialised variables. In this question, rather than the previous, spotting missing semi-colons is important. I'd love to publish this piece of source, but it's Egg's, so I just can't.

__In short, I think it's about looking at how people think, not where they've worked.__
