---
layout: post.pug 
title: "n-Tier Architecture"
date: 1999-12-03
tags: ["post","Enterprise Architecture"]
legacy_wordpress_id: 6
---

People keep asking me what a 3-tier architecture is and why we're building stuff that way...

Hmmmm...

<!-- excerpt -->

A 3-tier architecture can mean different things to different people. If you're building a software application then it makes sense to break down the code into different bits that do different things - usually the interface, the clever bits and the storage. And at an Enterprise level it pans out in a very similar way.

```
Presentation ---> Services ---> BackOffice
```

__Presentation Layer__
Responsible for several things:

Look & Feel
Flow of Interaction
Content

Many people think flow of interaction belongs in the services layer and, in some situations it can, but in most cases I believe it sits in the presentation layer. I think that because you have a different presentation layer for each client. One may be your call centre, another your web site and automated phone systems may be another still. Each of these has very different needs in terms of each of the three responsibilities, but can easily share the services layer and below.

__Services Layer__
Responsible for providing a single, clear and consistent view of the entire domain of the organisation. For a bank, like Egg, this means customers, accounts (savings, loans cards etc), transfers and so on. For a retailer it is more likely to mean customers, orders, stock etc.

The services layer must translate and atomise or aggregate the requests it receives in order to hide the backoffice systems from the presentation layer and vice-versa.

This layer allows a many-to-many relationship between presentation layers and systems providing the backoffice fulfillment.

__BackOffice Systems__
Databases, banking systems, third party suppliers and so on sit here, hidden from the organisations public face and protected from the many clients they may service.

__Benefits__
This structure allows a few things to happen, for security of Internet services it allows a high degree of seperation between publicly accessible servers providing the presentation layer and the more valuable services layer. It allows the layers to be scaled seperately and provides key points to provide performance enhancements such as caching and, maybe most importantly, it allow many different technologies to participate in a heterogenous network of clients and services.
