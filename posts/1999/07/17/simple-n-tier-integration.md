---
layout: post.pug 
title: "Simple n-Tier Integration"
date: 1999-07-17
tags: ["post","Enterprise Architecture"]
legacy_wordpress_id: 4
---

We had to tackle the integration of StoryServer into the rest of the enterprise this week. We decided to try and do this as simply as possible.

<!-- excerpt -->

The architecture of the problem is quite simple:

```
StoryServer  ---->  NAS  ---->  TopEnd  ---->  BackOffice
```

NAS (Netscape Application Server) is talking to TopEnd very nicely thanks, using the pooled connection libraries that Paul Umbers has written, now StoryServer needs to talk to NAS.

NAS already has a HTTP interface, so the obvious thing to do is to use that. StoryServer can open sockets easily enough from TCL, so that's what I've written - a simple TCL HTTP client to talk to NAS.

Paul, Iain and I got down to designing the messages to go across the pipe. TopEnd is using a fixed message format that is a bit nasty, so we want to improve on the usability of that. Efficiency isn't as big a concern as being able to easily use the services, so we decided on an XML structure. Or several to be precise.

Requests are simple name/value pairs, and are sent using a HTTP GET or POST, leaving NAS to map them to the Applogic interfaces. Responses are more descriptive, consisting of two parts. A Header, which follows the same DTD for all responses and contains status, debug and trace information etc and the Body which has a DTD specific to the service being used.

The end result looks like this:

```

<Header>
<StatusCode>2</StatusCode>
<StatusMessage>
MultipleAddressesFound
</StatusMessage>
<Events>
<Event type="debug">
Received Request Info: Postcode 'BH55 9BQ'; Number '555'
</Event>
<Event type="debug">
Calling Postal Adress Files
</Event>
<Event type="debug">
PAF returned 3 addresses in 11mS
</Event>
</Events>
</Header>
<Body>
<Addresses>
[...]
</Addresses>
</Body>

```
