---
layout: post.pug 
title: "Code Red Prevalent"
date: 2001-08-12
tags: ["post","Enterprise Architecture"]
legacy_wordpress_id: 12
---

We've been working on some proposition prototypes here over the past few weeks, based on Apache, PHP and MySql. We decided to run it on one of our win2k servers in the lab and get everything up and running before putting out onto the public Internet for testing.

We had some problems with one of the PHP extensions under win2k, libmcurl, so after much digging around newsgroups decided to switch to Linux - where we knew it worked. And lucky we did... When we came to put the box public, we set up a NAT address for it and opened port 80 only for that address. Within 30 seconds it was taking requests, not for our work, but for Code Red and variants.

<!-- excerpt -->

We probably wouldn't have patched the win2k box, it was only a little bit of prototyping, and we didn't harden the Linux install. But what a difference. Win2k would have been compromised in seconds. And, the server wasn't even listed in DNS, these were the straight forward random ip address attacks that these worms perform, and most were from IP addresses close by on our providers network.

I can't understand how, with all the publicity and so on, these worms are still so prevalent.
