---
layout: post.pug 
title: "Broadband, The Future's Bright..."
date: 2001-05-01
tags: ["post","Enterprise Architecture"]
legacy_wordpress_id: 11
---

When looking at the future of the Internet, through information from ISPs, Analysts, Telcos and Thinktanks, it is clear that one thing will change everything. The widespread availability and adoption of Broadband.

Here are a few basic thoughts on what the people in the know say is happening when...

<!-- excerpt -->

<ul>
<li>New Born and Academic Web, 1989 onwards</li>
<ul>
<li>Tim Berners-Lee invents the world wide web...</li>
<li>Small Number of highly technical users with mainly textual static content</li>
</ul>
<li>Broader Adoption Web, 1996 onwards</li>
<ul>
<li>Larger number of users although technical skills still required</li>
<li>Mainly Static Information or Brochureware</li>
<li>Security not good enough for ?Serious? transactions</li>
<li>Graduates having experiences it in Universities gaining positions in business high enough to influence growth</li>
</ul>
<li>Integrated ?Secure? Internet, __we are here__</li>
<ul>
<li>Secure Interaction possible enabling retail and financial transactions</li>
<li>Broader appeal due to less technical skill required, (some technical support still required)</li>
<li>Although more users, still generally limited to those who are PC literate</li>
<li>Sites still limited to ?Book? or ?Page? paradigm due to technology restrictions - still flat and unattractive compared to entertainment media, (TV or Video Games)</li>
<li>Key attractors are Brand and Product, retention can be achieved through mediocre Customer Experience, (Good or excellent is not possible due to bandwidth limitations)</li>
</ul>
<li>Rich Content, Broadband Internet, 2003 onwards</li>
<ul>
<li>Portal offerings vary enormously and become fully immersive due to the rich possibilities presented by the technology, (Broadcast quality video, 3D characters and worlds, interaction with Rules-Based Avatars)</li>
<li>Customer?s are retained by the quality of the online experience</li>
<li>Customer experience becomes a powerful attractor</li>
<li>Portal?s become more accessible, (easier to do business with), by being available on Personal devices and consumer branded White goods</li>
<li>Minimal skill is required to access a Portal/Service</li>
<li>Available to the entire population</li>
</ul>
</ul>

<a href="/assets/diagrams/broadband/current.html" onclick="window.open('/assets/diagrams/broadband/current.html','popup','width=640,height=480,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false"><img src="/assets/diagrams/broadband/current-thumb.jpg" width="75" height="56" border="0" /></a>Current Internet, 2001

<a href="/assets/diagrams/broadband/transitional.html" onclick="window.open('/assets/diagrams/broadband/transitional.html','popup','width=640,height=480,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false"><img src="/assets/diagrams/broadband/transitional-thumb.jpg" width="75" height="56" border="0" /></a>Transition to Broadband, 2002

<a href="/assets/diagrams/broadband/broadband.html" onclick="window.open('/assets/diagrams/broadband/broadband.html','popup','width=640,height=480,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false"><img src="/assets/diagrams/broadband/broadband-thumb.jpg" width="75" height="56" border="0" /></a>Broadband Internet, 2003

<a href="/assets/diagrams/broadband/growth-figures.html" onclick="window.open('/assets/diagrams/broadband/growth-figures.html','popup','width=640,height=480,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false"><img src="/assets/diagrams/broadband/growth-figures-thumb.jpg" width="75" height="56" border="0" /></a>Broadband and Device Providers' Sales Predictions

The interesting challenge here isn't the growth of Broadband, or the resulting richness of content that can be offered - although looking at American television it's clear that provision of quality content will be a problem - the real challenge is in the explosion of devices, new form factors and browsers.
