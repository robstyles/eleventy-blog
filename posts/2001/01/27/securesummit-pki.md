---
layout: post.pug 
title: "SecureSummit & PKI"
date: 2001-01-27
tags: ["post","Blogroll","Enterprise Architecture"]
legacy_wordpress_id: 9
---

I was talking earlier this month about Entrust's latest purchase, GetAccess (formerly EnCommerce GetAccess), at their SecureSummit conference in San Diego. It was great fun and we had a great turn out of people wanting to hear all about Egg and what we've been doing.

I managed to get some of the humorous bits from our adverts in which lightened up the topic a bit and even got a few laughs from what was a very friendly audience.

But it still strikes me as odd that Entrust have gotten so big on, essentially, PKI.

<!-- excerpt -->

The problem I have is that PKI is great, mathematically. Very clever. But it doesn't actually solve my problem.

I want to be able to establish someone's identity to a certain degree of accuracy. A standard username/password combination gives me that to some degree, largely dependant on how well the owner of the credentials looks after them.

PKI doesn't help increase that trust because I can't trust the end user to manage the certificates or key pairs correctly given the current interfaces available for doing that in the browsers.

The other fundamental issue is that they may well be weaker than passwords.

If you employ a password policy, such as minimum length, non-dictionary based etc then you can help your users devlop strong passwords. Albeit to a point as beyond a certain point they'll write them down.

So, say the password has a minimum length of 6 characters and has to have upper and lower case letters and at some numeric digits. That's 62^6, about 56 billion options or 56*10^9. It will be less than that as any brute force attack can be optimised to match the policy, so let's say that it's 30 billion in the eyes of an attacker.

As certificates have pass phrases and don't have policies enforcing the content of the pass phrase you can assume that it will be about 6 to 10 words long and make some sort of sense. If we assume long pass phrases of 10 words and a substantial vocabulary on the part of the user of 1500 words then we get the gargantuan posisbility of 57*10^30. But if we assume some real world rules such as the average vocabulary in use everyday is about 400 words and that pass phrases will actually be 6 words not 10 then the options are just 4*10^15.

If you now extend the usual dictionary attacks to cover phrases rather than words, such things as song lyrics, full names, film titles and so on we can see that an attack against pass phrase is very pheasible.

So, pass phrases are bigger than passwords, but not substantially. And because you have no policy control over the pass phrases used you can assume that a great number will be even weaker.

This brings me onto the next big difference...

When using passwords you prevent a brute force attack by only allowing a limited number of attempts before locking the account, either for a short period as Unix systems do or until it is explicitly unlocked by an administrator.

Certificate pass phrases are used on a client machine to unlock the certificate. If an attacker has obtained a copy of the certificate then they have unlimited attempts and you have no way of knowing. This 'offline' behaviour makes a brute force attack very feasible.

But how can someone get my certificates? In the same way that they get your keystrokes, your Outlook address book or anything else on your machine.

To minimise this offline threat and also the inconvenience of certificates being tied to a machine I was also told about Entrust's Roaming Certificates technology. This is a simple tool that allows you to store your certificates centrally and fetch them when you need them... And how, pray tell, are they protected I asked? With a username and password, of course...
