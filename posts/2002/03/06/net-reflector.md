---
layout: post.pug 
title: ".Net Reflector"
date: 2002-03-06
tags: ["post",".Net Technical"]
legacy_wordpress_id: 13
---

Morgan Skinner put me onto a very nice little toy today called .Net Reflector.

Basically, it uses reflection to scan through assemblies and list the namespaces, classes, methods and properties therein. Incredibly useful for scanning through the framework classes and finding the bits you need.

<!-- excerpt -->

[http://aisto.com/roeder/](http://aisto.com/roeder/)
