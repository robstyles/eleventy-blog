---
layout: post.pug 
title: "Developer Productivity Case Study"
date: 2002-10-03
tags: ["post"]
legacy_wordpress_id: 411
---

The team I've been leading for the past six months has been developing new stuff in C#. The tools are great, we like the language and the .Net framework is full of useful and well-organised classes. Having said all of that it was inevitable Microsoft would want to print it, so they did. [Online Bank Increases Developer Productivity and Improves Performance by Switching from Sun/UNIX to .NET](http://download.microsoft.com/documents/customerevidence/6180_Egg_Final.doc)

It's full of mis-quotes, several things I said are attributed to Dave Fellows, and vice-versa. Still, nice to do.

<!-- excerpt -->

As well as the written case study they also turned up with a film crew and lights and everything to film us all, Damian, Ben, Dave and Carol. Very much fun.
