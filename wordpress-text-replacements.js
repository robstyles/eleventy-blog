const { decode } = require("html-entities");

module.exports = [
  // HTML
  [/\r\n/g, "\n"],
  [/^\s*<div>([\s\S]+)<\/div>\s*$/g, "$1"],
  [/<p>([\s\S]*?)<\/p>/g, "\n\n$1\n\n"],
  [/<em>([\s\S]*?)<\/em>/g, "_$1_"],
  [/<i>([\s\S]*?)<\/i>/g, "_$1_"],
  [/<strong>([\s\S]*?)<\/strong>/g, "__$1__"],
  [/<b>([\s\S]*?)<\/b>/g, "__$1__"],
  [
    /<blockquote>([\s\S]*?)<\/blockquote>/g,
    (m, p) => `\n\n> ${decode(p).trim().replaceAll(/\n/g, "\n> ")}`,
  ],
  [
    /<p class="quote">([\s\S]*?)<\/p>/g,
    (m, p) => `\n\n> ${decode(p).trim().replaceAll(/\n/g, "\n> ")}`,
  ],
  [
    /<div class="quote">([\s\S]*?)<\/div>/g,
    (m, p) => `\n\n> ${decode(p).trim().replaceAll(/\n/g, "\n> ")}`,
  ],
  [
    /<div class="code">([\s\S]*?)<\/div>/g,
    (m, p) => "\n\n```\n" + decode(p) + "\n```\n\n",
  ],
  [
    /<p class="code">([\s\S]*?)<\/p>/g,
    (m, p) => "\n\n```\n" + decode(p) + "\n```\n\n",
  ],
  [
    /<pre>\s*<code>([\s\S]*?)<\/code>\s*<\/pre>/g,
    (m, p) => "\n\n```\n" + decode(p) + "\n```\n\n",
  ],
  [/<pre>([\s\S]*?)<\/pre>/g, (m, p) => "\n\n```\n" + decode(p) + "\n```\n\n"],
  [/<a href="([^"]+)">([\s\S]*?)<\/a>/g, "[$2]($1)"],

  // Links
  [
    /http:\/\/www.dynamicorange.com\/([0-9]{4})\/([0-9]{2})\/([0-9]{2})\//g,
    "/$1/$2/$3/",
  ],
  [
    /http:\/\/dynamicorange.com\/([0-9]{4})\/([0-9]{2})\/([0-9]{2})\//g,
    "/$1/$2/$3/",
  ],
  [/\.\.\/([0-9]{4})\/([0-9]{2})\/([0-9]{2})\//g, "/$1/$2/$3/"],
  ["http://www.dynamicorange.com/blog/diagrams/", "/assets/diagrams/"],
  ["http://www.dynamicorange.com/blog/archives/diagrams/", "/assets/diagrams/"],
  ["http://www.dynamicorange.com/blog/archives/code/", "/assets/code/"],
  ["http://www.dynamicorange.com/blog/archives/photos/", "/assets/photos/"],
  ["http://www.dynamicorange.com/blog/archives/slides/", "/assets/slides/"],
  ["http://www.dynamicorange.com/blog/wallpaper/", "/assets/wallpaper/"],
  ["http://www.dynamicorange.com/blog/archives/tools/", "/assets/tools/"],
  ["http://www.dynamicorange.com/blog/archives/designing", "/assets/designing"],
  [
    "http://www.dynamicorange.com/blog/archives/Semantic%20Marcup.pdf",
    "/assets/Semantic%20Marcup.pdf",
  ],
  [
    "http://dynamicorange.com/uploads/2009/04/embeddedrdfaediting-sfsw2009.pdf",
    "/assets/embeddedrdfaediting-sfsw2009.pdf",
  ],
  [
    "http://dynamicorange.com/uploads/2009/07/excelrdf.xls",
    "/assets/excelrdf.xls",
  ],
  [
    "http://www.dynamicorange.com/blog/WindowsLiveWriter/",
    "/assets/WindowsLiveWriter/",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/000003.html",
    "/1999/11/04/grey-hair-versus-grey-matter",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/000014.html",
    "/2001/01/27/securesummit-pki",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/000029.html",
    "/2002/05/22/dynamic-image-generation",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/000036.html",
    "/2003/03/12/xp-customer-as-interaction-designer",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/000052.html",
    "/2004/03/09/the-hum-of-the-machine",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/000060.html",
    "/2004/04/04/better-bars-2",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/000075.html",
    "/2004/08/10/what-is-professionalism-really-about",
  ],
  ["http://www.dynamicorange.com/blog/archives/000079.html", "/2004/09/25/kit"],
  [
    "http://www.dynamicorange.com/blog/archives/000080.html",
    "/2004/09/30/protecting-digital-assets",
  ],
  ["http://www.dynamicorange.com/blog/archives/000091.html", "/2004/12/17/poo"],
  [
    "http://www.dynamicorange.com/blog/archives/000110.html",
    "/2005/08/30/duplication-code-re-use-and-effort",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/other-technical/better_bars_2.html",
    "/2004/04/04/better-bars-2",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/software-engineering/what_is_profess.html",
    "/2004/08/10/what-is-professionalism-really-about",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/software-engineering/the_promises_an.html",
    "/2004/11/24/the-promises-and-arrogance-of-youth",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/other-technical/better_bars_2.html",
    "/2004/04/04/better-bars-2",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/five_things.html",
    "/2007/01/11/five-things",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/sometimes_you_h.html",
    "/2007/01/30/sometimes-you-have-to-ask-yourself-why",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/internet-social-impact/steve_jobs_appl.html",
    "/2007/02/07/steve-jobs-apple-thoughts-on-music",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/interaction-design/more_multitouch.html",
    "/2007/02/07/more-multi-touch-ui",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/code4lib_2007.html",
    "/2007/02/28/code4lib-2007",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/its_all_just_ta.html",
    "/2007/01/15/its-all-just-talking",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/internet-social-impact/youre_so_five_m.html",
    "/2007/05/11/youre-so-five-minutes-ago-aka-bill-buxton-part-one",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/interaction-design/more_touch_ui_v.html",
    "/2007/02/16/more-touch-ui-videos",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/interaction-design/microsoft_in_on.html",
    "/2007/05/30/microsoft-in-on-the-multi-touch-act",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/random-thought/twitter_no_more.html",
    "/2007/03/14/twitter-no-more",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/library-tech/multilingual_au_1.html",
    "/2007/10/18/multi-lingual-authority",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/internet-social-impact/why_virtual_wor.html",
    "/2006/12/22/why-virtual-worlds-suck",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/library-tech/marc_rdf_and_fr.html",
    "/2008/01/25/marc-rdf-and-frbr",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/personal/big_retro_headp.html",
    "/2006/12/08/big-retro-headphones",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/gridfatherhood/poo.html",
    "/2004/12/17/poo",
  ],
  [
    "http://www.dynamicorange.com/blog/archives/cat_gridblogpaper.html",
    "/tags/grid::blogpaper/",
  ],
  [
    "/blog/archives/photos/D20Samples/leaves.jpeg",
    "/assets/photos/D20Samples/leaves.jpeg",
  ],
  ["relflection", "reflection"],
  // Excerpts
  [/\s*<!--more-->\s*/g, "\n\n<!-- excerpt -->\n\n"],
  // Whitespace tidying
  [/^\s*/g, ""],
  [/\s*$/g, ""],
  [/\n\s*\n/g, "\n\n"],
  [/\n\s*&nbsp;\s*\n/g, "\n\n"],
  [/\n{3,}/g, "\n\n"],
];
