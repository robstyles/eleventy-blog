using Microsoft.ContentManagement.Publishing.Extensions.Placeholders;
using Microsoft.ContentManagement.WebControls;
using System.Web;
using System.Web.UI.WebControls;

namespace Foo.ContentManagementServer.Extensions
{
	public class SimpleXmlPlaceholderControl : BasePlaceholderControl
	{
		public SimpleXmlPlaceholderControl()
		{
			EnableViewState = false;
		}

		protected Literal PresentationControl;
		protected TextBox AuthoringControl;

		protected XmlPlaceholder BoundXmlPlaceholder
		{
			get
			{
				return BoundPlaceholder as XmlPlaceholder;
			}
		}
	
		protected override void CreatePresentationChildControls(BaseModeContainer presentationContainer)
		{
			PresentationControl = new Literal();
			PresentationControl.ID = "PresentationControl";
			Controls.Add(PresentationControl);
		}
	
		protected override void CreateAuthoringChildControls(BaseModeContainer authoringContainer)
		{
			AuthoringControl = new TextBox();
			AuthoringControl.ID = "AuthoringControl";
			Controls.Add(AuthoringControl);
		}
	
		protected override void LoadPlaceholderContentForPresentation(PlaceholderControlEventArgs e)
		{
			EnsureChildControls();
			PresentationControl.Text = HttpUtility.HtmlEncode(BoundXmlPlaceholder.XmlAsString);
		}
	
		protected override void LoadPlaceholderContentForAuthoring(PlaceholderControlEventArgs e)
		{
			EnsureChildControls();
			AuthoringControl.Text = BoundXmlPlaceholder.XmlAsString;
		}
	
		protected override void SavePlaceholderContent(PlaceholderControlSaveEventArgs e)
		{
			EnsureChildControls();
			BoundXmlPlaceholder.XmlAsString = AuthoringControl.Text;
		}
	}
}
