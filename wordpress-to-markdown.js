#!/usr/bin/env node

const fs = require("fs");
const moment = require("moment");
const { decode } = require("html-entities");
const textReplacements = require("./wordpress-text-replacements");
const contentCorrections = require("./manual-content-corrections");

const wp_posts = require("./wordpress-db-export/wp_posts.json").rows;
const tags = require("./wordpress-db-export/posts-to-tags.json").rows.filter(
  (tag) => tag.name !== "Uncategorized"
);

const posts = wp_posts.filter(
  (p) => p.post_type === "post" && p.post_status === "publish"
);
posts.sort((a, b) => a.ID - b.ID); // Earliest to latest

posts.forEach((post) => {
  // Correct post content (like flash embeds)
  if (contentCorrections[post.ID]) {
    contentCorrections[post.ID](post);
  }

  // Run text replacement corrections
  textReplacements.forEach(([pattern, sub]) => {
    post.post_content = post.post_content.replaceAll(pattern, sub);
  });

  // Mix in the tags
  const tagsForPost = tags.filter((tag) => tag.post_id === post.ID);
  post.tags = ["post", ...tagsForPost.map((tag) => tag.name)];

  // Add excerpt
  if (/<!-- excerpt -->/.test(post.post_content)) {
    // Do nothing
  } else if (/([^\n]+\n\n[^\n]+\n\n)/.test(post.post_content)) {
    post.post_content = post.post_content.replace(
      /([^\n]+\n\n[^\n]+\n\n)/,
      "$1<!-- excerpt -->\n\n"
    );
  } else if (post.post_content.length < 1000) {
    post.post_content = `${post.post_content}\n\n<!-- excerpt -->`;
  }
});

// Write to files
posts.forEach((p) => {
  const date = Date.parse(p.post_date_gmt);
  const dir = `./posts/${moment(date).format("YYYY/MM/DD")}`;
  fs.mkdirSync(dir, { recursive: true });
  const md = `---
layout: post.pug 
title: ${JSON.stringify(decode(p.post_title))}
date: ${moment(date).format("YYYY-MM-DD")}
tags: ${JSON.stringify(p.tags)}
legacy_wordpress_id: ${p.ID}
---

${p.post_content}
`;
  fs.writeFileSync(`${dir}/${decodeURI(p.post_name)}.md`, md);
});
