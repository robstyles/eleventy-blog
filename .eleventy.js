global.moment = require("moment");

const markdownIt = require("markdown-it");
const mila = require("markdown-it-link-attributes");

const markdownItOptions = { html: true };
const milaOptions = {
  pattern: /^(?!(https:\/\/rob\.styles\.to|#|\/))/gm,
  attrs: { target: "_blank", rel: "noopener noreferrer nofollow" },
};

module.exports = function (eleventyConfig) {
  const md = markdownIt(markdownItOptions);
  md.use(mila, milaOptions);
  eleventyConfig.setLibrary("md", md);

  global.md = md;
  eleventyConfig.addJavaScriptFunction("md", md);
  eleventyConfig.addJavaScriptFunction("moment", moment);

  global.filters = eleventyConfig.javascriptFunctions;
  eleventyConfig.setPugOptions({ globals: ["filters"] });

  // Don't inject browsersync snippet
  // Useful for lighthouse tests
  eleventyConfig.setBrowserSyncConfig({
    snippetOptions: { ignorePaths: "/*" },
  });

  eleventyConfig.setFrontMatterParsingOptions({
    excerpt: true,
    // Optional, default is "---"
    excerpt_separator: "<!-- excerpt -->",
  });

  return {
    dir: {
      input: "posts",
      output: "public",
      layouts: "../layouts",
      data: "../data",
    },
  };
};
