module.exports = {
  347: (post) => {
    post.post_title = "Schroedinger's WorldCat";
  },
  455: (post) => {
    post.post_title = "Open Data Licensing, An Unnatural Thought";
    post.post_name = "open-data-licensing-an-unnatural-thought";
  },
  644: (post) => {
    post.post_title = "Introducing the Web of Data";
  },
  603: (post) => {
    post.post_content = post.post_content.replace(/^<object .*<\/object>/, "");
    post.post_content = post.post_content.replace(
      /$/,
      `\n\n<!-- excerpt -->\n\n<div style="max-width:854px"><div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://embed.ted.com/talks/pranav_mistry_the_thrilling_potential_of_sixthsense_technology" width="854" height="480" style="position:absolute;left:0;top:0;width:100%;height:100%" frameborder="0" scrolling="no" allowfullscreen></iframe></div></div>`
    );
  },
  407: (post) => {
    post.post_content = post.post_content.replace(
      /The Slides:[\s\S]*/,
      `<!-- excerpt -->\n\nThe Slides:\n\n<iframe src="//www.slideshare.net/slideshow/embed_code/key/aV9czE54EBuRKY" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/mmmmmrob/eusidic-the-outlook-and-the-future" title="Eusidic, The Outlook And The Future" target="_blank">Eusidic, The Outlook And The Future</a> </strong> from <strong><a href="https://www.slideshare.net/mmmmmrob" target="_blank">mmmmmrob</a></strong> </div>`
    );
  },
  236: (post) => {
    post.post_content = post.post_content.replaceAll(
      /<p>([\s\S]+?)<\/p>/g,
      `\n\n$1\n\n`
    );
    post.post_content = post.post_content.replace(
      /<embed[\s\S]*<\/embed>/,
      `<iframe src="https://archive.org/embed/code4lib.conf.2008.pres.Styles.MarcRelationships" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>`
    );
  },
  242: (post) => {
    post.post_content = post.post_content.replaceAll(
      /<p>([\s\S]+?)<\/p>/g,
      `\n\n$1\n\n`
    );
    post.post_content = post.post_content.replace(
      /<object[\s\S]*<\/object>/,
      `<p><iframe width="560" height="315" src="https://www.youtube.com/embed/AyoNHIl-QLQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p><p><iframe width="560" height="315" src="https://www.youtube.com/embed/jNCblGv0zjU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>`
    );
  },
  300: (post) => {
    post.post_content = post.post_content.replace(
      /<object[\s\S]*<\/object>/,
      `<p><iframe width="560" height="315" src="https://www.youtube.com/embed/EhqZ0RU95d4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>`
    );
  },
  445: (post) => {
    post.post_content = post.post_content.replace(
      /<object[\s\S]*<\/object>/,
      `<div style="max-width:854px"><div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://embed.ted.com/talks/david_merrill_toy_tiles_that_talk_to_each_other" width="854" height="480" style="position:absolute;left:0;top:0;width:100%;height:100%" frameborder="0" scrolling="no" allowfullscreen></iframe></div></div>`
    );
  },
  496: (post) => {
    post.post_content = post.post_content.replaceAll(/\r\n/g, "\n");
    const replacementBlockquote = post.post_content
      .match(/<blockquote>([\s\S]*)<\/blockquote>/)[1]
      .replaceAll(/(^|\n)/g, "\n> ");
    post.post_content = post.post_content.replace(
      /<blockquote>([\s\S]*)<\/blockquote>/,
      `${replacementBlockquote}\n`
    );
    post.post_content = post.post_content.replace(
      /<object[\s\S]*<\/object>/,
      `<div style="max-width:854px"><div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://embed.ted.com/talks/tim_berners_lee_the_next_web" width="854" height="480" style="position:absolute;left:0;top:0;width:100%;height:100%" frameborder="0" scrolling="no" allowfullscreen></iframe></div></div>`
    );
  },
  551: (post) => {
    post.post_content = post.post_content.replaceAll(/\r\n/g, "\n");
    const replacementBlockquote = post.post_content
      .match(/<blockquote>([\s\S]*?)<\/blockquote>/)[1]
      .replaceAll(/(^|\n)/g, "\n> ");
    post.post_content = post.post_content.replace(
      /<blockquote>([\s\S]*?)<\/blockquote>/,
      "> $1\n"
    );
    post.post_content = post.post_content.replace(
      /<blockquote><object[\s\S]*<\/object><\/blockquote>/,
      `<p><iframe width="560" height="315" src="https://www.youtube.com/embed/nBJV56WUDng" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>`
    );
  },
  576: (post) => {
    post.post_content = post.post_content.replace(
      /<object[\s\S]*<\/a>./,
      `<iframe src="https://player.vimeo.com/video/7209269" width="640" height="352" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe><p><a href="https://vimeo.com/7209269">Hardy&#039;s Nottage Hill FreshCase</a> from <a href="https://vimeo.com/documentally">Documentally</a> on <a href="https://vimeo.com">Vimeo</a>.</p>`
    );
  },
  553: (post) => {
    post.post_content = post.post_content.replaceAll(/\r\n/g, "\n");
    post.post_content = post.post_content.replace(/<object[\s\S]*?<\/a>\./, "");
    const replacementVideo = `<iframe src="https://player.vimeo.com/video/5677879?title=0&byline=0&portrait=0" width="640" height="368" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe><p><a href="https://vimeo.com/5677879">NuFormer, 3D video mapping projection - Live performance in Zierikzee, the Netherlands - 2009</a> from <a href="https://vimeo.com/nuformer">NuFormer</a> on <a href="https://vimeo.com">Vimeo</a>.</p>`;
    post.post_content = post.post_content.replace(
      /<blockquote>\n\n([\s\S]*?)<\/blockquote>/,
      `\n> $1\n\n<!-- excerpt -->\n\n${replacementVideo}`
    );
    post.post_content = post.post_content.replace(
      /<object[\s\S]*?<\/object>/,
      `<p><iframe width="560" height="315" src="https://www.youtube.com/embed/7AcSs2Sp2hI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>`
    );
  },
  229: (post) => {
    post.post_content = post.post_content.replace(
      "<p>&lt;http://.../foo&gt; &lt;http://.../relns/1234&gt; &lt;http://.../schema#thing&gt; .    <br />&lt;http://.../relns/1234&gt; <em>means</em> rdf:Type .</p>",
      "```\n<http://.../foo> <http://www.w3.org/1999/02/22-rdf-syntax-ns#Type> <http://.../schema#thing> .\n```"
    );
  },
};
