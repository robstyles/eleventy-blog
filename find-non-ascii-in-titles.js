#!/usr/bin/env node

const posts = require("./wordpress-db-export/wp_posts.json").rows;
const titles = posts.map((p) => p.post_title);

const all_titles = titles.join(" ");

const non_ascii_chars = new Set(
  [...all_titles].filter((char) => char.charCodeAt(0) > 127)
);

// const non_ascii_titles = titles.filter((title) =>
//   [...title].some((char) => char.charCodeAt(0) > 127)
// );

console.log(non_ascii_chars);
